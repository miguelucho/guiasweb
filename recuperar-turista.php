<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
		<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" >
		<meta name="keywords" lang="es" content="">
		<meta name="robots" content="All">
		<meta name="description" lang="es" content="">
		<title>Contactar Guiás de Turismo Colombia</title>
		<meta http-equiv="Cache-control" content="public">
		<link rel="stylesheet" href="css/slider.css" />
		<link rel="stylesheet" href="css/stylesheet.css" />
		<link rel="stylesheet" href="css/load.css" />
		<link rel="stylesheet" href="css/jquery.modal.css" />
		<link rel="stylesheet" href="css/stylesheetnew.css" />
	</head>
	<body>
		<header>
			<?php include "new-header-top.php";?>
		</header>
		<section>
		</section><section>
		<div class="Contener">
			<div class="Contener-int">
				<div class="Contener-int-contenido">
					<div class="Login">
						<div class="Login-int">
							<h2>Recuperar contraseña</h2>
							<p>Ingrese los datos registrados a la cuenta</p>
							<form id="recuperar">
								<label>Nombre de Usuario / Correo Electrónico</label>
								<input type="text" placeholder="Usuario" name="turista-usuario">
								<label>Verifica que la información sea correcta</label>
								<input type="submit" class="Btn-azul" value="Recuperar" name="">
								<p><a href="iniciar" class="Contra">Login.</a></p>
							</form>
						</div>
					</div>
					<br>
					
				</div>
				<div class="Conten-botones-ff">
					<p>
						<a href="iniciar" class="Btn-atras">Atrás</a>
					</p>
				</div>
			</div>
		</div>
	</section>
	<footer>
		<?php include "new-footer.php";?>
	</footer>
	<script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
	<script src="js/jquery.modal.min.js"></script>
	<script src="js/recuperar-pass.js" type="text/javascript"></script>
</body>
</html>