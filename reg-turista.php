<!DOCTYPE html>
<html lang="es">
	<head>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-44406258-20"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-44406258-20');
		</script>

		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
		<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" >
		<meta name="keywords" lang="es" content="">
		<meta name="robots" content="All">
		<meta name="description" lang="es" content="">
		<title>Contactar Guiás de Turismo Colombia</title>
		<meta http-equiv="Cache-control" content="public">
		<link rel="stylesheet" href="css/slider.css" />
		<link rel="stylesheet" href="css/load.css" />
		<link rel="stylesheet" href="css/jquery.modal.css" />
		<link rel="stylesheet" href="css/stylesheet.css" />
		<link rel="stylesheet" href="css/stylesheetnew.css" />
	</head>
	<body>
		<header>
			<?php include "new-header-top.php";?>
		</header>
		<section>
			<div class="Contener">
				<div class="Contener-int">
					<div class="Contener-int-contenido">
						<div class="Login">
							<div class="Login-int">
								<h2>Nuevo Turista</h2>
								<div id="gsingbtn" class="customGPlusSignIn">
							      <span class="icon"></span>
							      <span class="buttonText">Google</span>
							    </div>
							    <br>
							   	<br>
								<hr>
								<p>Ingresa la siguiente información para registrate por primera vez.</p>
								<p>Obligatorio*</p>
								<form id="registro">
									<label>Correo Electrónico*</label>
									<input type="email" placeholder="Correo Electrónico" name="registro[correo]" required>
									<label>Contraseña*</label>
									<input type="password" placeholder="Contraseña" name="registro[contrasena]" required>
									<label>Nombres*</label>
									<input type="text" placeholder="Nombre" name="registro[nombres]" required>
									<label>Apellidos*</label>
									<input type="text" placeholder="Apellido" name="registro[apellidos]" required>
									<label>Genero*</label>
									<select name="registro[genero]" required>
										<option value="M">Masculino</option>
										<option value="F">Femenino</option>
									</select>
									<label>Fecha de nacimiento*</label>
									<div class="Form-date">
										<div class="Form-date-int">
											<label>Día</label>
											<select name="registro[nacimiento_dia]" required>
												<option value="">Día</option>
												<option value="01">1</option>
												<option value="02">2</option>
												<option value="03">3</option>
												<option value="04">4</option>
												<option value="05">5</option>
												<option value="06">6</option>
												<option value="07">7</option>
												<option value="08">8</option>
												<option value="09">9</option>
												<option value="10">10</option>
												<option value="11">11</option>
												<option value="12">12</option>
												<option value="13">13</option>
												<option value="14">14</option>
												<option value="15">15</option>
												<option value="16">16</option>
												<option value="17">17</option>
												<option value="18">18</option>
												<option value="19">19</option>
												<option value="20">20</option>
												<option value="21">21</option>
												<option value="22">22</option>
												<option value="23">23</option>
												<option value="24">24</option>
												<option value="25">25</option>
												<option value="26">26</option>
												<option value="27">27</option>
												<option value="28">28</option>
												<option value="29">29</option>
												<option value="30">30</option>
												<option value="31">31</option>
											</select>
										</div>
										<span> - </span>
										<div class="Form-date-int">
											<label>Mes</label>
											<select name="registro[nacimiento_mes]" required>
												<option value="">Mes</option>
												<option value="01">Enero</option>
												<option value="02">Febrero</option>
												<option value="03">Marzo</option>
												<option value="04">Abril</option>
												<option value="05">Mayo</option>
												<option value="06">Junio</option>
												<option value="07">Julio</option>
												<option value="08">Agosto</option>
												<option value="09">Septiembre</option>
												<option value="10">Octubre</option>
												<option value="11">Noviembre</option>
												<option value="12">Diciembre</option>
											</select>
										</div>
										<span> - </span>
										<div class="Form-date-int">
											<label>Año</label>
											<select name="registro[nacimiento_ano]" required>
												<option value="">Año</option>
												<option value="2000">2000</option>
												<option value="1999">1999</option>
												<option value="1998">1998</option>
												<option value="1997">1997</option>
												<option value="1996">1996</option>
												<option value="1995">1995</option>
												<option value="1994">1994</option>
												<option value="1993">1993</option>
												<option value="1992">1992</option>
												<option value="1991">1991</option>
												<option value="1990">1990</option>
												<option value="1989">1989</option>
												<option value="1988">1988</option>
												<option value="1987">1987</option>
												<option value="1986">1986</option>
												<option value="1985">1985</option>
												<option value="1984">1984</option>
												<option value="1983">1983</option>
												<option value="1982">1982</option>
												<option value="1981">1981</option>
												<option value="1980">1980</option>
												<option value="1979">1979</option>
												<option value="1978">1978</option>
												<option value="1977">1977</option>
												<option value="1976">1976</option>
												<option value="1975">1975</option>
												<option value="1974">1974</option>
												<option value="1973">1973</option>
												<option value="1972">1972</option>
												<option value="1971">1971</option>
												<option value="1970">1970</option>
												<option value="1969">1969</option>
												<option value="1968">1968</option>
												<option value="1967">1967</option>
												<option value="1966">1966</option>
												<option value="1965">1965</option>
												<option value="1964">1964</option>
												<option value="1963">1963</option>
												<option value="1962">1962</option>
												<option value="1961">1961</option>
												<option value="1960">1960</option>
												<option value="1959">1959</option>
												<option value="1958">1958</option>
												<option value="1957">1957</option>
												<option value="1956">1956</option>
												<option value="1955">1955</option>
												<option value="1954">1954</option>
												<option value="1953">1953</option>
												<option value="1952">1952</option>
												<option value="1951">1951</option>
												<option value="1950">1950</option>
												<option value="1949">1949</option>
												<option value="1948">1948</option>
												<option value="1947">1947</option>
												<option value="1946">1946</option>
												<option value="1945">1945</option>
												<option value="1944">1944</option>
												<option value="1943">1943</option>
												<option value="1942">1942</option>
												<option value="1941">1941</option>
												<option value="1940">1940</option>
												<option value="1939">1939</option>
												<option value="1938">1938</option>
												<option value="1937">1937</option>
												<option value="1936">1936</option>
												<option value="1935">1935</option>
												<option value="1934">1934</option>
												<option value="1933">1933</option>
												<option value="1932">1932</option>
												<option value="1931">1931</option>
												<option value="1930">1930</option>
												<option value="1929">1929</option>
												<option value="1928">1928</option>
												<option value="1927">1927</option>
												<option value="1926">1926</option>
												<option value="1925">1925</option>
												<option value="1924">1924</option>
												<option value="1923">1923</option>
												<option value="1922">1922</option>
												<option value="1921">1921</option>
												<option value="1920">1920</option>
												<option value="1919">1919</option>
												<option value="1918">1918</option>
											</select>
										</div>
									</div>
									<label>Numero de celular*</label>
									<input type="text" placeholder="Numero de celular" name="registro[celular]" required="" class="requerido" minlength="10" maxlength="10" title="Son 10 numeros">
									<!-- <label>Imagen de perfil:</label>
									<input type="file" name="registro[]" style="display:none;">
									<div class="Form-imagen">
										<p>Haz click para cargar tu imagen</p>
									</div> -->
									<p class="Acepta-t"><input type="checkbox" name="registro[terminos]" value="1" required> Aceptas <a href="terminos" target="_blank">términos y condiciones</a><br></p>
									<p><label>Verifica que la información sea correcta</label></p>
									<input type="submit" class="Btn-azul" value="Registrarme" name="">
								</form>
							</div>
						</div>
						<br>
					</div>
					<div class="Conten-botones-ff">
						<p>
							<a href="contactar" class="Btn-atras">Atrás</a>
						</p>
					</div>
				</div>
			</div>
		</section>
		<footer>
			<?php include "new-footer.php";?>
		</footer>
		<script src="https://apis.google.com/js/api:client.js"></script>
		<script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
		<script src="js/jquery.modal.min.js"></script>
		<script src="js/registro-turista.js"></script>
		<script type="text/javascript">
			$(function(){
				$('#Drop').bind('click',function() {
				$('.Top-inf').toggleClass('Top-inf-apa');
			});
			});
		</script>
	</body>
</html>