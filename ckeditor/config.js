/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	// The toolbar groups arrangement, optimized for two toolbar rows.
	   config.filebrowserBrowseUrl = window.location.protocol+'//'+window.location.host+'/Elfinder/elfinder.html?CKEditor=contenidoEmail&CKEditorFuncNum=1&langCode=es';
 // Define changes to default configuration here. For example:

	config.language = 'es';			
/*	config.uiColor = '#AADC6E';		*/

/*	config.width  = 50;				*/
/*	config.height = 50;				*/
config.toolbarCanCollapse = false;
config.resize_enabled = false;
config.readOnly = false;
/*	config.basicEntities = false;				Per sopprimere la trasformation dell'output		*/
//config.removePlugins = 'elementspath';


/*	Toolbar definition

config.toolbar_Forum =
[
	{ name: 'riga1',  	items : ['Bold','Italic','Underline','-','TextColor','BGColor','-',
								 'NumberedList','BulletedList','-',
								 'HorizontalRule','SpecialChar','-','Link','Unlink','-',
 								 'Undo','Redo'] },
	'/',
	{ name: 'riga2',  	items : ['Outdent','Indent','-','Cut','Copy','Paste','PasteText','PasteFromWord','-','Maximize'] }
];

config.toolbar = 'Forum';	*/

};

/*	End CKEDITOR.editorConfig	*/

