<?php require_once('Connections/datos.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_datos, $datos);
$query_rds_ciudades = "SELECT profesionales.ciudad FROM profesionales GROUP BY profesionales.ciudad order by profesionales.ciudad ";
$rds_ciudades = mysql_query($query_rds_ciudades, $datos) or die(mysql_error());
$row_rds_ciudades = mysql_fetch_assoc($rds_ciudades);
$totalRows_rds_ciudades = mysql_num_rows($rds_ciudades);

mysql_select_db($database_datos, $datos);
$query_rds_departamentos = "SELECT departamentos.id, departamentos.nombre, departamentos.codigo, Count(profesionales.id) AS CountOfid
FROM profesionales RIGHT JOIN departamentos ON profesionales.departamento = departamentos.id
GROUP BY departamentos.id, departamentos.nombre, departamentos.codigo ORDER BY nombre ASC";
$rds_departamentos = mysql_query($query_rds_departamentos, $datos) or die(mysql_error());
$row_rds_departamentos = mysql_fetch_assoc($rds_departamentos);
$totalRows_rds_departamentos = mysql_num_rows($rds_departamentos);

mysql_select_db($database_datos, $datos);
$query_rds_idiomas = "SELECT * FROM idiomas ORDER BY idioma ASC";
$rds_idiomas = mysql_query($query_rds_idiomas, $datos) or die(mysql_error());
$row_rds_idiomas = mysql_fetch_assoc($rds_idiomas);
$totalRows_rds_idiomas = mysql_num_rows($rds_idiomas);

$colname_rds_profesionales = "-1";
$colname_rds_profesionales2 = "-1";
$colname_rds_profesionales3 = "-1";
if (isset($_POST['departamento'])) {
  $colname_rds_profesionales = (get_magic_quotes_gpc()) ? $_POST['departamento'] : addslashes($_POST['departamento']);
}
if (isset($_POST['ciudad'])) {
  $colname_rds_profesionales2 = (get_magic_quotes_gpc()) ? $_POST['ciudad'] : addslashes($_POST['ciudad']);
}
if (isset($_POST['nombre'])) {
 if (strlen($_POST['nombre']) > 3 ) {
  $colname_rds_profesionales3 = (get_magic_quotes_gpc()) ? $_POST['nombre'] : addslashes($_POST['nombre']);
  }
  else
  {
  $colname_rds_profesionales3 = "-1";
  }
}
mysql_select_db($database_datos, $datos);
$query_rds_profesionales = sprintf("SELECT profesionales.id, profesionales.auto_registro, profesionales.tarjetano, profesionales.registronacionaldeturismo, profesionales.apellidos, profesionales.nombres, profesionales.telefonos, profesionales.celular, profesionales.ciudad, profesionales.correoe,  departamentos.nombre as departamento
FROM profesionales LEFT JOIN departamentos ON profesionales.departamento = departamentos.id WHERE (departamento = %s) or (ciudad like %s)  or (nombres like %s) or (apellidos like %s) order by departamento, ciudad, apellidos", GetSQLValueString($colname_rds_profesionales, "text"), GetSQLValueString($colname_rds_profesionales2, "text"), GetSQLValueString("%".$colname_rds_profesionales3, "text"), GetSQLValueString("%".$colname_rds_profesionales3, "text"));
$rds_profesionales = mysql_query($query_rds_profesionales, $datos) or die(mysql_error());
$row_rds_profesionales = mysql_fetch_assoc($rds_profesionales);
$totalRows_rds_profesionales = mysql_num_rows($rds_profesionales);

$colname_rds_idiomasguia = "-1";
if (isset($_POST['idioma'])) {
  $colname_rds_idiomasguia = (get_magic_quotes_gpc()) ? $_POST['idioma'] : addslashes($_POST['idioma']);
}
mysql_select_db($database_datos, $datos);
$query_rds_idiomasguia = sprintf("SELECT * FROM profesionales_idiomas WHERE id_idioma = %s", GetSQLValueString($colname_rds_idiomasguia, "int"));
$rds_idiomasguia = mysql_query($query_rds_idiomasguia, $datos) or die(mysql_error());
$row_rds_idiomasguia = mysql_fetch_assoc($rds_idiomasguia);
$totalRows_rds_idiomasguia = mysql_num_rows($rds_idiomasguia);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Consejo Profesional de Gu&iacute;as de Turismo</title>
<link href="css/estilos.css" rel="stylesheet" type="text/css" />
</head>

<body onload="MM_preloadImages('imagenes/botones/btn_nostros_ovr.gif','imagenes/botones/btn_tarjeta_ovr.gif','imagenes/botones/btn_profesionales_ovr.gif','imagenes/botones/btn_asociaciones_ovr.gif','imagenes/botones/btn_normatividad_ovr.gif','imagenes/botones/btn_normatect_ovr.gif','imagenes/botones/btn_infoacadem_ovr.gif','imagenes/botones/btn_eventos_ovr.gif','imagenes/botones/btn_contactenos_ovr.gif')">
<table width="1016" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2"><?php require_once('encabezado.php'); ?></td>
  </tr>
  <tr>
    <td colspan="2" align="right"><?php require_once('buscar.php'); ?></td>
  </tr>
  <tr>
    <td width="851"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="207" valign="top" background="imagenes/fondo_izq.jpg"><?php require_once('menu.php'); ?></td>
        <td width="638" valign="top" bgcolor="#ECECEC"><table width="100%" border="0" cellspacing="0" cellpadding="2">
          <tr>
          <td valign="top"><script type="text/javascript">
AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0','width','633','height','129','src','imagenes/bnn_profesionales','quality','high','pluginspage','http://www.macromedia.com/go/getflashplayer','movie','imagenes/bnn_profesionales' ); //end AC code
</script><noscript><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0" width="633" height="129">
            <param name="movie" value="imagenes/bnn_profesionales.swf" />
            <param name="quality" value="high" />
            <embed src="imagenes/bnn_profesionales.swf" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="633" height="129"></embed>
          </object>
            </noscript><p>
			      <?php // --------------------------------------------------Contenido -------------------------------------------?>
			      </p>
			    <p><img src="imagenes/bullet.gif" width="12" height="12" /> <strong><font face="Arial, Helvetica, sans-serif">Encuentre un Gu&iacute;a Profesional de Turismo aqu&iacute;: </font> </strong>			      </p>
			    <div align="left" class="rounded">
        <table width="100%" border="0" cellspacing="0" cellpadding="6">
          <tr>
            <td><form id="form1" name="form1" method="post" action="">
              <table width="100%" border="0" cellspacing="0" cellpadding="4">
                
                    <tr>
                      <td colspan="2" bgcolor="#CCCCCC">Seleccione localizaci&oacute;n:</td>
                      <td width="56%" rowspan="6" align="left" valign="top"><p><em><?php
				mysql_select_db($database_datos, $datos);
$query_rds_textos = "SELECT * FROM ttextosvarios WHERE id = 4";
$rds_textos = mysql_query($query_rds_textos, $datos) or die(mysql_error());
$row_rds_textos = mysql_fetch_assoc($rds_textos);
$totalRows_rds_textos = mysql_num_rows($rds_textos);
echo urldecode($row_rds_textos['contenido']); 
 if (!isset($_SESSION)) {   session_start(); } 
 if ((isset($_SESSION['MM_UserGroup'])) and  ($_SESSION['MM_UserGroup']=="1")) { ?>
                       <?php } ?> <br><a href="javascript:;" onclick="window.open('editarv2.php?id=4&ec=2','Editor','scrollbars=yes,resizable=yes,width=800,height=600')">
                          <img src="imagenes/iconos/editor.png" alt="editar" width="32" height="32" border="0" /></a></em></p>
                        <p><em><font color="#333333">Si usted est&aacute; registrado en este directorio, puede hacer <a href="profesionales_login.php">clic aqu&iacute;</a> para actualizar su registro.  </font><br>
                              <br>
                              
                        </em></p>  <?php if (!isset($_SESSION)) {   session_start(); } if ((isset($_SESSION['MM_UserGroup'])) and  ($_SESSION['MM_UserGroup']=="1")) { ?> <a href="javascript:;" onclick="window.open('index_profesionales.php','Editor','scrollbars=yes,resizable=yes,width=800,height=600')"><img src="imagenes/iconos/admin-bolet.png" width="38" height="34" border="0" /></a><?php } ?></td>
                    </tr>
                    <tr>
                  <td width="13%">Departamento:</td>
                  <td width="31%"><select name="departamento" id="departamento">
                    <option value="0">Seleccione...</option>
                    <?php
do {  
?>
                    <option value="<?php echo $row_rds_departamentos['id']?>"><?php echo $row_rds_departamentos['nombre']?> (<?php echo $row_rds_departamentos['CountOfid']?>)</option>
                    <?php
} while ($row_rds_departamentos = mysql_fetch_assoc($rds_departamentos));
  $rows = mysql_num_rows($rds_departamentos);
  if($rows > 0) {
      mysql_data_seek($rds_departamentos, 0);
	  $row_rds_departamentos = mysql_fetch_assoc($rds_departamentos);
  }
?>
                                    </select></td>
                  </tr>
                <tr>
                  <td>Ciudad:</td>
                  <td><select name="ciudad" id="ciudad">
                    <option value="0">Seleccione...</option>
                    <?php
do {  
?>
                    <option value="<?php echo $row_rds_ciudades['ciudad']?>"><?php echo $row_rds_ciudades['ciudad']?></option>
                    <?php
} while ($row_rds_ciudades = mysql_fetch_assoc($rds_ciudades));
  $rows = mysql_num_rows($rds_ciudades);
  if($rows > 0) {
      mysql_data_seek($rds_ciudades, 0);
	  $row_rds_ciudades = mysql_fetch_assoc($rds_ciudades);
  }
?>
                                    </select></td>
                  </tr>
                <tr>
                  <td colspan="2" bgcolor="#CCCCCC">Buscar por nombre: </td>
                  </tr>
                <tr>
                  <td colspan="2"><input name="nombre" type="text" id="nombre" size="40" /></td>
                  </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td><input type="submit" name="Submit" value="Buscar" /></td>
                  </tr>
              </table>
                        </form>            </td>
          </tr>
        </table>
      </div>
			  
			  
			  
	 <br />
	 <?php // ------------------------------------------------- Fin -Contenido -------------------------------------------?></td>
          </tr>  <?php if ($totalRows_rds_profesionales > 0) 
		  { ?>
          <tr>
            <td valign="top">  <div align="left" class="rounded">&nbsp;
              <table width="100%" border="0" cellspacing="0" cellpadding="6">
                <tr>
                  <td bgcolor="#CCCCCC"><center>
                    Tarjeta Profesional 
                  </center></td>
                  <td bgcolor="#CCCCCC"><center>
                    Nombres
                  </center></td>
                  <td bgcolor="#CCCCCC"><div align="center"></div></td>
                  <td bgcolor="#CCCCCC"><center>
                    Ciudad
                  </center></td>
                  <td bgcolor="#CCCCCC"><center>
                    Departamento
                  </center></td>
                  <td>&nbsp;</td>
                </tr>
                <?php do { 
				
				
				$colname_rds_idiomasguia = "-1";
if (isset($_POST['idioma'])) {
  $colname_rds_idiomasguia = (get_magic_quotes_gpc()) ? $_POST['idioma'] : addslashes($_POST['idioma']);
}
mysql_select_db($database_datos, $datos);
$query_rds_idiomasguia = sprintf("SELECT * FROM profesionales_idiomas WHERE id_idioma = %s and id_guia = %s ", GetSQLValueString($colname_rds_idiomasguia, "int"),  GetSQLValueString($row_rds_profesionales['id'], "int"));
$rds_idiomasguia = mysql_query($query_rds_idiomasguia, $datos) or die(mysql_error());
$row_rds_idiomasguia = mysql_fetch_assoc($rds_idiomasguia);
$totalRows_rds_idiomasguia = mysql_num_rows($rds_idiomasguia);
				
				?>
                  <tr>
                      <td><div align="center"><font size="1"><?php echo $row_rds_profesionales['tarjetano']; ?></font></div></td>
                    <td><font size="1"><?php if ($row_rds_profesionales['auto_registro']==1) { ?><a href="profesionales_detalle.php?id=<?php echo $row_rds_profesionales['id']; ?>"><?php } ?><?php echo $row_rds_profesionales['nombres']; ?> <?php echo $row_rds_profesionales['apellidos']; ?></a></font></td>
                    <td>&nbsp;</td>
                    <td><font size="1"><?php echo $row_rds_profesionales['ciudad']; ?></font></td>
                    <td><font size="1"><?php echo $row_rds_profesionales['departamento']; ?></font></td>
                    <td>&nbsp;</td>
                  </tr>
                  <?php } while ($row_rds_profesionales = mysql_fetch_assoc($rds_profesionales)); ?>
                <tr>
                  <td colspan="5" bgcolor="#CCCCCC">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $totalRows_rds_profesionales ?> registro(s) encontrado. </td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
              </table>
            </div></td>
            </tr> <?php
			 }   ?>
        </table></td>
      </tr>
    </table></td>
    <td width="167" valign="top" background="imagenes/fondo_der.jpg">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><?php require_once('footer.php'); ?></td>
  </tr>
</table>
<map name="Map" id="Map"><area shape="rect" coords="839,201,923,223" href="tarjeta.php" /><area shape="rect" coords="930,200,1001,224" href="tarjeta.php" /></map>
<map name="Map2" id="Map2"><area shape="rect" coords="626,4,668,29" href="mapa.php" alt="Mapa del sitio" /></body>
</body>
</html>
<?php
mysql_free_result($rds_ciudades);

mysql_free_result($rds_departamentos);

mysql_free_result($rds_idiomas);

mysql_free_result($rds_profesionales);

mysql_free_result($rds_idiomasguia);
?>
