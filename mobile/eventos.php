<?php require_once('Connections/datos.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_rds_eventos = 5;
$pageNum_rds_eventos = 0;
if (isset($_GET['pageNum_rds_eventos'])) {
  $pageNum_rds_eventos = $_GET['pageNum_rds_eventos'];
}
$startRow_rds_eventos = $pageNum_rds_eventos * $maxRows_rds_eventos;

mysql_select_db($database_datos, $datos);
$query_rds_eventos = "SELECT * FROM event ORDER BY id desc";
$query_limit_rds_eventos = sprintf("%s LIMIT %d, %d", $query_rds_eventos, $startRow_rds_eventos, $maxRows_rds_eventos);
$rds_eventos = mysql_query($query_limit_rds_eventos, $datos) or die(mysql_error());
$row_rds_eventos = mysql_fetch_assoc($rds_eventos);

if (isset($_GET['totalRows_rds_eventos'])) {
  $totalRows_rds_eventos = $_GET['totalRows_rds_eventos'];
} else {
  $all_rds_eventos = mysql_query($query_rds_eventos);
  $totalRows_rds_eventos = mysql_num_rows($all_rds_eventos);
}
$totalPages_rds_eventos = ceil($totalRows_rds_eventos/$maxRows_rds_eventos)-1;

$queryString_rds_eventos = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_rds_eventos") == false && 
        stristr($param, "totalRows_rds_eventos") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_rds_eventos = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_rds_eventos = sprintf("&totalRows_rds_eventos=%d%s", $totalRows_rds_eventos, $queryString_rds_eventos);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Consejo Profesional de Gu&iacute;as de Turismo</title>
<link href="css/estilos.css" rel="stylesheet" type="text/css" />
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
</head>

<body onload="MM_preloadImages('imagenes/botones/btn_nostros_ovr.gif','imagenes/botones/btn_tarjeta_ovr.gif','imagenes/botones/btn_profesionales_ovr.gif','imagenes/botones/btn_asociaciones_ovr.gif','imagenes/botones/btn_normatividad_ovr.gif','imagenes/botones/btn_normatect_ovr.gif','imagenes/botones/btn_infoacadem_ovr.gif','imagenes/botones/btn_eventos_ovr.gif','imagenes/botones/btn_contactenos_ovr.gif')">
<table width="1016" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><?php require_once('encabezado.php'); ?></td>
  </tr>

  <tr>
    <td width="851"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="638" valign="top" bgcolor="#ECECEC"><table width="100%" border="0" cellspacing="0" cellpadding="6">
          
          <tr>
            <td width="51%" valign="top">
			   
			    <p>
			      <?php // --------------------------------------------------Contenido -------------------------------------------?>
				</p>
			    <p><img src="imagenes/bullet.gif" width="12" height="12" /> <strong><font face="Arial, Helvetica, sans-serif">Ultimos eventos </font></strong></p>
			    
				
				
				
			    <?php do { ?>
			      <div align="left" class="roundedevento">
			        <table width="100%" border="0" cellspacing="0" cellpadding="6">
			          <tr>
			            <td><p align="left"><strong><font size="2"><?php echo $row_rds_eventos['titulo']; ?></font></strong><br />
		                      <em><?php echo $row_rds_eventos['fechatexto']; ?></em><br />
		                      <em><?php echo $row_rds_eventos['Lugar']; ?></em><br />
			              <br />
			              <em><?php echo $row_rds_eventos['body']; ?></em></p>
                 
              <?php 
			
		//	$month=4;
		//	 $day = 24;
			// $year = 2011;
		//	$timeStamp = mktime(0,0,0, $month, $day, $year); 
		//	echo $timeStamp;
			?>
			              <div align="left"></div></td>
              </tr>
			          </table>
                    </div> <br />
			      <?php } while ($row_rds_eventos = mysql_fetch_assoc($rds_eventos)); ?>
			  
			      
			      <table border="0" width="50%" align="center">
                    <tr>
                      <td width="23%" align="center"><?php if ($pageNum_rds_eventos > 0) { // Show if not first page ?>
                            <a href="<?php printf("%s?pageNum_rds_eventos=%d%s", $currentPage, 0, $queryString_rds_eventos); ?>"><img src="First.gif" border=0></a>
                            <?php } // Show if not first page ?>                      </td>
                      <td width="31%" align="center"><?php if ($pageNum_rds_eventos > 0) { // Show if not first page ?>
                            <a href="<?php printf("%s?pageNum_rds_eventos=%d%s", $currentPage, max(0, $pageNum_rds_eventos - 1), $queryString_rds_eventos); ?>"><img src="Previous.gif" border=0></a>
                            <?php } // Show if not first page ?>                      </td>
                      <td width="23%" align="center"><?php if ($pageNum_rds_eventos < $totalPages_rds_eventos) { // Show if not last page ?>
                            <a href="<?php printf("%s?pageNum_rds_eventos=%d%s", $currentPage, min($totalPages_rds_eventos, $pageNum_rds_eventos + 1), $queryString_rds_eventos); ?>"><img src="Next.gif" border=0></a>
                            <?php } // Show if not last page ?>                      </td>
                      <td width="23%" align="center"><?php if ($pageNum_rds_eventos < $totalPages_rds_eventos) { // Show if not last page ?>
                            <a href="<?php printf("%s?pageNum_rds_eventos=%d%s", $currentPage, $totalPages_rds_eventos, $queryString_rds_eventos); ?>"><img src="Last.gif" border=0></a>
                            <?php } // Show if not last page ?>                      </td>
                    </tr>
                  </table>
                  <p>
                    <?php // ------------------------------------------------- Fin -Contenido -------------------------------------------?>
                  <?php if (!isset($_SESSION)) {   session_start(); } if ((isset($_SESSION['MM_UserGroup'])) and  ($_SESSION['MM_UserGroup']=="1")) { ?><?php } ?></p>	            </td>
            </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><?php require_once('footer.php'); ?></td>
  </tr>
</table>
<map name="Map" id="Map"><area shape="rect" coords="839,201,923,223" href="tarjeta.php" /><area shape="rect" coords="930,200,1001,224" href="tarjeta.php" /></map>
<map name="Map2" id="Map2"><area shape="rect" coords="626,4,668,29" href="mapa.php" alt="Mapa del sitio" /></body>
</body>
</html>
<?php
mysql_free_result($rds_eventos);
?>
