<?php require_once('Connections/datos.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rds_departamentos = "1";
if (isset($_GET['idm'])) {
  $colname_rds_departamentos = (get_magic_quotes_gpc()) ? $_GET['idm'] : addslashes($_GET['idm']);
}
mysql_select_db($database_datos, $datos);
$query_rds_departamentos = sprintf("SELECT * FROM departamentos WHERE id = %s", GetSQLValueString($colname_rds_departamentos, "int"));
$rds_departamentos = mysql_query($query_rds_departamentos, $datos) or die(mysql_error());
$row_rds_departamentos = mysql_fetch_assoc($rds_departamentos);
$totalRows_rds_departamentos = mysql_num_rows($rds_departamentos);

$colname_rds_instituciones = "-1";
if (isset($_GET['idm'])) {
  $colname_rds_instituciones = (get_magic_quotes_gpc()) ? $_GET['idm'] : addslashes($_GET['idm']);
}
mysql_select_db($database_datos, $datos);
$query_rds_instituciones = sprintf("SELECT * FROM asociaciones WHERE id_dep = %s", GetSQLValueString($colname_rds_instituciones, "int"));
$rds_instituciones = mysql_query($query_rds_instituciones, $datos) or die(mysql_error());
$row_rds_instituciones = mysql_fetch_assoc($rds_instituciones);
$totalRows_rds_instituciones = mysql_num_rows($rds_instituciones);

mysql_select_db($database_datos, $datos);
$query_rds_listdep = "SELECT departamentos.id, departamentos.nombre, departamentos.codigo, Count(asociaciones.id) AS CountOfid FROM departamentos LEFT JOIN asociaciones ON departamentos.id = asociaciones.id_dep GROUP BY departamentos.id, departamentos.nombre, departamentos.codigo  ORDER BY nombre ASC";
$rds_listdep = mysql_query($query_rds_listdep, $datos) or die(mysql_error());
$row_rds_listdep = mysql_fetch_assoc($rds_listdep);
$totalRows_rds_listdep = mysql_num_rows($rds_listdep);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Consejo Profesional de Gu&iacute;as de Turismo</title>
<link href="css/estilos.css" rel="stylesheet" type="text/css" />
</head>

<body onload="MM_preloadImages('imagenes/botones/btn_nostros_ovr.gif','imagenes/botones/btn_tarjeta_ovr.gif','imagenes/botones/btn_profesionales_ovr.gif','imagenes/botones/btn_asociaciones_ovr.gif','imagenes/botones/btn_normatividad_ovr.gif','imagenes/botones/btn_normatect_ovr.gif','imagenes/botones/btn_infoacadem_ovr.gif','imagenes/botones/btn_eventos_ovr.gif','imagenes/botones/btn_contactenos_ovr.gif','imagenes/botones/btn_enlaces_ovr.gif')">
<table width="1016" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2"><?php require_once('encabezado.php'); ?></td>
  </tr>
  <tr>
    <td colspan="2" align="right"><?php require_once('buscar.php'); ?></td>
  </tr>
  <tr>
    <td width="851"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="207" valign="top" background="imagenes/fondo_izq.jpg"><?php require_once('menu.php'); ?></td>
        <td width="638" valign="top" bgcolor="#ECECEC"><table width="100%" border="0" cellspacing="0" cellpadding="6">
          <tr>
            <td colspan="2" valign="top"><script type="text/javascript">
AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0','width','633','height','129','src','imagenes/bnn_asociaciones','quality','high','pluginspage','http://www.macromedia.com/go/getflashplayer','movie','imagenes/bnn_asociaciones' ); //end AC code
</script><noscript><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0" width="633" height="129">
                <param name="movie" value="imagenes/bnn_asociaciones.swf" />
                <param name="quality" value="high" />
                <embed src="imagenes/bnn_asociaciones.swf" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="633" height="129"></embed>
              </object></noscript></td>
            </tr>
          <tr>
            <td width="49%" align="center" valign="top">
			<?php if (!isset($_GET['idm'])) { ?>
			<div align="left"><img src="imagenes/mapa.png" width="450" height="506" border="0" usemap="#Map2" /><br />
                <font size="2" face="Arial, Helvetica, sans-serif">Seleccione el departamento o haga clic en el mapa.
                  <map name="Map2" id="Map2">
                    <area shape="poly" coords="221,31" href="#" />
                    <area shape="poly" coords="173,363,198,362,218,365,237,362,251,351,263,346,290,360,298,374,317,380,320,405,306,454,289,450,304,417,268,407,252,404,226,410,210,410,199,392,176,375,174,364" href="asociaciones.php?idm=2" alt="Amazonas"  title="Amazonas"/>
                    <area shape="poly" coords="167,245,182,234,185,222,196,228,204,230,205,221,195,215,186,198,177,200,167,190,158,193,157,210,154,228" href="asociaciones.php?idm=3" title="Cundinamarca" />
                    <area shape="poly" coords="152,44,168,48,173,38,185,40,190,48,190,60,179,70,183,84,180,92,183,102,166,95,152,71,158,56,160,50" href="asociaciones.php?idm=25" title="Magdalena" />
                    <area shape="poly" coords="269,213,296,198,316,184,346,179,380,177,378,190,374,207,372,230,372,238,338,246,302,253,288,272,274,269,269,259" title="Vichada" href="asociaciones.php?idm=5" />
                    <area shape="poly" coords="386,413" href="#" />
                    <area shape="poly" coords="268,269,232,269,207,280,190,282,186,304,158,282,161,270,155,262,185,239,189,230,207,232,208,223,226,235,266,217" href="asociaciones.php?idm=6" title="Meta" />
                    <area shape="poly" coords="74,338,80,338,80,322,83,313,87,308,81,302,75,296,70,286,57,280,44,278,35,286,36,295,32,301,23,301,32,311" href="asociaciones.php?idm=7" title="Nari�o" />
                    <area shape="poly" coords="76,235,88,236,96,238,102,222,102,207,108,195,113,190,108,183,100,179,88,167,87,156,97,156,86,138,82,133,84,128,78,129,71,140,58,145,71,166,75,188" href="asociaciones.php?idm=8" title="Choc&oacute;" />
                    <area shape="poly" coords="119,146,132,136,144,127,154,126,152,118,141,116,136,108,140,101,125,93,111,100,106,106,108,124,104,140" href="asociaciones.php?idm=9" title="C&oacute;rdoba" />
                    <area shape="poly" coords="211,219,222,228,241,224,270,213,286,200,300,192,302,180,276,179,252,179,234,183,236,194,224,206,214,206" href="asociaciones.php?idm=10" title="Casanare" />
                    <area shape="poly" coords="83,422" href="#" />
                    <area shape="poly" coords="95,340,112,337,126,345,147,352,163,361,177,359,158,350,146,338,135,329,118,322,106,320,98,315,90,311,84,323,84,340" href="asociaciones.php?idm=11" title="Putumayo" />
                    <area shape="poly" coords="118,409" href="#" />
                    <area shape="poly" coords="111,314,132,324,146,334,164,348,182,357,216,361,236,356,246,351,257,344,226,321,211,325,193,312,176,304,161,296,155,282,158,272,150,274,140,282" href="asociaciones.php?idm=12" title="Caquet&aacute;" />
                    <area shape="poly" coords="196,285,228,277,245,272,275,272,282,279,290,286,303,287,268,296,257,308,242,325,224,315,210,318,192,303" href="asociaciones.php?idm=13" title="Guaviare" />
                    <area shape="poly" coords="247,330,269,345,286,353,298,359,301,372,317,376,314,362,302,348,302,334,310,329,322,326,330,326,326,318,311,317,310,308,304,296,286,298,265,303" href="asociaciones.php?idm=14" title="Vaupes" />
                    <area shape="poly" coords="291,276,304,258,346,249,375,242,386,254,373,268,389,281,394,290,376,294,360,294,340,298,308,295,310,282" href="asociaciones.php?idm=15" title="Guain&iacute;a" />
                    <area shape="poly" coords="243,155,234,173,242,177,272,174,296,175,306,173,313,180,314,169,304,159,281,154" href="asociaciones.php?idm=16" title="Arauca" />
                    <area shape="poly" coords="60,274,68,262,77,261,94,262,104,260,114,268,117,278,104,282,95,291,100,301,108,309,103,315" href="asociaciones.php?idm=17" title="Cauca" />
                    <area shape="poly" coords="104,290,119,281,118,266,136,256,148,257,162,245,152,264,140,277,130,292,114,302,102,298" href="asociaciones.php?idm=18" title="Huila" />
                    <area shape="poly" coords="76,252,84,241,93,240,102,235,104,224,112,215,118,225,122,234,115,249,106,256,83,259" href="asociaciones.php?idm=19" title="Valle del Cauca" />
                    <area shape="poly" coords="116,194,108,202,106,211,114,212,124,220,130,216,123,206,126,200" href="asociaciones.php?idm=20" title="Risaralda" />
                    <area shape="poly" coords="120,224,124,234,132,227,136,220,130,219" href="asociaciones.php?idm=21" title="Quindio" />
                    <area shape="poly" coords="135,214,144,206,154,202,156,190,150,191,142,195,136,194,130,192,126,196,126,208" href="asociaciones.php?idm=22" title="Caldas" />
                    <area shape="poly" coords="92,114,92,128,88,132,92,141,100,153,92,162,96,174,107,178,114,185,118,193,126,190,137,192,150,190,157,185,159,174,164,168,172,159,179,154,168,156,166,148,160,146,157,139,160,133,156,128,145,130,135,138,128,141,120,150,109,149,100,143,99,128,103,115,100,108" href="asociaciones.php?idm=23" title="Antioquia" />
                    <area shape="poly" coords="131,73,129,80,130,89,138,97,144,104,140,108,144,114,151,116,159,120,162,113,162,105,150,93,144,84" href="asociaciones.php?idm=24" title="Sucre" />
                    <area shape="poly" coords="138,54,146,65,150,68,155,57,154,48,150,45" href="asociaciones.php?idm=4" title="Atl�ntico" />
                    <area shape="poly" coords="132,68,145,83,152,93,162,102,165,116,162,124,164,136,164,143,170,151,180,147,183,137,184,124,183,110,176,102,160,95,152,83,151,74,142,65,138,59" href="asociaciones.php?idm=26" title="Bol&iacute;var" />
                    <area shape="poly" coords="198,54,205,56,206,64,213,65,210,80,200,94,196,100,194,108,193,115,198,130,195,136,188,131,188,115,184,99,184,83,184,69" href="asociaciones.php?idm=27" title="Cesar" />
                    <area shape="poly" coords="211,97,218,104,226,119,226,133,231,145,238,153,229,157,218,155,210,140,202,140,194,138,201,134,198,122,197,110,200,98" href="asociaciones.php?idm=28" title="Norte de Santander" />
                    <area shape="poly" coords="170,170,182,156,187,140,197,142,206,144,212,154,220,164,218,169,211,181,202,184,190,183,184,188,172,180" href="asociaciones.php?idm=29" title="Santander" />
                    <area shape="poly" coords="164,176,161,186,169,188,175,194,182,197,192,200,195,209,205,218,212,201,227,198,228,189,227,178,235,164,237,158,226,162,221,171,218,182,205,188,198,189,193,191,181,191,166,182" href="asociaciones.php?idm=30" title="Boyac&aacute;" />
                      <area shape="poly" coords="117,254,124,237,141,214,151,208,150,232,159,243,148,253,120,259" href="asociaciones.php?idm=31" title="Tolima" />
                  <area shape="rect" coords="633,4,667,30" href="mapa.php" />
                  <area shape="poly" coords="193,47,203,36,231,24,247,17,259,11,268,21,237,42,222,54,212,62" href="asociaciones.php?idm=1" title="Guajira" />
                  <area shape="rect" coords="23,9,107,90" href="asociaciones.php?idm=32" title="San Andr�s y Providencia"/>
                  </map>
                  </font><br />
			 </div>
			<center>
			  <form id="form1" name="form1" method="get" action="">
			    <br />
			    <select name="idm" id="idm" onchange="MM_callJS('form1.submit();')">
			      <option value="">Seleccione...</option>
			      <?php
do {  
?>
			      <option value="<?php echo $row_rds_listdep['id']?>"><?php echo $row_rds_listdep['nombre']?> (<?php echo $row_rds_listdep['CountOfid']; ?>)</option>
			      <?php
} while ($row_rds_listdep = mysql_fetch_assoc($rds_listdep));
  $rows = mysql_num_rows($rds_listdep);
  if($rows > 0) {
      mysql_data_seek($rds_listdep, 0);
	  $row_rds_listdep = mysql_fetch_assoc($rds_listdep);
  }
?>
			      </select>
                      </form>
			  <?php }  else { ?>
			<div align="left">   <a href="asociaciones.php?id=31"><img  border=0  src="imagenes/departamentos/<?php echo $row_rds_departamentos['id']; ?>.png" usemap="#Map3" /></a>  <map name="Map3" id="Map3">
                    <area shape="poly" coords="221,31" href="#" />
                    <area shape="poly" coords="173,363,198,362,218,365,237,362,251,351,263,346,290,360,298,374,317,380,320,405,306,454,289,450,304,417,268,407,252,404,226,410,210,410,199,392,176,375,174,364" href="asociaciones.php?idm=2" alt="Amazonas"  title="Amazonas"/>
                    <area shape="poly" coords="167,245,182,234,185,222,196,228,204,230,205,221,195,215,186,198,177,200,167,190,158,193,157,210,154,228" href="asociaciones.php?idm=3" title="Cundinamarca" />
                    <area shape="poly" coords="152,44,168,48,173,38,185,40,190,48,190,60,179,70,183,84,180,92,183,102,166,95,152,71,158,56,160,50" href="asociaciones.php?idm=25" title="Magdalena" />
                    <area shape="poly" coords="269,213,296,198,316,184,346,179,380,177,378,190,374,207,372,230,372,238,338,246,302,253,288,272,274,269,269,259" title="Vichada" href="asociaciones.php?idm=5" />
                    <area shape="poly" coords="386,413" href="#" />
                    <area shape="poly" coords="268,269,232,269,207,280,190,282,186,304,158,282,161,270,155,262,185,239,189,230,207,232,208,223,226,235,266,217" href="asociaciones.php?idm=6" title="Meta" />
                    <area shape="poly" coords="74,338,80,338,80,322,83,313,87,308,81,302,75,296,70,286,57,280,44,278,35,286,36,295,32,301,23,301,32,311" href="asociaciones.php?idm=7" title="Nari�o" />
                    <area shape="poly" coords="76,235,88,236,96,238,102,222,102,207,108,195,113,190,108,183,100,179,88,167,87,156,97,156,86,138,82,133,84,128,78,129,71,140,58,145,71,166,75,188" href="asociaciones.php?idm=8" title="Choc&oacute;" />
                    <area shape="poly" coords="119,146,132,136,144,127,154,126,152,118,141,116,136,108,140,101,125,93,111,100,106,106,108,124,104,140" href="asociaciones.php?idm=9" title="C&oacute;rdoba" />
                    <area shape="poly" coords="211,219,222,228,241,224,270,213,286,200,300,192,302,180,276,179,252,179,234,183,236,194,224,206,214,206" href="asociaciones.php?idm=10" title="Casanare" />
                    <area shape="poly" coords="83,422" href="#" />
                    <area shape="poly" coords="95,340,112,337,126,345,147,352,163,361,177,359,158,350,146,338,135,329,118,322,106,320,98,315,90,311,84,323,84,340" href="asociaciones.php?idm=11" title="Putumayo" />
                    <area shape="poly" coords="118,409" href="#" />
                    <area shape="poly" coords="111,314,132,324,146,334,164,348,182,357,216,361,236,356,246,351,257,344,226,321,211,325,193,312,176,304,161,296,155,282,158,272,150,274,140,282" href="asociaciones.php?idm=12" title="Caquet&aacute;" />
                    <area shape="poly" coords="196,285,228,277,245,272,275,272,282,279,290,286,303,287,268,296,257,308,242,325,224,315,210,318,192,303" href="asociaciones.php?idm=13" title="Guaviare" />
                    <area shape="poly" coords="247,330,269,345,286,353,298,359,301,372,317,376,314,362,302,348,302,334,310,329,322,326,330,326,326,318,311,317,310,308,304,296,286,298,265,303" href="asociaciones.php?idm=14" title="Vaupes" />
                    <area shape="poly" coords="291,276,304,258,346,249,375,242,386,254,373,268,389,281,394,290,376,294,360,294,340,298,308,295,310,282" href="asociaciones.php?idm=15" title="Guain&iacute;a" />
                    <area shape="poly" coords="243,155,234,173,242,177,272,174,296,175,306,173,313,180,314,169,304,159,281,154" href="asociaciones.php?idm=16" title="Arauca" />
                    <area shape="poly" coords="60,274,68,262,77,261,94,262,104,260,114,268,117,278,104,282,95,291,100,301,108,309,103,315" href="asociaciones.php?idm=17" title="Cauca" />
                    <area shape="poly" coords="104,290,119,281,118,266,136,256,148,257,162,245,152,264,140,277,130,292,114,302,102,298" href="asociaciones.php?idm=18" title="Huila" />
                    <area shape="poly" coords="76,252,84,241,93,240,102,235,104,224,112,215,118,225,122,234,115,249,106,256,83,259" href="asociaciones.php?idm=19" title="Valle del Cauca" />
                    <area shape="poly" coords="116,194,108,202,106,211,114,212,124,220,130,216,123,206,126,200" href="asociaciones.php?idm=20" title="Risaralda" />
                    <area shape="poly" coords="120,224,124,234,132,227,136,220,130,219" href="asociaciones.php?idm=21" title="Quindio" />
                    <area shape="poly" coords="135,214,144,206,154,202,156,190,150,191,142,195,136,194,130,192,126,196,126,208" href="asociaciones.php?idm=22" title="Caldas" />
                    <area shape="poly" coords="92,114,92,128,88,132,92,141,100,153,92,162,96,174,107,178,114,185,118,193,126,190,137,192,150,190,157,185,159,174,164,168,172,159,179,154,168,156,166,148,160,146,157,139,160,133,156,128,145,130,135,138,128,141,120,150,109,149,100,143,99,128,103,115,100,108" href="asociaciones.php?idm=23" title="Antioquia" />
                    <area shape="poly" coords="131,73,129,80,130,89,138,97,144,104,140,108,144,114,151,116,159,120,162,113,162,105,150,93,144,84" href="asociaciones.php?idm=24" title="Sucre" />
                    <area shape="poly" coords="138,54,146,65,150,68,155,57,154,48,150,45" href="asociaciones.php?idm=4" title="Atl�ntico" />
                    <area shape="poly" coords="132,68,145,83,152,93,162,102,165,116,162,124,164,136,164,143,170,151,180,147,183,137,184,124,183,110,176,102,160,95,152,83,151,74,142,65,138,59" href="asociaciones.php?idm=26" title="Bol&iacute;var" />
                    <area shape="poly" coords="198,54,205,56,206,64,213,65,210,80,200,94,196,100,194,108,193,115,198,130,195,136,188,131,188,115,184,99,184,83,184,69" href="asociaciones.php?idm=27" title="Cesar" />
                    <area shape="poly" coords="211,97,218,104,226,119,226,133,231,145,238,153,229,157,218,155,210,140,202,140,194,138,201,134,198,122,197,110,200,98" href="asociaciones.php?idm=28" title="Norte de Santander" />
                    <area shape="poly" coords="170,170,182,156,187,140,197,142,206,144,212,154,220,164,218,169,211,181,202,184,190,183,184,188,172,180" href="asociaciones.php?idm=29" title="Santander" />
                    <area shape="poly" coords="164,176,161,186,169,188,175,194,182,197,192,200,195,209,205,218,212,201,227,198,228,189,227,178,235,164,237,158,226,162,221,171,218,182,205,188,198,189,193,191,181,191,166,182" href="asociaciones.php?idm=30" title="Boyac&aacute;" />
                      <area shape="poly" coords="117,254,124,237,141,214,151,208,150,232,159,243,148,253,120,259" href="asociaciones.php?idm=31" title="Tolima" />
                  <area shape="rect" coords="633,4,667,30" href="mapa.php" />
                  <area shape="poly" coords="193,47,203,36,231,24,247,17,259,11,268,21,237,42,222,54,212,62" href="asociaciones.php?idm=1" title="Guajira" />
                  <area shape="rect" coords="23,9,107,90" href="asociaciones.php?idm=32" title="San Andr�s y Providencia"/>
                  </map>
			  <br /></div>
			  <a href="asociaciones.php?id=31"><br />
			  <img src="imagenes/mapacol.gif" width="105" height="118" border="0" title="Regresar al mapa de Colombia"/>
			  <br />
			  <font size="2" face="Verdana, Arial, Helvetica, sans-serif">Regresar</font>
			</center></a>
			<?php } ?></td>
            <td width="51%" valign="top">
			  
			    <?php // --------------------------------------------------Contenido -------------------------------------------?>
			  
			  
			        <img src="imagenes/bullet.gif" width="12" height="12" /> <strong><font face="Arial, Helvetica, sans-serif"><?php echo $row_rds_departamentos['nombre']; ?></font> </strong>
                     <?php if ($totalRows_rds_instituciones > 0) { // Show if recordset not empty ?>
                      <?php do { ?>
                        <div align="left" class="rounded">
                          <table width="100%" border="0" cellspacing="0" cellpadding="6">
                            <tr>
                              <td><p><strong><?php echo $row_rds_instituciones['nombre']; ?></strong><br />
                                <?php echo $row_rds_instituciones['ciudad']; ?><br />
                                <em><font color="#333333"><?php echo $row_rds_instituciones['direccion']; ?><?php echo $row_rds_instituciones['telefono']; ?><br />
                              <?php echo $row_rds_instituciones['contacto']; ?></font></em></p></td>
                            </tr>
                          </table>
                      </div>
                      <br />
                        <?php } while ($row_rds_instituciones = mysql_fetch_assoc($rds_instituciones)); ?>
                      <br />
                    <?php // ------------------------------------------------- Fin -Contenido -------------------------------------------?>
                 <?php } // Show if recordset empty ?>
				    <?php if ($totalRows_rds_instituciones == 0) { // Show if recordset empty ?>
				      <div align="left" class="rounded">
				        <table width="100%" border="0" cellspacing="0" cellpadding="6">
				          <tr>
				            <td><p><font size="1">No hay Asociaciones en el departamento.</font> </p></td>
                          </tr>
			            </table>
                     </div>
				     
                     <?php } // Show if recordset not empty ?>
				
				

			   <?php if (!isset($_SESSION)) {   session_start(); } if ((isset($_SESSION['MM_UserGroup'])) and  ($_SESSION['MM_UserGroup']=="1")) { ?><a href="javascript:;" onclick="window.open('index_asociaciones.php','Editor','scrollbars=yes,resizable=yes,width=800,height=600')"><img src="imagenes/iconos/editor.png" alt="editar" width="32" height="32" border="0" /></a><?php } ?>
				</td>
          </tr>
        </table></td>
      </tr>
    </table></td>
    <td width="167" valign="top" background="imagenes/fondo_der.jpg">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><?php require_once('footer.php'); ?></td>
  </tr>
</table>
<map name="Map" id="Map"><area shape="rect" coords="839,201,923,223" href="tarjeta.php" /><area shape="rect" coords="930,200,1001,224" href="tarjeta.php" /></map>
<map name="Map2" id="Map2"><area shape="rect" coords="626,4,668,29" href="mapa.php" alt="Mapa del sitio" />
</body>
</body>
</html>
<?php
mysql_free_result($rds_departamentos);

mysql_free_result($rds_instituciones);

mysql_free_result($rds_listdep);
?>
