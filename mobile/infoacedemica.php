<?php require_once('Connections/datos.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_rds_programas = 5;
$pageNum_rds_programas = 0;
if (isset($_GET['pageNum_rds_programas'])) {
  $pageNum_rds_programas = $_GET['pageNum_rds_programas'];
}
$startRow_rds_programas = $pageNum_rds_programas * $maxRows_rds_programas;

mysql_select_db($database_datos, $datos);
$query_rds_programas = "SELECT * FROM progacademicos ORDER BY id ASC";
$query_limit_rds_programas = sprintf("%s LIMIT %d, %d", $query_rds_programas, $startRow_rds_programas, $maxRows_rds_programas);
$rds_programas = mysql_query($query_limit_rds_programas, $datos) or die(mysql_error());
$row_rds_programas = mysql_fetch_assoc($rds_programas);

if (isset($_GET['totalRows_rds_programas'])) {
  $totalRows_rds_programas = $_GET['totalRows_rds_programas'];
} else {
  $all_rds_programas = mysql_query($query_rds_programas);
  $totalRows_rds_programas = mysql_num_rows($all_rds_programas);
}
$totalPages_rds_programas = ceil($totalRows_rds_programas/$maxRows_rds_programas)-1;

$queryString_rds_programas = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_rds_programas") == false && 
        stristr($param, "totalRows_rds_programas") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_rds_programas = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_rds_programas = sprintf("&totalRows_rds_programas=%d%s", $totalRows_rds_programas, $queryString_rds_programas);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Consejo Profesional de Gu&iacute;as de Turismo</title>
<link href="css/estilos.css" rel="stylesheet" type="text/css" />
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
</head>

<body onload="MM_preloadImages('imagenes/botones/btn_nostros_ovr.gif','imagenes/botones/btn_tarjeta_ovr.gif','imagenes/botones/btn_profesionales_ovr.gif','imagenes/botones/btn_asociaciones_ovr.gif','imagenes/botones/btn_normatividad_ovr.gif','imagenes/botones/btn_normatect_ovr.gif','imagenes/botones/btn_infoacadem_ovr.gif','imagenes/botones/btn_eventos_ovr.gif','imagenes/botones/btn_contactenos_ovr.gif')">
<table width="1016" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><?php require_once('encabezado.php'); ?></td>
  </tr>
 
  <tr>
    <td width="851"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="638" valign="top" bgcolor="#ECECEC"><table width="100%" border="0" cellspacing="0" cellpadding="6">
          <tr>
            <td valign="top"><br /> 
			         <br />
			       <div align="center">
		            
		              <?php do { ?>
	              </div>
		            <div align="left" class="rounded">
		               <div  >
		                 <table width="100%" border="0" cellspacing="0" cellpadding="6">
		                   <tr>
		                     <td> <table width="100%" border="0" cellspacing="0" cellpadding="1">
                    <tr>
                      <td width="19%" rowspan="4"><img src="imagenes/cursos/<?php echo $row_rds_programas['id']; ?>.jpg" width="104" height="91" /></td>
                      <td width="81%"><strong><font size="3"><?php echo $row_rds_programas['titulo']; ?></font></strong></td>
                    </tr>
		                       <tr>
		                         <td>Nivel: <?php echo $row_rds_programas['tipo']; ?></td>
                    </tr>
		                       <tr>
		                         <td><a href="<?php echo $row_rds_programas['web']; ?>"><?php echo $row_rds_programas['web']; ?></a></td>
                    </tr>
		                       <tr>
		                         <td><p><em><font size="1">t <?php echo $row_rds_programas['descripcion']; ?></font></em></p></td>
                    </tr>
                
                  </table></td>
              </tr>
                         </table>
	                  </div>
		          </div>
			         <div align="center"><?php } while ($row_rds_programas = mysql_fetch_assoc($rds_programas)); // dsfh ?>
			         <br />
                    <?php // ------------------------------------------------- Fin -Contenido -------------------------------------------?>
                  </div> 
			         <font size="1" face="Arial, Helvetica, sans-serif"><center>Programas del <?php echo ($startRow_rds_programas + 1) ?> al <?php echo min($startRow_rds_programas + $maxRows_rds_programas, $totalRows_rds_programas) ?> de <?php echo $totalRows_rds_programas ?>                    </font></center>
			         <table border="0" width="50%" align="center">
       <tr>
         <td width="23%" align="center"><?php if ($pageNum_rds_programas > 0) { // Show if not first page ?>
               <a href="<?php printf("%s?pageNum_rds_programas=%d%s", $currentPage, 0, $queryString_rds_programas); ?>"><img src="First.gif" border=0></a>
               <?php } // Show if not first page ?>         </td>
         <td width="31%" align="center"><?php if ($pageNum_rds_programas > 0) { // Show if not first page ?>
               <a href="<?php printf("%s?pageNum_rds_programas=%d%s", $currentPage, max(0, $pageNum_rds_programas - 1), $queryString_rds_programas); ?>"><img src="Previous.gif" border=0></a>
               <?php } // Show if not first page ?>         </td>
         <td width="23%" align="center"><?php if ($pageNum_rds_programas < $totalPages_rds_programas) { // Show if not last page ?>
               <a href="<?php printf("%s?pageNum_rds_programas=%d%s", $currentPage, min($totalPages_rds_programas, $pageNum_rds_programas + 1), $queryString_rds_programas); ?>"><img src="Next.gif" border=0></a>
               <?php } // Show if not last page ?>         </td>
         <td width="23%" align="center"><?php if ($pageNum_rds_programas < $totalPages_rds_programas) { // Show if not last page ?>
               <a href="<?php printf("%s?pageNum_rds_programas=%d%s", $currentPage, $totalPages_rds_programas, $queryString_rds_programas); ?>"><img src="Last.gif" border=0></a>
               <?php } // Show if not last page ?>         </td>
       </tr>
     </table></td>
          </tr>
        </table>  <?php if (!isset($_SESSION)) {   session_start(); } if ((isset($_SESSION['MM_UserGroup'])) and  ($_SESSION['MM_UserGroup']=="1")) { ?>
            <?php } ?></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><?php require_once('footer.php'); ?></td>
  </tr>
</table>
<map name="Map" id="Map"><area shape="rect" coords="839,201,923,223" href="tarjeta.php" /><area shape="rect" coords="930,200,1001,224" href="tarjeta.php" /></map>
<map name="Map2" id="Map2"><area shape="rect" coords="626,4,668,29" href="mapa.php" alt="Mapa del sitio" /></body>
</body>
</html>
<?php
mysql_free_result($rds_programas);
?>
