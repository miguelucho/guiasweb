<?php require_once('Connections/datos.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
   
   $archivo = $_FILES["archivo"]['name']; 
   if ($archivo != "" ) {
  
  
    	$destino =  "documentos/normas/".$archivo;
        if (copy($_FILES['archivo']['tmp_name'],$destino)) {
            $status = "Archivo subido: <b>".$archivo."</b>";
        } else {
            $status = "Error al subir el archivo";
        }
					
 
  $updateSQL = sprintf("UPDATE normastecnicas SET titulo=%s, titulo_eng=%s, descriptores=%s, fecha=%s, archivo=%s, NTS=%s WHERE id=%s",
                       GetSQLValueString($_POST['titulo'], "text"),
                       GetSQLValueString($_POST['titulo_eng'], "text"),
                       GetSQLValueString($_POST['descriptores'], "text"),
                       GetSQLValueString($_POST['fecha'], "date"),
                       GetSQLValueString($archivo, "text"),
                       GetSQLValueString($_POST['NTS'], "text"),
                       GetSQLValueString($_POST['id'], "int"));

 }
else
{

  $updateSQL = sprintf("UPDATE normastecnicas SET titulo=%s, titulo_eng=%s, descriptores=%s, fecha=%s,   NTS=%s WHERE id=%s",
                       GetSQLValueString($_POST['titulo'], "text"),
                       GetSQLValueString($_POST['titulo_eng'], "text"),
                       GetSQLValueString($_POST['descriptores'], "text"),
                       GetSQLValueString($_POST['fecha'], "date"),
                     
                       GetSQLValueString($_POST['NTS'], "text"),
                       GetSQLValueString($_POST['id'], "int"));
					   
					   
}
 
 
  mysql_select_db($database_datos, $datos);
  $Result1 = mysql_query($updateSQL, $datos) or die(mysql_error());

  $updateGoTo = "index_normastec.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

 



$colname_rds_normas = "-1";
if (isset($_GET['id'])) {
  $colname_rds_normas = (get_magic_quotes_gpc()) ? $_GET['id'] : addslashes($_GET['id']);
}
mysql_select_db($database_datos, $datos);
$query_rds_normas = sprintf("SELECT * FROM normastecnicas WHERE id = %s ORDER BY titulo ASC", GetSQLValueString($colname_rds_normas, "int"));
$rds_normas = mysql_query($query_rds_normas, $datos) or die(mysql_error());
$row_rds_normas = mysql_fetch_assoc($rds_normas);
$totalRows_rds_normas = mysql_num_rows($rds_normas);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Consejo Profesional de Gu�as de Turismo </title>
<script type="text/JavaScript">
<!--
function MM_callJS(jsStr) { //v2.0
  return eval(jsStr)
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.name; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
      } else if (test!='R') { num = parseFloat(val);
        if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' es requeridos.\n'; }
  } if (errors) alert('Hay los siguientes errores:\n'+errors);
  document.MM_returnValue = (errors == '');
}

function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}
//-->
</script>
</head>

<body onload="MM_callJS('focus();')">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td colspan="3" bgcolor="#ECF2FF"><div align="left"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Editar Norma t&eacute;cnica </strong></font></div></td>
  </tr>
  <tr>
    <td width="7%">&nbsp;</td>
    <td width="93%" colspan="2"><form action="<?php echo $editFormAction; ?>" method="POST" enctype="multipart/form-data" name="form1" id="form1"  >
      <table width="100%" border="0" cellpadding="6" cellspacing="0" class="tablatexto">
        <tr>
          <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif">NTS</font></td>
          <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif">
            <input name="NTS" type="text" id="NTS" value="<?php echo $row_rds_normas['NTS']; ?>" size="60" />
            <input name="id" type="hidden" id="id" value="<?php echo $row_rds_normas['id']; ?>" />
          </font></td>
        </tr>
        <tr>
          <td width="6%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">T&iacute;tulo</font></td>
          <td width="94%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">
            <input name="titulo" type="text" id="titulo" value="<?php echo $row_rds_normas['titulo']; ?>" size="60" />
          </font></td>
        </tr>
        <tr>
          <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif">T&iacute;tulo ingl&eacute;s </font></td>
          <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif">
            <input name="titulo_eng" type="text" id="titulo_eng" value="<?php echo $row_rds_normas['titulo_eng']; ?>" size="60" />
          </font></td>
        </tr>
        <tr>
          <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Fecha</font></td>
          <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif">
            <input name="fecha" type="text" id="fecha" value="<?php echo $row_rds_normas['fecha']; ?>" size="10" maxlength="10" />
          (AAAA-MM-DD)</font></td>
        </tr>
        
        <tr>
          <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Archivo </font></td>
          <td><input name="archivo" type="file" id="archivo" />
            <strong><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Actual</font>:</strong> <font size="1" face="Verdana, Arial, Helvetica, sans-serif"> <?php echo $row_rds_normas['archivo']; ?><br />
(renombrar los archivos sin espacios, tildes o e&ntilde;es. Utilizar solo min&uacute;sculas) </font></td>
        </tr>
        
        <tr>
          <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Descriptores </font></td>
          <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif">
            <input name="descriptores" type="text" id="descriptores" value="<?php echo $row_rds_normas['descriptores']; ?>" size="120" />
          </font></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><input name="Submit" type="submit" onclick="MM_validateForm('NTS','','R','titulo','','R','titulo_eng','','R','descriptores','','R');return document.MM_returnValue" value="Guardar" /></td>
        </tr>
      </table>
        <input type="hidden" name="MM_insert" value="form1">
        <input type="hidden" name="MM_update" value="form1">
     
    </form>    </td>
  </tr>
</table>
<blockquote>&nbsp;</blockquote>
</body>
</html>
<?php
mysql_free_result($rds_normas);
?>
