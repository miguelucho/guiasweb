<?php require_once('Connections/datos.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$currentPage = $_SERVER["PHP_SELF"];
// where aprobada=1
$maxRows_rds_solicitudes = 30;
$pageNum_rds_solicitudes = 0;
if (isset($_GET['pageNum_rds_solicitudes'])) {
  $pageNum_rds_solicitudes = $_GET['pageNum_rds_solicitudes'];
}
$startRow_rds_solicitudes = $pageNum_rds_solicitudes * $maxRows_rds_solicitudes;

mysql_select_db($database_datos, $datos);
$query_rds_solicitudes = "SELECT * FROM tramite  ORDER BY apellidos ASC";
$query_limit_rds_solicitudes = sprintf("%s LIMIT %d, %d", $query_rds_solicitudes, $startRow_rds_solicitudes, $maxRows_rds_solicitudes);
$rds_solicitudes = mysql_query($query_limit_rds_solicitudes, $datos) or die(mysql_error());
$row_rds_solicitudes = mysql_fetch_assoc($rds_solicitudes);

if (isset($_GET['totalRows_rds_solicitudes'])) {
  $totalRows_rds_solicitudes = $_GET['totalRows_rds_solicitudes'];
} else {
  $all_rds_solicitudes = mysql_query($query_rds_solicitudes);
  $totalRows_rds_solicitudes = mysql_num_rows($all_rds_solicitudes);
}
$totalPages_rds_solicitudes = ceil($totalRows_rds_solicitudes/$maxRows_rds_solicitudes)-1;

$queryString_rds_solicitudes = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_rds_solicitudes") == false && 
        stristr($param, "totalRows_rds_solicitudes") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_rds_solicitudes = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_rds_solicitudes = sprintf("&totalRows_rds_solicitudes=%d%s", $totalRows_rds_solicitudes, $queryString_rds_solicitudes);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Consejo Profesional de Gu�as de Turismo </title>
</head>

<body>
<table border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td>&nbsp;</td>
    <td><font size="1" face="Arial, Helvetica, sans-serif">Apellidos</font></td>
    <td><font size="1" face="Arial, Helvetica, sans-serif">Nombres</font></td>
    <td><font size="1" face="Arial, Helvetica, sans-serif">ID</font></td>
    <td><font size="1" face="Arial, Helvetica, sans-serif">Fecha solicitud </font></td>
    <td><font size="1" face="Arial, Helvetica, sans-serif">&nbsp;</font></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <?php do { ?>
    <tr>
      <td>&nbsp;</td>
      <td><font size="1" face="Arial, Helvetica, sans-serif"><?php echo $row_rds_solicitudes['apellidos']; ?></font></td>
      <td><font size="1" face="Arial, Helvetica, sans-serif"><?php echo $row_rds_solicitudes['nombres']; ?></font></td>
      <td><font size="1" face="Arial, Helvetica, sans-serif"><?php echo $row_rds_solicitudes['numeroid']; ?></font></td>
      <td><font size="1" face="Arial, Helvetica, sans-serif"><?php echo $row_rds_solicitudes['fecha_formulario']; ?></font></td>
      <td><font size="1" face="Arial, Helvetica, sans-serif"><a href="profesionales_nuevo.php?idp=<?php echo $row_rds_solicitudes['id']; ?>">Aplicar</a></font></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <?php } while ($row_rds_solicitudes = mysql_fetch_assoc($rds_solicitudes)); ?>
  <tr>
    <td>&nbsp;</td>
    <td colspan="5">&nbsp;
      <table border="0" width="50%" align="center">
        <tr>
          <td width="23%" align="center"><?php if ($pageNum_rds_solicitudes > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_rds_solicitudes=%d%s", $currentPage, 0, $queryString_rds_solicitudes); ?>"><img src="First.gif" border=0></a>
                <?php } // Show if not first page ?>          </td>
          <td width="31%" align="center"><?php if ($pageNum_rds_solicitudes > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_rds_solicitudes=%d%s", $currentPage, max(0, $pageNum_rds_solicitudes - 1), $queryString_rds_solicitudes); ?>"><img src="Previous.gif" border=0></a>
                <?php } // Show if not first page ?>          </td>
          <td width="23%" align="center"><?php if ($pageNum_rds_solicitudes < $totalPages_rds_solicitudes) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_rds_solicitudes=%d%s", $currentPage, min($totalPages_rds_solicitudes, $pageNum_rds_solicitudes + 1), $queryString_rds_solicitudes); ?>"><img src="Next.gif" border=0></a>
                <?php } // Show if not last page ?>          </td>
          <td width="23%" align="center"><?php if ($pageNum_rds_solicitudes < $totalPages_rds_solicitudes) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_rds_solicitudes=%d%s", $currentPage, $totalPages_rds_solicitudes, $queryString_rds_solicitudes); ?>"><img src="Last.gif" border=0></a>
                <?php } // Show if not last page ?>          </td>
        </tr>
      </table></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>
<?php
mysql_free_result($rds_solicitudes);
?>
