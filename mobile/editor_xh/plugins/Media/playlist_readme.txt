Media's Playlist

The Playlist functionality is configured on startup using ExtendedFileManager's config.inc.php: written to whichever folder is
set and if new directories are allowed/safe mode is off then playlists are placed in a sub-folder called media_playlist.

If ExtendedFileManager is not able to write new files to it's set folder the playlist option will not be shown.

The playlist itself is written with an .xml extention and is in an rss format. When adding new elements to the playlist the rss
is shown. If your browser does not support the viewing of rss feeds then you will not see the playlist in this state - but there
is an alternative edit view so little functionality is lost.

Supported file types - flv, mp3, png, jpg & gif 