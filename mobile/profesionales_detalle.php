<?php require_once('Connections/datos.php'); ?>
<?php

 
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$currentPage = $_SERVER["PHP_SELF"]; 

$colname_rds_profesionales = "-1";
if (isset($_GET['id'])) {
  $colname_rds_profesionales = (get_magic_quotes_gpc()) ? $_GET['id'] : addslashes($_GET['id']);
}
mysql_select_db($database_datos, $datos);
$query_rds_profesionales = sprintf("SELECT profesionales.*, departamentos.nombre as departamentoN  FROM profesionales LEFT JOIN departamentos ON profesionales.departamento = departamentos.id WHERE profesionales.id = %s", GetSQLValueString($colname_rds_profesionales, "text"));
$rds_profesionales = mysql_query($query_rds_profesionales, $datos) or die(mysql_error());
$row_rds_profesionales = mysql_fetch_assoc($rds_profesionales);
$totalRows_rds_profesionales = mysql_num_rows($rds_profesionales);

$colname_rds_idiomas = "-1";
if (isset($_GET['id'])) {
  $colname_rds_idiomas = (get_magic_quotes_gpc()) ? $_GET['id'] : addslashes($_GET['id']);
}
mysql_select_db($database_datos, $datos);
$query_rds_idiomas = sprintf("SELECT profesionales_idiomas.*, idiomas.idioma FROM profesionales_idiomas left join idiomas on profesionales_idiomas.id_idioma = idiomas.id WHERE id_guia = %s", GetSQLValueString($colname_rds_idiomas, "int"));
$rds_idiomas = mysql_query($query_rds_idiomas, $datos) or die(mysql_error());
$row_rds_idiomas = mysql_fetch_assoc($rds_idiomas);
$totalRows_rds_idiomas = mysql_num_rows($rds_idiomas);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Consejo Profesional de Gu&iacute;as de Turismo</title>
<link href="css/estilos.css" rel="stylesheet" type="text/css" />
</head>

<body onload="MM_preloadImages('imagenes/botones/btn_nostros_ovr.gif','imagenes/botones/btn_tarjeta_ovr.gif','imagenes/botones/btn_profesionales_ovr.gif','imagenes/botones/btn_asociaciones_ovr.gif','imagenes/botones/btn_normatividad_ovr.gif','imagenes/botones/btn_normatect_ovr.gif','imagenes/botones/btn_infoacadem_ovr.gif','imagenes/botones/btn_eventos_ovr.gif','imagenes/botones/btn_contactenos_ovr.gif')">
<table width="1016" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2"><?php require_once('encabezado.php'); ?></td>
  </tr>
  <tr>
    <td colspan="2" align="right"><?php require_once('buscar.php'); ?></td>
  </tr>
  <tr>
    <td width="851"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="207" valign="top" background="imagenes/fondo_izq.jpg"><?php require_once('menu.php'); ?></td>
        <td width="638" valign="top" bgcolor="#ECECEC"><table width="100%" border="0" cellspacing="0" cellpadding="2">
          <tr>
          <td valign="top"><img src="imagenes/banner_profesionales.jpg" />
			   
			    <p>
			      <?php // --------------------------------------------------Contenido -------------------------------------------?>
			      </p>
			    <p><img src="imagenes/bullet.gif" width="12" height="12" /> <strong><font face="Arial, Helvetica, sans-serif"><?php echo $row_rds_profesionales['nombres']; ?> <?php echo $row_rds_profesionales['apellidos']; ?></font></strong><br />
			      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size="1" face="Arial, Helvetica, sans-serif"><i>Ultima actualizaci&oacute;n: <?php echo $row_rds_profesionales['utlimaactualizacion']; ?></i></font> 			    <div align="left" class="rounded">
        <table width="100%" border="0" cellspacing="0" cellpadding="6">
          <tr>
            <td>              <table width="100%" border="0" cellspacing="0" cellpadding="4">
                
                <tr>
                  <td colspan="2" bgcolor="#CCCCCC">Datos generales </td>
                  <td width="23%" rowspan="6" align="left" valign="top">
				  <?php
$nombre_archivo = "imagenes/profesionales/".$row_rds_profesionales['id'].".jpg";

if (file_exists($nombre_archivo)) {
   ?>

				  <img src="imagenes/profesionales/<?php echo $row_rds_profesionales['id']; ?>.jpg" width="113" height="151" hspace="10" vspace="10" /> 
	<?php 			  
			
} else { ?>
    <img src="imagenes/profesionales/nophoto.gif" width="113" height="151" hspace="10" vspace="10" />  <?php
}
?>				  </td>
	            </tr>
                <tr>
                  <td bgcolor="#FFFFFF"><font color="#999999">Tarjeta Profesional de Gu&iacute;a de Turismo n&uacute;mero: </font></td>
                  <td bgcolor="#FFFFFF"><?php echo $row_rds_profesionales['tarjetano']; ?></td>
                  </tr>
                <tr>
                  <td bgcolor="#FFFFFF"><font color="#999999">Registro Nacional de Turismo n&uacute;mero: </font></td>
                  <td bgcolor="#FFFFFF"><?php echo $row_rds_profesionales['registronacionaldeturismo']; ?></td>
                  </tr>
                <tr>
                  <td colspan="2" bgcolor="#CCCCCC">Localizaci&oacute;n</td>
                          </tr>
                <tr>
                  <td width="29%"><font color="#999999">Ciudad:</font></td>
                      <td width="48%"><?php echo $row_rds_profesionales['ciudad']; ?></td>
                      </tr>
                <tr>
                  <td><font color="#999999">Departamento:</font></td>
                      <td><?php echo $row_rds_profesionales['departamentoN']; ?></td>
                      </tr>
                <tr>
                  <td colspan="3" bgcolor="#CCCCCC">Idiomas hablados  </td>
                      </tr>
                <tr>
                  <td>&nbsp;</td>
                      <td>Espa&ntilde;ol<?php if ($totalRows_rds_idiomas > 0) { // Show if recordset not empty ?><?php do { ?>, <?php echo $row_rds_idiomas['idioma']; ?><?php } while ($row_rds_idiomas = mysql_fetch_assoc($rds_idiomas)); ?><?php } // Show if recordset not empty ?></td>
                      <td width="23%" align="left" valign="top">&nbsp;</td>
                </tr>
                
				   <?php if( ($row_rds_profesionales['auto_especialidad']+$row_rds_profesionales['auto_profesiones']+$row_rds_profesionales['auto_disponibilidad']+$row_rds_profesionales['auto_calidadcert']) > 0 ) {  ?>
				<tr>
                  <td colspan="3" bgcolor="#CCCCCC">Perf&iacute;l </td>
                      </tr>
                 <?php } ?>
				
				
				<?php if(1==$row_rds_profesionales['auto_especialidad']) {  ?>
				<tr>
                  <td><font color="#999999">Especialidad:</font></td>
                  <td colspan="2"><?php echo $row_rds_profesionales['especialidad']; ?></td>
                  </tr>
				  <?php } ?>
				  
				  
                <?php if(1== $row_rds_profesionales['auto_profesiones']) { ?>
                <tr>
                  <td><font color="#999999">Otras profesiones: </font></td>
                  <td colspan="2"><?php echo $row_rds_profesionales['otraprofesion']; ?></td>
                  </tr>
				  <?php } ?>
				  
				  <?php if(1== $row_rds_profesionales['auto_disponibilidad']) {  ?>
                <tr>
                  <td><font color="#999999">Disponibilidad:</font></td>
                  <td colspan="2"><?php echo $row_rds_profesionales['disponibilidad']; ?></td>
                  </tr>
				  <?php } ?>
				  
				  
				  <?php if(1== $row_rds_profesionales['auto_calidadcert']) {  ?>
                <tr>
                  <td><font color="#999999">Certificaciones de calidad : </font></td>
                  <td colspan="2"><?php echo $row_rds_profesionales['certificaciones']; ?></td>
                  </tr>
                
				<?php } ?>
                  
				  
				  
				  
 
				  <tr>
				  <td colspan="3" bgcolor="#CCCCCC">Datos de contacto </td>
                  </tr>
			 
				  
				 
                <tr>
                  <td><font color="#999999">Tel&eacute;fonos:</font></td>
                  <td colspan="2"><?php echo $row_rds_profesionales['telefonos']; ?></td>
                </tr> 
				
			 
                <tr>
                  <td><font color="#999999">Celular:</font></td>
                  <td colspan="2"> </td>
                </tr>
				 
				
				 
                <tr>
                  <td><font color="#999999">Correo electr&oacute;nica: </font></td>
                  <td colspan="2"><?php echo $row_rds_profesionales['correoe'];   ?></td>
                </tr>
				 
              </table></td></tr>
        </table>
      </div>
			  
			  
			  
	 <br />
	 <?php // ------------------------------------------------- Fin -Contenido -------------------------------------------?></td>
          </tr> 
        </table></td>
      </tr>
    </table></td>
    <td width="167" valign="top" background="imagenes/fondo_der.jpg">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><?php require_once('footer.php'); ?></td>
  </tr>
</table>
<map name="Map" id="Map"><area shape="rect" coords="839,201,923,223" href="tarjeta.php" /><area shape="rect" coords="930,200,1001,224" href="tarjeta.php" /></map>
<map name="Map2" id="Map2"><area shape="rect" coords="626,4,668,29" href="mapa.php" alt="Mapa del sitio" /></body>
</body>
</html>
<?php
mysql_free_result($rds_idiomas);

 
?>
