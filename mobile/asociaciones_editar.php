<?php require_once('Connections/datos.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE asociaciones SET id_dep=%s, nombre=%s, direccion=%s, telefono=%s, ciudad=%s, contacto=%s WHERE id=%s",
                       GetSQLValueString($_POST['id_dep'], "int"),
                       GetSQLValueString($_POST['nombre'], "text"),
                       GetSQLValueString($_POST['direccion'], "text"),
                       GetSQLValueString($_POST['telefono'], "text"),
                       GetSQLValueString($_POST['ciudad'], "text"),
                       GetSQLValueString($_POST['contacto'], "text"),
                       GetSQLValueString($_POST['id'], "int"));

  mysql_select_db($database_datos, $datos);
  $Result1 = mysql_query($updateSQL, $datos) or die(mysql_error());

  $updateGoTo = "index_asociaciones.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_rds_asociaciones = "-1";
if (isset($_GET['id'])) {
  $colname_rds_asociaciones = (get_magic_quotes_gpc()) ? $_GET['id'] : addslashes($_GET['id']);
}
mysql_select_db($database_datos, $datos);
$query_rds_asociaciones = sprintf("SELECT * FROM asociaciones WHERE id = %s", GetSQLValueString($colname_rds_asociaciones, "int"));
$rds_asociaciones = mysql_query($query_rds_asociaciones, $datos) or die(mysql_error());
$row_rds_asociaciones = mysql_fetch_assoc($rds_asociaciones);
$totalRows_rds_asociaciones = mysql_num_rows($rds_asociaciones);

mysql_select_db($database_datos, $datos);
$query_rds_departamentos = "SELECT * FROM departamentos ORDER BY nombre ASC";
$rds_departamentos = mysql_query($query_rds_departamentos, $datos) or die(mysql_error());
$row_rds_departamentos = mysql_fetch_assoc($rds_departamentos);
$totalRows_rds_departamentos = mysql_num_rows($rds_departamentos);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Consejo Profesional de Gu�as de Turismo </title>
<script type="text/JavaScript">
<!--
function MM_callJS(jsStr) { //v2.0
  return eval(jsStr)
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.name; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
      } else if (test!='R') { num = parseFloat(val);
        if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' es requeridos.\n'; }
  } if (errors) alert('Hay los siguientes errores:\n'+errors);
  document.MM_returnValue = (errors == '');
}

function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}
//-->
</script>
</head>

<body onload="MM_callJS('focus();')">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td colspan="2" bgcolor="#ECF2FF"><div align="left"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Editar Asociaciones </strong></font></div></td>
  </tr>
  <tr>
    <td width="8%">&nbsp;</td>
    <td width="92%"><form action="<?php echo $editFormAction; ?>" method="POST" name="form1" id="form1"  >
      <table width="100%" border="0" cellpadding="6" cellspacing="0" class="tablatexto">
        <tr>
          <td width="6%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Nombre</font></td>
          <td width="94%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">
            <input name="nombre" type="text" id="nombre" value="<?php echo $row_rds_asociaciones['nombre']; ?>" size="60" />
            <input name="id" type="hidden" id="id" value="<?php echo $row_rds_asociaciones['id']; ?>" />
          </font></td>
        </tr>
        <tr>
          <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Direcci&oacute;n</font></td>
          <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif">
            <input name="direccion" type="text" id="direccion" value="<?php echo $row_rds_asociaciones['direccion']; ?>" size="60" />
          </font></td>
        </tr>
        <tr>
          <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Tel&eacute;fono</font></td>
          <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif">
            <input name="telefono" type="text" id="telefono" value="<?php echo $row_rds_asociaciones['telefono']; ?>" size="60" />
          </font></td>
        </tr>
        <tr>
          <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Ciudad</font></td>
          <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif">
            <input name="ciudad" type="text" id="ciudad" value="<?php echo $row_rds_asociaciones['ciudad']; ?>" size="60" />
          </font></td>
        </tr>
        <tr>
          <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Departamento</font></td>
          <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif">
            <select name="id_dep" id="id_dep">
              <?php
do {  
?>
              <option value="<?php echo $row_rds_departamentos['id']?>"<?php if (!(strcmp($row_rds_departamentos['id'], $row_rds_asociaciones['id_dep']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rds_departamentos['nombre']?></option>
              <?php
} while ($row_rds_departamentos = mysql_fetch_assoc($rds_departamentos));
  $rows = mysql_num_rows($rds_departamentos);
  if($rows > 0) {
      mysql_data_seek($rds_departamentos, 0);
	  $row_rds_departamentos = mysql_fetch_assoc($rds_departamentos);
  }
?>
              </select>
          </font></td>
        </tr>
        <tr>
          <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Contacto</font></td>
          <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif">
            <input name="contacto" type="text" id="contacto" value="<?php echo $row_rds_asociaciones['contacto']; ?>" size="60" />
          </font></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><input name="Submit" type="submit" onclick="MM_validateForm('nombre','','R');return document.MM_returnValue" value="Guardar" /> &nbsp;&nbsp;&nbsp;&nbsp;
            <input name="Submit2" type="button" onclick="MM_goToURL('parent','index_asociaciones.php');return document.MM_returnValue" value="Cancelar" /></td>
        </tr>
      </table>
        
     
      <input type="hidden" name="MM_update" value="form1">
    </form>    </td>
  </tr>
</table>
<blockquote>&nbsp;</blockquote>
</body>
</html>
<?php
mysql_free_result($rds_asociaciones);

mysql_free_result($rds_departamentos);
?>
