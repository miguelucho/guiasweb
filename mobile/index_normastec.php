<?php require_once('Connections/datos.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {

   $archivo = $_FILES["archivo"]['name'];
$archivo = str_replace(" ","_",$archivo);


  $insertSQL = sprintf("INSERT INTO normastecnicas (titulo, titulo_eng, descriptores, fecha, archivo, NTS) VALUES (%s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['titulo'], "text"),
                       GetSQLValueString($_POST['titulo_eng'], "text"),
                       GetSQLValueString($_POST['descriptores'], "text"),
                       GetSQLValueString($_POST['fecha'], "date"),
                       GetSQLValueString($archivo, "text"),
                       GetSQLValueString($_POST['NTS'], "text"));

  mysql_select_db($database_datos, $datos);
  $Result1 = mysql_query($insertSQL, $datos) or die(mysql_error());
  
  
   // obtenemos los datos del archivo de imagen
  //  $archivo = $_FILES["a"]['name'];
         
    if ($archivo != "") {
         
        $destino =  "documentos/normas/".$archivo;
        if (copy($_FILES['archivo']['tmp_name'],$destino)) {
            $status = "Archivo subido: <b>".$archivo."</b>";
        } else {
            $status = "Error al subir el archivo";
        }
    }  
  
  
}



mysql_select_db($database_datos, $datos);
$query_rds_normas = "SELECT * from normastecnicas ORDER BY titulo ASC";
$rds_normas = mysql_query($query_rds_normas, $datos) or die(mysql_error());
$row_rds_normas = mysql_fetch_assoc($rds_normas);
$totalRows_rds_normas = mysql_num_rows($rds_normas);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Consejo Profesional de Gu�as de Turismo </title>
<script type="text/JavaScript">
<!--
function MM_callJS(jsStr) { //v2.0
  return eval(jsStr)
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.name; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
      } else if (test!='R') { num = parseFloat(val);
        if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' es requeridos.\n'; }
  } if (errors) alert('Hay los siguientes errores:\n'+errors);
  document.MM_returnValue = (errors == '');
}

function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}
//-->
</script>
</head>

<body onload="MM_callJS('focus();')">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td colspan="2" bgcolor="#ECF2FF"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Editar Normas T&eacute;cnicas </strong></font></td>
    <td width="5%" bgcolor="#ECF2FF"><div align="right"><a href="cerrar.php"><img src="imagenes/iconos/cerrar.gif" width="17" height="14" border="0" /></a></div>
    <div align="right"></div></td>
  </tr>
  <tr>
    <td width="8%"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;</font></td>
    <td colspan="2"><table border="0" cellspacing="0" cellpadding="2">
      <tr>
        <td bgcolor="#333333">&nbsp;</td>
        <td bgcolor="#333333"><font color="#FFFFFF" size="1" face="Verdana, Arial, Helvetica, sans-serif">NTS</font></td>
        <td bgcolor="#333333"><div align="center"><font color="#FFFFFF" size="1"><strong><font face="Verdana, Arial, Helvetica, sans-serif">T&iacute;tulo</font></strong></font></div></td>
        <td bgcolor="#333333"><div align="center"><strong><font color="#FFFFFF" size="1" face="Verdana, Arial, Helvetica, sans-serif">T&iacute;tulo ingl&eacute;s </font></strong></div></td>
        <td bgcolor="#333333"><div align="center"><font color="#FFFFFF" size="1" face="Verdana, Arial, Helvetica, sans-serif">Descriptores</font></div></td>
        <td bgcolor="#333333"><div align="center"><font color="#FFFFFF" size="1"><strong><font face="Verdana, Arial, Helvetica, sans-serif">Fecha</font></strong></font></div></td>
        <td bgcolor="#333333"><div align="center"><font size="1"><strong><font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif">Archivo</font></strong></font></div></td>
        <td bgcolor="#333333"><div align="center"><font color="#FFFFFF"></font></div></td>
        <td bgcolor="#333333"><div align="center"><font color="#FFFFFF"></font></div></td>
      </tr><?php $al=0; ?>
      <?php do { ?>
	  
        <tr  <?php if($a1 == 1) { ?>bgcolor="#EEECFF" <?php $a1=0; } else { $a1=$a1+1; } ?>>
          <td><font size="1"><?php echo $row_rds_normas['id']; ?></font></td>
          <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><?php echo $row_rds_normas['NTS']; ?></font></td>
          <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><?php echo $row_rds_normas['titulo']; ?></font></td>
          <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><?php echo $row_rds_normas['titulo_eng']; ?></font></td>
          <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><?php echo $row_rds_normas['descriptores']; ?></font></td>
          <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><?php echo $row_rds_normas['fecha']; ?></font></td>
          <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><?php echo $row_rds_normas['archivo']; ?></font></td>
          <td><a href="normastec_editar.php?id=<?php echo $row_rds_normas['id']; ?>"><img src="imagenes/iconos/editor.png" width="12" height="12" border="0" /></a></td>
          <td><a href="javascript:;" onclick="if(confirm('Est� seguro de eliminar el registro de <?php echo $row_rds_normas['titulo']; ?> ?')){MM_goToURL('parent','normastec_del.php?id= <?php echo $row_rds_normas['id']; ?>');return document.MM_returnValue}"><img src="imagenes/iconos/user-trash.png" width="12" height="12" border="0" /></a></td>
        </tr>
        <?php } while ($row_rds_normas = mysql_fetch_assoc($rds_normas)); ?>
      <tr>
        <td colspan="4" bgcolor="#333333"><em><font color="#FFFFFF" size="1" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;<?php echo $totalRows_rds_normas ?> registros.</font></em> </td>
        <td bgcolor="#333333">&nbsp;</td>
        <td bgcolor="#333333"><font color="#FFFFFF">&nbsp;</font></td>
        <td bgcolor="#333333">&nbsp;</td>
        <td bgcolor="#333333"><font color="#FFFFFF">&nbsp;</font></td>
        <td bgcolor="#333333"><font color="#FFFFFF">&nbsp;</font></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="3" bgcolor="#ECF2FF"><div align="left"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Agregar Norma t&eacute;cnica </strong></font></div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2"><form action="<?php echo $editFormAction; ?>" method="POST" enctype="multipart/form-data" name="form1" id="form1"  >
      <table width="100%" border="0" cellpadding="6" cellspacing="0" class="tablatexto">
        <tr>
          <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif">NTS</font></td>
          <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif">
            <input name="NTS" type="text" id="NTS" size="60" />
          </font></td>
        </tr>
        <tr>
          <td width="6%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">T&iacute;tulo</font></td>
          <td width="94%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">
            <input name="titulo" type="text" id="titulo" size="60" />
          </font></td>
        </tr>
        <tr>
          <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif">T&iacute;tulo ingl&eacute;s </font></td>
          <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif">
            <input name="titulo_eng" type="text" id="titulo_eng" size="60" />
          </font></td>
        </tr>
        <tr>
          <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Fecha</font></td>
          <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif">
            <input name="fecha" type="text" id="fecha" size="10" maxlength="10" />
          (AAAA-MM-DD)</font></td>
        </tr>
        
        <tr>
          <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Archivo </font></td>
          <td><input name="archivo" type="file" id="archivo" />
            <font size="1" face="Verdana, Arial, Helvetica, sans-serif"> (renombrar los archivos sin espacios, tildes o e&ntilde;es. Utilizar solo min&uacute;sculas) </font></td>
        </tr>
        
        <tr>
          <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Descriptores </font></td>
          <td><font size="1" face="Verdana, Arial, Helvetica, sans-serif">
            <input name="descriptores" type="text" id="descriptores" size="120" />
          </font></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><input name="Submit" type="submit" onclick="MM_validateForm('NTS','','R','titulo','','R','titulo_eng','','R','descriptores','','R','archivo','','R');return document.MM_returnValue" value="Guardar" /></td>
        </tr>
      </table>
        <input type="hidden" name="MM_insert" value="form1">
     
    </form>    </td>
  </tr>
</table>
<blockquote>&nbsp;</blockquote>
</body>
</html>
<?php
mysql_free_result($rds_normas);
?>
