<?php
	require_once 'libs/conexion.php';

	$ls_departamentos = '';
	$departamentos = $db
		->where('paise_id',1)
		->objectBuilder()->get('departamentos');

	foreach ($departamentos as $departamento) {
		$ls_departamentos .= '<option value="'.$departamento->id.'">'.$departamento->departamento.'</option>';
	}

	$ls_ciudades = '';
	$ciudades = $db
		->orderBy('ciudad','ASC')
		->objectBuilder()->get('ciudades');

	foreach ($ciudades as $ciudad) {
		$ls_ciudades .= '<option value="'.$ciudad->id.'" class="dp-'.$ciudad->departamento_id .'" style="display:none">'.$ciudad->ciudad .'</option>';
	}


	$ls_idiomas = '';
	$idiomas = $db
		->objectBuilder()->get('idiomas');

	foreach ($idiomas as $idioma) {
		$ls_idiomas .= '<option value="'.$idioma->id.'">'.$idioma->idioma.'</option>';
	}

	$ls_servicios = '';
	$servicios = $db
		->objectBuilder()->get('servicios');

	foreach ($servicios as $servicio) {
		$ls_servicios .= '<option value="'.$servicio->id.'">'.$servicio->nombre.'</option>';
	}

	$ls_especialidades = '';
	$especialidades = $db
		->objectBuilder()->get('especialidades');

	foreach ($especialidades as $especialidad) {
		$ls_especialidades .= '<option value="'.$especialidad->id.'" >'.$especialidad->nombre.'</option>';
	}
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-44406258-20"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-44406258-20');
		</script>

		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
		<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" >
		<meta name="keywords" lang="es" content="">
		<meta name="robots" content="All">
		<meta name="description" lang="es" content="">
		<title>Contactar Guiás de Turismo Colombia</title>
		<meta http-equiv="Cache-control" content="public">
		<link rel="stylesheet" href="css/slider.css" />
		<link rel="stylesheet" href="css/stylesheet.css" />
		<link rel="stylesheet" href="css/load.css" />
		<link rel="stylesheet" href="css/paginacion.css" />
		<link rel="stylesheet" href="js/flatpickr/dist/flatpickr.min.css" />
		<link rel="stylesheet" href="css/multiple-select.css" />
		<link rel="stylesheet" href="css/stylesheetnew.css" />
		<link rel="stylesheet" href="css/jquery.modal.css" />
		<link rel="stylesheet" href="css/jquery.remodal.css" />
	</head>
	<body>
		<header>
			<?php include "new-header-top.php";?>
		</header>
		<section>
			<div class="Contener-guias">
				<div class="Contener-guias-int">
					<div class="Contener-guias-int-izq Oculto">
						<div class="Form-filtro-guia">
							<img src="images/guias1.png">
							<form class="Cont-filtrar-guia">
								<label>Departamento</label>
								<select class="departamento" required>
									<option value="">Seleccione</option>
									<?php echo $ls_departamentos ?>
								</select>
								<label>Ciudad</label>
								<select class="ciudad" required>
									<option value="">Seleccione</option>
									<?php echo $ls_ciudades ?>
								</select>
								<label>Idiomas</label>
								<select class="idioma multiple" multiple="multiple">
									<!-- <option value="">Seleccionar idioma</option> -->
									<?php echo $ls_idiomas ?>
								</select>
								<label>Servicios</label>
								<select class="servicio multiple" multiple="multiple">
									<!-- <option value="">Seleccionar servicios</option> -->
									<?php echo $ls_servicios ?>
								</select>
								<label>Especialidades</label>
								<select class="especialidad multiple" multiple="multiple">
									<!-- <option value="">Seleccionar especialidades</option> -->
									<?php echo $ls_especialidades ?>
								</select>
								<label>Fecha de disponibilidad</label>
								<input type="date" class="fecha" placeholder="Fecha de disponibilidad" required>
								<p><a href="javascript://" class="Btn-azul reset-busqueda">Reiniciar Búsqueda</a></p>
								<p><a href="javascript://" class="Btn-azul submit-busqueda">Buscar</a></p>
							</form>
						</div>
					</div>
					<div class="Contener-guias-int-der">
						<br>
						<div class="Contener-guias-int-der-contexto">
							<div class="Form-busca nombre Forms">
								<form id="buscarnom">
									<label>Buscar por Nombres o Apellidos</label>
									<input type="text" placeholder="Ingresar texto" id="nombrebus">
									<input type="submit" class="Btn-azul submit-nombre-busqueda" value="Buscar" style="width: auto;" name="">
								</form>
							</div>
							<div class="Contener-guias-int-der-contexto">
								<div class="load">
									<img src="images/load.gif" width="50px">
								</div>
								<div style="display: block;" id="ls-guias">
								</div>
								<div class="Listar-paginacion">
									<div id="paginacion">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<div class="remodal" data-remodal-id="mod_contactar">
			<div class="sol-modal">
				<div class="Login-int">
					<p>Seleccione el Departamento y fecha de disponibilidad</p>
					<form id="ck-contactar">
						<label>Departamento</label>
						<select class="departamento-md" required>
							<option value="">Seleccione</option>
							<?php echo $ls_departamentos?>
						</select>
						<label>Ciudad</label>
						<select class="ciudad-md" required>
							<option value="">Seleccione</option>
							<?php echo $ls_ciudades ?>
						</select>
                        <label>Fecha de disponibilidad</label>
                        <input type="date" class="fecha fecha-md" placeholder="Fecha de disponibilidad" required>
                        <input type="submit" class="Btn-azul" value="Contactar">
					</form>
				</div>
			</div>
		</div>
		<footer>
			<?php include "new-footer.php";?>
		</footer>
		<script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
		<script src="js/flatpickr/dist/flatpickr.min.js" type="text/javascript"></script>
		<script src="js/flatpickr/dist/l10n/es.js"></script>
		<script src="js/multiple-select.js" type="text/javascript"></script>
		<script src="js/jquery.modal.min.js"></script>
		<script src="js/jquery.remodal.js"></script>
		<script src="js/busquedaguia.js"></script>
		<script type="text/javascript">
			$(function(){
				$('#Drop').bind('click',function() {
				$('.Top-inf').toggleClass('Top-inf-apa');
			});
			});
		</script>
	</body>
</html>