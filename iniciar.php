<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
		<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" >
		<meta name="keywords" lang="es" content="">
		<meta name="robots" content="All">
		<meta name="description" lang="es" content="">
		<title>Contactar Guiás de Turismo Colombia</title>
		<meta http-equiv="Cache-control" content="public">
		<!-- <meta name="google-signin-client_id" content="308794357018-vkd467nppss3m7uogfajafhqle430rct.apps.googleusercontent.com"> -->
		<!-- <link rel="stylesheet" href="contactarguia/css/slider.css" />
		<link rel="stylesheet" href="contactarguia/css/stylesheet.css" /> -->
		<link rel="stylesheet" href="css/slider.css" />
		<link rel="stylesheet" href="css/stylesheet.css" />
		<link rel="stylesheet" href="css/load.css" />
		<link rel="stylesheet" href="css/jquery.modal.css" />
		<link rel="stylesheet" href="css/stylesheetnew.css" />
	</head>
	<body>
		<header>
			<?php include "new-header-top.php";?>
		</header>
		<section>
			<section>
				<div class="Contener">
					<div class="Contener-int">
						<div class="Contenedor-desc-int">
							<section class="nav_tabs">
								<div class="nav_tabs_back Vin-anc">
									<nav>
										<ul class="tabs">
											<li id="login-turista"><a href="#" class="Vin  Vin-active">SOY TURISTA</a></li>
											<li id="login-guia"><a href="#" class="Vin ">SOY GUIA DE TURISMO</a></li>
											<li id="renovar-guia"><a href="#" class="Vin">ACTUALIZAR DATOS GUIA</a></li>
										</ul>
									</nav>
								</div>
							</section>
						</div>
						<div class="Contener-int-contenido">
							<div class="sec-login-turista">
								<div class="Login-int">
									<h2>Iniciar Sesión Turista</h2>
									<p>Si ya realizo el registro como Turista, ingrese los datos de acceso.</p>
									<form id="login-turista">
										<label>Correo Electrónico</label>
										<input type="text" placeholder="Usuario" name="log[turcorreo]" required>
										<label>Contraseña</label>
										<input type="password" placeholder="Contraseña" name="log[turcontrsena]" required>
										<label>Verifica que la información sea correcta</label>
										<input type="submit" class="Btn-azul" value="Entrar" name="">
										<!-- <div class="g-signin2 googlein" ></div> -->
										<p></p>
										<!-- <div id="gSignIn"></div> -->
										<div id="gsingbtn" class="customGPlusSignIn">
									      <span class="icon"></span>
									      <span class="buttonText">Google</span>
									    </div>
										<p><a href="recuperar-turista" class="Contra">Obtenga o Recuerde su contraseña.</a></p>
									</form>
								</div>
							</div>
							<div class="sec-login-guia" style="display: none">
								<div class="Login-int">
									<h2>Iniciar Sesión Guía</h2>
									<p>Ingrese los datos de acceso o de click en "Recuerde su contraseña", el sistema enviará un correo electrónico con sus nuevos datos de acceso.</p>
									<form id="login-guia">
										<label>Correo Electrónico</label>
										<input type="email" placeholder="Usuario" name="log[lgcorreo]" required">
										<label>Contraseña</label>
										<input type="password" placeholder="Contraseña" name="log[lgcontrasena]" required>
										<label>Verifica que la información sea correcta</label>
										<input type="submit" class="Btn-azul" value="Entrar" name="">
										<p><a href="recuperar-guia" class="Contra">Obtenga o Recuerde su contraseña.</a></p>
									</form>
								</div>
							</div>
							<div class="sec-renovar-guia" style="display: none">
								<div class="Login-int renovar-guia-p1">
									<h2>Iniciar Sesión</h2>
									<p>Ingrese su RNT y de click en validar para actualizar sus datos de acceso.</p>
									<form id="validar-guia">
										<label>RNT</label>
										<input type="text" placeholder="RNT" name="log[rnt]" required>
										<input type="submit" class="Btn-azul" value="Validar" name="">
									</form>
								</div>
								<div class="renovar-guia-p2" style="display: none">
									<div class="Login-int">
										<h2>Renovación de ingreso</h2>
										<p>Ingrese los datos de acceso</p>
										<form id="renovar-datos">
											<label>Identificación</label>
							                <select name="log[tipoid]" required>
							                    <option value="1">C&eacute;dula de ciudadan&iacute;a</option>
							                    <option value="2">C&eacute;dula Extranjeria</option>
							                    <option value="3">Tarjeta de identidad</option>
							                </select>
											<label>Número:</label>
                  							<input name="log[numeroid]" type="text" size="12" required/>
                  							<label>Número de Celular:</label>
                  							<input name="log[telefono]" type="text" minlength="10" maxlength="10"  required>
											<label>Correo Electrónico</label>
											<input type="email" placeholder="Correo Electrónico" name="log[rncorreo]" required>
											<label>Contraseña</label>
											<input type="password" placeholder="Contraseña" name="log[rncontrasena]" required>
											<label>Verifica que la información sea correcta</label>
											<input type="submit" class="Btn-azul" value="Entrar" name="">
										</form>
									</div>
									<p><a href="javascript://" class="Btn-atras volver-renovar-p1">Volver</a></p>
								</div>
							</div>
							<br>
							<!-- <p class="Center">Si eres Guía de Turismo, tu nombre de USUARIO se compone de la siguiente manera:<br>
								Iniciales de los NOMBRES: Ejemplo: <span class="Rojo-strong">J</span>uan <span class="Rojo-strong">D</span>arío (jd)
								Primer apellido completo: Ejemplo: <span class="Rojo-strong">Martinez</span> (martinez)
								Iniciales del segundo APELLIDO Ejemplo: <span class="Rojo-strong">A</span>ngarita (a)
								El USUARIO para el Guía de Turismo JUAN DARIO MARTINEZ ANGARITA será, (todo en letras minúsculas)<br>
								<span class="Rojo-strong">jdmartineza</span>
								En la casilla de CONTRASEÑA deberás escribir la clave que se te enviará automáticamente a tu correo electrónico personal. Si nunca has registrado algún correo electrónico PERSONAL, será necesario informarlo a la coordinación del Consejo Profesional de Guías de Turismo, a cualquiera de los siguientes correos electrónicos:
							amoreno@guiasdeturismodecolombia.com.co - 					guiaturismo@mincit.gov.co</p> -->
						</div>
						<div class="Conten-botones-ff">
							<p>
								<a href="contactar" class="Btn-atras">Atrás</a>
							</p>
						</div>
					</div>
				</div>
			</section>
		</section>
		<footer>
			<?php include "new-footer.php";?>
		</footer>
		<script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
		<!-- <script src="https://apis.google.com/js/platform.js" async defer></script> -->
		<script src="https://apis.google.com/js/api:client.js"></script>
		<script>
			$(function(){
				$('#Drop').bind('click',function() {
					$('.Top-inf').toggleClass('Top-inf-apa');
				});
			});
		</script>
		<script src="js/jquery.modal.min.js"></script>
		<script src="js/login.js" type="text/javascript"></script>
	</body>
</html>