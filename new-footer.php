<div class="Descargas">
	<div class="Descargas-int">
		<h2>Descarga nuestra aplicación</h2>
		<p>Instala nuestra aplicación en tu celular y podrás contactar los Guiás Oficiales de Turismo de Colombia.</p>
		<p>
			<a href="#">
				<img src="images/icon-ios.jpg">
			</a>
			<a href="https://play.google.com/store/apps/details?id=co.mobilecorp.guiasdeturismodecolombia" target="_blank">
				<img src="images/icon-android.jpg">
			</a>
		</p>
	</div>
</div>
<div class="Foot">

		<?php require_once 'libs/conexion.php';

		$canales = $db
			->objectBuilder()->get('canales_atencion');

			$correo = $canales[0]->correo;
			$telefono = $canales[0]->telefono;
			$horario = $canales[0]->horario;

		?>
		<div class="Foot-cont">
		<div class="Foot-int">
			<div class="Foot-int-sec">
				<img src="images/logomincit.png" class="Foot1">
			</div>
			<div class="Foot-int-sec">
				<img src="images/LogoGobiernoDeColombia.png" class="Foot3" style="width: 205px;">
			</div>
			<div class="Foot-int-sec">
				<img src="images/cpgpng.png" class="Foot2">
			</div>
			<div class="Foot-int-sec">
				<img src="images/appicon.png" class="Foot4">
			</div>
			<div class="Foot-int-sec">
				<ul>
					<li>
						<span><img src="images/recurso1.png"></span>
						<span>Teléfono: <br><?php echo $telefono ?></span>
					</li>
					<li>
						<span><img src="images/recurso2.png"></span>
						<!-- <span>amoreno@guiasdeturismodecolombia.com.co <br> Consejo Profesional</span> -->
						<span><?php echo $correo ?><br> Consejo Profesional</span>
					</li>
				</ul>
			</div>
			<div class="Foot-int-sec">
				<ul>
					<li>
						<span><img src="images/recurso4.png"></span>
						<span>Horario de atención: <br><?php echo $horario ?></span>
					</li>
					<!-- <li>
						<span><img src="images/recurso5.png"></span>
						<span>(Dirección) <br>Bogotá - Colombia</span>
					</li> -->
				</ul>
			</div>
		</div>
		</div>
	</div>