$(document).ready(function() {

	ventana_estado = $('[data-remodal-id=mod_estado]').remodal();

	$('#ck-tramite').on('submit', function(e) {
		e.preventDefault();
		data = $(this).serializeArray();
		data.push({
			name: 'estado',
			value: 'revisar'
		});

		loader();
		$.post('libs/registro-tarjeta.php', data, function(data) {
			$('#fondo').remove();
			ventana_estado.close();
			$('input').not('[type=submit]').val('');
			if (data.status == true) {
				nmensaje('success', 'Correcto', data.motivo);
			} else {
				nmensaje('error', 'Error', data.motivo);
			}
		}, 'json');
	});

	function loader() {
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">' + '<div class="loader-inner ball-beat">' + '<div></div>' + '<div></div>' + '<div></div>' + '</div>' + '</div>');
		setTimeout(function() {
			$('#fondo').fadeIn('fast');
		}, 400);
	}

	function nmensaje(tipo, titulo, mensaje) {
		modal({
			type: tipo,
			title: titulo,
			text: mensaje,
			size: 'small',
			closeClick: false,
			animate: true,
		});
	}
});