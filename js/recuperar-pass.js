$(document).ready(function() {

	$('#recuperar').on('submit', function(e) {
		e.preventDefault();
		loader();
		data = $(this).serializeArray();
		$.post('libs/recuperar-pass.php', data, function(data) {
			$('#fondo').remove();
			if (data.status == true) {
				$('input').not('[type=submit]').val('');
				nmensaje('success', 'Correcto', data.motivo);
			} else {
				nmensaje('error', 'Error', data.motivo);
			}
		}, 'json');
	});

	function loader() {
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">' + '<div class="loader-inner ball-beat">' + '<div></div>' + '<div></div>' + '<div></div>' + '</div>' + '</div>');
		setTimeout(function() {
			$('#fondo').fadeIn('fast');
		}, 400);
	}

	function nmensaje(tipo, titulo, mensaje) {
		modal({
			type: tipo,
			title: titulo,
			text: mensaje,
			size: 'small',
			closeClick: false,
			animate: true,
		});
	}
});