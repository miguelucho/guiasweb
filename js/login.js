	var googleUser = {};
	var startApp = function() {
		gapi.load('auth2', function() {
			auth2 = gapi.auth2.init({
				client_id: '934778405264-c9n6f4886fs1rl846dj1tao1ldahc069.apps.googleusercontent.com',
				cookiepolicy: 'single_host_origin',
			});
			attachSignin(document.getElementById('gsingbtn'));
		});
	};

	function attachSignin(element) {
		// console.log(element.id);
		auth2.attachClickHandler(element, {},
			function(googleUser) {
				$.post('libs/acc_login.php', {
						'log[turnombre]': googleUser.getBasicProfile().getGivenName(),
						'log[turapellido]': googleUser.getBasicProfile().getFamilyName(),
						'log[turcorreo]': googleUser.getBasicProfile().getEmail(),
						'log[turgoogle]': 1,
					},
					function(data) {
						$('#fondo').remove();
						if (data.status == true) {
							window.location.href = data.redirect;
						} else {
							nmensaje('error', 'Error', data.motivo);
						}
					}, 'json');
			},
			function(error) {
				// alert(JSON.stringify(error, undefined, 2));
			});
	}

	function nmensaje(tipo, titulo, mensaje) {
		modal({
			type: tipo,
			title: titulo,
			text: mensaje,
			size: 'small',
			closeClick: false,
			animate: true,
		});
	}

	startApp();

	$(document).ready(function() {

		$('.tabs li').on('click', function() {
			$('.tabs a').removeClass('Vin-active');
			$('a', this).addClass('Vin-active');
			section = $(this).prop('id');
			$('[class^=sec]').hide();
			$('.sec-' + section).show();
		});

		$('.volver-renovar-p1').on('click', function() {
			$('.renovar-guia-p2').hide();
			$('.renovar-guia-p1').show();
			$('input', '.renovar-guia-p2').not('[type=submit]').val('');
		});

		$('#validar-guia').on('submit', function(e) {
			e.preventDefault();
			loader();
			$('.renovar-guia-p2').hide();
			$('input', '.renovar-guia-p2').not('[type=submit]').val('');
			data = $(this).serializeArray();
			$.post('libs/acc_login.php', data, function(data) {
				$('#fondo').remove();
				if (data.status == true) {
					$('.renovar-guia-p1').hide();
					$('.renovar-guia-p2').show();
				} else {
					nmensaje('error', 'Error', data.motivo);
				}
			}, 'json');
		});

		$('body').on('submit', '#renovar-datos', function(e) {
			e.preventDefault();
			loader();
			data = $(this).serializeArray();
			$.post('libs/acc_login.php', data, function(data) {
				$('#fondo').remove();
				if (data.status == true) {
					window.location.href = data.redirect;
				} else {
					nmensaje('error', 'Error', data.motivo);
				}
			}, 'json');
		});

		$('body').on('submit', '#login-guia', function(e) {
			e.preventDefault();
			loader();
			data = $(this).serializeArray();
			$.post('libs/acc_login.php', data, function(data) {
				$('#fondo').remove();
				if (data.status == true) {
					window.location.href = data.redirect;
				} else {
					nmensaje('error', 'Error', data.motivo);
				}
			}, 'json');
		});

		$('body').on('submit', '#login-turista', function(e) {
			e.preventDefault();
			loader();
			data = $(this).serializeArray();
			$.post('libs/acc_login.php', data, function(data) {
				$('#fondo').remove();
				if (data.status == true) {
					window.location.href = data.redirect;
				} else {
					nmensaje('error', 'Error', data.motivo);
				}
			}, 'json');
		});


		/*$('body').on('click', '.googlein', function() {
			loader();
			// var googleUser = gapi.auth2.getAuthInstance().currentUser.get();
			// var profile = googleUser.getBasicProfile();
			// if (googleUser.isSignedIn()) {
			// 	console.log(true);
			// } else {
			// 	console.log(false);
			// }
			// $.post('libs/acc_login.php', {
			// 	'log[turnombre]': profile.getName(),
			// 	'log[turapellido]': profile.getFamilyName(),
			// 	'log[turcorreo]': profile.getEmail(),
			// 	'log[turgoogle]': 1,
			// }, function(data) {
			// 	$('#fondo').remove();
			// 	if (data.status == true) {
			// 		window.location.href = data.redirect;
			// 	} else {
			// 		nmensaje('error', 'Error', data.motivo);
			// 	}
			// }, 'json');


			// console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
			// console.log('Full Name: ' + profile.getName());
			// console.log('Name: ' + profile.getGivenName());
			// console.log('LastName: ' + profile.getFamilyName());
			// console.log('Image URL: ' + profile.getImageUrl());
			// console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
		});*/

		function loader() {
			$('#fondo').remove();
			$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
			$('#fondo').append('<div class="loader">' + '<div class="loader-inner ball-beat">' + '<div></div>' + '<div></div>' + '<div></div>' + '</div>' + '</div>');
			setTimeout(function() {
				$('#fondo').fadeIn('fast');
			}, 400);
		}

	});