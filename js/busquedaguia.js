$(document).ready(function() {
	$('.fecha').flatpickr({
		locale: 'es',
		enableTime: false,
		dateFormat: "Y-m-d",
	});

	$('.multiple').multipleSelect({
		selectAllText: 'Seleccionar Todos',
		allSelected: 'Todas',
		countSelected: '# de % seleccionados',
		placeholder: 'Seleccione'
	});

	ventana_contacto = $('[data-remodal-id=mod_contactar]').remodal();

	ciudad = idioma = servicio = especialidad = fecha = busnom = '';
	lista_guias(1);

	function lista_guias(pagina) {
		data = [];
		data.push({
			name: 'busqueda[pagina]',
			value: pagina
		}, {
			name: 'busqueda[ciudad]',
			value: ciudad
		}, {
			name: 'busqueda[idioma]',
			value: idioma
		}, {
			name: 'busqueda[servicio]',
			value: servicio
		}, {
			name: 'busqueda[especialidad]',
			value: especialidad
		}, {
			name: 'busqueda[fecha]',
			value: fecha
		}, {
			name: 'busqueda[nombre]',
			value: busnom
		});
		$('.load').fadeIn(0);
		$('#ls-guias').empty().fadeOut(0);
		$.getJSON('libs/busqueda.php', data, function(data) {
			$('#fondo').remove();
			$('.load').fadeOut(0);
			$('#ls-guias').html(data.listado);
			$('#paginacion').html(data.paginacion);
			$('#ls-guias').fadeIn();
		});
	}

	$('body').on('click', '.mpag', function() {
		lista_guias($(this).prop('id'));
	});

	$('.departamento').on('change', function() {
		dep = $(this).val();
		if ($(this).val() != '') {
			$.each($('.ciudad option').not(':first'), function() {
				iddep = $(this).attr('class').split('-');
				if (dep == iddep[1]) {
					$(this).show();
				} else {
					$(this).hide();
				}
			});
		} else {
			$.each($('.ciudad option').not(':first'), function() {
				$(this).hide();
			});
		}
	});

	$('.submit-busqueda').on('click', function() {
		ciudad = $('.ciudad').val();
		idioma = $('.idioma').val();
		servicio = $('.servicio').val();
		especialidad = $('.especialidad').val();
		fecha = $('.fecha').val();
		if (ciudad != '' && fecha != '') {
			loader();
			lista_guias(1);
		} else {
			nmensaje('warning', 'Error', 'Debe seleccionar la <strong>ciudad</strong> y <strong>fecha de disponibilidad</strong>.');
		}
	});

	$('.reset-busqueda').on('click', function() {
		loader();
		ciudad = idioma = servicio = especialidad = fecha = '';
		$('select').val('');
		lista_guias(1);
	});


	$('body').on('click', '.btn-contactar', function() {
		guia = $(this).prop('id').split('-');
		if (!$(this).hasClass('disabled')) {
			if (ciudad != '' && fecha != '') {
				data = [];
				data.push({
					name: 'turista[accion]',
					value: 'contactar'
				}, {
					name: 'turista[ciudad]',
					value: ciudad
				}, {
					name: 'turista[fecha]',
					value: fecha
				}, {
					name: 'turista[guia]',
					value: guia[1]
				});
				$.post('libs/contactar.php', data, function(data) {
					$('#fondo').remove();
					if (data.status == true) {
						nmensaje('success', 'Correcto', data.motivo);
					} else {
						nmensaje('error', 'Error', data.motivo);
					}
				}, 'json');
			} else {
				ventana_contacto.open();
			}
		} else {
			nmensaje('error', 'Error', 'Debe hacer el registro como Turista e iniciar sesión para realizar esta acción.');
		}
	});

	$('.departamento-md').on('change', function() {
		dep = $(this).val();
		if ($(this).val() != '') {
			$.each($('.ciudad-md option').not(':first'), function() {
				iddep = $(this).attr('class').split('-');
				if (dep == iddep[1]) {
					$(this).show();
				} else {
					$(this).hide();
				}
			});
		} else {
			$.each($('.ciudad-md option').not(':first'), function() {
				$(this).hide();
			});
		}
	});

	$('#ck-contactar').on('submit', function(e) {
		e.preventDefault();
		ciudad = $('.ciudad-md').val();
		fecha = $('.fecha-md').val();
		if (ciudad != '' && fecha != '') {
			data = [];
			data.push({
				name: 'turista[accion]',
				value: 'contactar'
			}, {
				name: 'turista[ciudad]',
				value: ciudad
			}, {
				name: 'turista[fecha]',
				value: fecha
			}, {
				name: 'turista[guia]',
				value: guia[1]
			});
			loader();
			$.post('libs/contactar.php', data, function(data) {
				ventana_contacto.close();
				$('#fondo').remove();
				if (data.status == true) {
					$('.departamento-md').val('').change();
					$('.fecha-md').val('');
					ciudad = fecha = '';
					nmensaje('success', 'Correcto', data.motivo);
				} else {
					nmensaje('error', 'Error', data.motivo);
					ciudad = idioma = servicio = especialidad = fecha = '';
				}
			}, 'json');
		} else {
			nmensaje('warning', 'Error', 'Debe seleccionar la <strong>ciudad</strong> y <strong>fecha de disponibilidad</strong>.');
		}
	});

	$('#buscarnom').on('submit', function(e) {
		e.preventDefault();
		busnom = $('#nombrebus').val();
		ciudad = idioma = servicio = especialidad = fecha = '';
		$('select').val('');
		lista_guias(1);
	});

	function loader() {
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">' + '<div class="loader-inner ball-beat">' + '<div></div>' + '<div></div>' + '<div></div>' + '</div>' + '</div>');
		setTimeout(function() {
			$('#fondo').fadeIn('fast');
		}, 400);
	}

	function nmensaje(tipo, titulo, mensaje) {
		modal({
			type: tipo,
			title: titulo,
			text: mensaje,
			size: 'small',
			closeClick: false,
			animate: true,
		});
	}
});