var googleUser = {};
var startApp = function() {
	gapi.load('auth2', function() {
		auth2 = gapi.auth2.init({
			client_id: '934778405264-c9n6f4886fs1rl846dj1tao1ldahc069.apps.googleusercontent.com',
			cookiepolicy: 'single_host_origin',
		});
		attachSignin(document.getElementById('gsingbtn'));
	});
};

function attachSignin(element) {
	// console.log(element.id);
	auth2.attachClickHandler(element, {},
		function(googleUser) {
			$.post('libs/registro.php', {
					'registro[turnombre]': googleUser.getBasicProfile().getGivenName(),
					'registro[turapellido]': googleUser.getBasicProfile().getFamilyName(),
					'registro[turcorreo]': googleUser.getBasicProfile().getEmail(),
					'registro[turgoogle]': 1,
				},
				function(data) {
					$('#fondo').remove();
					if (data.status == true) {
						window.location.href = data.redirect;
					} else {
						nmensaje('error', 'Error', data.motivo);
					}
				}, 'json');
		},
		function(error) {
			// alert(JSON.stringify(error, undefined, 2));
		});
}

function nmensaje(tipo, titulo, mensaje) {
	modal({
		type: tipo,
		title: titulo,
		text: mensaje,
		size: 'small',
		closeClick: false,
		animate: true,
	});
}

startApp();

$(document).ready(function() {

	$('#registro').on('submit', function(e) {
		e.preventDefault();
		loader();
		data = $(this).serializeArray();
		$.post('libs/registro.php', data, function(data) {
			$('#fondo').remove();
			if (data.status == true) {
				$('input').not('[type=submit], [type=checkbox]').val('');
				$('select').val('');
				nmensaje('success', 'Correcto', data.motivo);
			} else {
				nmensaje('error', 'Error', data.motivo);
			}
		}, 'json');
	});


	function loader() {
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">' + '<div class="loader-inner ball-beat">' + '<div></div>' + '<div></div>' + '<div></div>' + '</div>' + '</div>');
		setTimeout(function() {
			$('#fondo').fadeIn('fast');
		}, 400);
	}

	function nmensaje(tipo, titulo, mensaje) {
		modal({
			type: tipo,
			title: titulo,
			text: mensaje,
			size: 'small',
			closeClick: false,
			animate: true,
		});
	}
});