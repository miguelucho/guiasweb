<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" >
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">	
	<title>Contactar Guiás de Turismo Colombia</title>
	<meta http-equiv="Cache-control" content="public">
	<link rel="stylesheet" href="contactarguia/css/slider.css" />
	<link rel="stylesheet" href="contactarguia/css/stylesheet.css" />	
	<link rel="stylesheet" href="css/stylesheetnew.css" />	
</head>
<body>
	<header>
		<?php include "new-header-top.php";?>	
	</header>


<section>
		<div class="Contener">
			<div class="Contener-int">
				<div class="Contener-int-contenido">
					<h1>Términos y Condiciones del sitio</h1>
					<p><strong>Organización y responsabilidades</strong></p>
					<p>El sitio en internet del Consejo Profesional de Guías de Turismo, declara que actúa como intermediario entre los usuarios o turistas y los guías de turismo encargados de proporcionar sus servicios profesionales o cualquier otro servicio contratado a través del sitio de Internet del Consejo Profesional de Guías de Turismo. En este sentido El Consejo Profesional de Guías de Turismo se compromete a cumplir con los servicios de intermediación mencionados, con las salvedades especificadas en estas Condiciones Generales y no se responsabiliza por el incumplimiento de los guías de turismo en la ejecución de sus obligaciones, ni por imprevistos ocasionados por huelgas, condiciones climáticas, atrasos, terremotos, cuarentenas, así como por los perjuicios materiales, personales o morales que pueda sufrir el usuario. De todo ello el usurario deberá reclamar directamente ante los guías en primera instancia y en segunda instancia ante la Superintendencia de Industria y Comercio. Ante faltas a la ética por parte de un Guía de Turismo el usuario podrá presentar queja formal ante el Consejo Profesional de Guías de Turismo.</p>

					<p><strong>Exclusión de responsabilidad</strong></p>
					<p>El Consejo Profesional de Guías de Turismo no garantiza que esta aplicación funcionará de manera interrumpida o sin errores, que los defectos serán corregidos o que el sitio o el servidor están libres de virus u otros componentes peligrosos. En ningún caso El Consejo Profesional de Guías de Turismo podrá ser culpado por cualquier daño directo o indirecto, o por daños consecuenciales, incluyendo, sin limitaciones, lucro cesante, inhabilidad para usar el contenido, errores cometidos en accesos por clic del ratón, incluso si el Consejo Profesional de Guías de Turismo es advertido sobre la posibilidad de dichos daños.</p>

					<p><strong>Declaración de Confidencialidad de la información</strong></p>
					<p>El Consejo Profesional de Guías de Turismo valora a sus usuarios y está comprometido a salvaguardar su confidencialidad. En el desempeño de dicho compromiso, el Consejo ha desarrollado esta “Declaración de Confidencialidad”, que describe las políticas y prácticas del Consejo en lo que se refiere a la recolección y divulgación de información personal en su página web.</p>

					<p><strong>Identificación y contraseña de los afiliados:</strong> Al registrarse como usuario, el aplicativo le solicita elegir una identificación de usuario y una contraseña. Si Ud. pierde el control de su contraseña puede perder el control de su información personal y podría estar sujeto a transacciones legalmente válidas llevadas a cabo en su nombre. Por lo tanto, si por cualquier razón su contraseña llega a estar comprometida, Ud. debe de inmediato (1) cambiarla, modificando su información de registro que fue entregada a esta página, y (2) contactarnos.</p>

					<p><strong>Información de registro:</strong> Al registrase como usuario, el aplicativo le solicita llenar un formato de registro en el cual se especifican su nombre, dirección, país, número de teléfono, dirección de correo electrónico y otra información. Utilizamos esta información de registro para asistirnos en llevar a cabo sus reservaciones, contactarlo para servicio al cliente y recolectar estadísticas sobre el servicio prestado por nuestros guías de turismo. Podemos utilizar su dirección de correo electrónico para contactarlo en relación con nuevos servicios o para enviarle cartas circulares o mensajes electrónicos, salvo que Ud. no lo desee (ver más adelante: política de renuncia opt. out).</p>

					<p><strong>Perfil del guía de turismo:</strong> En su calidad de guía de turismo registrado, Ud. tendrá la oportunidad de suministrar al Consejo Profesional de Guías de Turismo, información relacionada con su dirección para entrega de información y/o notificaciones, datos personales, agenda de actividades, y otra información personal. Si Ud. nos suministra esa información nos ayudará a mantener su información personal actualizada y a manejar su agenda de actividades para que los usuarios puedan acceder más fácilmente a sus servicios como guía.</p>

					<p><strong>Encuestas:</strong> El Consejo Profesional de Guías de Turismo podrá llevar a cabo o presentar periódicamente encuestas en línea. La información obtenida por medio de dichas encuestas se usa para calificar al guía de turismo y escalafonar a los más destacados.</p>

					<p><strong>Divulgación a terceros:</strong> El Consejo Profesional de Guías de Turismo no vende a terceros “listados de usuarios” o nombres de miembros individuales, y no tiene intención de hacerlo en el futuro. El Consejo divulgará información personal a funcionarios de gobierno o entidades legales cuando creamos de buena fe que la Ley así lo requiere, y divulgaremos información personal, si creemos que es necesario hacerlo para protección contra interferencia de los derechos o propiedades de sus usuarios.</p>

					<p><strong>Política de renuncia (opt out):</strong> Ud. puede notificar al Consejo Profesional de Guías de Turismo en cualquier tiempo que no desea recibir cartas, circulares, promocionales o mensajes de correo electrónico y respetaremos sus deseos. El Consejo Profesional de Guías de Turismo no suministrará en ningún caso su información personal a un tercero en manera alguna que no esté autorizada bajo esta “Política de Privacidad” sin antes dar a Ud. la oportunidad de renunciar (opt out) a la cláusula de información.</p>

					<p><strong>Acceso público a la información:</strong> Al colocar información en un área pública de esta página (por ejemplo: avisos, grupos de chat, álbumes de fotografías electrónicos) esta información estará disponible a otros miembros y usuarios de la página. El Consejo Profesional de Guías de Turismo no puede controlar la utilización que hagan los miembros y usuarios de la información publicada en la página. Por favor, recuerde que cualquier información que se divulgue en estas áreas puede convertirse en información pública y se debe ejercer cuidado cuando se decida divulgar tal información.</p>

					<p><strong>Seguridad:</strong> Esta página contiene medidas de seguridad para protección contra pérdida, uso indebido, o alteración de la información, bajo control del Consejo Profesional de Guías de Turismo.</p>

					<p>Acceso, actualización y corrección de su información: Ud. puede acceder, actualizar y corregir su información de registro en cualquier tiempo por medio del vínculo “XXXX” en la página web del Consejo.</p>

					<p><strong>Para contactarnos:</strong> Si tiene preguntas sobre esta “Declaración de Confidencialidad”, nos puede contactar en la sección de “Contáctenos”.</p>

					<p><strong>Modificaciones:</strong> Cualquier modificación a la manera como el Consejo Profesional de Guías de Turismo utiliza la Información Personal será reflejada en versiones futuras de esta “Declaración de Confidencialidad”, y serán publicadas en esta página. El Consejo Profesional de Guías de Turismo solicita a los usuarios revisar periódicamente esta “Declaración de Confidencialidad”.</p>

					<p><strong>Aplicabilidad:</strong> Al navegar y/o usar esta página usted acepta expresamente cualquier utilización y divulgación de la información que Ud. entrega a la página (“información personal”) de acuerdo con las condiciones de uso del Consejo Profesional de Guías de Turismo y de esta Declaración de Confidencialidad. Esta Declaración de Confidencialidad está incorporada y sujeta a las condiciones de uso del Consejo Profesional de Guías de Turismo.</p>
				</div>
				<div class="Conten-botones-ff">
					<p>
						<a href="contactar" class="Btn-atras">Atrás</a>
					</p>
				</div>
			</div>
		</div>
	</section>
	
<footer>
<?php include "new-footer.php";?>	
</footer>
<script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
<script type="text/javascript">
	$(function(){
		$('#Drop').bind('click',function() {
		$('.Top-inf').toggleClass('Top-inf-apa');
	});
	});
</script>
</body>
</html>
