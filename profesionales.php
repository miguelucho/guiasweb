<?php require_once('Connections/datos.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$currentPage = $_SERVER["PHP_SELF"];

mysql_select_db($database_datos, $datos);
$query_rds_ciudades = "SELECT profesionales.ciudad FROM profesionales GROUP BY profesionales.ciudad order by profesionales.ciudad ";
$rds_ciudades = mysql_query($query_rds_ciudades, $datos) or die(mysql_error());
$row_rds_ciudades = mysql_fetch_assoc($rds_ciudades);
$totalRows_rds_ciudades = mysql_num_rows($rds_ciudades);

mysql_select_db($database_datos, $datos);
$query_rds_departamentos = "SELECT departamentos.id, departamentos.nombre, departamentos.codigo, Count(profesionales.id) AS CountOfid
FROM profesionales RIGHT JOIN departamentos ON profesionales.departamento = departamentos.id
GROUP BY departamentos.id, departamentos.nombre, departamentos.codigo ORDER BY nombre ASC";
$rds_departamentos = mysql_query($query_rds_departamentos, $datos) or die(mysql_error());
$row_rds_departamentos = mysql_fetch_assoc($rds_departamentos);
$totalRows_rds_departamentos = mysql_num_rows($rds_departamentos);

mysql_select_db($database_datos, $datos);
$query_rds_idiomas = "SELECT * FROM idiomas ORDER BY idioma ASC";
$rds_idiomas = mysql_query($query_rds_idiomas, $datos) or die(mysql_error());
$row_rds_idiomas = mysql_fetch_assoc($rds_idiomas);
$totalRows_rds_idiomas = mysql_num_rows($rds_idiomas);

 
$maxRows_rds_idiomasguia = 1;
$pageNum_rds_idiomasguia = 0;
if (isset($_GET['pageNum_rds_idiomasguia'])) {
  $pageNum_rds_idiomasguia = $_GET['pageNum_rds_idiomasguia'];
}
$startRow_rds_idiomasguia = $pageNum_rds_idiomasguia * $maxRows_rds_idiomasguia;

$colname_rds_idiomasguia = "-1";
if (isset($_POST['idioma'])) {
  $colname_rds_idiomasguia = (get_magic_quotes_gpc()) ? $_POST['idioma'] : addslashes($_POST['idioma']);
}
mysql_select_db($database_datos, $datos);
$query_rds_idiomasguia = sprintf("SELECT * FROM profesionales_idiomas WHERE id_idioma = %s", GetSQLValueString($colname_rds_idiomasguia, "int"));
$query_limit_rds_idiomasguia = sprintf("%s LIMIT %d, %d", $query_rds_idiomasguia, $startRow_rds_idiomasguia, $maxRows_rds_idiomasguia);
$rds_idiomasguia = mysql_query($query_limit_rds_idiomasguia, $datos) or die(mysql_error());
$row_rds_idiomasguia = mysql_fetch_assoc($rds_idiomasguia);

if (isset($_GET['totalRows_rds_idiomasguia'])) {
  $totalRows_rds_idiomasguia = $_GET['totalRows_rds_idiomasguia'];
} else {
  $all_rds_idiomasguia = mysql_query($query_rds_idiomasguia);
  $totalRows_rds_idiomasguia = mysql_num_rows($all_rds_idiomasguia);
}
$totalPages_rds_idiomasguia = ceil($totalRows_rds_idiomasguia/$maxRows_rds_idiomasguia)-1;

$maxRows_rds_profesionales = 40;
$pageNum_rds_profesionales = 0;
if (isset($_GET['pageNum_rds_profesionales'])) {
  $pageNum_rds_profesionales = $_GET['pageNum_rds_profesionales'];
}
$startRow_rds_profesionales = $pageNum_rds_profesionales * $maxRows_rds_profesionales;
$busqueda = "";
// WHERE (departamento = %s) or (ciudad like %s)  or (nombres like %s) or (apellidos like %s)

if (isset($_POST['departamento']) and ($_POST['departamento']!="0" )) {
   	$busqueda = " (departamento = ".$_POST['departamento'].") "; 	 
}
if (isset($_POST['ciudad']) and ($_POST['ciudad']!="0" )) {

   if ($busqueda!="") { $busqueda = $busqueda . " and " ; }
   $busqueda = $busqueda . " (ciudad =  '".$_POST['ciudad']."') "; 
}



 

if (isset($_POST['especialidad_his']) and ($_POST['especialidad_his']!="0" )) {

   if ($busqueda!="") { $busqueda = $busqueda . " and " ; }
   $busqueda = $busqueda . " (especialidad_his =  '".$_POST['especialidad_his']."') "; 
}

if (isset($_POST['especialidad_agr']) and ($_POST['especialidad_agr']!="0" )) {

   if ($busqueda!="") { $busqueda = $busqueda . " and " ; }
   $busqueda = $busqueda . " (especialidad_agr =  '".$_POST['especialidad_agr']."') "; 
}
if (isset($_POST['especialidad_eco']) and ($_POST['especialidad_eco']!="0" )) {

   if ($busqueda!="") { $busqueda = $busqueda . " and " ; }
   $busqueda = $busqueda . " (especialidad_eco =  '".$_POST['especialidad_eco']."') "; 
}
if (isset($_POST['especialidad_etn']) and ($_POST['especialidad_etn']!="0" )) {

   if ($busqueda!="") { $busqueda = $busqueda . " and " ; }
   $busqueda = $busqueda . " (especialidad_etn =  '".$_POST['especialidad_etn']."') "; 
}
if (isset($_POST['especialidad_dep']) and ($_POST['especialidad_dep']!="0" )) {

   if ($busqueda!="") { $busqueda = $busqueda . " and " ; }
   $busqueda = $busqueda . " (especialidad_dep =  '".$_POST['ciudad']."') "; 
}
if (isset($_POST['especialidad_ciu']) and ($_POST['especialidad_ciu']!="0" )) {

   if ($busqueda!="") { $busqueda = $busqueda . " and " ; }
   $busqueda = $busqueda . " (especialidad_ciu =  '".$_POST['especialidad_ciu']."') "; 
}
if (isset($_POST['especialidad_fer']) and ($_POST['especialidad_fer']!="0" )) {

   if ($busqueda!="") { $busqueda = $busqueda . " and " ; }
   $busqueda = $busqueda . " (especialidad_fer =  '".$_POST['especialidad_fer']."') "; 
}

if (isset($_POST['especialidad_sol']) and ($_POST['especialidad_sol']!="0" )) {

   if ($busqueda!="") { $busqueda = $busqueda . " and " ; }
   $busqueda = $busqueda . " (especialidad_sol =  '".$_POST['especialidad_sol']."') "; 
}




if (isset($_POST['nombre']) ){ //and ($_POST['nombre']!="" )
 if ($busqueda!="") { $busqueda = $busqueda . " and " ; }
     $busqueda = $busqueda  .  "  nombres like  '%".$_POST['nombre']."%'    "; 

}


if (isset($_POST['apellido']) ){ //and ($_POST['nombre']!="" )
 if ($busqueda!="") { $busqueda = $busqueda . " and " ; }
     $busqueda = $busqueda  .  "  apellidos like '%".$_POST['apellido']."%'  "; 

}


if ($busqueda!="") { $busqueda = " where " . $busqueda; }



//echo $busqueda;


//echo $busqueda;
mysql_select_db($database_datos, $datos);
$query_rds_profesionales = "SELECT profesionales.id, profesionales.auto_registro, profesionales.tarjetano, profesionales.registronacionaldeturismo, profesionales.apellidos, profesionales.nombres, profesionales.telefonos, profesionales.celular, profesionales.ciudad, profesionales.correoe,  departamentos.nombre as departamento
FROM profesionales LEFT JOIN departamentos ON profesionales.departamento = departamentos.id  ".$busqueda." order by departamento, ciudad, apellidos";
$query_limit_rds_profesionales = sprintf("%s LIMIT %d, %d", $query_rds_profesionales, $startRow_rds_profesionales, $maxRows_rds_profesionales);
$rds_profesionales = mysql_query($query_limit_rds_profesionales, $datos) or die(mysql_error());
$row_rds_profesionales = mysql_fetch_assoc($rds_profesionales);



if (isset($_GET['totalRows_rds_profesionales'])) {
  $totalRows_rds_profesionales = $_GET['totalRows_rds_profesionales'];
} else {
  $all_rds_profesionales = mysql_query($query_rds_profesionales);
  $totalRows_rds_profesionales = mysql_num_rows($all_rds_profesionales);
}
$totalPages_rds_profesionales = ceil($totalRows_rds_profesionales/$maxRows_rds_profesionales)-1;

$queryString_rds_profesionales = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_rds_profesionales") == false && 
        stristr($param, "totalRows_rds_profesionales") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_rds_profesionales = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_rds_profesionales = sprintf("&totalRows_rds_profesionales=%d%s", $totalRows_rds_profesionales, $queryString_rds_profesionales);
?>

<!DOCTYPE html>
<!-- saved from url=(0049)http://contactar.guiasdeturismodecolombia.com.co/ -->
<html style="height: 100%;"><head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>
					</title>
	<link href="http://contactar.guiasdeturismodecolombia.com.co/favicon.ico" type="image/x-icon" rel="icon"><link href="http://contactar.guiasdeturismodecolombia.com.co/favicon.ico" type="image/x-icon" rel="shortcut icon">
	<link rel="stylesheet" type="text/css" href="./contactar.guiasdeturismodecolombia.com.co_files/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="./contactar.guiasdeturismodecolombia.com.co_files/bootstrap-theme.css">
	<link rel="stylesheet" type="text/css" href="./contactar.guiasdeturismodecolombia.com.co_files/style.css">
	<link rel="stylesheet" type="text/css" href="./contactar.guiasdeturismodecolombia.com.co_files/fullcalendar.css">
	<link rel="stylesheet" type="text/css" href="./contactar.guiasdeturismodecolombia.com.co_files/jquery.bxslider.css">
<link href="./contactar.guiasdeturismodecolombia.com.co_files/css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="./contactar.guiasdeturismodecolombia.com.co_files/startbox.js"></script>
	<script type="text/javascript" src="./contactar.guiasdeturismodecolombia.com.co_files/guias.js"></script>
	<script type="text/javascript" src="./contactar.guiasdeturismodecolombia.com.co_files/jquery1.11.1.js"></script>
	<script type="text/javascript" src="./contactar.guiasdeturismodecolombia.com.co_files/bootstrap.min.js"></script>
	<script type="text/javascript" src="./contactar.guiasdeturismodecolombia.com.co_files/reservas.js"></script>
	<script type="text/javascript" src="./contactar.guiasdeturismodecolombia.com.co_files/fechas.js"></script>
	<script type="text/javascript" src="./contactar.guiasdeturismodecolombia.com.co_files/temp.js"></script>

	<script type="text/javascript" src="./contactar.guiasdeturismodecolombia.com.co_files/jquery.min.js"></script>
	<script type="text/javascript" src="./contactar.guiasdeturismodecolombia.com.co_files/moment.min.js"></script>
	<script type="text/javascript" src="./contactar.guiasdeturismodecolombia.com.co_files/fullcalendar.js"></script>
	<script type="text/javascript" src="./contactar.guiasdeturismodecolombia.com.co_files/es.js"></script>
	<script type="text/javascript" src="./contactar.guiasdeturismodecolombia.com.co_files/jquery-ui.custom.min.js"></script>

	<script type="text/javascript" src="./contactar.guiasdeturismodecolombia.com.co_files/jquery.bxslider.js"></script>
	<script type="text/javascript" src="./contactar.guiasdeturismodecolombia.com.co_files/jquery.bxslider.min.js"></script>
	<script type="text/javascript" src="./contactar.guiasdeturismodecolombia.com.co_files/tinymce.min.js"></script>
        <script type="text/javascript">
            function googleTranslateElementInit() {
                new google.translate.TranslateElement({pageLanguage: 'es', 
                    layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 
                'google_translate_element');
            }   

        </script>
        <script type="text/javascript" src="./contactar.guiasdeturismodecolombia.com.co_files/element.js">
        </script><link type="text/css" rel="stylesheet" charset="UTF-8" href="./contactar.guiasdeturismodecolombia.com.co_files/translateelement.css"><script type="text/javascript" charset="UTF-8" src="./contactar.guiasdeturismodecolombia.com.co_files/main_es.js"></script>
        <!--[if lte IE 8 ]> 
        <style>
        .container-bg{
            
        }
.btn-file{
    background: none;
}
.btn-file input[type="file"]{
    background: none;
    border: medium none;
    color: currentcolor;
    cursor: pointer;
    line-height: normal;
    opacity: inherit;
    overflow: auto;
    padding: inherit;
    text-decoration: none;
    width: auto;
}
.btn-file p{
    color:#000;
    margin: 5px 35px;
    left:auto;
    right:auto;
}
.submit{
    background: none repeat scroll 0 0 #2554A4;
    display: inline-block;
    text-align: center;
    width: 100px;
    margin: 0 0 0 15px;
    float: left;
}
.submit a{
    color: #FFFFFF !important; 
    padding: 8px 20px;
    width: 100%;
    background: rgba(0,0,0,0);
    border:0 none;
    text-decoration:none;
    line-height: normal;
    display:block;
}
.submit input[type="submit"]{
    background: none;
    border: medium none;
    color: #FFFFFF !important; 
    cursor: pointer;
    line-height: normal;
    opacity: inherit;
    overflow: auto;
    padding: inherit;
    text-decoration: none;
    width: auto;
}
.form-panel{
    border:none;
}

        </style>
        <!-- <![endif]-->        <!--[if lte IE 9 ]> 
        <style>
        .container-bg{
        }
.submit{
    background: none repeat scroll 0 0 #2554A4;
    display: inline-block;
    text-align: center;
    width: 100px;
    margin: 0 0 0 15px;
    float: left;
}
.submit a{
    color: #FFFFFF !important;
    padding: 8px 20px;
    width: 100%;
    background: rgba(0,0,0,0);
    border:0 none;
    text-decoration:none;
    line-height: normal;
    display:block;
}
.submit input[type="submit"]{
    background: none;
    border: medium none;
    color: #FFFFFF !important; 
    cursor: pointer;
    line-height: normal;
    opacity: inherit;
    overflow: auto;
    padding: inherit;
    text-decoration: none;
    width: auto;
}
.form-panel{
    border:none;
}

}
        </style>
        <!-- <![endif]-->    <meta name="google-translate-customization" content="9dc62c92819f43fe-25adf20c0852a567-g75fd6e5bab3c4efa-19">
<script type="text/javascript" charset="UTF-8" src="./contactar.guiasdeturismodecolombia.com.co_files/element_main.js"></script></head>
<body style="position: relative; min-height: 100%; top: 0px;">
	<div class="super-container">
		<div class="header">
            <div class="header-bg-yellow"></div>
            <div class="header-bg-orange"></div>
            <div class="container">
			<div class="row">
			<div class="col-md-5 col-lg-5 col-sm-12 col-xs-12 ">
                <a href="./contactar.guiasdeturismodecolombia.com.co_files/contactar.guiasdeturismodecolombia.com.co.htm" style="text-decoration:none;color:white;">
				
				</a>
             
				<div class="logocpg" style="
					z-index: 9999;  
					float: left;  position: relative;  text-align: right;  
					padding-left: 30px;
				">
					  <img src="./contactar.guiasdeturismodecolombia.com.co_files/logo.png" class="img-responsive" style="height: 94px;" alt="">    
					 </div>
           
                   </div>
                   <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12 ">
					  <form action="busqueda.php" method="post" enctype="multipart/form-data" name="buscar" class="navbar-form navbar-left"  role="search" style="padding: 0px;margin: 0px;margin-top: -10px;position: relative;z-index: 99999;float: right!important;width: 100%!important;						">
						  <div class="form-group"  id="ocultar" style="width: 100%;"> 
						  <div class="row">
                          <div class="col-md-1 col-lg-1 ">
						   <a href="javascript:;" onClick="buscar.submit();">
						   <img src="./contactar.guiasdeturismodecolombia.com.co_files/icobuscar.png" class="img-responsive" alt="" > 
						   </a>
						   </div>
						   <div class="col-md-7 col-lg-7  ">
							<input type="text" class="form-control" placeholder="Buscar" name="busq" type="text" size="24" style="width: 100%;">
							  </div>
							  <div class="col-md-4 col-lg-4 ">
							  <div style="margin: 5px 0px 0px 0px;">
							<a href="index.php" style="color: white;font-size: 18px;">Inicio</a> | <a href="contactenos.php" style="color: white;font-size: 18px;">Contactenos</a>   
</div>                            
							</div>							  
							 </div>
						  </div>
						 
						</form>
					 </div>
						
				
                <div class="header-bg visible-lg">
                    <img src="./contactar.guiasdeturismodecolombia.com.co_files/header.png" class="img-responsive" alt="">    
                </div>
				 </div>
            </div>
		</div>
		<div class="container bs-docs-container">
		<nav class="navbar navbar-inverse" role="navigation" style="				 top: -48px;				z-index: 998;				  height: 56px;				margin: 0px 30px 0px 30px;			border: 0px solid rgba(0, 0, 0, 0);				  background-image: linear-gradient(to bottom, #FF7000 0%, #BD4908 100%);			">
								  <div class="container-fluid">
									<!-- Brand and toggle get grouped for better mobile display -->
									<div class="navbar-header">
									  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-9">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									  </button>
									  <a class="navbar-brand" href="#"></a>
									</div>

									<!-- Collect the nav links, forms, and other content for toggling -->
									<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-9">
									  <ul class="nav navbar-nav">
										<li ><a href="tarjeta.php" style="font-size: 13px;">Tarjeta Profesional</a></li>
										<li><a href="eventos.php">Conozcanos</a></li>
										<li class="active"><a href="profesionales.php">Profesionales</a></li>
										<li ><a href="asociaciones.php?id=31">Asociaciones</a></li>
										<li><a href="normatividad.php">Normatividad</a></li>
										<li><a href="normastecnicas.php">Normas tecnicas</a></li>
										<li><a href="infoacedemica.php">Ofertas academicas</a></li>
										<li><a href="denuncias.php">Tramites de denuncias</a></li>
									  </ul>
									</div><!-- /.navbar-collapse -->
								  </div><!-- /.container-fluid -->
						 </nav>
						 
<div class="index-bg">
    <div class="bx-wrapper" style="max-width: 100%; top: -81px;">
	<div class="bx-viewport" style="width: 100%; overflow: hidden; position: relative; -webkit-box-shadow: 0 0 5px rgba(204, 204, 204, 0);">
	<ul class="bxslider" style="width: auto; position: relative;">
        <li style="float: none; list-style: none; position: absolute; width: 1349px; z-index: 0; display: none;">
		<img src="./contactar.guiasdeturismodecolombia.com.co_files/bannerslide0.png" class="img-responsive" alt=""></li>
		<li style="float: none; list-style: none; position: absolute; width: 1349px; z-index: 0; display: none;">
		<img src="./contactar.guiasdeturismodecolombia.com.co_files/bannerslide1.png" class="img-responsive" alt=""></li>
		    
	</ul>
</div>

<div class="bx-controls bx-has-pager bx-has-controls-direction bx-has-controls-auto">
</div>
 <img src="./contactar.guiasdeturismodecolombia.com.co_files/shaowslide.png" class="img-responsive" style="
   margin-top: -27px;" alt="">  
		</div>
		 
	
</div>
<div class="contenidopublicacion">
		 <div class="row">
			 <div class="col-lg-9 col-md-9 col-xs-12 col-sm-12">
				<div class="col-md-4 ">
					 <img src="./contactar.guiasdeturismodecolombia.com.co_files/shaowslide.png" class="img-responsive"  alt="">
				</div>
				<div class="col-md-4">
				<h1 class="titleseccion">	Profesionales</h1>
				
				</div>
				<div class="col-md-4">
					 <img src="./contactar.guiasdeturismodecolombia.com.co_files/shaowslide.png" class="img-responsive" alt="">
				</div>
				<div  class="row" id="contenidoPagina">
				<div class="col-lg-12">
<!-- inicio de contenido -->   

			    <p><img src="imagenes/bullet.gif" width="12" height="12" /> <strong><font face="Arial, Helvetica, sans-serif">Encuentre un Gu&iacute;a  de Turismo aqu&iacute;: </font> </strong>			      </p>
			    <div align="left" class="rounded">
        <table width="100%" border="0" cellspacing="0" cellpadding="6">
          <tr>
            <td><form id="form1" name="form1" method="post" action="profesionales.php">
              <table width="100%" border="0" cellspacing="0" cellpadding="4">
                
                    <tr>
                      <td colspan="2" bgcolor="#CCCCCC">Seleccione localizaci&oacute;n:</td>
                      <td width="56%" rowspan="8" align="left" valign="top"><p><em><?php
				mysql_select_db($database_datos, $datos);
$query_rds_textos = "SELECT * FROM ttextosvarios WHERE id = 4";
$rds_textos = mysql_query($query_rds_textos, $datos) or die(mysql_error());
$row_rds_textos = mysql_fetch_assoc($rds_textos);
$totalRows_rds_textos = mysql_num_rows($rds_textos);
echo urldecode($row_rds_textos['contenido']); 
 if (!isset($_SESSION)) {   session_start(); } 
 if ((isset($_SESSION['MM_UserGroup'])) and  ($_SESSION['MM_UserGroup']=="1")) { ?>
                       <br><a href="javascript:;" onclick="window.open('editarv2.php?id=4&ec=2','Editor','scrollbars=yes,resizable=yes,width=800,height=600')">
                          <img src="imagenes/iconos/editor.png" alt="editar" width="32" height="32" border="0" /></a> <?php } ?></em></p>
                        <p><em><font color="#333333"> </font><font color="#333333"> <a href="profesionales_login.php">Por favor ingrese <strong>AQUI</strong> y actualice sus datos.</a> .  </font><br>
                              <br>
                              
                        </em></p>  <?php if (!isset($_SESSION)) {   session_start(); } if ((isset($_SESSION['MM_UserGroup'])) and  ($_SESSION['MM_UserGroup']=="1")) { ?> <a href="javascript:;" onclick="window.open('index_profesionales.php','Editor','scrollbars=yes,resizable=yes,width=800,height=600')"><img src="imagenes/iconos/admin-bolet.png" width="38" height="34" border="0" /></a><?php } ?></td>
                    </tr>
                    <tr>
                  <td width="13%">Departamento:</td>
                  <td width="31%"><select name="departamento" id="departamento">
                    <option value="0">Seleccione...</option>
                    <?php
do {  
?>
                    <option value="<?php echo $row_rds_departamentos['id']?>"><?php echo $row_rds_departamentos['nombre']?> (<?php echo $row_rds_departamentos['CountOfid']?>)</option>
                    <?php
} while ($row_rds_departamentos = mysql_fetch_assoc($rds_departamentos));
  $rows = mysql_num_rows($rds_departamentos);
  if($rows > 0) {
      mysql_data_seek($rds_departamentos, 0);
	  $row_rds_departamentos = mysql_fetch_assoc($rds_departamentos);
  }
?>
                                    </select></td>
                  </tr>
                <tr>
                  <td>Ciudad:</td>
                  <td><select name="ciudad" id="ciudad">
                    <option value="0">Seleccione...</option>
                    <?php
do {  
?>
                    <option value="<?php echo $row_rds_ciudades['ciudad']?>"><?php echo $row_rds_ciudades['ciudad']?></option>
                    <?php
} while ($row_rds_ciudades = mysql_fetch_assoc($rds_ciudades));
  $rows = mysql_num_rows($rds_ciudades);
  if($rows > 0) {
      mysql_data_seek($rds_ciudades, 0);
	  $row_rds_ciudades = mysql_fetch_assoc($rds_ciudades);
  }
?>
                                    </select></td>
                  </tr>
                <tr>
                  <td colspan="2" bgcolor="#CCCCCC">Buscar por nombre / apellido: </td>
                  </tr>
                <tr>
                  <td colspan="2"><p>Nombre(s) 
                      <input name="nombre" type="text" id="nombre" size="40" />
                    <br />
                    Apellido(s)
                      <input name="apellido" type="text" id="apellido" size="40" />
</p>
                    </td>
                  </tr>
                <tr>
                  <td colspan="2" bgcolor="#CCCCCC">Filtrar por especialidades: </td>
                  </tr>
                <tr>
                  <td colspan="2"><input <?php if (!(strcmp($row_rds_profesionales['especialidad_sol'],1))) {echo "checked=\"checked\"";} ?> name="especialidad_sol" type="checkbox" id="especialidad_sol" value="1" />
Sol y Playa<br />
<input <?php if (!(strcmp($row_rds_profesionales['especialidad_his'],1))) {echo "checked=\"checked\"";} ?> name="especialidad_his" type="checkbox" id="especialidad_his" value="1" />
Historia y cultura<br />
<input <?php if (!(strcmp($row_rds_profesionales['especialidad_agr'],1))) {echo "checked=\"checked\"";} ?> name="especialidad_agr" type="checkbox" id="especialidad_agr" value="1" />
Agroturismo<br />
<input <?php if (!(strcmp($row_rds_profesionales['especialidad_eco'],1))) {echo "checked=\"checked\"";} ?> name="especialidad_eco" type="checkbox" id="especialidad_eco" value="1" />
Ecoturismo<br />
<input <?php if (!(strcmp($row_rds_profesionales['especialidad_etn'],1))) {echo "checked=\"checked\"";} ?> name="especialidad_etn" type="checkbox" id="especialidad_etn" value="1" />
Etnoturismo<br />
<input <?php if (!(strcmp($row_rds_profesionales['especialidad_dep'],1))) {echo "checked=\"checked\"";} ?> name="especialidad_dep" type="checkbox" id="especialidad_dep" value="1" />
Deportes y aventura<br />
<input <?php if (!(strcmp($row_rds_profesionales['especialidad_ciu'],1))) {echo "checked=\"checked\"";} ?> name="especialidad_ciu" type="checkbox" id="especialidad_ciu" value="1" />
Ciudades capitales<br />
<input <?php if (!(strcmp($row_rds_profesionales['especialidad_fer'],1))) {echo "checked=\"checked\"";} ?> name="especialidad_fer" type="checkbox" id="especialidad_fer" value="1" />
Ferias y fiestas<br /></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td><input type="submit" name="Submit" value="Buscar" /></td>
                  </tr>
              </table>
                        </form>            </td>
          </tr>
        </table>
      </div>
			  
			  
			  
	 <br />
	 <?php // ------------------------------------------------- Fin -Contenido -------------------------------------------?>
	 <?php if ($totalRows_rds_profesionales > 0) 
		  { ?>
          <tr>
            <td valign="top">  <div align="left" class="rounded">&nbsp;
              <table width="100%" border="0" cellspacing="0" cellpadding="6">
                <tr>
                  <td bgcolor="#CCCCCC"><center>
                    Tarjeta Profesional 
                  </center></td>
                  <td bgcolor="#CCCCCC"><center>
                    Nombres
                  </center></td>
                  <td bgcolor="#CCCCCC"><div align="center"></div></td>
                  <td bgcolor="#CCCCCC"><center>
                    Ciudad
                  </center></td>
                  <td bgcolor="#CCCCCC"><center>
                    Departamento
                  </center></td>
                  <td>&nbsp;</td>
                </tr>
                <?php do { 
				
				
				$colname_rds_idiomasguia = "-1";
if (isset($_POST['idioma'])) {
  $colname_rds_idiomasguia = (get_magic_quotes_gpc()) ? $_POST['idioma'] : addslashes($_POST['idioma']);
}
mysql_select_db($database_datos, $datos);
$query_rds_idiomasguia = sprintf("SELECT * FROM profesionales_idiomas WHERE id_idioma = %s and id_guia = %s ", GetSQLValueString($colname_rds_idiomasguia, "int"),  GetSQLValueString($row_rds_profesionales['id'], "int"));
$rds_idiomasguia = mysql_query($query_rds_idiomasguia, $datos) or die(mysql_error());
$row_rds_idiomasguia = mysql_fetch_assoc($rds_idiomasguia);
$totalRows_rds_idiomasguia = mysql_num_rows($rds_idiomasguia);
				
				?>
                  <tr>
                      <td><div align="center"><font size="1"><?php echo $row_rds_profesionales['tarjetano']; ?></font></div></td>
                    <td><font size="1"><?php if ($row_rds_profesionales['auto_registro']==1) { ?><a href="profesionales_detalle.php?id=<?php echo $row_rds_profesionales['id']; ?>"><?php } ?><?php echo $row_rds_profesionales['nombres']; ?> <?php echo $row_rds_profesionales['apellidos']; ?></a></font></td>
                    <td>&nbsp;</td>
                    <td><font size="1"><?php echo $row_rds_profesionales['ciudad']; ?></font></td>
                    <td><font size="1"><?php echo $row_rds_profesionales['departamento']; ?></font></td>
                    <td>&nbsp;</td>
                  </tr>   <?php } while ($row_rds_profesionales = mysql_fetch_assoc($rds_profesionales)); ?>
                  <tr>
                    <td colspan="5">&nbsp;
                      <table border="0" width="50%" align="center">
                        <tr>
                          <td width="23%" align="center"><?php if ($pageNum_rds_profesionales > 0) { // Show if not first page ?>
                                <a href="<?php printf("%s?pageNum_rds_profesionales=%d%s", $currentPage, 0, $queryString_rds_profesionales); ?>"><img src="First.gif" border=0></a>
                                <?php } // Show if not first page ?>
                          </td>
                          <td width="31%" align="center"><?php if ($pageNum_rds_profesionales > 0) { // Show if not first page ?>
                                <a href="<?php printf("%s?pageNum_rds_profesionales=%d%s", $currentPage, max(0, $pageNum_rds_profesionales - 1), $queryString_rds_profesionales); ?>"><img src="Previous.gif" border=0></a>
                                <?php } // Show if not first page ?>
                          </td>
                          <td width="23%" align="center"><?php if ($pageNum_rds_profesionales < $totalPages_rds_profesionales) { // Show if not last page ?>
                                <a href="<?php printf("%s?pageNum_rds_profesionales=%d%s", $currentPage, min($totalPages_rds_profesionales, $pageNum_rds_profesionales + 1), $queryString_rds_profesionales); ?>"><img src="Next.gif" border=0></a>
                                <?php } // Show if not last page ?>
                          </td>
                          <td width="23%" align="center"><?php if ($pageNum_rds_profesionales < $totalPages_rds_profesionales) { // Show if not last page ?>
                                <a href="<?php printf("%s?pageNum_rds_profesionales=%d%s", $currentPage, $totalPages_rds_profesionales, $queryString_rds_profesionales); ?>"><img src="Last.gif" border=0></a>
                                <?php } // Show if not last page ?>
                          </td>
                        </tr>
                      </table></td>
                    <td>&nbsp;</td>
                  </tr>
               
                <tr>
                  <td colspan="5" bgcolor="#CCCCCC"> Registros <?php echo ($startRow_rds_profesionales + 1) ?> a <?php echo min($startRow_rds_profesionales + $maxRows_rds_profesionales, $totalRows_rds_profesionales) ?> de <?php echo $totalRows_rds_profesionales ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
              </table>
            </div></td>
            </tr> <?php
			 }   ?>
        </table>
     <!-- fin de contenido -->   
	</div>
				<img src="./contactar.guiasdeturismodecolombia.com.co_files/shadowboxtext.png" class="img-responsive" alt="">
				</div>
			 </div>
			 <div class="col-lg-3 col-md-3 col-xs-10 col-sm-10">
				<h1 class="titleseccion2">	Canales de atencion</h1>
				<br>
				<div style="    margin: 0px 0px 20px 21px;    width: 100%;">
				<div class="row">
				<div class="col-lg-3 col-md-3 col-xs-3 col-sm-3 "> <img src="./contactar.guiasdeturismodecolombia.com.co_files/icocorreo.png" class="img-responsive" alt="" style="    width: 65%;    margin: 0px auto;"></div>				<div class="col-lg-9 col-md-9 col-xs-9 col-sm-9"><a href="mailto:amoreno@guiasdeturismodecolombia.com.co">amoreno@guiasde</br>turismodecolombia.com.co</a>
				<a href="mailto:guiaturismo@mincit.gov.co">guiaturismo@mincit.gov.co</a></div>
				</div>
				<br>
				<div class="row">
				<div class="col-lg-3 col-md-3 col-xs-3 col-sm-3"><img src="./contactar.guiasdeturismodecolombia.com.co_files/icotell.png" class="img-responsive" alt=""style="    width: 65%;    margin: 0px auto;"> </div>
				<div class="col-lg-9 col-md-9 col-xs-9 col-sm-9">3013613457,<br>6067676 Ext. 2118,<br>3138489691.<br>Bogot&#225;-Colombia</div>
				</div>
				<br>
				<div class="row">
				<div class="col-lg-3 col-md-3 col-xs-3 col-sm-3"><img src="./contactar.guiasdeturismodecolombia.com.co_files/icoubi.png" class="img-responsive" alt=""style="    width: 56%;    margin: 0px auto;"> 
				</div>
				<div class="col-lg-9 col-md-9 col-xs-9 col-sm-9">Calle 28<br>N 13A-15 piso 6</div>
				</div>
				</div>
				<a href="http://contactar.guiasdeturismodecolombia.com.co/"><img src="./contactar.guiasdeturismodecolombia.com.co_files/banner.png" class="img-responsive" alt="" style="width: 85%;
margin: 0px auto;"></a>
				<img src="./contactar.guiasdeturismodecolombia.com.co_files/shadowboxbanner.png" class="img-responsive" alt="">
				 <font face="Arial, Helvetica, sans-serif">Ultima actualizaci&oacute;n el <br /> 
					  <?php
				include ("ultima_actualizacion.php");
				?> </font></font><font size="1" face="Arial, Helvetica, sans-serif"><br />
					  <br />
					  <a href="privacidad.php">Pol&iacute;ticas de privacidad</a> | <a href="glosario.php">Glosario</a> <br />
					<br />
					Usted es el visitante # <?php
				include ("contador.php");
				?>
				 </font>
			 </div>
			  
		 </div>
		
</div>
           
		
		<div class="footer">
            <div class="container">
                <div class="row-fluid" style="margin-top: 10px;">    
                    <center>     
<div class="row">
	   <img src="./contactar.guiasdeturismodecolombia.com.co_files/line.png" class="img-responsive" alt="" style="width: 85%;
margin: 0px auto;">
	   </div>					
                    <div class="col-sm-2">
                        <!--White space-->
                    </div>                    
                    <div class="col-sm-2">
                       <a href="http://www.mincit.gov.co/">
                            <img src="./contactar.guiasdeturismodecolombia.com.co_files/footer_2.png" alt="Alianzas" border="0" class="img-responsive">                     </a>
                    </div>
                    <div class="col-sm-2">
                      <a href="http://www.mincit.gov.co/">
                            <img src="./contactar.guiasdeturismodecolombia.com.co_files/footer_3.png" alt="Alianzas" border="0" class="img-responsive">         </a>               
                    </div>                        
                    <div class="col-sm-2">
                       <a href="http://www.fontur.com.co/inicio">
                            <img src="./contactar.guiasdeturismodecolombia.com.co_files/footer_4.png" alt="Alianzas" border="0" class="img-responsive">                      </a>                     
                    </div>
                    <div class="col-sm-2">
                        <a href="http://www.consejoprofesionaldeguiasdeturismo.org/">
                            <img src="./contactar.guiasdeturismodecolombia.com.co_files/footer_1.png" alt="Alianzas" border="0" class="img-responsive">                        </a>
                    </div>
                    <div class="col-sm-2">
                        <!--White space-->
                    </div>
                    </center>
                </div>
                <div class="row-fluid">
                    <div class="col-sm-12">
                        <center>  
                            <p>&#169; <a href="http://www.consejoprofesionaldeguiasdeturismo.org/">Consejo Profesional de Gu&#205;as de Turismo</a>, Republica de Colombia. Calle 28 #13A - 15 piso 6
                        <br>Cont&#225;ctenos Tel&#233;fono 6067676 Ext. 2118-Bogot&#225;-Colombia</p>
                        </center>
                    </div>

                </div>
            </div>
		</div>
	</div><div id="goog-gt-tt" class="skiptranslate" dir="ltr"><div style="padding: 8px;"><div><div class="logo"><img src="./contactar.guiasdeturismodecolombia.com.co_files/translate-32.png" width="20" height="20"></div><iframe style="width:348px;height:1.1em;float:right;" id="signin_status" border="0" frameborder="0" src="./contactar.guiasdeturismodecolombia.com.co_files/tminfo.htm"></iframe></div></div><div class="top" style="padding: 8px; float: left; width: 100%;"><h1 class="title gray">Texto original</h1></div><div class="middle" style="padding: 8px;"><div class="original-text"></div></div><div class="bottom" style="padding: 8px;"><div class="activity-links"><span class="activity-link">Sugiere una traducción mejor</span><span class="activity-link"></span></div><div class="started-activity-container"><hr style="color: #CCC; background-color: #CCC; height: 1px; border: none;"><div class="activity-root"></div></div></div><div class="status-message" style="display: none;"></div></div>
	<script type="text/javascript">                                        
$(document).ready(function(){ 
    $("#container").addClass("index-container");
    $(".container-bg").addClass("index-container-bg");
    $('.bxslider').bxSlider({
      auto: true,
      autoControls: true,
      speed: 3000,
      mode: 'fade'
    });
	
});


</script> 
<iframe frameborder="0" class="goog-te-menu-frame skiptranslate" style="visibility: visible; box-sizing: content-box; width: 709px; height: 274px; display: none;"></iframe></body></html>
<?php
mysql_free_result($rds_ciudades);

mysql_free_result($rds_departamentos);

mysql_free_result($rds_idiomas);

mysql_free_result($rds_profesionales);

mysql_free_result($rds_idiomasguia);
?>
