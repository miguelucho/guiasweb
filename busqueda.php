<?php require_once('Connections/datos.php'); ?>
	<?php
	if (!function_exists("GetSQLValueString")) {
	function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
	{
	  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

	  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

	  switch ($theType) {
	    case "text":
	      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
	      break;
	    case "long":
	    case "int":
	      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
	      break;
	    case "double":
	      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
	      break;
	    case "date":
	      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
	      break;
	    case "defined":
	      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
	      break;
	  }
	  return $theValue;
	}
	}

	$colname_rds_textos = "-1";

	if (isset($_POST['busq'])) {

	  $colname_rds_textos = (get_magic_quotes_gpc()) ? $_POST['busq'] : addslashes($_POST['busq']);

	}

	mysql_select_db($database_datos, $datos);

	$query_rds_textos = sprintf("SELECT * FROM ttextosvarios WHERE url <> ''  and contenido LIKE CONCAT('%%', %s, '%%')", GetSQLValueString($colname_rds_textos, "text"));

	$rds_textos = mysql_query($query_rds_textos, $datos) or die(mysql_error());

	$row_rds_textos = mysql_fetch_assoc($rds_textos);

	$totalRows_rds_textos = mysql_num_rows($rds_textos);

	?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" >
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">
	<title>Consejo profesional de guiás de turismo</title>
	<meta http-equiv="Cache-control" content="public">
	<link rel="stylesheet" href="css/slider.css" />
	<link rel="stylesheet" href="css/stylesheetnew.css" />
</head>
<body>
<header>
	<?php include "new-header-top.php";?>
</header>
<div class="Impul">
	<div class="Impul-int">
		<a href="#">
			<img src="images/appicon.png" class="New-App">
		</a>
	</div>
</div>
<?php include "sliderweb.php";?>
<div class=""></div>
<section>
	<div class="Bloques">
		<div class="Bloques-int">
			<div class="col-md-4">
				<h1 style="color:rgb(255, 112, 0)">	Busqueda</h1>
			</div>
			<div  class="row" id="contenidoPagina">
				<div class="col-lg-12">
				<!-- inicio de contenido -->
              		<p><img src="imagenes/bullet.gif" width="12" height="12" /> <strong><font face="Arial, Helvetica, sans-serif">Resultados de la b&uacute;squeda </font></strong></p>
             	 	<div class="rounded">
	                	<table width="100%" border="0" cellspacing="0" cellpadding="6">
			                <tr>
				                <td>Resultados de la busqueda de: </td>
			                </tr>
		                    <?php do { ?>
		                    <tr>
		                        <td background="<?php echo $row_rds_textos['link']; ?>"><a href="<?php echo $row_rds_textos['url']; ?>"><b><?php echo $row_rds_textos['nombre']; ?></b></a></td>
		                    </tr>
		                    <?php } while ($row_rds_textos = mysql_fetch_assoc($rds_textos)); ?>
			                <tr>
			                    <td>&nbsp;<?php echo $totalRows_rds_textos ?> coincidencias encontradas. </td>
			                </tr>
	        	        </table>
					</div>
	            <!--FIn de contenido -->
				</div>
			</div>
		</div>
	</div>
</section>
<section>
	<div class="Conten-global">
		<div class="Conten-global-int">
			<div class="Conten-redes">
				<div class="Conten-redes-int">
                    <div class="Conten-redes-int-sec">
                        <div class="Redesg-facetw">
                            <div class="fb-page" data-href="https://www.facebook.com/pg/MincomercioCo/posts/?ref=page_internal" data-tabs="timeline" data-width="500" data-height="400" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/pg/MincomercioCo/posts/?ref=page_internal" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/pg/MincomercioCo/posts/?ref=page_internal">Ministerio de Comercio, Industria y Turismo</a></blockquote></div>
                        </div>
                    </div>
                    <div class="Conten-redes-int-sec">
                        <div class="Redesg-facetw">
                        <a class="twitter-timeline" href="https://twitter.com/mincomercioco?lang=es">Tweets by consejodeguias</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</div>
</section>

<footer>
<?php include "new-footer.php";?>
</footer>
<script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
<script>
    $(function() {
    $('#slides').superslides({
    inherit_width_from: '.wide-container',
    inherit_height_from: '.wide-container',
    play: 5000,
    animation: 'fade'
    });
    });
</script>
<script type="text/javascript">
	$(function(){
		$('#Drop').bind('click',function() {
		$('.Top-inf').toggleClass('Top-inf-apa');
	});
	});
</script>
<script src="js/jquery.superslides.min.js" type="text/javascript" charset="utf-8"></script>
<script src="js/mostrar-invest-index.js"></script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.7";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
</body>
</html>