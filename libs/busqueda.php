<?php
require_once 'conexion.php';

$data = $_REQUEST['busqueda'];

if ($data['nombre'] != '') {
    $pagina         = (int) (isset($data['pagina']) ? $data['pagina'] : 1);
    $pagina         = ($pagina == 0 ? 1 : $pagina);
    $resultados_pag = 8;
    $adyacentes     = 2;

    $totalitems = $db
        ->join('guias_disponibilidad gd', 'gd.guia_id=g.user_id')
        ->join('users2 u', 'u.id=g.user_id', 'LEFT')
        ->where('CONCAT(u.nombres," ",u.apellidos)', '%' . $data['nombre'] . '%', 'LIKE')
        ->where('gd.estado', 1)
        ->objectBuilder()->get('guias g');

        // print_r($db->getLastQuery());

    $numpags = ceil($db->count / $resultados_pag);
    if ($numpags >= 1) {
        require_once 'Paginacion.php';
        $listado       = '';
        $db->pageLimit = $resultados_pag;

        $listar = $db
            ->join('guias_disponibilidad gd', 'gd.guia_id=g.user_id')
            ->join('users2 u', 'u.id=g.user_id', 'LEFT')
            // ->where('u.nombres', '%' . $data['nombre'] . '%', 'LIKE')
             ->where('CONCAT(u.nombres," ",u.apellidos)', '%' . $data['nombre'] . '%', 'LIKE')
            ->where('gd.estado', 1)
            ->objectBuilder()->get('guias g', $pagina);

        foreach ($listar as $guia) {

            $guias_imagen = $db
                ->where('user_id', $guia->user_id)
                ->where('nombre', '', '!=')
                ->objectBuilder()->get('imagenes');

            $foto_guia = '';
            if ($db->count > 0) {
                // quitar al mover substr
                $foto_guia = substr($guias_imagen[0]->url, 1) . $guias_imagen[0]->nombre;
            } else {
                $foto_guia = 'images/default.png';
            }

            $guias_ubicaciones = $db
                ->join('guias g', 'g.id=cg.guia_id', 'LEFT')
                ->where('g.user_id', $guia->user_id)
                ->objectBuilder()->get('ciudades_guias cg');

            $ls_ubicaciones = '';
            if ($db->count > 0) {
                foreach ($guias_ubicaciones as $ubicacion) {
                    $ciudades = $db
                        ->where('id', $ubicacion->ciudade_id)
                        ->objectBuilder()->get('ciudades');

                    if ($db->count > 0) {
                        $ls_ubicaciones .= '<span class="Tarjeta-tag">' . $ciudades[0]->ciudad . '</span>';
                    }
                }
            }

            $guias_idiomas = $db
                ->where('user_id', $guia->user_id)
                ->objectBuilder()->get('user_idiomas');

            $ls_idiomas = '';
            if ($db->count > 0) {
                foreach ($guias_idiomas as $idioma) {
                    $idiomas = $db
                        ->where('id', $idioma->idioma_id)
                        ->objectBuilder()->get('idiomas');
                    if ($db->count > 0) {
                        $ls_idiomas .= '<span class="Tarjeta-tag">' . ($idiomas[0]->idioma == 'Lengua Nativa' ? 'Español' : $idiomas[0]->idioma) . '</span>';
                    }
                }
            }

            $descripcion = 'El guía no ha proporcionado una descripción.';

            if ($guia->descripcion != '') {
                $descripcion = $guia->descripcion;
            }

            $listado .= '<div class="Tarjeta-guia">
                        <div class="Tarjeta-guia-int">
                            <div class="Tarjeta-guia-int-nombre">
                                <h2>' . $guia->nombres . ' ' . $guia->apellidos . '</h2>
                            </div>
                            <div class="Tarjeta-guia-int-1">
                                <img src="' . $foto_guia . '">
                            </div>
                            <div class="Tarjeta-guia-int-2">
                                <div class="Tarjeta-guia-int-2-titul">
                                    <h3>Ubicaciones:</h3>
                                </div>
                                <div class="Tarjeta-guia-int-2-tags" style="overflow: auto;">
                                ' . $ls_ubicaciones . '
                                </div>
                            </div>
                            <div class="Tarjeta-guia-int-3">
                                <p>';
            @session_start();
            if (isset($_SESSION['turturista'])) {
                $listado .= '<a href="javascript://" class="Btn-azul btn-contactar" id="ctguia-' . $guia->guia_id . '">Contactar</a>';
            } else {
                $listado .= '<a class="Btn-azul btn-contactar disabled" style="background: #9e9fa0;">Contactar</a>';
            }

            $listado .= '</p>
                            </div>
                        </div>
                        <div class="Tarjeta-guia-int">
                            <div class="Tarjeta-guia-int-sub">
                                <div class="Tarjeta-guia-int-2">
                                    <h3>Credenciales oficiales:</h3>
                                </div>
                                <span class="Tarjeta-tag">RNT: ' . $guia->RNT . '</span>
                                <span class="Tarjeta-tag">TPG: ' . $guia->tarjeta_profesional . '</span>
                            </div>
                            <div class="Tarjeta-guia-int-sub">
                                <div class="Tarjeta-guia-int-2">
                                    <h3>Idiomas:</h3>
                                </div>
                                ' . $ls_idiomas . '
                            </div>
                        </div>
                        <div class="Tarjeta-guia-int">
                            <div class="Tarjeta-guia-int-2">
                                <h3>Descripción:</h3>
                            </div>
                            <p>' . $descripcion . '</p>
                        </div>
                    </div>';
        }

        $info['listado']    = $listado;
        $pagconfig          = array('pagina' => $pagina, 'totalrows' => $db->totalPages, 'ultima_pag' => $numpags, 'resultados_pag' => $resultados_pag, 'adyacentes' => $adyacentes);
        $paginar            = new Paginacion($pagconfig);
        $info['paginacion'] = $paginar->crearlinks();
    } else {
        $info['listado'] = '<div class="Tarjeta-guia">
                            <div class="Tarjeta-guia-int">
                                <span class="" title="">No se encontraron guias con el criterio de busqueda realizado.</span>
                            </div>
                        </div>';
        $info['paginacion'] = '';
    }

    echo json_encode($info);
} else {

    $pagina         = (int) (isset($data['pagina']) ? $data['pagina'] : 1);
    $pagina         = ($pagina == 0 ? 1 : $pagina);
    $resultados_pag = 8;
    $adyacentes     = 2;

    $db
        ->join('guias_disponibilidad gd', 'gd.guia_id=g.user_id')
        ->where('gd.estado', 1);

    if ($data['idioma'] != '') {
        $lista = $db->copy();
        $lista = $db->join('user_idiomas ud', 'ud.user_id=g.user_id');
    }

    if ($data['servicio'] != '') {
        $lista = $db->copy();
        $lista = $db->join('servicios_guias sg', 'sg.guia_id=g.user_id');
    }

    if ($data['especialidad'] != '') {
        $lista = $db->copy();
        $lista = $db->join('especialidades_guias eg', 'eg.guia_id=g.user_id');
    }

    $lista = $db->copy();
    $lista = $db->join('users2 u', 'u.id=gd.guia_id', 'LEFT');

    $lista = $db->copy();
    $lista = $db->where('u.estado', 1)
        ->where('g.suspendido', 0)
        ->where('g.aprobado', 1);

    if ($data['idioma'] != '') {
        $lista = $db->copy();
        $lista = $db->where('ud.idioma_id', explode(',', $data['idioma']), 'IN');
    }

    if ($data['servicio'] != '') {
        $lista = $db->copy();
        $lista = $db->where('sg.servicio_id', explode(',', $data['servicio']), 'IN');
    }

    if ($data['especialidad'] != '') {
        $lista = $db->copy();
        $lista = $db->where('eg.especialidade_id', explode(',', $data['especialidad']), 'IN');
    }

    if ($data['fecha'] != '') {
        $lista = $db->copy();
        $lista = $db->where('gd.inicio', $data['fecha'], '<=')
            ->where('gd.fin', $data['fecha'], '>=')
            ->where('gd.estado', 1);
    }

    if ($data['ciudad'] != '') {
        $lista = $db->copy();
        $lista = $db->where('gd.ciudad', $data['ciudad'])
            ->where('gd.estado', 1);
    }

    $totalitems = $db->copy();
    $totalitems = $db->objectBuilder()->get('guias g');

// print_r($db->getLastQuery());
    // print_r($db->count);

    $numpags = ceil($db->count / $resultados_pag);
    if ($numpags >= 1) {
        require_once 'Paginacion.php';
        $listado       = '';
        $db->pageLimit = $resultados_pag;

        $db
            ->join('guias_disponibilidad gd', 'gd.guia_id=g.user_id')
            ->where('gd.estado', 1);

        if ($data['idioma'] != '') {
            $lista = $db->copy();
            $lista = $db->join('user_idiomas ud', 'ud.user_id=g.user_id');
        }

        if ($data['servicio'] != '') {
            $lista = $db->copy();
            $lista = $db->join('servicios_guias sg', 'sg.guia_id=g.user_id');
        }

        if ($data['especialidad'] != '') {
            $lista = $db->copy();
            $lista = $db->join('especialidades_guias eg', 'eg.guia_id=g.user_id');
        }

        $lista = $db->copy();
        $lista = $db->join('users2 u', 'u.id=gd.guia_id', 'LEFT');

        $lista = $db->copy();
        $lista = $db->where('u.estado', 1)
            ->where('g.suspendido', 0)
            ->where('g.aprobado', 1);

        if ($data['idioma'] != '') {
            $lista = $db->copy();
            $lista = $db->where('ud.idioma_id', explode(',', $data['idioma']), 'IN');
        }

        if ($data['servicio'] != '') {
            $lista = $db->copy();
            $lista = $db->where('sg.servicio_id', explode(',', $data['servicio']), 'IN');
        }

        if ($data['especialidad'] != '') {
            $lista = $db->copy();
            $lista = $db->where('eg.especialidade_id', explode(',', $data['especialidad']), 'IN');
        }

        if ($data['fecha'] != '') {
            $lista = $db->copy();
            $lista = $db->where('gd.inicio', $data['fecha'], '<=')
                ->where('gd.fin', $data['fecha'], '>=')
                ->where('gd.estado', 1);
        }

        if ($data['ciudad'] != '') {
            $lista = $db->copy();
            $lista = $db->where('gd.ciudad', $data['ciudad'])
                ->where('gd.estado', 1);
        }

        $listar = $db->copy();
        $listar = $db->orderBy('u.apellidos', 'ASC')
            ->objectBuilder()->paginate('guias g', $pagina);

        foreach ($listar as $guia) {

            $guias_imagen = $db
                ->where('user_id', $guia->guia_id)
                ->where('nombre', '', '!=')
                ->objectBuilder()->get('imagenes');

            $foto_guia = '';
            if ($db->count > 0) {
                // quitar al mover substr
                $foto_guia = substr($guias_imagen[0]->url, 1) . $guias_imagen[0]->nombre;
            } else {
                $foto_guia = 'images/default.png';
            }

            $guias_ubicaciones = $db
                ->join('guias g', 'g.id=cg.guia_id', 'LEFT')
                ->where('g.user_id', $guia->guia_id)
                ->objectBuilder()->get('ciudades_guias cg');

            $ls_ubicaciones = '';
            if ($db->count > 0) {
                foreach ($guias_ubicaciones as $ubicacion) {
                    $ciudades = $db
                        ->where('id', $ubicacion->ciudade_id)
                        ->objectBuilder()->get('ciudades');

                    if ($db->count > 0) {
                        $ls_ubicaciones .= '<span class="Tarjeta-tag">' . $ciudades[0]->ciudad . '</span>';
                    }
                }
            }

            $guias_idiomas = $db
                ->where('user_id', $guia->guia_id)
                ->objectBuilder()->get('user_idiomas');

            $ls_idiomas = '';
            if ($db->count > 0) {
                foreach ($guias_idiomas as $idioma) {
                    $idiomas = $db
                        ->where('id', $idioma->idioma_id)
                        ->objectBuilder()->get('idiomas');
                    if ($db->count > 0) {
                        $ls_idiomas .= '<span class="Tarjeta-tag">' . ($idiomas[0]->idioma == 'Lengua Nativa' ? 'Español' : $idiomas[0]->idioma) . '</span>';
                    }
                }
            }

            $descripcion = 'El guía no ha proporcionado una descripción.';

            if ($guia->descripcion != '') {
                $descripcion = $guia->descripcion;
            }

            $listado .= '<div class="Tarjeta-guia">
                        <div class="Tarjeta-guia-int">
                            <div class="Tarjeta-guia-int-nombre">
                                <h2>' . $guia->nombres . ' ' . $guia->apellidos . '</h2>
                            </div>
                            <div class="Tarjeta-guia-int-1">
                                <img src="' . $foto_guia . '">
                            </div>
                            <div class="Tarjeta-guia-int-2">
                                <div class="Tarjeta-guia-int-2-titul">
                                    <h3>Ubicaciones:</h3>
                                </div>
                                <div class="Tarjeta-guia-int-2-tags" style="overflow: auto;">
                                ' . $ls_ubicaciones . '
                                </div>
                            </div>
                            <div class="Tarjeta-guia-int-3">
                                <p>';
            @session_start();
            if (isset($_SESSION['turturista'])) {
                $listado .= '<a href="javascript://" class="Btn-azul btn-contactar" id="ctguia-' . $guia->guia_id . '">Contactar</a>';
            } else {
                $listado .= '<a class="Btn-azul btn-contactar disabled" style="background: #9e9fa0;">Contactar</a>';
            }

            $listado .= '</p>
                            </div>
                        </div>
                        <div class="Tarjeta-guia-int">
                            <div class="Tarjeta-guia-int-sub">
                                <div class="Tarjeta-guia-int-2">
                                    <h3>Credenciales oficiales:</h3>
                                </div>
                                <span class="Tarjeta-tag">RNT: ' . $guia->RNT . '</span>
                                <span class="Tarjeta-tag">TPG: ' . $guia->tarjeta_profesional . '</span>
                            </div>
                            <div class="Tarjeta-guia-int-sub">
                                <div class="Tarjeta-guia-int-2">
                                    <h3>Idiomas:</h3>
                                </div>
                                ' . $ls_idiomas . '
                            </div>
                        </div>
                        <div class="Tarjeta-guia-int">
                            <div class="Tarjeta-guia-int-2">
                                <h3>Descripción:</h3>
                            </div>
                            <p>' . $descripcion . '</p>
                        </div>
                    </div>';
        }

        $info['listado']    = $listado;
        $pagconfig          = array('pagina' => $pagina, 'totalrows' => $db->totalPages, 'ultima_pag' => $numpags, 'resultados_pag' => $resultados_pag, 'adyacentes' => $adyacentes);
        $paginar            = new Paginacion($pagconfig);
        $info['paginacion'] = $paginar->crearlinks();
    } else {
        $info['listado'] = '<div class="Tarjeta-guia">
                            <div class="Tarjeta-guia-int">
                                <span class="" title="">No se encontraron guias con el criterio de busqueda realizado.</span>
                            </div>
                        </div>';
        $info['paginacion'] = '';
    }

    echo json_encode($info);
}
