<?php
require_once "conexion.php";
$data = $_REQUEST['contacto'];
$msg  = [];

$template = 'contactotemp.php';
$message  = file_get_contents($template);
$message  = str_replace('$tiposol$', $data['tipo'], $message);
$message  = str_replace('$nombre$', $data['nombre'] . ' ' . $data['apellido'], $message);
$message  = str_replace('$identificacion$', $data['tipoiden'] . ' ' . $data['identificacion'], $message);
$message  = str_replace('$correo$', $data['correo'], $message);
$message  = str_replace('$comentario$', $data['comentario'], $message);
include_once 'phpmailer/class.phpmailer.php';
$mail = new PHPMailer();

$canales = $db
    ->objectBuilder()->get('canales_atencion');

if ($canales[0]->correo != '') {
    $mail->Host = "localhost";
    $mail->SetFrom('noreply@guiasdeturismodecolombia.com.co');
    $mail->AddAddress($canales[0]->correo);
    $mail->Subject = 'Contactenos -  guiasdeturismodecolombia.com.co';
    $mail->MsgHTML($message);
    $mail->IsHTML(true);
    $mail->CharSet = "utf-8";

    if ($mail->Send()) {
        $msg['status'] = true;
    } else {
        $msg['status'] = false;
        $msg['motivo'] = 'El mensaje no pudo ser enviado.';
    }
} else {
    $msg['status'] = false;
    $msg['motivo'] = 'El mensaje no pudo ser enviado.';
}

echo json_encode($msg);
