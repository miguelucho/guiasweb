<?php
require_once 'conexion.php';

$data = $_POST;

$comprobar = $db
    ->where('username', $data['correo'])
    ->objectBuilder()->get('users2');

if ($db->count == 0) {
    if (!isset($data['estado'])) {
        $p_asociacion = '0';
        if (isset($data['pertenece_asociacion'])) {
            $p_asociacion = '1';
        }

        date_default_timezone_set("America/Bogota");
        $fecha_actual = date('Y-m-d H:i:s');

        $datos = ['aprobada' => 0, 'apellidos' => $data['apellidos'], 'nombres' => $data['nombres'], 'fecha_nacimiento' => $data['fecha_nacimiento'], 'pertenece_asociacion' => $p_asociacion, 'asociacion_a_quepertenece' => $data['asociacion_a_quepertenece'], 'tipoid' => $data['tipoid'], 'numeroid' => $data['numeroid'], 'expedicionid' => $data['expedicionid'], 'correoe' => $data['correo'], 'correoe2' => $data['correo2'], 'carne' => $data['carnet'], 'rh' => $data['rh'], 'id_departamento' => $data['depart_nacimiento'], 'ciudad_nacimiento' => $data['ciudad_nacimiento'], 'direccion' => $data['direccion_residencia'], 'barrio' => $data['barrio_residencia'], 'ciudad' => $data['ciudad_residencia'], 'fax' => $data['fax'], 'celular' => $data['celular'], 'celular2' => $data['celular2'], 'fecha_formulario' => $fecha_actual, 'especialidades' => implode(',', $data['especialidades'])];

        $registro =
        $db->insert('tramite', $datos);

        if ($registro) {

            $error = 0;

            $destino = '../documentos_guias/' . $registro . "_foto.jpg";
            $archivo = $_FILES["foto"]['name'];
            $archivo = str_replace(" ", "_", $archivo);
            if ($archivo != "") {
                if (move_uploaded_file($_FILES['foto']['tmp_name'], $destino)) {
                    // if (copy($_FILES['foto']['tmp_name'], $destino)) {
                    $up = $db
                        ->where('id', $registro)
                        ->update('tramite', ['foto' => $destino]);
                } else {
                    $msg['motivo'] = 'Error, al subir la foto.';
                    $msg['status'] = false;
                    $error         = 1;
                }
            } else {
                $msg['motivo'] = 'Error, al subir la foto.';
                $msg['status'] = false;
                $error         = 1;
            }

            $destino = '../documentos_guias/' . $registro . "_cedula.jpg";
            $archivo = $_FILES["cedula"]['name'];
            $archivo = str_replace(" ", "_", $archivo);
            if ($archivo != "") {
                // if (copy($_FILES['cedula']['tmp_name'], $destino)) {
                if (move_uploaded_file($_FILES['cedula']['tmp_name'], $destino)) {
                    $up = $db
                        ->where('id', $registro)
                        ->update('tramite', ['cedula' => $destino]);
                } else {
                    $msg['motivo'] = 'Error, al subir la cedula.';
                    $msg['status'] = false;
                    $error         = 1;
                }
            } else {
                $msg['motivo'] = 'Error, al subir la cedula.';
                $msg['status'] = false;
                $error         = 1;
            }

            $destino = '../documentos_guias/' . $registro . "_certificado.jpg";
            $archivo = $_FILES["certificado"]['name'];
            $archivo = str_replace(" ", "_", $archivo);
            if ($archivo != "") {
                // if (copy($_FILES['certificado']['tmp_name'], $destino)) {
                if (move_uploaded_file($_FILES['certificado']['tmp_name'], $destino)) {
                    $up = $db
                        ->where('id', $registro)
                        ->update('tramite', ['certificado' => $destino]);
                } else {
                    $msg['motivo'] = 'Error, al subir la certificado.';
                    $msg['status'] = false;
                    $error         = 1;
                }
            } else {
                $msg['motivo'] = 'Error, al subir la certificado.';
                $msg['status'] = false;
                $error         = 1;
            }

            $destino = '../documentos_guias/' . $registro . "_titulo.jpg";
            $archivo = $_FILES["titulo"]['name'];
            $archivo = str_replace(" ", "_", $archivo);
            if ($archivo != "") {
                // if (copy($_FILES['titulo']['tmp_name'], $destino)) {
                if (move_uploaded_file($_FILES['titulo']['tmp_name'], $destino)) {
                    $up = $db
                        ->where('id', $registro)
                        ->update('tramite', ['titulo' => $destino]);
                } else {
                    $msg['motivo'] = 'Error, al subir la titulo.';
                    $msg['status'] = false;
                    $error         = 1;
                }
            } else {
                $msg['motivo'] = 'Error, al subir la titulo.';
                $msg['status'] = false;
                $error         = 1;
            }

            if ($error == 0) {
                $template = 'nuevo-guia.php';
                $message  = file_get_contents($template);
                $message  = str_replace('$usuario$', $data['nombres'] . ' ' . $data['apellidos'], $message);
                $message  = str_replace('$correo$', $data['correo'], $message);
                include_once 'phpmailer/class.phpmailer.php';
                $mail = new PHPMailer();

                $mail->Host = "localhost";
                $mail->SetFrom('noreply@guiasdeturismodecolombia.com.co');
                $mail->AddAddress('coordinacion@guiasdeturismodecolombia.com.co');
                $mail->Subject = 'Registro nuevo guia -  guiasdeturismodecolombia.com.co';
                $mail->MsgHTML($message);
                $mail->IsHTML(true);
                $mail->CharSet = "utf-8";

                $mail->Send();

                $msg['motivo'] = '';
                $msg['status'] = true;
            } else {
                $dele = $db
                    ->where('id', $registro)
                    ->delete('tramite');
                $msg['status'] = false;
            }

        } else {
            $msg['motivo'] = 'Error, al crear la solicitud.';
            $msg['status'] = false;
        }

        echo json_encode($msg);
    } else {
        $comprobar = $db
            ->where('tipoid', $data['tipoid'])
            ->where('numeroid', $data['numeroid'])
            ->objectBuilder()->get('tramite');

        if ($db->count > 0) {
            if ($comprobar[0]->aprobada == 0) {
                $msg['motivo'] = 'Su solicitud no ha sido aprobada.';
                $msg['status'] = true;
            } else {
                $msg['motivo'] = 'Su solicitud fue aprobada, se le ha enviado un correo con los datos de acceso a la plataforma.';
                $msg['status'] = true;
            }
        } else {
            $msg['motivo'] = 'No se encontró ningún tramite para el numero de identificación dado.';
            $msg['status'] = false;
        }

        echo json_encode($msg);
    }
} else {
    $msg['motivo'] = 'El correo ya se encuentra registrado.';
    $msg['status'] = false;

    echo json_encode($msg);
}
