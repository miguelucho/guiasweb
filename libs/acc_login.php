<?php

// google
// Client ID   308794357018-vkd467nppss3m7uogfajafhqle430rct.apps.googleusercontent.com

require_once "conexion.php";
$data = $_REQUEST['log'];
$msg  = [];

if (isset($data['rnt'])) {
    $rnt = $db->escape($data['rnt']);

    $check = $db
        ->where('RNT', $rnt)
        // ->where('aprobado', 1)
        ->objectBuilder()->get('guias');
    if ($db->count > 0) {
        session_start();
        $_SESSION['ckrnt'] = $check[0]->RNT;

        $msg['status'] = true;
    } else {
        $msg['status'] = false;
        $msg['motivo'] = 'Error, RNT no válido.';
    }
    echo json_encode($msg);
}
if (isset($data['rncorreo'])) {
    session_start();
    if (isset($_SESSION['ckrnt']) && $_SESSION['ckrnt'] > 0) {
        $check = $db
            ->where('RNT', $_SESSION['ckrnt'])
            // ->where('aprobado', 1)
            ->objectBuilder()->get('guias');

        $idus       = $check[0]->user_id;
        $correo     = trim($data['rncorreo']);
        $contrasena = trim($data['rncontrasena']);

        require_once "password.php";
        $npass = password_hash($contrasena, PASSWORD_BCRYPT);

        $renovar = $db
            ->where('id', $idus)
            ->where('estado', 1)
            ->update('users2', ['username' => $correo, 'password' => $npass, 'tipoid' => $data['tipoid'], 'numeroid' => $data['numeroid'], 'telefono1' => $data['telefono']]);
        if ($renovar) {
            unset($_SESSION['ckrnt']);
            $_SESSION['turguia'] = $idus;
            $msg['status']       = true;
            $msg['redirect']     = 'guia/guias-home';
        } else {
            $msg['status'] = false;
            $msg['motivo'] = 'Error, guia no válido.';
        }
    } else {
        $msg['status'] = false;
        $msg['motivo'] = 'Error, guia no válido.';
    }
    echo json_encode($msg);
}

if (isset($data['lgcorreo'])) {
    $check = $db
        ->join('guias g', 'g.user_id=u.id', 'Left')
        // ->where('g.aprobado', 1)
        // ->where('g.suspendido', 0)
        // ->where('u.estado', 1)
        ->where('u.username', $data['lgcorreo'])
        ->objectBuilder()->get('users2 u');
    if ($db->count > 0) {
        if ($check[0]->suspendido == 0) {
            if (password_verify($data['lgcontrasena'], $check[0]->password)) {
                session_start();
                $_SESSION['turguia'] = $check[0]->user_id;
                $msg['status']       = true;
                $msg['redirect']     = 'guia/guias-home';
            } else {
                $msg['status'] = false;
                $msg['motivo'] = 'Error, datos no válidos.';
            }
        } else {
            $msg['status'] = false;
            $msg['motivo'] = 'Su cuenta ha sido suspendida';
        }

    } else {
        $msg['status'] = false;
        $msg['motivo'] = 'Error, datos no válidos.';
    }
    echo json_encode($msg);
}

if (isset($data['turcorreo'])) {
    $check = $db
        ->where('tipo', 3)
        ->where('username', $data['turcorreo'])
        ->objectBuilder()->get('users2');
    if ($db->count > 0) {
        if (isset($data['turgoogle']) && $data['turgoogle'] == 1) {
            session_start();
            $_SESSION['turturista'] = $check[0]->id;
            $msg['status']          = true;
            $msg['redirect']        = 'turista/turista-home';
        } else {
            if (!empty($data['turcontrsena'])) {
                if (password_verify($data['turcontrsena'], $check[0]->password)) {
                    session_start();
                    $_SESSION['turturista'] = $check[0]->id;
                    $msg['status']          = true;
                    $msg['redirect']        = 'turista/turista-home';
                } else {
                    $msg['status'] = false;
                    $msg['motivo'] = 'Error, datos no válidos.';
                }
            } else {
                $msg['status'] = false;
                $msg['motivo'] = 'Error, datos no válidos.';
            }
        }
    } else {
        $msg['status'] = false;
        $msg['motivo'] = 'Error, datos no válidos.';
    }
    echo json_encode($msg);
}
