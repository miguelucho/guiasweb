<?php

require_once "conexion.php";
$data = $_POST;
$msg  = [];

if (isset($data['turista-usuario'])) {
    $comprobar = $db
        ->where('username', $data['turista-usuario'])
        ->where('tipo', 3)
        ->objectBuilder()->get('users2');

    if ($db->count > 0) {
        $token        = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet .= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet .= "0123456789";
        $max = strlen($codeAlphabet);
        for ($i = 0; $i < 8; $i++) {
            $token .= $codeAlphabet[rand(0, $max - 1)];
        }

        require_once "password.php";
        $pass = password_hash($token, PASSWORD_BCRYPT);

        $up = $db
            ->where('username', $data['turista-usuario'])
            ->where('tipo', 3)
            ->update('users2', ['password' => $pass]);

        if ($up) {

            $template = 'passreset.php';

            $message = file_get_contents($template);
            $message = str_replace('$nuevopass$', $token, $message);
            $message = str_replace('$usuario$', $comprobar[0]->nombres . ' ' . $comprobar[0]->apellidos, $message);
            $message = str_replace('$enlace$', 'http://guiasdeturismodecolombia.com.co/iniciar.php', $message);
            include_once 'phpmailer/class.phpmailer.php';
            $mail = new PHPMailer();

            $mail->Host = "localhost";
            $mail->SetFrom('noreply@guiasdeturismodecolombia.com.co');
            $mail->AddAddress($comprobar[0]->username);
            $mail->Subject = 'Restablecer contraseña -  guiasdeturismodecolombia.com.co';
            $mail->MsgHTML($message);
            $mail->IsHTML(true);
            $mail->CharSet = "utf-8";

            $mail->Send();

            $msg['status'] = true;
            $msg['motivo'] = 'Se te ha enviado un correo con la nueva contraseña.';
        } else {
            $msg['status'] = false;
            $msg['motivo'] = 'No se ha podido llevar a cabo la acción requerida.';
        }

    } else {
        $msg['status'] = false;
        $msg['motivo'] = 'El usuario / correo electrónico no se encuentra registrado';
    }
    echo json_encode($msg);
} elseif (isset($data['guia-tarjeta'])) {
    $comprobar = $db
        ->where('tarjeta_profesional', $data['guia-tarjeta'])
        ->objectBuilder()->get('guias');

    if ($db->count > 0) {
        $usuarios = $db
            ->where('id', $comprobar[0]->user_id)
            ->where('tipo', 2)
            ->objectBuilder()->get('users2');

        $token        = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet .= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet .= "0123456789";
        $max = strlen($codeAlphabet);
        for ($i = 0; $i < 8; $i++) {
            $token .= $codeAlphabet[rand(0, $max - 1)];
        }

        require_once "password.php";
        $pass = password_hash($token, PASSWORD_BCRYPT);

        $up = $db
            ->where('id', $comprobar[0]->user_id)
            ->where('tipo', 2)
            ->update('users2', ['password' => $pass]);

        if ($up) {

            $template = 'passreset.php';

            $message = file_get_contents($template);
            $message = str_replace('$nuevopass$', $token, $message);
            $message = str_replace('$usuario$', $usuarios[0]->nombres . ' ' . $usuarios[0]->apellidos, $message);
            $message = str_replace('$enlace$', 'http://guiasdeturismodecolombia.com.co/iniciar.php', $message);
            include_once 'phpmailer/class.phpmailer.php';
            $mail = new PHPMailer();

            $mail->Host = "localhost";
            $mail->SetFrom('noreply@guiasdeturismodecolombia.com.co');
            $mail->AddAddress($usuarios[0]->username);
            $mail->Subject = 'Restablecer contraseña -  guiasdeturismodecolombia.com.co';
            $mail->MsgHTML($message);
            $mail->IsHTML(true);
            $mail->CharSet = "utf-8";

            $mail->Send();

            $msg['status'] = true;
            $msg['motivo'] = 'Se te ha enviado un correo con la nueva contraseña.';
        } else {
            $msg['status'] = false;
            $msg['motivo'] = 'No se ha podido llevar a cabo la acción requerida.';
        }

    } else {
        $msg['status'] = false;
        $msg['motivo'] = 'El número de tarjeta profesional no se encuentra registrada';
    }
    echo json_encode($msg);
}
