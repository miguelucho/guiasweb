<?php

require_once "conexion.php";
$data = $_REQUEST['turista'];
$msg  = [];

switch ($data['accion']) {
    case 'contactar':
        session_start();
        $_SESSION['turturista'];

        $check = $db
            ->where('guia_id', $data['guia'])
            ->where('ciudad', $data['ciudad'])
            ->where('inicio', $data['fecha'], '<=')
            ->where('fin', $data['fecha'], '>=')
            ->where('estado', 1)
            ->objectBuilder()->get('guias_disponibilidad');

        if ($db->count > 0) {
            $check = $db
                ->where('guia_id', $data['guia'])
                ->where('ciudad', $data['ciudad'])
                ->where('fecha', $data['fecha'])
                ->where('estado', 1)
                ->where('user_id', $_SESSION['turturista'])
                ->objectBuilder()->get('guias_contactos');

            if ($db->count > 0) {
                $msg['status'] = true;
                $msg['motivo'] = 'Petición de contacto enviada.';
            } else {
                $datos = ['user_id' => $_SESSION['turturista'], 'guia_id' => $data['guia'], 'fecha' => $data['fecha'], 'ciudad' => $data['ciudad'], 'estado' => 1];

                $contacto = $db
                    ->insert('guias_contactos', $datos);

                if ($contacto) {
                    $msg['status'] = true;
                    $msg['motivo'] = 'Petición de contacto enviada.';
                } else {
                    $msg['motivo'] = 'Error al crear la petición de contacto.';
                    $msg['status'] = false;
                }
            }
        } else {
            $msg['motivo'] = 'Error al crear la petición de contacto.';
            $msg['status'] = false;
        }
        echo json_encode($msg);
        break;
}
