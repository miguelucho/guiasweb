<?php require_once('Connections/datos.php'); ?>
	<?php
	if (!function_exists("GetSQLValueString")) {
	function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
	{
	  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

	  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

	  switch ($theType) {
	    case "text":
	      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
	      break;
	    case "long":
	    case "int":
	      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
	      break;
	    case "double":
	      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
	      break;
	    case "date":
	      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
	      break;
	    case "defined":
	      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
	      break;
	  }
	  return $theValue;
	}
	}

	?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" >
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">
	<title>Consejo profesional de guiás de turismo</title>
	<meta http-equiv="Cache-control" content="public">
	<link rel="stylesheet" href="css/slider.css" />
	<link rel="stylesheet" href="css/stylesheetnew.css" />
	<link rel="stylesheet" href="css/jquery-jvectormap.css" />
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-44406258-20"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-44406258-20');
    </script>
</head>
<body>
<header>
	<?php include "new-header-top.php";?>
</header>
<div class="Impul">
	<div class="Impul-int">
		<a href="#">
			<img src="images/appicon.png" class="New-App">
		</a>
	</div>
</div>
<?php include "sliderweb.php";?>
<div class=""></div>
<section>
	<div class="Bloques">
		 <div class="Bloques-int">
			 <div class="col-lg-9 col-md-9 col-xs-12 col-sm-12">
				<div class="col-md-4 ">
					<img src="./contactar.guiasdeturismodecolombia.com.co_files/shaowslide.png" class="img-responsive"  alt="">
				</div>
				<div class="col-md-4">
				<h1 style="color:rgb(255, 112, 0)">	Asociaciones Regionales de Guías</h1>
				</div>
			 	<div class="col-md-4">
					<img src="./contactar.guiasdeturismodecolombia.com.co_files/shaowslide.png" class="img-responsive" alt="">
				</div>
				<div  class="row" id="contenidoPagina">
					<div class="text-left col-lg-12">
					<!-- inicio de contenido -->
			        	<div style="margin:20px">
                			<p>Haga clic en el mapa en el departamento en el cual desea consultar.</p>
            			</div>
           				<div id="map" style="width: 100%; height: 500px"></div>
           				<div class="datosDpto"></div>
            		<!--  -->
					</div>
					<img src="./contactar.guiasdeturismodecolombia.com.co_files/shadowboxtext.png" class="img-responsive" alt="">
				</div>
			 </div>
		 </div>
	</div>
</section>
<section>
	<div class="Conten-global">
		<div class="Conten-global-int">
			<div class="Conten-redes">
				<div class="Conten-redes-int">
                    <div class="Conten-redes-int-sec">
                        <div class="Redesg-facetw">
                            <div class="fb-page" data-href="https://www.facebook.com/pg/MincomercioCo/posts/?ref=page_internal" data-tabs="timeline" data-width="500" data-height="400" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/pg/MincomercioCo/posts/?ref=page_internal" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/pg/MincomercioCo/posts/?ref=page_internal">Ministerio de Comercio, Industria y Turismo</a></blockquote></div>
                        </div>
                    </div>
                    <div class="Conten-redes-int-sec">
                        <div class="Redesg-facetw">
                        <a class="twitter-timeline" href="https://twitter.com/mincomercioco?lang=es">Tweets by consejodeguias</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</div>
</section>

<footer>
<?php include "new-footer.php";?>
</footer>
<script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
<script>
    $(function() {
    $('#slides').superslides({
    inherit_width_from: '.wide-container',
    inherit_height_from: '.wide-container',
    play: 5000,
    animation: 'fade'
    });
    });
</script>
<script type="text/javascript">
	$(function(){
		$('#Drop').bind('click',function() {
		$('.Top-inf').toggleClass('Top-inf-apa');
	});
	});
</script>
<script>
    var idDpto = "6";
    var arrDivipola = {
        23: 'ANTIOQUIA',
        4: 'ATLÁNTICO',
        33: 'BOGOTÁ, D. C.',
        26: 'BOLÍVAR',
        30: 'BOYACÁ',
        22: 'CALDAS',
        12: 'CAQUETÁ',
        17: 'CAUCA',
        27: 'CESAR',
        9: 'CÓRDOBA',
        3: 'CUNDINAMARCA',
        8: 'CHOCÓ',
        18: 'HUILA',
        1: 'LA GUAJIRA',
        25: 'MAGDALENA',
        6: 'META',
        7: 'NARIÑO',
        28: 'NORTE DE SANTANDER',
        21: 'QUINDÍO',
        20: 'RISARALDA',
        29: 'SANTANDER',
        24: 'SUCRE',
        31: 'TOLIMA',
        19: 'VALLE DEL CAUCA',
        16: 'ARAUCA',
        10: 'CASANARE',
        11: 'PUTUMAYO',
        32: 'ARCHIPIÉLAGO DE SAN ANDRÉS, PROVIDENCIA Y SAN CATALINA',
        2: 'AMAZONAS',
        15: 'GUAINÍA',
        13: 'GUAVIARE',
        14: 'VAUPÉS',
        5: 'VICHADA',
        SE: 'SE_05',
        SI: 'Sin Especificar',
        EX: 'Extranjero'
    };
    verDatos('6');


    var gdpData = {
    };

    $(function () {
        var map,
                markerIndex = 0,
                markersCoords = {};

        map = new jvm.Map({
            map: 'co_merc',
            backgroundColor: 'white',
            regionsSelectable: true,
            regionsSelectableOne: true,
            markersSelectable: true,
            regionStyle: {
                initial: {
                    fill: '#FFB61F',
                    "fill-opacity": .8,
                    stroke: 'gray',
                    "stroke-width": 1,
                    "stroke-opacity": 1

                },
                hover: {
                    "fill-opacity": 0.8,
                    cursor: 'pointer',
                    fill: '#FF9538'
                },
                selected: {
                    fill: '#F39538',
                    "fill-opacity": 1,
                },
                selectedHover: {
                }
            },
            markerStyle: {
                initial: {
                    "image": '/img/map-marker-icon.png'
                },
                hover: {
                    "fill-opacity": 0.8,
                    cursor: 'pointer',
                    fill: '#F39538'
                },
                selected: {
                    fill: '#F39538',
                    "fill-opacity": 1,
                }
            },
            selectedRegions: "g6",
            regionsSelectableOne: false,
            container: $('#map'),
            onMarkerTipShow: function (e, label, code) {
                // console.log(e)
            },
            onMarkerClick: function (e, code) {
                map.removeMarkers([code]);
                map.tip.hide();
            },
            onRegionClick: function (event, code) {
                var dpto = code.substr(1);
                if (dpto.indexOf("x") != -1) {
                    dpto = dpto.substr(0, dpto.indexOf("x"));
                }
                map.clearSelectedRegions();
                if (dpto=='32') {
                    var isla = ['g32','g32x2','g32x3'];
                    if (code=='g32x1') {
                        isla.push('g32x1');
                    } else {
                        isla.splice( isla.indexOf(code), 1 );
                    }
                    map.setSelectedRegions(isla); //to set
                }
                verDatos(dpto);
            }
        });

        map.container.click(function (e) {
            var latLng = map.pointToLatLng(
                    e.pageX - map.container.offset().left,
                    e.pageY - map.container.offset().top
                    ),
                    targetCls = $(e.target).attr('class');

            map.removeAllMarkers();
            if (latLng && (!targetCls || (targetCls && $(e.target).attr('class').indexOf('jvectormap-marker') === -1))) {
                markersCoords[markerIndex] = latLng;
                map.addMarker(markerIndex, {latLng: [latLng.lat, latLng.lng]});
                markerIndex += 1;
            }

        });

    });

    function verDatos(dpto) {
        idDpto = dpto;
        $(".datosDpto").fadeOut(0);
        // Busca los datos
        $.ajax({
            url: "info-asociaciones.php?idm=" + dpto,
        }).done(function (dats) {
            dats = JSON.parse(dats);

            if(dats.informacion != ''){
                $.each(dats,function(i,dat){
                    nmdp = dat.departamento;
                    insts = '';
                    $.each(dat.instituciones,function(j,t){
                       insts += '<div>'+
                                    '<table width="100%" border="0" cellspacing="0" cellpadding="6">'+
                                        '<tbody>'+
                                            '<tr>'+
                                                '<td>'+
                                                    '<p><strong>'+t.nombre+'</strong><br>'+
                                                    t.representante+'<br>'+
                                                    '<em><font color="#333333">'+t.direccion+'<br>'+
                                                    t.telefono+'<br>'+
                                                    t.contacto+'</font></em></p>'+
                                                '</td>'+
                                            '</tr>'+
                                        '</tbody>'+
                                    '</table>'+
                                '</div><br>';
                    });

                    $(".datosDpto").html('<strong>'+nmdp+'</strong><br><br>'+insts);
                });
            }else{
                $(".datosDpto").html('<strong>No se encontraron asociaciones</strong');
            }
            $(".datosDpto").fadeIn();
        });

    }

    function irA(enlace) {

    }

    jQuery(document).ready(function () {

        //       App.setPage(); //Set current page
        //       App.init(); //Initialise plugins and elements
    });
</script>
<script src="js/jquery.superslides.min.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery-jvectormap-2.0.3.min.js"></script>
<script src="js/jquery-jvectormap-co-merc.js"></script>
<script src="js/jquery.maphilight.js"></script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.7";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
</body>
</html>