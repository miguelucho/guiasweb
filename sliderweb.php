<?php
	require_once 'libs/conexion.php';

	$ls_imagenes = '';

	$imagenes = $db
		->where('tipo_sd',2)
		->objectBuilder()->get('slider');

		foreach ($imagenes as $imagen) {
			$img = substr($imagen->imagen_sd,6);
			$ls_imagenes .= '<li>
								<a href="'.($imagen->link_sd != "" ? $imagen->link_sd : "#").'" '.($imagen->link_sd != "" ? "target=\"_blank\"" : "").'><img src="'.$img.'"></a>
							</li>';
		}
?>
<section>
	<div class="Slider">
		<div class="Slider-imagen">
			<div class="wide-container">
				<div id="slides">
					<ul class="slides-container">
						<?php echo $ls_imagenes ?>
					</ul>
				</div>
			</div>
			<div class="Slider-conten">
				<div class="Slider-conten-int"></div>
			</div>
		</div>
	</div>
</section>