<?php
	session_start();
	if(!isset($_SESSION['cpguser'])){
		header('Location: login');
	}
	require_once 'libs/conexion.php';

	if(!isset( $_REQUEST['guia'])){
		header('Location: admin-guias');
	}else{
		$guia = $_REQUEST['guia'];
	}

	$guias = $db
		->where('user_id',$guia)
		->objectBuilder()->get('guias');

	if($db->count == 0){
		header('Location: admin-guias');
	}else{
		$tarjeta = $guias[0]->tarjeta_profesional;
		$RNT = $guias[0]->RNT;
		$descripcion =  $guias[0]->descripcion;
		$suspendido = $guias[0]->suspendido;

		$usuario = $db
			->where('id',$guia)
			->objectBuilder()->get('users2');

		$nombre = $usuario[0]->nombres;
		$apellido = $usuario[0]->apellidos;
		$tipoid = $usuario[0]->tipoid;
		$numeroid = $usuario[0]->numeroid;
		$telefono1 = $usuario[0]->telefono1;
		$telefono2 = $usuario[0]->telefono2;
		$correo = $usuario[0]->username;
		$estado = $usuario[0]->estado;

		$ls_idiomas = '';
		$idiomas = $db
			->objectBuilder()->get('idiomas');

		foreach ($idiomas as $idioma) {
			$guia_idiomas = $db
			   ->where('user_id', $guia)
			   ->where('idioma_id', $idioma->id)
			   ->objectBuilder()->get('user_idiomas');

			$ls_idiomas .= '<option value="'.$idioma->id.'" '.($db->count > 0 ? "selected" : " ").'>'.$idioma->idioma.'</option>';
		}

		$ls_especialidades = '';
		$especialidades = $db
			->objectBuilder()->get('especialidades');

		foreach ($especialidades as $especialidad) {
			$guia_idiomas = $db
			   ->where('guia_id', $guias[0]->id)
			   ->where('especialidade_id', $especialidad->id)
			   ->objectBuilder()->get('especialidades_guias');

			$ls_especialidades .= '<option value="'.$especialidad->id.'" '.($db->count > 0 ? "selected" : " ").'>'.$especialidad->nombre.'</option>';
		}

		$ls_servicios = '';
		$servicios = $db
			->objectBuilder()->get('servicios');

		foreach ($servicios as $servicio) {
			$guia_idiomas = $db
			   ->where('guia_id', $guias[0]->id)
			   ->where('servicio_id', $servicio->id)
			   ->objectBuilder()->get('servicios_guias');

			$ls_servicios .= '<option value="'.$servicio->id.'" '.($db->count > 0 ? "selected" : " ").'>'.$servicio->nombre.'</option>';
		}

		$ls_estado = '';
		$estados =  array('Activo'=>1,'Suspendido'=>2,'Inactivo'=>0);

		foreach ($estados as $key => $value) {
			$ls_estado .= '<option '.($estado == $value ? "selected" : "").' value="'.$value.'">'.$key.'</option>';
		}

		$foto = '';

		$imagenes = $db
			->where('user_id',$guia)
			->objectBuilder()->get('imagenes');


		if($db->count > 0){
			$foto = '../'.$imagenes[0]->url.$imagenes[0]->nombre;
		}

		$ls_tipos = '';
		$tipos = array('Cédula de ciudadanía'=>1,'Cédula Extranjeria'=>2,'Tarjeta de identidad'=>3);
		foreach ($tipos as $key => $value) {
			$ls_tipos .= '<option '.($tipoid == $value ? "selected" : "").' value="'.$value.'">'.$key.'</option>';
		}
	}


?>

<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
		<link rel="shortcut icon" href="images/favicon2.ico" type="image/x-icon" >
		<meta name="keywords" lang="es" content="">
		<meta name="robots" content="All">
		<title>Administrador</title>
		<link rel="stylesheet" href="css/stylesheet.css" />
		<link rel="stylesheet" href="css/load.css" />
		<link rel="stylesheet" href="css/jquery.modal.css" />
		<link rel="stylesheet" href="css/multiple-select.css" />
	</head>
	<body class="">
		<header>
			<?php include "header-top.php";?>
		</header>
		<section>
			<?php include "menu-izq-admin.php";?>
		</section>
		<section>
			<div class="Conten-white2">
				<div class="Conten-white-full">
					<div class="Conten-dashboard">
						<div class="Conten-dashboard-int">
							<div class="Conten-dashboard-int-title">
								<h2 class="Landing_title">GUIAS EDITAR</h2>
							</div>
							<div class="Admin-listar-propietarios">
								<div class="Contenedor-desc">
									<div class="">
										<div class="Cont-formulario">
											<div class="Cont-formulario-int" id="crear-guia">
												<form id="editar-guia">
													<div class="Cont-formulario-int-sec">
														<img src="<?php echo $foto ?>" width="100px">
													</div>
													<div class="Cont-formulario-int-sec">
													</div>
													<div class="Cont-formulario-int-sec">
														<label>Numero de tarjeta</label>
														<input type="text" placeholder="Numero" name="guia[tarjeta]" value="<?php echo $tarjeta ?>">
														<label>RNT</label>
														<input type="text" placeholder="Numero" name="guia[rnt]" value="<?php echo $RNT ?>">
														<label>Apellidos</label>
														<input type="text" placeholder="Apellidos" name="guia[apellidos]" value="<?php echo $apellido ?>">
														<label>Nombre</label>
														<input type="text" placeholder="Nombre" name="guia[nombres]" value="<?php echo $nombre ?>">
														<label>Identificación</label>
														<select name="guia[tipoid]">
															<?php echo $ls_tipos ?>
														</select>
														<label>Número</label>
														<input type="text" placeholder="Número" name="guia[numeroid]" value="<?php echo $numeroid ?>">
														<label>Teléfono</label>
														<input type="text" placeholder="Teléfono" name="guia[telefono1]" value="<?php echo $telefono1 ?>">
														<label>Teléfono 2</label>
														<input type="text" placeholder="Teléfono" name="guia[telefono2]" value="<?php echo $telefono2 ?>">
														<label>Correo</label>
														<input type="text" placeholder="Correo" name="guia[correo]" value="<?php echo $correo ?>">
														<label>Contraseña</label>
														<input type="text" name="guia[contrasena]" value="">
														<input type="hidden" name="guia[idguia]" value="<?php echo $_REQUEST['guia'] ?>">
													</div>
													<div class="Cont-formulario-int-sec">
														<label>Idiomas</label>
														<select class="multiple" multiple="multiple" name="guia[idiomas][]">
															<?php echo $ls_idiomas ?>
														</select>
														<label>Especialidades</label>
														<select class="multiple" multiple="multiple" name="guia[especialidades][]">
															<?php echo $ls_especialidades ?>
														</select>
														<label>Servicios</label>
														<select class="multiple" multiple="multiple" name="guia[servicios][]">
															<?php echo $ls_servicios ?>
														</select>
														<label>Estado</label>
														<select name="guia[estado]">
															<?php echo $ls_estado ?>
														</select>
														<label>Descripción</label>
														<textarea name="guia[descripcion]" style="width: 100%;height: 100px"><?php echo $descripcion ?></textarea>
														<label>Foto</label>
														<input type="file" name="" id="foto" >
													</div>
													<div class="Con-filtro">
														<input type="submit" name="Guardar" value="Guardar">
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/jquery.remodal.js"></script>
	<script src="js/script-menuhome.js"></script>
	<script src="js/jquery.modal.min.js"></script>
	<script src="js/script-menu-slide.js"></script>
	<script src="js/multiple-select.js" type="text/javascript"></script>
	<script src="js/guias-editar.js"></script>
</body>
</html>