$(document).ready(function() {

	$('.fecha').flatpickr({
		locale: 'es',
		enableTime: false,
		dateFormat: "Y-m-d",
	});

	$('.multiple').multipleSelect({
		selectAllText: 'Seleccionar Todos',
		allSelected: 'Todas',
		countSelected: '# de % seleccionados',
		placeholder: 'Seleccione'
	});

	$('#depart_nacimiento').on('change', function() {
		dep = $(this).val();
		$('#ciudad_nacimiento option').not('first').remove();
		$.each($('#lsciudades option'), function(i, dat) {
			ciu_dep = $(this).attr('class').split('-');
			ciu_dep = ciu_dep[1];
			if (dep == ciu_dep) {
				$(this).clone().appendTo('#ciudad_nacimiento');
			}
		});
	});

	$('#depart_residencia').on('change', function() {
		dep = $(this).val();
		$('#ciudad_residencia option').not('first').remove();
		$.each($('#lsciudades option'), function(i, dat) {
			ciu_dep = $(this).attr('class').split('-');
			ciu_dep = ciu_dep[1];
			if (dep == ciu_dep) {
				$(this).clone().appendTo('#ciudad_residencia');
			}
		});
	});

	$('#form-tarjeta').on('submit', function(e) {
		e.preventDefault();
		loader();
		var data = new FormData();
		var form_data = $('#form-tarjeta').serializeArray();
		$.each(form_data, function(key, input) {
			data.append(input.name, input.value);
		});

		foto = document.getElementById('foto');
		foto = foto.files[0];
		// foto = $('#foto')[0].files;
		data.append("foto", foto);
		cedula = document.getElementById('scan_cedula');
		cedula = cedula.files[0];
		// cedula = $('#scan_cedula')[0].files;
		data.append("cedula", cedula);
		certificado = document.getElementById('scan_certificado');
		certificado = certificado.files[0];
		// certificado = $('#scan_certificado')[0].files;
		data.append("certificado", certificado);
		titulo = document.getElementById('scan_titulo');
		titulo = titulo.files[0];
		// titulo = $('#scan_titulo')[0].files;
		data.append("titulo", titulo);

		$.ajax({
			url: 'libs/registro-tarjeta.php',
			type: 'POST',
			contentType: false,
			data: data,
			processData: false,
			cache: false,
			dataType: 'json',
			success: function(data) {
				$('#fondo').remove();
				if (data.status == true) {
					$('input').not('[type=submit] ,[type=checkbox]').val('');
					nmensaje('success', 'Correcto', 'Solicitud Creada');
				} else if (data.status == false) {
					nmensaje('error', 'Error', data.motivo);
				}
			}
		});
	});


	function loader() {
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">' + '<div class="loader-inner ball-beat">' + '<div></div>' + '<div></div>' + '<div></div>' + '</div>' + '</div>');
		setTimeout(function() {
			$('#fondo').fadeIn('fast');
		}, 400);
	}

	function nmensaje(tipo, titulo, mensaje) {
		modal({
			type: tipo,
			title: titulo,
			text: mensaje,
			size: 'small',
			closeClick: false,
			animate: true,
		});
	}
});