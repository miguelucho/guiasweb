$(document).ready(function() {

	$('#enviar-notificacion').on('submit', function(e) {
		e.preventDefault();
		loader();
		$.post('libs/acc_notificaciones', function(data) {
			$('#fondo').remove();
			if (data.status == true) {
				nmensaje('success', 'Correcto', data.motivo);
				$('.total').html('Notificaciones enviadas: ' + data.enviadas);
			} else {
				nmensaje('error', '', data.motivo);
			}
		}, 'json');
	});

	function loader() {
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">' +
			'<div class="loader-inner ball-beat">' +
			'<div></div>' +
			'<div></div>' +
			'<div></div>' +
			'</div>' +
			'</div>');
		setTimeout(function() {
			$('#fondo').fadeIn('fast');
		}, 400);
	}

	function nmensaje(tipo, titulo, mensaje) {
		modal({
			type: tipo,
			title: titulo,
			text: mensaje,
			size: 'small',
			closeClick: false,
			animate: true,
		});
	}
});