$(document).ready(function() {
	guia = '%';

	calificacion_listar(1, guia);

	function calificacion_listar(pagina) {
		data = [];
		data.push({
			name: 'guia[pagina]',
			value: pagina
		}, {
			name: 'guia[guia]',
			value: guia
		}, {
			name: 'accion',
			value: 'lista-calificaciones'
		});
		$('.load').fadeIn(0);
		$('#ls-calificacion').empty().fadeOut(0);
		$.post('libs/acc_informes', data, function(data) {
			$('.load').fadeOut(0);
			$('#ls-calificacion').html(data.listado);
			$('#paginacion').html(data.paginacion);
			$('#ls-calificacion').fadeIn();
		}, 'json');
	}

	$('body').on('click', '.mpag', function() {
		calificacion_listar($(this).prop('id'));
	});

	$('#filtrar').on('submit', function(e) {
		e.preventDefault();
		guia = $('#guia').val();
		usuarios_listar(1, guia);
	});

});