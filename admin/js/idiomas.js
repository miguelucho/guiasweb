$(document).ready(function() {
	id_idioma = 0;
	idiomas_listar(1);

	function idiomas_listar(pagina) {
		data = [];
		data.push({
			name: 'idioma[pagina]',
			value: pagina
		}, {
			name: 'accion',
			value: 'lista-idiomas'
		});
		$('.load').fadeIn(0);
		$('#listado').empty().fadeOut(0);
		$.post('libs/acc_idiomas', data, function(data) {
			$('.load').fadeOut(0);
			$('#listado').html(data.listado);
			$('#paginacion').html(data.paginacion);
			$('#listado').fadeIn();
		}, 'json');
	}
	$('body').on('click', '.mpag', function() {
		idiomas_listar($(this).prop('id'));
	});

	$('#crear-idioma').on('submit', function(e) {
		e.preventDefault();
		loader();
		data = $(this).serializeArray();
		if (id_idioma == 0) {
			data.push({
				name: 'accion',
				value: 'crear-idiomas'
			});
		} else {
			data.push({
				name: 'accion',
				value: 'editar-idiomas'
			}, {
				name: 'idioma[id_idioma]',
				value: id_idioma
			});
		}
		$.post('libs/acc_idiomas', data, function(data) {
			$('#fondo').remove();
			if (data.status == true) {
				$('input').not('[type=submit]').val('');
				id_idioma = 0;
				nmensaje('success', 'Correcto', data.motivo);
				$('.listado-existen').show();
				idiomas_listar(1);
			} else {
				nmensaje('error', 'Error', data.motivo);
			}
		}, 'json');
	});

	$('body').on('click', '.editar', function() {
		idioma = $(this).prop('id').split('-');
		id_idioma = idioma[1];
		$('.nmidioma').val(idioma[2]);
		$('.listado-existen').hide();
	});

	$('body').on('click', '.eliminar', function() {
		idioma = $(this).prop('id').split('-');
		id_idioma = idioma[1];
		modal({
			type: 'confirm',
			title: 'Eliminar el idioma',
			text: 'Desea eliminar el idioma ?',
			size: 'small',
			callback: function(result) {
				if (result === true) {
					loader();
					$.post('libs/acc_idiomas', {
						'accion': 'eliminar-idiomas',
						'idioma[id_idioma]': id_idioma
					}, function(data) {
						$('#fondo').remove();
						if (data.status == true) {
							id_idioma = 0;
							nmensaje('success', 'Correcto', data.motivo);
							idiomas_listar(1);
						} else {
							nmensaje('error', 'Error', data.motivo);
						}
					}, 'json');
				} else {
					id_idioma = 0;
				}
			},
			closeClick: false,
			animate: true,
			buttonText: {
				yes: 'Confirmar',
				cancel: 'Cancelar'
			}
		});
	});

	function loader() {
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">' +
			'<div class="loader-inner ball-beat">' +
			'<div></div>' +
			'<div></div>' +
			'<div></div>' +
			'</div>' +
			'</div>');
		setTimeout(function() {
			$('#fondo').fadeIn('fast');
		}, 400);
	}

	function nmensaje(tipo, titulo, mensaje) {
		modal({
			type: tipo,
			title: titulo,
			text: mensaje,
			size: 'small',
			closeClick: false,
			animate: true,
		});
	}
});