$(document).ready(function() {
	guia = '%';
	rnt = '%';

	guias_listar(1);

	$('.tabs li').on('click', function() {
		$('.tabs a').removeClass('Vin-active');
		$('a', this).addClass('Vin-active');
		switch ($(this).index()) {
			// case 0:
			// 	$('#crear-guia').fadeIn();
			// 	$('#listar-guia').fadeOut(0);
			// 	$('#listar-pendientes').fadeOut(0);
			// 	break;
			case 0:
				$('#crear-guia').fadeOut(0);
				$('#listar-pendientes').fadeOut(0);
				$('#listar-guia').fadeIn();
				guias_listar(1);
				break;
			case 1:
				$('#crear-guia').fadeOut(0);
				$('#listar-guia').fadeOut(0);
				$('#listar-pendientes').fadeIn();
				pendientes_listar(1);
				break;
		}
	});

	function guias_listar(pagina) {
		data = [];
		data.push({
			name: 'guia[pagina]',
			value: pagina
		}, {
			name: 'guia[guia]',
			value: guia
		}, {
			name: 'guia[rnt]',
			value: rnt
		}, {
			name: 'accion',
			value: 'lista-guias'
		});
		$('.load').fadeIn(0);
		$('#ls-usuarios').empty().fadeOut(0);
		$.post('libs/acc_guias', data, function(data) {
			$('.load').fadeOut(0);
			$('#ls-usuarios').html(data.listado);
			$('#listar-guia #paginacion').html(data.paginacion);
			$('#ls-usuarios').fadeIn();
		}, 'json');
	}
	$('body').on('click', '#listar-guia .mpag', function() {
		guias_listar($(this).prop('id'));
	});

	$('#filtrar').on('submit', function(e) {
		e.preventDefault();
		guia = $('#guia').val();
		rnt = $('#rnt').val();
		guias_listar(1);
	});

	function pendientes_listar(pagina) {
		data = [];
		data.push({
			name: 'guia[pagina]',
			value: pagina
		}, {
			name: 'accion',
			value: 'lista-pendientes'
		});
		$('.load').fadeIn(0);
		$('#ls-pendientes').empty().fadeOut(0);
		$.post('libs/acc_guias', data, function(data) {
			$('.load').fadeOut(0);
			$('#ls-pendientes').html(data.listado);
			$('#listar-pendientes #paginacion').html(data.paginacion);
			$('#ls-pendientes').fadeIn();
		}, 'json');
	}
	$('body').on('click', '#listar-pendientes .mpag', function() {
		pendientes_listar($(this).prop('id'));
	});

	$('.multiple').multipleSelect({
		selectAllText: 'Seleccionar Todas',
		allSelected: 'Todas',
		countSelected: '# de % seleccionados',
		placeholder: 'Seleccione'
	});

	// $('body').on('click', '.editar', function() {

	// });

});