$(document).ready(function() {

	listar(1);


	imgmuestra_w = 'images/slider_add_w.jpg';
	var formData = new FormData();

	var $uploadCrop,
		tempFilename,
		rawImg;

	function readFile(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function(e) {
				rawImg = e.target.result;
			};
			reader.readAsDataURL(input.files[0]);
		} else {
			nmensaje('error', 'Error', 'Su navegador no soporta FileReader API');
		}
	}

	$('.item-img-w').on('change', function() {
		tempFilename = $(this).val().replace(/C:\\fakepath\\/i, '');
		tempFilename = tempFilename.substr(0, tempFilename.lastIndexOf('.')) || tempFilename;
		readFile(this);
		modal({
			type: 'info',
			title: 'Recortar Imagen',
			text: '<div class="modal-body">' +
				'<div id="upload-demo-w" class="center-block"></div>' +
				'</div>',
			size: 'large',
			callback: function(result) {
				if (result === true) {
					$uploadCrop.croppie('result', {
						type: 'base64',
						format: 'jpeg',
						size: {
							width: 1261,
							height: 435
						}
					}).then(function(resp) {
						$('#item-img-output-w').attr('src', resp);
						formData.delete('img-slide-w');
						formData.delete('img-slide-nombre-w');
						formData.append('img-slide-w', resp);
						formData.append('img-slide-nombre-w', tempFilename);
					});
				}
			},
			onShow: function(result) {
				$uploadCrop = $('#upload-demo-w').croppie({
					viewport: {
						width: 1261,
						height: 435,
					},
					enforceBoundary: false,
					enableExif: true
				});
				$uploadCrop.croppie('bind', {
					url: rawImg
				}).then(function() {
					console.log('jQuery bind complete');
				});
			},
			closeClick: false,
			animate: true,
			buttonText: {
				yes: 'Confirmar',
				cancel: 'Cancelar'
			}
		});
	});

	$('#add-banner').on('submit', function(e) {
		e.preventDefault();
		if ($('.item-img-w').val() != '') {
			addimage_w();
		} else {
			nmensaje('error', 'Error', 'La Imagen es requerida');
		}
	});

	function addimage_w() {
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader"></div>');
		setTimeout(function() {
			$('#fondo').fadeIn('fast');
		}, 400);
		data = $('#add-slide-w').serializeArray();

		$.each(data, function(key, input) {
			formData.append(input.name, input.value);
		});

		formData.append('accion', 'nuevo-banner');

		$.ajax('libs/acc_banner', {
			method: "POST",
			data: formData,
			cache: false,
			processData: false,
			contentType: false,
			dataType: 'json',
			success: function(data) {
				$('#fondo').remove();
				if (data.status == true) {
					$('input', '#add-slide-w').not('[type=radio],[type=submit]').val('');
					$('.item-img-w').val('');
					$('#listado-imagenes').fadeIn();
					$('#item-img-output-w').attr('src', imgmuestra_w);
					listar(1);
					formData = new FormData();
					nmensaje('success', 'Correcto', 'Imagen ingresada');
				} else {
					nmensaje('error', 'Error', data.motivo);
				}
			}
		});
	}

	function listar(pagina) {
		$('.load-w').fadeIn(0);
		$('#ls-imagenes').empty().fadeOut(0);
		data = 'accion=listar-banner&pagina=' + pagina;
		$.post('libs/acc_banner', data, function(data) {
			$('#ls-imagenes').html(data.listado);
			$('.load-w').fadeOut(0);
			$('#ls-imagenes').fadeIn();
		}, 'json');
	}

	$('body').on('click', '.eliminar-w', function() {
		id_imagen = $(this).prop('id').split('-');
		id_imagen = id_imagen[1];
		modal({
			type: 'confirm',
			title: 'Eliminar Imagen',
			text: 'Desea eliminar la imagen ?',
			size: 'small',
			callback: function(result) {
				if (result === true) {
					$('#fondo').remove();
					$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
					$('#fondo').append('<div class="loader"></div>');
					setTimeout(function() {
						$('#fondo').fadeIn('fast');
					}, 400);
					$.post('libs/acc_banner', {
						'accion': 'eliminar-banner',
						'idimagen': id_imagen,
					}, function(data) {
						if (data.status == true) {
							$('#fondo').remove();
							id_imagen = 0;
							listar(1);
						} else {
							nmensaje('error', 'Error', data.status);
						}
					}, 'json');
				} else {

				}
			},
			closeClick: false,
			animate: true,
			theme: 'xenon',
			buttonText: {
				yes: 'Confirmar',
				cancel: 'Cancelar'
			}
		});
	});

	function nmensaje(tipo, titulo, mensaje) {
		modal({
			type: tipo,
			title: titulo,
			text: mensaje,
			size: 'small',
			closeClick: false,
			animate: true,
		});
	}

});