$(document).ready(function() {
	id_espe = 0;
	especialidades_listar(1);

	function especialidades_listar(pagina) {
		data = [];
		data.push({
			name: 'especialidad[pagina]',
			value: pagina
		}, {
			name: 'accion',
			value: 'lista-especialidades'
		});
		$('.load').fadeIn(0);
		$('#listado').empty().fadeOut(0);
		$.post('libs/acc_especialidades', data, function(data) {
			$('.load').fadeOut(0);
			$('#listado').html(data.listado);
			$('#paginacion').html(data.paginacion);
			$('#listado').fadeIn();
		}, 'json');
	}
	$('body').on('click', '.mpag', function() {
		especialidades_listar($(this).prop('id'));
	});

	$('#crear-especialidad').on('submit', function(e) {
		e.preventDefault();
		loader();
		data = $(this).serializeArray();
		if (id_espe == 0) {
			data.push({
				name: 'accion',
				value: 'crear-especialidades'
			});
		} else {
			data.push({
				name: 'accion',
				value: 'editar-especialidades'
			}, {
				name: 'especialidad[id_espe]',
				value: id_espe
			});
		}
		$.post('libs/acc_especialidades', data, function(data) {
			$('#fondo').remove();
			if (data.status == true) {
				$('input').not('[type=submit]').val('');
				id_espe = 0;
				nmensaje('success', 'Correcto', data.motivo);
				$('.listado-existen').show();
				especialidades_listar(1);
			} else {
				nmensaje('error', 'Error', data.motivo);
			}
		}, 'json');
	});

	$('body').on('click', '.editar', function() {
		especi = $(this).prop('id').split('-');
		id_espe = especi[1];
		$('.nmespecialidad').val(especi[2]);
		$('.listado-existen').hide();
	});

	$('body').on('click', '.eliminar', function() {
		especi = $(this).prop('id').split('-');
		id_espe = especi[1];
		modal({
			type: 'confirm',
			title: 'Eliminar la especialidad',
			text: 'Desea eliminar la especialidad ?',
			size: 'small',
			callback: function(result) {
				if (result === true) {
					loader();
					$.post('libs/acc_especialidades', {
						'accion': 'eliminar-especialidades',
						'especialidad[id_espe]': id_espe
					}, function(data) {
						$('#fondo').remove();
						if (data.status == true) {
							id_espe = 0;
							nmensaje('success', 'Correcto', data.motivo);
							especialidades_listar(1);
						} else {
							nmensaje('error', 'Error', data.motivo);
						}
					}, 'json');
				} else {
					id_espe = 0;
				}
			},
			closeClick: false,
			animate: true,
			buttonText: {
				yes: 'Confirmar',
				cancel: 'Cancelar'
			}
		});
	});

	function loader() {
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">' +
			'<div class="loader-inner ball-beat">' +
			'<div></div>' +
			'<div></div>' +
			'<div></div>' +
			'</div>' +
			'</div>');
		setTimeout(function() {
			$('#fondo').fadeIn('fast');
		}, 400);
	}

	function nmensaje(tipo, titulo, mensaje) {
		modal({
			type: tipo,
			title: titulo,
			text: mensaje,
			size: 'small',
			closeClick: false,
			animate: true,
		});
	}
});