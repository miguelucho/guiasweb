$(document).ready(function() {
	turista = 0;

	usuarios_listar(1, turista);

	function usuarios_listar(pagina) {
		data = [];
		data.push({
			name: 'guia[pagina]',
			value: pagina
		}, {
			name: 'guia[buscar]',
			value: turista
		}, {
			name: 'accion',
			value: 'lista-turistas'
		});
		$('.load').fadeIn(0);
		$('#ls-usuarios').empty().fadeOut(0);
		$.post('libs/acc_informes', data, function(data) {
			$('.load').fadeOut(0);
			$('#ls-usuarios').html(data.listado);
			$('#paginacion').html(data.paginacion);
			$('#ls-usuarios').fadeIn();
		}, 'json');
	}

	$('body').on('click', '.mpag', function() {
		usuarios_listar($(this).prop('id'));
	});

	$('#filtrar').on('submit', function(e) {
		e.preventDefault();
		turista = $('#bsq').val();
		usuarios_listar(1, turista);
	});

});