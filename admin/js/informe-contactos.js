$(document).ready(function() {

	contactos_listar();

	function contactos_listar(pagina) {
		data = [];
		data.push({
			name: 'guia[pagina]',
			value: pagina
		}, {
			name: 'accion',
			value: 'lista-contactos'
		});
		$('.load').fadeIn(0);
		$('#ls-contactos').empty().fadeOut(0);
		$.post('libs/acc_informes', data, function(data) {
			$('.load').fadeOut(0);
			$('#ls-contactos').html(data.listado);
			$('#paginacion').html(data.paginacion);
			$('#ls-contactos').fadeIn();
		}, 'json');
	}
	$('body').on('click', '.mpag', function() {
		contactos_listar($(this).prop('id'));
	});

});