$(document).ready(function() {

	$('#aprobar').on('click', function() {
		if ($(this).is(':checked')) {
			if ($('#tarjeta').val() == '' || $('#rnt').val() == '' || $('#contrasena').val() == '') {
				nmensaje('error', 'Error', 'Al aprobar, debe ingresar el N° de tarjeta, RNT y contraseña');
				$(this).prop('checked', false);
			}
		}
	});

	$('#pendiente-guia').on('submit', function(e) {
		e.preventDefault();
		if ($('#aprobar').is(':checked')) {
			if ($('#tarjeta').val() == '' || $('#rnt').val() == '' || $('#contrasena').val() == '') {
				nmensaje('error', 'Error', 'Al aprobar, debe ingresar el N° de tarjeta, RNT y contraseña');
				$('#aprobar').prop('checked', false);
			} else {
				loader();
				data = $(this).serializeArray();
				data.push({
					name: "accion",
					value: 'aprobar-guia'
				});

				$.post('libs/acc_guias.php', data, function(data) {
					$('#fondo').remove();
					if (data.status == true) {
						nmensaje('success', 'Correcto', 'Guia aprobado');
						setTimeout(function() {
							window.location.href = 'admin-guias';
						}, 1000);
					} else if (data.status == false) {
						nmensaje('error', 'Error', data.motivo);
					}
				}, 'json');
			}
		} else {
			window.location.href = 'admin-guias';
		}
	});


	function loader() {
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">' + '<div class="loader-inner ball-beat">' + '<div></div>' + '<div></div>' + '<div></div>' + '</div>' + '</div>');
		setTimeout(function() {
			$('#fondo').fadeIn('fast');
		}, 400);
	}

	function nmensaje(tipo, titulo, mensaje) {
		modal({
			type: tipo,
			title: titulo,
			text: mensaje,
			size: 'small',
			closeClick: false,
			animate: true,
		});
	}


});