function nnotifica(op) {
    n_plantilla = $('<div class="cj_notificacion"><div class="flag note"><div class="flag__image note__icon"><i class="fa fa-icon"></i></div><div class="flag__body note__text"></div><a href="#" class="note__close"><i class="fa fa-times"></i></a></div></div>');
    n_tipo = '.note';
    n_icon = '.fa-icon';
    n_contenido = '.note__text';
    n_cerrar = '.note__close';

    switch (op.tipo) {
        case 'normal':
            n_plantilla.find(n_tipo).addClass('note--secondary');
            n_plantilla.find(n_icon).addClass('fa-comment');
            n_plantilla.find(n_contenido).html(op.texto);
            break;
        case 'correcto':
            n_plantilla.find(n_tipo).addClass('note--success');
            n_plantilla.find(n_icon).addClass('fa-check');
            n_plantilla.find(n_contenido).html(op.texto);
            break;
        case 'alerta':
            n_plantilla.find(n_tipo).addClass('note--warning');
            n_plantilla.find(n_icon).addClass('fa-exclamation');
            n_plantilla.find(n_contenido).html(op.texto);
            break;
        case 'error':
            n_plantilla.find(n_tipo).addClass('note--error');
            n_plantilla.find(n_icon).addClass('fa-times');
            n_plantilla.find(n_contenido).html(op.texto);
            break;
        case 'informativo':
            n_plantilla.find(n_tipo).addClass('note--info');
            n_plantilla.find(n_icon).addClass('fa-info');
            n_plantilla.find(n_contenido).html(op.texto);
            break;
    }

    $('body').append(n_plantilla);
    setTimeout(function() {
        $('.note__close').click();
    }, (op.duracion * 1000));

    $('body').on('click', '.note__close', function() {
        $(this).parent()
            .animate({
                opacity: 0
            }, 250, function() {
                $(this).animate({
                    marginBottom: 0
                }, 250).children().animate({
                    padding: 0
                }, 250).wrapInner("<div />").children().slideUp(250, function() {
                    $(this).closest(".note").remove();
                    $('.cj_notificacion').remove();
                });
            });
    });

}