$(document).ready(function() {

	slider1_listar(1);

	$('.tabs li').on('click', function() {
		$('.tabs a').removeClass('Vin-active');
		$('a', this).addClass('Vin-active');
		switch ($(this).index()) {
			case 0:
				$('.sl-web').fadeIn();
				$('.sl-movil').fadeOut(0);
				slider1_listar(1);
				break;
			case 1:
				$('.sl-movil').fadeIn(0);
				$('.sl-web').fadeOut();
				slider2_listar(1);
				break;
		}
	});

	////// web

	imgmuestra_w = 'images/slider_add_w.jpg';
	var formData = new FormData();

	var $uploadCrop,
		tempFilename,
		rawImg;

	function readFile(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function(e) {
				rawImg = e.target.result;
			};
			reader.readAsDataURL(input.files[0]);
		} else {
			nmensaje('error', 'Error', 'Su navegador no soporta FileReader API');
		}
	}

	$('.item-img-w').on('change', function() {
		tempFilename = $(this).val().replace(/C:\\fakepath\\/i, '');
		tempFilename = tempFilename.substr(0, tempFilename.lastIndexOf('.')) || tempFilename;
		readFile(this);
		modal({
			type: 'info',
			title: 'Recortar Imagen',
			text: '<div class="modal-body">' +
				'<div id="upload-demo-w" class="center-block"></div>' +
				'</div>',
			size: 'large',
			callback: function(result) {
				if (result === true) {
					$uploadCrop.croppie('result', {
						type: 'base64',
						format: 'jpeg',
						size: {
							width: 1261,
							height: 435
						}
					}).then(function(resp) {
						$('#item-img-output-w').attr('src', resp);
						formData.delete('img-slide-w');
						formData.delete('img-slide-nombre-w');
						formData.append('img-slide-w', resp);
						formData.append('img-slide-nombre-w', tempFilename);
					});
				}
			},
			onShow: function(result) {
				$uploadCrop = $('#upload-demo-w').croppie({
					viewport: {
						width: 1261,
						height: 435,
					},
					enforceBoundary: false,
					enableExif: true
				});
				$uploadCrop.croppie('bind', {
					url: rawImg
				}).then(function() {
					console.log('jQuery bind complete');
				});
			},
			closeClick: false,
			animate: true,
			buttonText: {
				yes: 'Confirmar',
				cancel: 'Cancelar'
			}
		});
	});

	$('#add-slide-w').on('submit', function(e) {
		e.preventDefault();
		if ($('.item-img-w').val() != '') {
			addimage_w();
		} else {
			nmensaje('error', 'Error', 'La Imagen es requerida');
		}
	});

	function addimage_w() {
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader"></div>');
		setTimeout(function() {
			$('#fondo').fadeIn('fast');
		}, 400);
		data = $('#add-slide-w').serializeArray();

		$.each(data, function(key, input) {
			formData.append(input.name, input.value);
		});

		formData.append('link', $('#link-web').val());
		formData.append('accion', 'nueva-slider-web');

		$.ajax('libs/acc_slider', {
			method: "POST",
			data: formData,
			cache: false,
			processData: false,
			contentType: false,
			dataType: 'json',
			success: function(data) {
				$('#fondo').remove();
				if (data.status == true) {
					$('input', '#add-slide-w').not('[type=radio],[type=submit]').val('');
					$('.item-img-w').val('');
					$('#listado-imagenes-w').fadeIn();
					$('#item-img-output-w').attr('src', imgmuestra_m);
					slider1_listar(1);
					formData = new FormData();
					$('#link-web').val('');
					nmensaje('success', 'Correcto', 'Imagen ingresada');
				} else {
					nmensaje('error', 'Error', data.motivo);
				}
			}
		});
	}

	function slider1_listar(pagina) {
		$('.load-w').fadeIn(0);
		$('#ls-imagenes-w').empty().fadeOut(0);
		data = 'accion=listar-slider-web&pagina=' + pagina;
		$.post('libs/acc_slider', data, function(data) {
			$('#ls-imagenes-w').html(data.listado);
			$('.load-w').fadeOut(0);
			$('#ls-imagenes-w').fadeIn();
		}, 'json');
	}

	$('body').on('click', '.eliminar-w', function() {
		id_imagen = $(this).prop('id').split('-');
		id_imagen = id_imagen[1];
		modal({
			type: 'confirm',
			title: 'Eliminar Imagen',
			text: 'Desea eliminar la imagen ?',
			size: 'small',
			callback: function(result) {
				if (result === true) {
					$('#fondo').remove();
					$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
					$('#fondo').append('<div class="loader"></div>');
					setTimeout(function() {
						$('#fondo').fadeIn('fast');
					}, 400);
					$.post('libs/acc_slider', {
						'accion': 'eliminar-slider-web',
						'idimagen': id_imagen,
					}, function(data) {
						if (data.status == true) {
							$('#fondo').remove();
							id_imagen = 0;
							slider1_listar(1);
						} else {
							nmensaje('error', 'Error', data.status);
						}
					}, 'json');
				} else {

				}
			},
			closeClick: false,
			animate: true,
			theme: 'xenon',
			buttonText: {
				yes: 'Confirmar',
				cancel: 'Cancelar'
			}
		});
	});

	//////////////  movil
	imgmuestra_m = 'images/slider_add.jpg';
	formData_m = new FormData();

	var uploadCrop_m,
		tempFilename_m,
		rawImg_m;

	function readFile_m(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function(e) {
				rawImg_m = e.target.result;
			};
			reader.readAsDataURL(input.files[0]);
		} else {
			nmensaje('error', 'Error', 'Su navegador no soporta FileReader API');
		}
	}

	$('.item-img-m').on('change', function() {
		tempFilename_m = $(this).val().replace(/C:\\fakepath\\/i, '');
		tempFilename_m = tempFilename_m.substr(0, tempFilename_m.lastIndexOf('.')) || tempFilename_m;
		readFile_m(this);
		modal({
			type: 'info',
			title: 'Recortar Imagen',
			text: '<div class="modal-body">' +
				'<div id="upload-demo" class="center-block"></div>' +
				'</div>',
			size: 'large',
			callback: function(result) {
				if (result === true) {
					uploadCrop_m.croppie('result', {
						type: 'base64',
						format: 'jpeg',
						size: {
							width: 500,
							height: 250
						}
					}).then(function(resp) {
						$('#item-img-output-m').attr('src', resp);
						formData_m.delete('img-slide-m');
						formData_m.delete('img-slide-nombre-m');
						formData_m.append('img-slide-m', resp);
						formData_m.append('img-slide-nombre-m', tempFilename_m);
					});
				}
			},
			onShow: function(result) {
				uploadCrop_m = $('#upload-demo').croppie({
					viewport: {
						width: 500,
						height: 250,
					},
					enforceBoundary: false,
					enableExif: true
				});
				uploadCrop_m.croppie('bind', {
					url: rawImg_m
				}).then(function() {
					console.log('jQuery bind complete');
				});
			},
			closeClick: false,
			animate: true,
			buttonText: {
				yes: 'Confirmar',
				cancel: 'Cancelar'
			}
		});
	});

	$('#add-slide-m').on('submit', function(e) {
		e.preventDefault();
		if ($('.item-img-m').val() != '') {
			addimage_m();
		} else {
			nmensaje('error', 'Error', 'La Imagen es requerida');
		}
	});

	function addimage_m() {
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader"></div>');
		setTimeout(function() {
			$('#fondo').fadeIn('fast');
		}, 400);
		data = $('#add-slide-m').serializeArray();

		$.each(data, function(key, input) {
			formData_m.append(input.name, input.value);
		});

		formData_m.append('link', $('#link-mov').val());
		formData_m.append('accion', 'nueva-slider-min');

		$.ajax('libs/acc_slider', {
			method: "POST",
			data: formData_m,
			cache: false,
			processData: false,
			contentType: false,
			dataType: 'json',
			success: function(data) {
				$('#fondo').remove();
				if (data.status == true) {
					$('input', '#add-slide-m').not('[type=radio],[type=submit]').val('');
					$('.item-img-m').val('');
					$('#listado-imagenes-m').fadeIn();
					$('#item-img-output-m').attr('src', imgmuestra_m);
					slider2_listar(1);
					formData_m = new FormData();
					$('#link-mov').val('');
					nmensaje('success', 'Correcto', 'Imagen ingresada');
				} else {
					nmensaje('error', 'Error', data.motivo);
				}
			}
		});
	}

	function slider2_listar(pagina) {
		$('.load-m').fadeIn(0);
		$('#ls-imagenes-m').empty().fadeOut(0);
		data = 'accion=listar-slider-min&pagina=' + pagina;
		$.post('libs/acc_slider', data, function(data) {
			$('#ls-imagenes-m').html(data.listado);
			$('.load-m').fadeOut(0);
			$('#ls-imagenes-m').fadeIn();
		}, 'json');
	}

	$('body').on('click', '.eliminar-m', function() {
		id_imagen = $(this).prop('id').split('-');
		id_imagen = id_imagen[1];
		modal({
			type: 'confirm',
			title: 'Eliminar Imagen',
			text: 'Desea eliminar la imagen ?',
			size: 'small',
			callback: function(result) {
				if (result === true) {
					$('#fondo').remove();
					$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
					$('#fondo').append('<div class="loader"></div>');
					setTimeout(function() {
						$('#fondo').fadeIn('fast');
					}, 400);
					$.post('libs/acc_slider', {
						'accion': 'eliminar-slider-min',
						'idimagen': id_imagen,
					}, function(data) {
						if (data.status == true) {
							$('#fondo').remove();
							id_imagen = 0;
							slider2_listar(1);
						} else {
							nmensaje('error', 'Error', data.status);
						}
					}, 'json');
				} else {

				}
			},
			closeClick: false,
			animate: true,
			theme: 'xenon',
			buttonText: {
				yes: 'Confirmar',
				cancel: 'Cancelar'
			}
		});
	});

	function nmensaje(tipo, titulo, mensaje) {
		modal({
			type: tipo,
			title: titulo,
			text: mensaje,
			size: 'small',
			closeClick: false,
			animate: true,
		});
	}

});