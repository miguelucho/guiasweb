$(document).ready(function() {
	$('#login').on('submit', function(e) {
		e.preventDefault();
		loader();
		data = $(this).serializeArray();
		$.post('libs/acc_login', data, function(data) {
			$('#fondo').remove();
			if (data.status == true) {
				window.location.href = data.redirect;
			} else {
				nmensaje('error', 'Error', data.motivo);
			}
		}, 'json');
	});

	function loader() {
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">' +
			'<div class="loader-inner ball-beat">' +
			'<div></div>' +
			'<div></div>' +
			'<div></div>' +
			'</div>' +
			'</div>');
		setTimeout(function() {
			$('#fondo').fadeIn('fast');
		}, 400);
	}

	function nmensaje(tipo, titulo, mensaje) {
		modal({
			type: tipo,
			title: titulo,
			text: mensaje,
			size: 'small',
			closeClick: false,
			animate: true,
		});
	}
});