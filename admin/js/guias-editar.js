$(document).ready(function() {

	$('.multiple').multipleSelect({
		selectAllText: 'Seleccionar Todas',
		allSelected: 'Todas',
		countSelected: '# de % seleccionados',
		placeholder: 'Seleccione'
	});

	$('#editar-guia').on('submit', function(e) {
		e.preventDefault();
		loader();
		var data = new FormData();
		var form_data = $('#editar-guia').serializeArray();
		$.each(form_data, function(key, input) {
			data.append(input.name, input.value);
		});

		foto = document.getElementById('foto');
		foto = foto.files[0];
		data.append("foto", foto);

		data.append("accion", 'editar-guia');

		$.ajax({
			url: 'libs/acc_guias.php',
			type: 'POST',
			contentType: false,
			data: data,
			processData: false,
			cache: false,
			dataType: 'json',
			success: function(data) {
				$('#fondo').remove();
				if (data.status == true) {
					nmensaje('success', 'Correcto', 'Información editada');
					setTimeout(function() {
						location.reload();
					}, 1000);
				} else if (data.status == false) {
					nmensaje('error', 'Error', data.motivo);
				}
			}
		});
	});


	function loader() {
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">' + '<div class="loader-inner ball-beat">' + '<div></div>' + '<div></div>' + '<div></div>' + '</div>' + '</div>');
		setTimeout(function() {
			$('#fondo').fadeIn('fast');
		}, 400);
	}

	function nmensaje(tipo, titulo, mensaje) {
		modal({
			type: tipo,
			title: titulo,
			text: mensaje,
			size: 'small',
			closeClick: false,
			animate: true,
		});
	}


});