$(document).ready(function() {
	id_servicio = 0;
	servicios_listar(1);

	function servicios_listar(pagina) {
		data = [];
		data.push({
			name: 'servicio[pagina]',
			value: pagina
		}, {
			name: 'accion',
			value: 'lista-servicios'
		});
		$('.load').fadeIn(0);
		$('#listado').empty().fadeOut(0);
		$.post('libs/acc_servicios', data, function(data) {
			$('.load').fadeOut(0);
			$('#listado').html(data.listado);
			$('#paginacion').html(data.paginacion);
			$('#listado').fadeIn();
		}, 'json');
	}
	$('body').on('click', '.mpag', function() {
		servicios_listar($(this).prop('id'));
	});

	$('#crear-servicio').on('submit', function(e) {
		e.preventDefault();
		loader();
		data = $(this).serializeArray();
		if (id_servicio == 0) {
			data.push({
				name: 'accion',
				value: 'crear-servicios'
			});
		} else {
			data.push({
				name: 'accion',
				value: 'editar-servicios'
			}, {
				name: 'servicio[id_servicio]',
				value: id_servicio
			});
		}
		$.post('libs/acc_servicios', data, function(data) {
			$('#fondo').remove();
			if (data.status == true) {
				$('input').not('[type=submit]').val('');
				id_servicio = 0;
				nmensaje('success', 'Correcto', data.motivo);
				$('.listado-existen').show();
				servicios_listar(1);
			} else {
				nmensaje('error', 'Error', data.motivo);
			}
		}, 'json');
	});

	$('body').on('click', '.editar', function() {
		servicio = $(this).prop('id').split('-');
		id_servicio = servicio[1];
		$('.nmservicio').val(servicio[2]);
		$('.listado-existen').hide();
	});

	$('body').on('click', '.eliminar', function() {
		servicio = $(this).prop('id').split('-');
		id_servicio = servicio[1];
		modal({
			type: 'confirm',
			title: 'Eliminar el servicio',
			text: 'Desea eliminar el servicio ?',
			size: 'small',
			callback: function(result) {
				if (result === true) {
					loader();
					$.post('libs/acc_servicios', {
						'accion': 'eliminar-servicios',
						'servicio[id_servicio]': id_servicio
					}, function(data) {
						$('#fondo').remove();
						if (data.status == true) {
							id_servicio = 0;
							nmensaje('success', 'Correcto', data.motivo);
							servicios_listar(1);
						} else {
							nmensaje('error', 'Error', data.motivo);
						}
					}, 'json');
				} else {
					id_servicio = 0;
				}
			},
			closeClick: false,
			animate: true,
			buttonText: {
				yes: 'Confirmar',
				cancel: 'Cancelar'
			}
		});
	});

	function loader() {
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">' +
			'<div class="loader-inner ball-beat">' +
			'<div></div>' +
			'<div></div>' +
			'<div></div>' +
			'</div>' +
			'</div>');
		setTimeout(function() {
			$('#fondo').fadeIn('fast');
		}, 400);
	}

	function nmensaje(tipo, titulo, mensaje) {
		modal({
			type: tipo,
			title: titulo,
			text: mensaje,
			size: 'small',
			closeClick: false,
			animate: true,
		});
	}
});