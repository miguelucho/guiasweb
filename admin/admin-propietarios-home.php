<?php
require_once 'libs/conexion.php';

$inmuebles = $db
        ->ObjectBuilder()->get('propietarios');
         $Listado = '';

        foreach ($inmuebles as $propietario) {
        	$Listado.='

        							<div class="Listar-table">
											<div class="Listar-table-dato Left">
												<span class="">'.$propietario->nombre_prp.'</span>
											</div>
											<div class="Listar-table-dato Left">
												<span class="">'.$propietario->apellido_prp.'</span>
											</div>
											<div class="Listar-table-dato Left">
												<span class="">'.$propietario->movil_prp.'</span>
											</div>
											<div class="Listar-table-dato Left">
												<span class="">'.$propietario->email_prp.'</span>
											</div>
											<div class="Listar-table-dato Left">
												<span class="">'.$propietario->registrado_prp.'</span>
											</div>
											<div class="Listar-table-dato">
												<span class="">'.$propietario->paso_prp.'</span>
											</div>
									</div>
        				';

        }

$Laun_tot = $db->count;

?>

<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
		<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" >
		<meta name="keywords" lang="es" content="">
		<meta name="robots" content="All">
		<title>Administrador | Mobitco</title>
		<link rel="stylesheet" href="css/stylesheet.css" />
		<link rel="stylesheet" href="css/animate.css" />
		<link rel="stylesheet" href="css/loader.css" />
		<link rel="stylesheet" href="css/easy-autocomplete.min.css" />
		<link rel="stylesheet" href="css/nnotificaciones.css" />
	</head>
	<body class="">
	<header>
		<?php include "header-top.php";?>
	</header>
		<section>
		<?php include "menu-izq-admin.php";?>
		</section>

		<section>
			<div class="Conten-white2">
                <div class="Conten-white-full">
                    <div class="Conten-dashboard">
                        <div class="Conten-dashboard-int">
                            <div class="Conten-dashboard-int-title">
                                <h2 class="Landing_title"><span>PROPIETARIOS REGISTRADOS ( <?php echo ''.$Laun_tot.''; ?> )  </span></h2>
                            </div>
                            <div class="Admin-listar-propietarios">
                            	<p>En esta lista se muestra los propietarios registrados</p>
                            	<div class="">
                            		<div class="Listar-table Oculto">
											<div class="Listar-table-dato Left">
												<span class="Title">Nombre</span>
											</div>
											<div class="Listar-table-dato Left">
												<span class="Title">Apellido</span>
											</div>
											<div class="Listar-table-dato Left">
												<span class="Title">Telefono</span>
											</div>
											<div class="Listar-table-dato Left">
												<span class="Title">Email</span>
											</div>
											<div class="Listar-table-dato Left">
												<span class="Title">Fecha de registro</span>
											</div>
											<div class="Listar-table-dato">
												<span class="Title">Paso</span>
											</div>
									</div>
									<?php echo $Listado ;?>
                            	</div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</section>


		<script src="js/jquery-1.11.1.min.js"></script>
		<script src="js/jquery.remodal.js"></script>
		<script src="js/script-menuhome.js"></script>
		<script src="js/contacto.js"></script>
		<script src="js/nnotificaciones.js"></script>
		<script src="js/script-menu-slide.js"></script>
		<script src="js/index.js"></script>
		<style type="text/css" media="screen">
			.menu_scroll{
				 background-color: #fff;
			    -moz-animation-duration: 1s;
			    -moz-animation-fill-mode: both;
			    -moz-animation-name: fade1;
			    -ms-animation-duration: 1s;
			    -ms-animation-fill-mode: both;
			    -ms-animation-name: fade1;
			    -o-animation-duration: 1s;
			    -o-animation-fill-mode: both;
			    -o-animation-name: fade1;
			    -webkit-animation-duration: 1s;
			    -webkit-animation-fill-mode: both;
			    -webkit-animation-name: fade1;
			    -webkit-box-shadow: 0 2px 5px 0 rgba(0,0,0,0.16), 0 2px 10px 0 rgba(0,0,0,0.12);
			    animation-duration: 1s;
			    animation-fill-mode: both;
			    animation-name: fade1;
			    box-shadow: 0 2px 5px 0 rgba(0,0,0,0.16), 0 2px 10px 0 rgba(0,0,0,0.12);
			}
			.menu_scroll .Top-der nav ul li a{
				color: #00bfa5;
			}
		</style>
		<script type="text/javascript">
			 var ww = document.body.clientWidth;
		    $(window).bind('resize orientationchange', function() {
		        ww = document.body.clientWidth;
		    });

		    $(window).scroll(function() {
		        if (ww > 1024) {
		            if ($(this).scrollTop() > 60) {
		                $('.Top').addClass('menu_scroll').fadeIn(500);
		            } else {
		                $('.Top').removeClass('menu_scroll');
		            }
		        }
		    });
		</script>
	</body>
</html>
