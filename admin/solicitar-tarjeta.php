<?php
session_start();
  if(!isset($_SESSION['cpguser'])){
    header('Location: login');
  }

require_once 'libs/conexion.php';

$ls_departamentos = '';
$departamentos = $db
   ->where('paise_id',1)
   ->objectBuilder()->get('departamentos');

foreach ($departamentos as $departamento) {
  $ls_departamentos .= '<option value="'.$departamento->id.'">'.$departamento->departamento.'</option>';
}

$ls_ciudades = '';
$ciudades = $db
  ->orderBy('ciudad','ASC')
  ->objectBuilder()->get('ciudades');

foreach ($ciudades as $ciudad) {
  $ls_ciudades .= '<option value="'.$ciudad->id.'" class="dep-'.$ciudad->departamento_id.'">'.$ciudad->ciudad .'</option>';
}

$ls_especialidades = '';
$especialidades = $db
  ->objectBuilder()->get('especialidades');

foreach ($especialidades as $especialidad) {
  $ls_especialidades .= '<option value="'.$especialidad->id.'">'.$especialidad->nombre.'</option>';
}

?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" >
    <meta name="keywords" lang="es" content="">
    <meta name="robots" content="All">
    <meta name="description" lang="es" content="">
    <title>Administrador</title>
    <meta http-equiv="Cache-control" content="public">
    <link rel="stylesheet" href="../css/slider.css" />
    <link rel="stylesheet" href="../js/flatpickr/dist/flatpickr.min.css" />
    <link rel="stylesheet" href="../css/multiple-select.css" />
    <link rel="stylesheet" href="../css/load.css" />
    <link rel="stylesheet" href="../css/jquery.modal.css" />
    <link rel="stylesheet" href="../css/stylesheet.css" />
    <link rel="stylesheet" href="css/stylesheet.css" />
    <link rel="stylesheet" href="../css/stylesheetnew.css" />
  </head>
  <body>
    <header>
      <?php include "header-top.php";?>
    </header>
    <section>
      <?php include "menu-izq-admin.php";?>
    </section>
      <section>
      <div class="Contener">
        <div class="Contener-int">
          <div class="Contener-int-contenido">
            <div class="Login">
              <div class="Login-int" style="width: 100%;">
                <h2>Tarjeta Profesional</h2>
                <p>Ingresa la siguiente información.</p>
                <p>Obligatorio*</p>
                <form  enctype="multipart/form-data" name="form1" id="form-tarjeta">
                  <label>Apellidos</label>
                  <input name="apellidos" type="text" size="52" />
                  <label>Nombres</label>
                  <input name="nombres" type="text"  size="52" />
                  <label>Identificaci&oacute;n</label>
                  <select name="tipoid" id="tipoid">
                    <option value="1">C&eacute;dula de ciudadan&iacute;a</option>
                    <option value="2">C&eacute;dula Extranjeria</option>
                    <option value="3">Tarjeta de identidad</option>
                  </select>
                  <label>N&uacute;mero:</label>
                  <input name="numeroid" type="text" size="12" />
                  <label> Expedida en:</label>
                  <input name="expedicionid" type="text"  />
                  <label>Departamento:</label>
                  <select name="depart_nacimiento" id="depart_nacimiento">
                    <option value="">Seleccione...</option>
                    <?php echo $ls_departamentos ?>
                  </select>
                  <label>Ciudad de nacimiento</label>
                  <select name="ciudad_nacimiento" id="ciudad_nacimiento">
                    <option value="">Seleccione...</option>
                  </select>
                  <label>Fecha de nacimiento (AAAA-MM-DD)</label>
                  <input name="fecha_nacimiento" type="text" class="fecha" />
                  <label>Asociaci&oacute;n de guias de turismo</label>
                  <label>Pertenece a alguna</label>
                  <input type="checkbox" name="pertenece_asociacion" value="1" />
                  <label>&iquest;C&uacute;al?</label>
                  <input name="asociacion_a_quepertenece" type="text"  size="52" />
                  <label>Correo electr&oacute;nico</label>
                  <input name="correo" type="email"  size="52" />
                  <label>Correo electr&oacute;nico alterno</label>
                  <input name="correo2" type="email"  size="52" />
                  <label>Departamento</label>
                  <select name="departamento_residencia" id="depart_residencia">
                    <option value="">Seleccione</option>
                     <?php echo $ls_departamentos ?>
                  </select>
                  <label>Ciudad de residencia</label>
                  <select name="ciudad_residencia" id="ciudad_residencia">
                    <option value="">Seleccione...</option>
                  </select>
                  <label>Direcci&oacute;n domicilio</label>
                  <input name="direccion_residencia" type="text"  size="52" />
                  <label>Barrio</label>
                  <input name="barrio_residencia" type="text"  size="52" />
                  <label>Fax</label>
                  <input name="fax" type="text" size="52" />
                  <label>Celular</label>
                  <input name="celular" type="text" size="52" />
                  <label>Celular alterno</label>
                  <input name="celular2" type="text"  size="52" />
                  <label>Grupo sanguineo</label>
                  <input name="rh" type="text"  size="52" />
                  <label>Areas de especializaci&oacute;n</label>
                  <select name="especialidades[]" class="multiple" multiple="multiple">
                     <?php echo $ls_especialidades ?>
                  </select>
                  <label>Carn&eacute; de gu&iacute;a de Turismo C.N.T. No</label>
                  <input name="carnet" type="text" size="52" />
                  <p>Recuerde que para este tr&aacute;mite debe radicar los documentos requeridos en medio f&iacute;sico. <a href="requisitos.php?idp=2">Ver instructivo</a>. </p>
                  <p align="left"><strong><em>NOTA: Los documentos deben legajarse en carpeta de   Celugu&iacute;a (Cart&oacute;n), tama&ntilde;o oficio.</em></strong></p>
                  <p>&nbsp;</p>
                  <b>Documentos escaneados (todos los archivos son obligatorios) </b>
                  <label>Fotograf&iacute;a</label>
                  <input  type="file" id="foto" required/>
                  <label>C&eacute;dula</label>
                  <input type="file" id="scan_cedula" required />
                  <label>Certificado de un segundo Idioma</label>
                  <input type="file" id="scan_certificado" required />
                  <label>Diploma ó Certificado de curso de homologación</label>
                  <input  type="file" id="scan_titulo" />
                  <p><b>Nota</b>: Para la expedici&oacute;n del tr&aacute;mite por primera vez no es necesario radicar los documentos f&iacute;sicamente. Sin embargo, <b>todos</b> los archivos deben estar en el formato<b>JPG</b>.</p>
                  <p>Al <b>escanear</b> los documentos seleccione las opciones del dispositivo as&iacute;:</p>
                  <table cellpadding="5">
                    <thead>
                      <tr>
                        <th>Elemento</th>
                        <th>Modo de color</th>
                        <th>Calidad JPG</th>
                        <th>Resolución</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Fotograf&iacute;a</td>
                        <td>Color</td>
                        <td>Alta</td>
                        <td>150 DPI</td>
                      </tr>
                      <tr>
                        <td>C&eacute;dula</td>
                        <td>Color</td>
                        <td>Alta</td>
                        <td>150 DPI</td>
                      </tr>
                      <tr>
                        <td>Certificado de un segundo Idioma</td>
                        <td>Color</td>
                        <td>Media</td>
                        <td>150 DPI</td>
                      </tr>
                      <tr>
                        <td>Diploma ó Certificado de curso de homologación</td>
                        <td>Color</td>
                        <td>Media</td>
                        <td>150 DPI</td>
                      </tr>
                    </tbody>
                  </table>
                  <p>Los archivos deben pesar un <b>m&aacute;ximo de 500kb</b> cada uno. Para lograr este peso, aseg&uacute;rese de guardar los archivos con las especificaciones descritas anteriormente. </p>
                  <input type="submit" class="Btn-azul" value="Registrarme" name="">
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div style="display: none">
        <select id="lsciudades">
          <?php echo $ls_ciudades ?>
        </select>
      </div>
    </section>
    <footer>

    </footer>
    <script src="../js/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="../js/flatpickr/dist/flatpickr.min.js" type="text/javascript"></script>
    <script src="../js/flatpickr/dist/l10n/es.js"></script>
    <script src="../js/multiple-select.js" type="text/javascript"></script>
    <script src="../js/jquery.modal.min.js"></script>
    <script src="js/registro-tarjeta.js"></script>
    <script src="js/script-menu-slide.js"></script>
    <script type="text/javascript">
    $(function(){
    $('#Drop').bind('click',function() {
    $('.Top-inf').toggleClass('Top-inf-apa');
    });
    });
    </script>
  </body>
</html>