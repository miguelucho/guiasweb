<?php
	session_start();
	if(!isset($_SESSION['cpguser'])){
		header('Location: login');
	}
	require_once 'libs/conexion.php';

	$ls_idiomas = '';
	$idiomas = $db
		->objectBuilder()->get('idiomas');

	foreach ($idiomas as $idioma) {
		$ls_idiomas .= '<option value="'.$idioma->id.'">'.$idioma->idioma.'</option>';
	}

	$ls_especialidades = '';
	$especialidades = $db
		->objectBuilder()->get('especialidades');

	foreach ($especialidades as $especialidad) {
		$ls_especialidades .= '<option value="'.$especialidad->id.'">'.$especialidad->nombre.'</option>';
	}

	$ls_servicios = '';
	$servicios = $db
		->objectBuilder()->get('servicios');

	foreach ($servicios as $servicio) {
		$ls_servicios .= '<option value="'.$servicio->id.'">'.$servicio->nombre.'</option>';
	}


/*
$usus = $db
	->orderBy('id','ASC')
    ->objectBuilder()->get('users');
    set_time_limit(0);
foreach ($usus as $us) {
    $telefonos = str_replace('F', '', $us->telefonos);
    $telefonos = str_replace('C', '', $telefonos);
    $telefonos = str_replace('&', '', $telefonos);
    $telefonos = explode('#', $telefonos);

    $telefono1 = $telefono2 = '';
    if (count($telefonos) > 2) {
        $telefono1 = $telefonos[0] . ' - ' . $telefonos[1];
        $telefono2 = $telefonos[2];
    } elseif (count($telefonos) > 1) {
        $telefono1 = $telefonos[0];
        $telefono2 = $telefonos[1];
    } else {
        $telefono1 = $telefonos[0];
    }

    $correo = '';
    $correos = explode('#', $us->emails);
    if(count($correos) > 2){
    	$correo = $correos[0];
    }else{
    	$correo = $us->emails;
    }

    $datos = ['id'=>$us->id,'id_ciudad' => $us->ciudade_id, 'id_pais' => $us->paise_id, 'tipo' => $us->tipo, 'nombres' => $us->nombres, 'apellidos' => $us->apellidos, 'username' => $us->username, 'password' => $us->password, 'telefono1' => $telefono1, 'telefono2' => $telefono2, 'email' => $correo, 'creado' => $us->created, 'modificado' => $us->modified, 'estado' => 1];

   $db->insert('users2',$datos);
}*/
?>

<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
		<link rel="shortcut icon" href="images/favicon2.ico" type="image/x-icon" >
		<meta name="keywords" lang="es" content="">
		<meta name="robots" content="All">
		<title>Administrador</title>
		<link rel="stylesheet" href="css/stylesheet.css" />
		<link rel="stylesheet" href="css/load.css" />
		<link rel="stylesheet" href="css/easy-autocomplete.min.css" />
		<link rel="stylesheet" href="css/paginacion.css" />
		<link rel="stylesheet" href="css/multiple-select.css" />
	</head>
	<body class="">
		<header>
			<?php include "header-top.php";?>
		</header>
		<section>
			<?php include "menu-izq-admin.php";?>
		</section>
		<section>
			<div class="Conten-white2">
				<div class="Conten-white-full">
					<div class="Conten-dashboard">
						<div class="Conten-dashboard-int">
							<div class="Conten-dashboard-int-title">
								<h2 class="Landing_title">GUIAS</h2>
							</div>
							<div class="Admin-listar-propietarios">
								<div class="Contenedor-desc">
									<div class="Contenedor-desc-int">
										<section class="nav_tabs">
											<div class="nav_tabs_back Vin-anc">
												<nav>
													<ul class="tabs">
														<!-- <li><a href="#" class="Vin  Vin-active">CREAR</a></li> -->
														<li><a href="#" class="Vin Vin-active">LISTAR</a></li>
														<li><a href="#" class="Vin">PENDIENTES</a></li>
													</ul>
												</nav>
											</div>
										</section>
									</div>
									<div class="">
										<div class="Cont-formulario">
											<!-- <div class="Cont-formulario-int" id="crear-guia">
												<form>
													<div class="Cont-formulario-int-sec">
														<label>Numero de tarjeta</label>
														<input type="text" placeholder="Numero" name="">
														<label>RNT</label>
														<input type="text" placeholder="Numero" name="">
														<label>Apellidos</label>
														<input type="text" placeholder="Apellidos" name="">
														<label>Nombre</label>
														<input type="text" placeholder="Nombre" name="">
														<label>Teléfono</label>
														<input type="text" placeholder="Teléfono" name="">
														<label>Teléfono 2</label>
														<input type="text" placeholder="Teléfono" name="">
														<label>Correo</label>
														<input type="text" placeholder="Correo" name="">
													</div>
													<div class="Cont-formulario-int-sec">
														<label>Idiomas</label>
														<select class="multiple" multiple="multiple">
															<?php echo $ls_idiomas ?>
														</select>
														<label>Especialidades</label>
														<select class="multiple" multiple="multiple">
															<?php echo $ls_especialidades ?>
														</select>
														<label>Servicios</label>
														<select class="multiple" multiple="multiple">
															<?php echo $ls_servicios ?>
														</select>
														<label>Estado</label>
														<select>
															<option>Activo</option>
															<option>Suspendido</option>
															<option>Inactivo</option>
															v
														</select>
													</div>
												</form>
											</div> -->
											<div class="Contenedor-tabla" id="listar-guia">
												<div class="Con-filtro">
													<div style="float: right;">
														<a href="libs/informes_excel?tip=4"  target="_blank" style="background: green;   padding: 12px;border-radius: 4px;color: #fff;font-family: Roboto-Bold;">Exportar</a>
													</div>
													<form id="filtrar">
														<input type="text" placeholder="Buscar por nombre" name="" id="guia">
														<input type="text" placeholder="Buscar por RNT" name="" id="rnt">
														<input type="submit" name="" value="Buscar">
													</form>
												</div>
												<div class="Listar-table Bg-gris Oculto">
													<div class="Listar-table-dato">
														<span class="Title" title="">N. de tarjeta</span>
													</div>
													<div class="Listar-table-dato">
														<span class="Title" title="">RNT</span>
													</div>
													<div class="Listar-table-dato Center">
														<span class="Title" title="">Nombre</span>
													</div>
													<div class="Listar-table-dato Center">
														<span class="Title" title="">Apellido</span>
													</div>
													<div class="Listar-table-dato Center">
														<span class="Title" title="">Telefonos</span>
													</div>
													<div class="Listar-table-dato Center">
														<span class="Title" title="">Correo</span>
													</div>
													<div class="Listar-table-dato Center">
														<span class="Title" title="">Estado</span>
													</div>
													<div class="Listar-table-dato Center">
														<span class="Title" title="">Acción</span>
													</div>
												</div>
												<div class="load" >
													<img src="images/load.gif" width="50px">
												</div>
												<div style="display: block;" id="ls-usuarios">
												</div>
												<div class="Listar-paginacion">
													<div id="paginacion">
													</div>
												</div>
											</div>
											<div class="Contenedor-tabla" id="listar-pendientes" style="display: none">
												<div class="Listar-table Bg-gris Oculto">
													<div class="Listar-table-dato">
														<span class="Title" title="">Identificación</span>
													</div>
													<div class="Listar-table-dato">
														<span class="Title" title="">Correo</span>
													</div>
													<div class="Listar-table-dato Center">
														<span class="Title" title="">Nombre</span>
													</div>
													<div class="Listar-table-dato Center">
														<span class="Title" title="">Apellido</span>
													</div>
													<div class="Listar-table-dato Center">
														<span class="Title" title="">Teléfono</span>
													</div>
													<div class="Listar-table-dato Center">
														<span class="Title" title="">Acción</span>
													</div>
												</div>
												<div class="load" >
													<img src="images/load.gif" width="50px">
												</div>
												<div style="display: block;" id="ls-pendientes">
												</div>
												<div class="Listar-paginacion">
													<div id="paginacion">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/jquery.remodal.js"></script>
	<script src="js/script-menuhome.js"></script>
	<script src="js/jquery.modal.min.js"></script>
	<script src="js/script-menu-slide.js"></script>
	<script src="js/multiple-select.js" type="text/javascript"></script>
	<script src="js/guias.js"></script>
</body>
</html>