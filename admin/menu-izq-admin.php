<div class="Menuadmin-left">
    <div class="Contenedor-principal-izq">
		<div class="Contenedor-principal-izq-menu">
			<ul class="Menu-izq">
				<li><a href="solicitar-tarjeta"><i class="icon-office"></i>Guías Registro</a></li>
				<li><a href="admin-guias"><i class="icon-office"></i>Guías Lista</a></li>
				<li><a href="admin-calificar"><i class="icon-envelop"></i>Enviar a notificaciones</a></li>
				<li><a href="admin-informe-idiomas"><i class="icon-clipboard"></i>Idiomas</a></li>
				<li><a href="admin-informe-servicios"><i class="icon-clipboard"></i>Servicios</a></li>
				<li><a href="admin-home-espcialidades"><i class="icon-clipboard"></i>Especialidades</a></li>
				<li><a href="admin-slider"><i class="icon-image"></i>Slider</a></li>
				<li><a href="admin-banner"><i class="icon-image"></i>Banner</a></li>
				<li><a href="admin-informe-canales"><i class="icon-phone"></i>Canales atención</a></li>
				<li><a href="admin-informe-guias"><i class="icon-file-text"></i>Informes</a></li>
				<li><a href="libs/logout"><i class="icon-switch"></i>Salir</a></li>
			</ul>
		</div>
	</div>
 </div>