<?php
	session_start();
	if(isset($_SESSION['cpguser'])){
		header('Location: admin-guias');
	}
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
		<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
		<meta name="keywords" lang="es" content="">
		<meta name="robots" content="All">
		<meta name="description" lang="es" content="">
		<title>Login</title>
		<link rel="stylesheet" href="css/stylesheet.css" />
		<link rel="stylesheet" href="css/login.css" />
		<link rel="stylesheet" href="css/load.css" />
		<link rel="stylesheet" href="css/jquery.modal.css" />
	</head>
	<body class="BG-gris">
		<div class="Cliente-iniciar">
			<div class="Cliente-iniciar-int">
				<div class="Cliente-iniciar-int-form">
					<img src="images/cpgpng.png">
					<h1>INICIAR SESIÓN</h1>
					<form id="login">
						<label>Usuario:</label>
						<input type="input" placeholder="Usuario" name="user[username]" required="true">
						<label>Contraseña:</label>
						<input type="password" placeholder="Contraseña" name="user[password]" required="true">
						<div class="error">
							<p></p>
						</div>
						<input type="submit" value="Iniciar Sesión">
					</form>
				</div>
			</div>
		</div>
		<script src="js/jquery-1.11.1.min.js"></script>
		<script src="js/jquery.modal.min.js"></script>
		<script src="js/login.js" type="text/javascript"></script>
	</body>
</html>
