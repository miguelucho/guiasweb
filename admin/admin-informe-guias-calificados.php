<?php
	session_start();
	if (!isset($_SESSION['cpguser'])) {
	    header('Location: login');
	}

	require_once 'libs/conexion.php';

	$totalcalifica = $db
    	->objectBuilder()->get('guias_calificaciones');

	$registrados = $db->count;

?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
		<link rel="shortcut icon" href="images/favicon2.ico" type="image/x-icon" >
		<meta name="keywords" lang="es" content="">
		<meta name="robots" content="All">
		<title>Administrador</title>
		<link rel="stylesheet" href="css/stylesheet.css" />
		<link rel="stylesheet" href="css/load.css" />
		<link rel="stylesheet" href="css/easy-autocomplete.min.css" />
		<link rel="stylesheet" href="css/paginacion.css" />
	</head>
	<body class="">
		<header>
			<?php include "header-top.php";?>
		</header>
		<section>
			<?php include "menu-izq-admin.php";?>
		</section>
		<section>
			<div class="Conten-white2">
				<div class="Conten-white-full">
					<div class="Conten-dashboard">
						<div class="Conten-dashboard-int">
							<div class="Conten-dashboard-int-title">
								<h2 class="Landing_title">GUIAS CALIFICADOS (<?php echo $registrados ?>)</h2>
							</div>
							<div class="Admin-listar-propietarios">
								<div class="Contenedor-desc">
									<div class="Contenedor-desc-int">
										<section class="nav_tabs">
											<div class="nav_tabs_back Vin-anc">
												<nav>
													<ul class="tabs">
														<li><a href="admin-informe-guias" class="Vin">USUARIOS REG</a></li>
														<li><a href="admin-informe-guias-contactados" class="Vin ">GUIAS CONTACTADOS</a></li>
														<li><a href="#" class="Vin Vin-active">CALIFICACIONES</a></li>
													</ul>
												</nav>
											</div>
										</section>
									</div>
									<div class="">
										<div class="Con-filtro">
											<div style="float: right;">
												<a href="libs/informes_excel?tip=3" target="_blank" style="background: green;   padding: 12px;border-radius: 4px;color: #fff;font-family: Roboto-Bold;">Exportar</a>
											</div>
											<form id="filtrar">
												<input type="text" placeholder="Buscar guia" name="" id="guia">
												<input type="submit" name="" value="Buscar">
											</form>
										</div>
										<div class="Contenedor-tabla">
											<div class="Listar-table Bg-gris Oculto">
												<div class="Listar-table-dato Center">
													<span class="Title" title="">Guía</span>
												</div>
												<div class="Listar-table-dato Center">
													<span class="Title" title="">Turista</span>
												</div>
												<div class="Listar-table-dato Center">
													<span class="Title" title="">Fecha</span>
												</div>
												<div class="Listar-table-dato Center">
													<span class="Title" title="">Ciudad</span>
												</div>
												<div class="Listar-table-dato Center">
													<span class="Title" title="Conocimientos especializados">Conocimientos especializados</span>
												</div>
												<div class="Listar-table-dato Center">
													<span class="Title" title="Dominio en otros idiomas">Dominio en otros idiomas</span>
												</div>
												<div class="Listar-table-dato Center">
													<span class="Title" title="Vocación de servicio">Vocación de servicio</span>
												</div>
												<div class="Listar-table-dato Center">
													<span class="Title" title="Presentación personal">Presentación personal</span>
												</div>
												<div class="Listar-table-dato Center">
													<span class="Title" title="Entusiasmo y convicción">Entusiasmo y convicción</span>
												</div>
											</div>
											<div class="load" >
												<img src="images/load.gif" width="50px">
											</div>
											<div style="display: block;" id="ls-calificacion">
											</div>
											<div class="Listar-paginacion">
												<div id="paginacion">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/jquery.modal.min.js"></script>
	<script src="js/script-menu-slide.js"></script>
	<script src="js/informe-calificacion.js"></script>
</body>
</html>