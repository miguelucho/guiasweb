<?php
	session_start();
	if(!isset($_SESSION['cpguser'])){
		header('Location: login');
	}
	require_once 'libs/conexion.php';

	if(!isset( $_REQUEST['pnd'])){
		header('Location: admin-guias');
	}else{
		$pendiente = $_REQUEST['pnd'];
	}

	$pendientes = $db
		->where('id',$pendiente)
		->objectBuilder()->get('tramite');

	if($db->count == 0){
		header('Location: admin-guias');
	}else{
		$nombre = $pendientes[0]->nombres;
		$apellido = $pendientes[0]->apellidos;
		$celular = $pendientes[0]->celular;
		$celular2 = $pendientes[0]->celular2;
		$correo = $pendientes[0]->correoe;
		$correo2 = $pendientes[0]->correoe2;
		$identifica = $pendientes[0]->numeroid;
		$nacimiento = $pendientes[0]->fecha_nacimiento;
		$asociacion = $pendientes[0]->asociacion_a_quepertenece;
		$rh = $pendientes[0]->rh;
		$direccion = $pendientes[0]->direccion;
		$barrio = $pendientes[0]->barrio;
		$carnet = $pendientes[0]->carne;

		$ls_especialidades = '<ul>';
		$especialidades = $db
			->objectBuilder()->get('especialidades');

		foreach ($especialidades as $especialidad) {
			$espe = explode(',', $pendientes[0]->especialidades);
			if(in_array($especialidad->id,$espe)){
				$ls_especialidades .= '<li>'.$especialidad->nombre.'</li>';
			}
		}

		$ls_especialidades .= '</ul>';

		$foto = $pendientes[0]->foto;
		$cedula = $pendientes[0]->cedula;
		$certificado = $pendientes[0]->certificado;
		$diploma = $pendientes[0]->titulo;

	}


?>

<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
		<link rel="shortcut icon" href="images/favicon2.ico" type="image/x-icon" >
		<meta name="keywords" lang="es" content="">
		<meta name="robots" content="All">
		<title>Administrador</title>
		<link rel="stylesheet" href="css/stylesheet.css" />
		<link rel="stylesheet" href="css/load.css" />
		<link rel="stylesheet" href="css/jquery.modal.css" />
		<link rel="stylesheet" href="css/multiple-select.css" />
	</head>
	<body class="">
		<header>
			<?php include "header-top.php";?>
		</header>
		<section>
			<?php include "menu-izq-admin.php";?>
		</section>
		<section>
			<div class="Conten-white2">
				<div class="Conten-white-full">
					<div class="Conten-dashboard">
						<div class="Conten-dashboard-int">
							<div class="Conten-dashboard-int-title">
								<h2 class="Landing_title">GUIAS PENDIENTES</h2>
							</div>
							<div class="Admin-listar-propietarios">
								<div class="Contenedor-desc">
									<div class="">
										<div class="Cont-formulario">
											<div class="Cont-formulario-int" id="crear-guia">
												<form id="pendiente-guia">
													<div class="Cont-formulario-int-sec">
														<label>Apellidos</label>
														<input type="text" placeholder="Apellidos" name="guia[]" value="<?php echo $apellido ?>" readonly>
														<label>Nombre</label>
														<input type="text" placeholder="Nombre" name="guia[]" value="<?php echo $nombre ?>" readonly>
														<label>Identificación</label>
														<input type="text" placeholder="Nombre" name="guia[]" value="<?php echo $identifica ?>" readonly>
														<label>Fecha de nacimiento</label>
														<input type="text" placeholder="Nombre" name="guia[]" value="<?php echo $nacimiento ?>" readonly>
														<label>Asociación de guias de turismo a la que pertenece</label>
														<input type="text" placeholder="Nombre" name="guia[]" value="<?php echo $asociacion ?>" readonly>
														<label>Celular</label>
														<input type="text" placeholder="Celular" name="guia[]" value="<?php echo $celular ?>" readonly>
														<label>Celular alterno</label>
														<input type="text" placeholder="Celular alterno" name="guia[]" value="<?php echo $celular2 ?>" readonly>
														<label>Correo</label>
														<input type="text" placeholder="Correo" name="guia[]" value="<?php echo $correo ?>" readonly>
														<label>Correo alterno</label>
														<input type="text" placeholder="Correo alterno" name="guia[]" value="<?php echo $correo2 ?>" readonly>
														<label>Dirección</label>
														<input type="text" placeholder="Dirección" name="guia[]" value="<?php echo $direccion ?>" readonly>
														<label>Barrio</label>
														<input type="text" placeholder="Barrio" name="guia[]" value="<?php echo $barrio ?>" readonly>
														<input type="hidden" name="guia[idpendiente]" value="<?php echo $_REQUEST['pnd'] ?>">
													</div>
													<div class="Cont-formulario-int-sec">
														<label>Grupo Sanguineo</label>
														<input type="text" placeholder="Grupo Sanguineo" name="guia[]" value="<?php echo $rh ?>" readonly>
														<label>Carné de guía de Turismo C.N.T. No</label>
														<input type="text" placeholder="Carné de guía de Turismo C.N.T. No" name="guia[]" value="<?php echo $carnet ?>" readonly>
														<label>Especialidades</label>
														<?php echo $ls_especialidades ?>
														<label>Foto</label>
														<a href="<?php echo $foto ?>" target="_blank" title="">Ver</a>
														<br>
														<label>Cédula</label>
														<a href="<?php echo $cedula ?>" target="_blank" title="">Ver</a>
														<br>
														<label>Certificado</label>
														<a href="<?php echo $certificado ?>" target="_blank" title="">Ver</a>
														<br>
														<label>Diploma ó Certificado de curso de homologación</label>
														<a href="<?php echo $diploma ?>" target="_blank" title="">Ver</a>
														<br>
														<br><br>
														<label>Ingresar Numero de tarjeta</label>
														<input type="text" placeholder="Numero de tarjeta" name="guia[tarjeta]" id="tarjeta">
														<label>Ingresar RNT</label>
														<input type="text" placeholder="RNT" name="guia[rnt]" id="rnt">
														<label>Ingresar Contraseña</label>
														<input type="text" name="guia[contrasena]" placeholder="Contraseña" value="" id="contrasena">
														<label>Aprobar</label>
														<input type="checkbox" name="guia[aprobar]" id="aprobar" value="1">
													</div>
													<div class="Con-filtro">
														<input type="submit" name="Guardar" value="Guardar">
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/jquery.remodal.js"></script>
	<script src="js/script-menuhome.js"></script>
	<script src="js/jquery.modal.min.js"></script>
	<script src="js/script-menu-slide.js"></script>
	<script src="js/multiple-select.js" type="text/javascript"></script>
	<script src="js/guias-pendientes.js"></script>
</body>
</html>