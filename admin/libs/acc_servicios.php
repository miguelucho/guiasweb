<?php

require_once 'conexion.php';

$accion = $_REQUEST['accion'];
$data   = $_REQUEST['servicio'];

switch ($accion) {
    case 'lista-servicios':
        $pagina         = (int) (isset($data['pagina']) ? $data['pagina'] : 1);
        $pagina         = ($pagina == 0 ? 1 : $pagina);
        $resultados_pag = 50;
        $adyacentes     = 2;

        $totalitems = $db->objectBuilder()->get('servicios');

        $numpags = ceil($db->count / $resultados_pag);
        if ($numpags >= 1) {
            require_once 'Paginacion.php';
            $listado       = '';
            $db->pageLimit = $resultados_pag;

            $listar = $db->objectBuilder()->paginate('servicios', $pagina);

            foreach ($listar as $servicio) {
                $listado .= '<div class="Listar-table">
                                <div class="Listar-table-dato">
                                    <span class="" title="' . $servicio->nombre . '">' . $servicio->nombre . '</span>
                                </div>
                                <div class="Listar-table-dato Center">
                                    <span class="" title="">
                                        <a href="#" class="Btn-gris editar" id="ed-' . $servicio->id . '-' . $servicio->nombre . '">Editar</a>
                                    </span>
                                </div>
                                <div class="Listar-table-dato Center">
                                    <span class="" title="">
                                        <a href="javascript://" class="Btn-rojo eliminar" id="el-' . $servicio->id . '">Eliminar</a>
                                    </span>
                                </div>
                            </div>';
            }

            $info['listado']    = $listado;
            $pagconfig          = array('pagina' => $pagina, 'totalrows' => $db->totalPages, 'ultima_pag' => $numpags, 'resultados_pag' => $resultados_pag, 'adyacentes' => $adyacentes);
            $paginar            = new Paginacion($pagconfig);
            $info['paginacion'] = $paginar->crearlinks();
        } else {
            $info['listado'] = '<div class="Listar-table">
                                        <div class="Listar-table-dato">
                                            <span class="" title="">No se encontraron servicios registrados</span>
                                        </div>
                                    </div>';
            $info['paginacion'] = '';
        }

        echo json_encode($info);
        break;
    case 'crear-servicios':
        $comprobar = $db
            ->where('nombre', trim($data['nombre']))
            ->objectBuilder()->get('servicios');

        if ($db->count > 0) {
            $info['status'] = false;
            $info['motivo'] = 'El servicio ya existe';
        } else {
            $nuevo = $db
                ->insert('servicios', ['nombre' => trim($data['nombre'])]);
            if ($nuevo) {
                $info['status'] = true;
                $info['motivo'] = 'Servicio creado';
            } else {
                $info['status'] = false;
                $info['motivo'] = 'El servicio no se pudo crear';
            }
        }

        echo json_encode($info);
        break;
    case 'editar-servicios':
        $comprobar = $db
            ->where('nombre', trim($data['nombre']))
            ->where('id', $data['id_servicio'], '!=')
            ->objectBuilder()->get('servicios');

        if ($db->count > 0) {
            $info['status'] = false;
            $info['motivo'] = 'El servicio ya existe';
        } else {
            $edita = $db
                ->where('id', $data['id_servicio'])
                ->update('servicios', ['nombre' => trim($data['nombre'])]);
            if ($edita) {
                $info['status'] = true;
                $info['motivo'] = 'Servicio editado';
            } else {
                $info['status'] = false;
                $info['motivo'] = 'El servicio no se pudo editar';
            }
        }

        echo json_encode($info);
        break;
    case 'eliminar-servicios':
        $comprobar = $db
            ->where('id', $data['id_servicio'])
            ->objectBuilder()->get('servicios');

        if ($db->count == 0) {
            $info['status'] = false;
            $info['motivo'] = 'El servicio no existe';
        } else {
            $guias = $db
                ->where('servicio_id', $data['id_servicio'])
                ->delete('servicios_guias');

             $reservas = $db
                ->where('servicio_id', $data['id_servicio'])
                ->delete('reservas_servicios');

            $eliminar = $db
                ->where('id', $data['id_servicio'])
                ->delete('servicios');

            if ($eliminar) {
                $info['status'] = true;
                $info['motivo'] = 'Servicio eliminado';
            } else {
                $info['status'] = false;
                $info['motivo'] = 'El servicio no se pudo eliminar';
            }
        }

        echo json_encode($info);
        break;
}
