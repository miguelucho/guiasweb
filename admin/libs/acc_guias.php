<?php

require_once 'conexion.php';

$accion = $_REQUEST['accion'];
$data   = $_REQUEST['guia'];

switch ($accion) {
    case 'lista-guias':

        $pagina         = (int) (isset($data['pagina']) ? $data['pagina'] : 1);
        $pagina         = ($pagina == 0 ? 1 : $pagina);
        $resultados_pag = 80;
        $adyacentes     = 2;

        if ($data['rnt'] == '') {
            $data['rnt'] = '%';
        }

        if ($data['guia'] == '') {
            $data['guia'] = '%';
        }

        $totalitems = $db
            ->join('users2 u', 'u.id=g.user_id', 'LEFT')
            ->where('g.RNT', $data['rnt'], 'LIKE')
            ->where('CONCAT(u.nombres," ",u.apellidos)', '%' . $data['guia'] . '%', 'LIKE')
            // ->where('u.nombres', '%' . $data['guia'] . '%', 'LIKE')
            // ->Where('u.apellidos', '%' . $data['guia'] . '%', 'LIKE')
            ->objectBuilder()->get('guias g');

        // print_r($db->getLastQuery());

        $numpags = ceil($db->count / $resultados_pag);
        if ($numpags >= 1) {
            require_once 'Paginacion.php';
            $listado       = '';
            $db->pageLimit = $resultados_pag;

            $listar = $db
                ->join('users2 u', 'u.id=g.user_id', 'LEFT')
                ->where('g.RNT', $data['rnt'], 'LIKE')
                ->where('CONCAT(u.nombres," ",u.apellidos)', '%' . $data['guia'] . '%', 'LIKE')
                // ->Where('u.apellidos', '%' . $data['guia'] . '%', 'LIKE')
                ->objectBuilder()->paginate('guias g', $pagina);

            foreach ($listar as $guia) {
                $estado = 'Activo';
                if ($guia->suspendido == 1) {
                    $estado = 'Suspendido';
                }
                $listado .= '<div class="Listar-table">
                                <div class="Listar-table-dato">
                                    <span class="Color-azul-bold" title="' . $guia->tarjeta_profesional . '">' . $guia->tarjeta_profesional . '</span>
                                </div>
                                <div class="Listar-table-dato">
                                    <span class="" title="' . $guia->RNT . '">' . $guia->RNT . '</span>
                                </div>
                                <div class="Listar-table-dato Center">
                                    <span class="" title="' . $guia->nombres . '">
                                        ' . $guia->nombres . '
                                    </span>
                                </div>
                                <div class="Listar-table-dato Center">
                                    <span class="" title="' . $guia->apellidos . '">
                                        ' . $guia->apellidos . '
                                    </span>
                                </div>
                                <div class="Listar-table-dato Center">
                                    <span class="" title="' . $guia->telefono1 . ' - ' . $guia->telefono2 . '">
                                        ' . $guia->telefono1 . ' - ' . $guia->telefono2 . '
                                    </span>
                                </div>
                                <div class="Listar-table-dato Center">
                                    <span class="" title="' . $guia->email . '">
                                        ' . $guia->email . '
                                    </span>
                                </div>
                                <div class="Listar-table-dato Center">
                                    <span class="" title="">
                                        ' . $estado . '
                                    </span>
                                </div>
                                <div class="Listar-table-dato Center">
                                    <span class="" title="">
                                        <a href="admin-guias-editar?guia=' . $guia->user_id . '" target="_blank" class="Btn-verde editar">Editar</a>
                                    </span>
                                </div>
                            </div>';
            }

            $info['listado']    = $listado;
            $pagconfig          = array('pagina' => $pagina, 'totalrows' => $db->totalPages, 'ultima_pag' => $numpags, 'resultados_pag' => $resultados_pag, 'adyacentes' => $adyacentes);
            $paginar            = new Paginacion($pagconfig);
            $info['paginacion'] = $paginar->crearlinks();
        } else {
            $info['listado'] = '<div class="Listar-table">
                                        <div class="Listar-table-dato">
                                            <span class="" title="">No se encontraron guias registrados</span>
                                        </div>
                                    </div>';
            $info['paginacion'] = '';
        }

        echo json_encode($info);
        break;
    case 'lista-pendientes':
        $pagina         = (int) (isset($data['pagina']) ? $data['pagina'] : 1);
        $pagina         = ($pagina == 0 ? 1 : $pagina);
        $resultados_pag = 50;
        $adyacentes     = 2;

        $totalitems = $db
            ->where('aprobada', 0)
            ->objectBuilder()->get('tramite');

        $numpags = ceil($db->count / $resultados_pag);
        if ($numpags >= 1) {
            require_once 'Paginacion.php';
            $listado       = '';
            $db->pageLimit = $resultados_pag;

            $listar = $db
                ->where('aprobada', 0)
                ->objectBuilder()->paginate('tramite', $pagina);

            foreach ($listar as $guia) {

                $tipo = '';

                switch ($guia->tipoid) {
                    case '1':
                        $tipo = 'C.C';
                        break;
                    case '2':
                        $tipo = 'C.E';
                        break;
                    case '3':
                        $tipo = 'T.I';
                        break;
                }

                $listado .= '<div class="Listar-table">
                                <div class="Listar-table-dato">
                                    <span class="Color-azul-bold" title="' . $tipo . ' - ' . $guia->numeroid . '">' . $tipo . ' - ' . $guia->numeroid . '</span>
                                </div>
                                <div class="Listar-table-dato">
                                    <span class="" title="' . $guia->correoe . '">' . $guia->correoe . '</span>
                                </div>
                                <div class="Listar-table-dato Center">
                                    <span class="" title="' . $guia->nombres . '">
                                        ' . $guia->nombres . '
                                    </span>
                                </div>
                                <div class="Listar-table-dato Center">
                                    <span class="" title="' . $guia->apellidos . '">
                                        ' . $guia->apellidos . '
                                    </span>
                                </div>
                                <div class="Listar-table-dato Center">
                                    <span class="" title="' . $guia->celular .' - '.$guia->celular2. '">
                                        ' . $guia->celular .' - '.$guia->celular2. '
                                    </span>
                                </div>
                                <div class="Listar-table-dato Center">
                                    <span class="" title="">
                                        <a href="admin-pendientes-ver?pnd=' . $guia->id . '" target="_blank" class="Btn-verde ver-pendiente">Ver</a>
                                    </span>
                                </div>
                            </div>';
            }

            $info['listado']    = $listado;
            $pagconfig          = array('pagina' => $pagina, 'totalrows' => $db->totalPages, 'ultima_pag' => $numpags, 'resultados_pag' => $resultados_pag, 'adyacentes' => $adyacentes);
            $paginar            = new Paginacion($pagconfig);
            $info['paginacion'] = $paginar->crearlinks();
        } else {
            $info['listado'] = '<div class="Listar-table">
                                        <div class="Listar-table-dato">
                                            <span class="" title="">No se encontraron solicitudes pendientes</span>
                                        </div>
                                    </div>';
            $info['paginacion'] = '';
        }

        echo json_encode($info);
        break;
    case 'editar-guia':
        $comprobar = $db
            ->where('username', $data['correo'])
            ->where('id', $data['idguia'], '!=')
            ->where('tipo', '2')
            ->objectBuilder()->get('users2');

        if ($db->count == 0) {
            date_default_timezone_set("America/Bogota");
            $fecha_actual = date('Y-m-d H:i:s');

            $estado = '1';
            if ($data['estado'] != 1) {
                $estado = 0;
            }

            $datos = ['nombres' => $data['nombres'], 'apellidos' => $data['apellidos'], 'username' => $data['correo'], 'telefono1' => $data['telefono1'], 'telefono2' => $data['telefono2'], 'tipoid' => $data['tipoid'], 'numeroid' => $data['numeroid'], 'modificado' => $fecha_actual, 'estado' => $estado];

            $actualiza = $db
                ->where('id', $data['idguia'])
                ->update('users2', $datos);

            $contrasena = trim($data['contrasena']);
            if ($contrasena != '') {
                require_once "password.php";
                $npass = password_hash($contrasena, PASSWORD_BCRYPT);

                $actualiza = $db
                    ->where('id', $data['idguia'])
                    ->update('users2', ['password' => $npass]);
            }

            $comprobar = $db
                ->where('tarjeta_profesional', $data['tarjeta'])
                ->where('user_id', $data['idguia'], '!=')
                ->objectBuilder()->get('guias');

            if ($db->count == 0) {
                $comprobar = $db
                    ->where('RNT', $data['rnt'])
                    ->where('user_id', $data['idguia'], '!=')
                    ->objectBuilder()->get('guias');

                if ($db->count == 0) {
                    $suspendio = 0;
                    if ($data['estado'] != 1) {
                        $suspendio = '1';
                    }

                    $datos2 = ['RNT' => $data['rnt'], 'tarjeta_profesional' => $data['tarjeta'], 'descripcion' => $data['descripcion'], 'suspendido' => $suspendio];

                    $actualiza2 = $db
                        ->where('user_id', $data['idguia'])
                        ->update('guias', $datos2);
                } else {
                    $info['status'] = false;
                    $info['motivo'] = 'El número de RNT pertenece a otro guía.';
                }

            } else {
                $info['status'] = false;
                $info['motivo'] = 'El número de tarjeta profesional pertenece a otro guía.';
            }

            $borrar = $db
                ->where('user_id', $data['idguia'])
                ->delete('user_idiomas');

            if (isset($data['idiomas']) && $data['idiomas'] != '') {
                foreach ($data['idiomas'] as $key) {
                    $idiomas = $db
                        ->insert('user_idiomas', ['user_id' => $data['idguia'], 'idioma_id' => $key]);
                }
            }

            $guias = $db
                ->where('user_id', $data['idguia'])
                ->objectBuilder()->get('guias');

            $borrar = $db
                ->where('guia_id', $guias[0]->id)
                ->delete('especialidades_guias');

            if (isset($data['especialidades']) && $data['especialidades'] != '') {
                foreach ($data['especialidades'] as $key) {
                    $especialidades = $db
                        ->insert('especialidades_guias', ['guia_id' => $guias[0]->id, 'especialidade_id' => $key]);
                }
            }

            $borrar = $db
                ->where('guia_id', $guias[0]->id)
                ->delete('servicios_guias');

            if (isset($data['servicios']) && $data['servicios'] != '') {
                foreach ($data['servicios'] as $key) {
                    $servicios = $db
                        ->insert('servicios_guias', ['guia_id' => $guias[0]->id, 'servicio_id' => $key]);
                }
            }

            if (isset($_FILES["foto"]['name'])) {
                $foto = $_FILES["foto"]['name'];
                $foto = str_replace(" ", "_", $foto);
                if ($foto != "") {
                    $imagenes = $db
                        ->where('user_id', $data['idguia'])
                        ->objectBuilder()->get('imagenes');

                    if ($db->count > 0) {
                        $destino = '../../' . $imagenes[0]->url . $foto;
                        if (move_uploaded_file($_FILES['foto']['tmp_name'], $destino)) {
                            $up = $db
                                ->where('user_id', $data['idguia'])
                                ->update('imagenes', ['nombre' => $foto]);
                        } else {
                            $msg['motivo'] = 'Error, al subir la foto.';
                            $msg['status'] = false;
                        }
                    } else {
                        $folder = '../../img/users/' . $data['idguia'] . '/';
                        if (!file_exists($folder)) {
                            mkdir($folder, 0777);
                            $destino = $folder . $foto;
                            if (move_uploaded_file($_FILES['foto']['tmp_name'], $destino)) {
                                $up = $db
                                    ->insert('imagenes', ['user_id' => $data['idguia'], 'nombre' => $foto, 'url' => '/img/users/' . $data['idguia'] . '/']);
                            } else {
                                $msg['motivo'] = 'Error, al subir la foto.';
                                $msg['status'] = false;
                            }
                        } else {
                            $destino = $folder . $foto;
                            if (move_uploaded_file($_FILES['foto']['tmp_name'], $destino)) {
                                $up = $db
                                    ->where('user_id', $data['idguia'])
                                    ->update('imagenes', ['nombre' => $foto]);
                            } else {
                                $msg['motivo'] = 'Error, al subir la foto.';
                                $msg['status'] = false;
                            }
                        }
                    }

                }
            }

            if ($actualiza) {
                $info['status'] = true;
            } else {

                $info['status'] = false;
                $info['motivo'] = 'La información no pudo se editada';
            }

        } else {
            $info['status'] = false;
            $info['motivo'] = 'El correo ya se encuentra registrado.';
        }

        echo json_encode($info);
        break;
    case 'aprobar-guia':
        if ($data['aprobar'] == '1') {
            require_once "password.php";
            $contrasena = trim($data['contrasena']);
            $npass      = password_hash($contrasena, PASSWORD_BCRYPT);

            $rnt     = trim($data['rnt']);
            $tarjeta = trim($data['tarjeta']);

            $tramite = $db
                ->where('id', $data['idpendiente'])
                ->objectBuilder()->get('tramite');

            if ($db->count > 0) {
                $comprobar = $db
                    ->where('RNT', $rnt)
                    ->objectBuilder()->get('guias');

                if ($db->count == 0) {
                    $comprobar = $db
                        ->where('tarjeta_profesional', $tarjeta)
                        ->objectBuilder()->get('guias');

                    if ($db->count == 0) {
                        date_default_timezone_set("America/Bogota");
                        $fecha_actual = date('Y-m-d H:i:s');

                        $datos = ['tipo' => 2, 'nombres' => $tramite[0]->nombres, 'apellidos' => $tramite[0]->apellidos, 'username' => $tramite[0]->correoe, 'password' => $npass, 'telefono1' => $tramite[0]->celular, 'telefono2' => $tramite[0]->celular2, 'email' => $tramite[0]->correoe2, 'tipoid' => $tramite[0]->tipoid, 'numeroid' => $tramite[0]->numeroid, 'nacimiento' => $tramite[0]->fecha_nacimiento, 'creado' => $fecha_actual, 'estado' => 1];

                        $usuario = $db
                            ->insert('users2', $datos);

                        if ($usuario) {
                            $datos2 = ['id' => null, 'user_id' => $usuario, 'RNT' => $rnt, 'tarjeta_profesional' => $tarjeta, 'descripcion' => null, 'calificacion_promedio' => 0, 'suspendido' => 0, 'motivo_suspendido' => null, 'aprobado' => '1'];

                            $guia = $db
                                ->insert('guias', $datos2);

                            if ($guia) {

                                $folder = '../../img/users/' . $usuario . '/';

                                mkdir($folder, 0777);
                                $destino = $folder . substr($tramite[0]->foto, 20);
                                if (copy('../' . $tramite[0]->foto, $destino)) {
                                    $fotonm = substr($tramite[0]->foto, 20);
                                    $up     = $db
                                        ->insert('imagenes', ['user_id' => $usuario, 'nombre' => $fotonm, 'url' => '/img/users/' . $usuario . '/']);
                                } else {

                                }

                                $template = '../templates/tarjetaprobar.php';
                                $message  = file_get_contents($template);
                                $message  = str_replace('$guia$', $tramite[0]->nombres . ' ' . $tramite[0]->apellidos, $message);
                                $message  = str_replace('$tarjeta$', $tarjeta, $message);
                                $message  = str_replace('$rnt$', $rnt, $message);
                                $message  = str_replace('$username$', $tramite[0]->correoe, $message);
                                $message  = str_replace('$contrasena$', $contrasena, $message);
                                include_once 'phpmailer/class.phpmailer.php';
                                $mail = new PHPMailer();

                                $mail->Host = "localhost";
                                $mail->SetFrom('noreply@guiasdeturismodecolombia.com.co');
                                $mail->AddAddress($tramite[0]->correoe);
                                $mail->Subject = 'Solicitud de tarjeta profesional aprobada -  guiasdeturismodecolombia.com.co';
                                $mail->MsgHTML($message);
                                $mail->IsHTML(true);
                                $mail->CharSet = "utf-8";

                                $mail->Send();

                                $tramite = $db
                                    ->where('id', $data['idpendiente'])
                                    ->update('tramite', ['aprobada' => 1, 'rnt' => $rnt]);

                                $info['status'] = true;
                            } else {
                                $usuario = $db
                                    ->where('id', $usuario)
                                    ->delete('users2');

                                $info['status'] = false;
                                $info['motivo'] = 'El guía no pudo ser aprobado';
                            }
                        }
                    } else {
                        $info['status'] = false;
                        $info['motivo'] = 'El número de tarjeta ya se encuentra asignado a otro guía.';
                    }
                } else {
                    $info['status'] = false;
                    $info['motivo'] = 'El RNT ya se encuentra asignado a otro guía.';
                }
            } else {
                $info['status'] = false;
                $info['motivo'] = 'El guía no pudo ser aprobado';
            }

        } else {
            $info['status'] = false;
            $info['motivo'] = 'El guía no pudo ser aprobado';
        }

        echo json_encode($info);
        break;
}
