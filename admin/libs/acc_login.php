<?php
require_once "conexion.php";
$data = $_REQUEST['user'];
$msg  = [];

$username = $db->escape($data['username']);

$check = $db
    ->where('username', $username)
    ->where('tipo', 1)
    ->objectBuilder()->get('users2');
if ($db->count > 0) {
    if (password_verify($data['password'], $check[0]->password)) {
        session_start();
        $_SESSION['cpguser'] = $check[0]->id;

        $msg['status']   = true;
        $msg['redirect'] = 'admin-guias';
    } else {
        $msg['status'] = false;
        $msg['motivo'] = 'Error, usuario o contraseña incorrectos';
    }
} else {
    $msg['status'] = false;
    $msg['motivo'] = 'Error, el usuario no existe';
}
echo json_encode($msg);
