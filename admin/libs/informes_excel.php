<?php

require_once 'conexion.php';
$accion = $_REQUEST['tip'];

require_once 'PHPExcel/PHPExcel.php';

switch ($accion) {
    case '1':
        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("")
            ->setLastModifiedBy("")
            ->setTitle("INFORME")
            ->setSubject("INFORME excel")
            ->setDescription("INFORME")
            ->setKeywords("INFORME")
            ->setCategory("INFORME excel");

        $tituloReporte = "";

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A4', 'NOMBRE')
            ->setCellValue('B4', 'APELLIDO')
            ->setCellValue('C4', 'TELÉFONO')
            ->setCellValue('D4', 'CORREO')
            ->setCellValue('E4', 'REGISTRADO');

        $i = 5;

        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '3500M');

        $lista = $db
            ->where('tipo', 3)
            ->objectBuilder()->get('users2');

        foreach ($lista as $item) {
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $i, $item->nombres)
                ->setCellValue('B' . $i, $item->apellidos)
                ->setCellValue('C' . $i, $item->telefono1 . ' - ' . $item->telefono2)
                ->setCellValue('D' . $i, $item->username)
                ->setCellValue('E' . $i, $item->creado);
            $i++;
        }

        $estiloTituloReporte = array(
            'font'      => array(
                'name'   => 'Calibri',
                'bold'   => true,
                'italic' => false,
                'strike' => false,
                'size'   => 10,
                'color'  => array(
                    'rgb' => '000000',
                ),
            ),
            'borders'   => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_NONE,
                ),
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'rotation'   => 0,
                'wrap'       => true,
            ),
        );

        $estiloTituloColumnas = array(
            'font'      => array(
                'name'  => 'Calibri',
                'bold'  => true,
                'color' => array(
                    'rgb' => '000000',
                ),
            ),
            'fill'      => array(
                'type'       => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
                'rotation'   => 90,
                'startcolor' => array(
                    'rgb' => 'A6D0E2',
                ),
                'endcolor'   => array(
                    'argb' => 'A6D0E2',
                ),
            ),
            'borders'   => array(
                'top'    => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array(
                        'rgb' => '143860',
                    ),
                ),
                'bottom' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array(
                        'rgb' => '143860',
                    ),
                ),
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'wrap'       => true,
            ));

        $estiloInformacion = new PHPExcel_Style();
        $estiloInformacion->applyFromArray(
            array(
                'font'      => array(
                    'name'  => 'Arial',
                    'color' => array(
                        'rgb' => '000000',
                    ),
                ),
                'fill'      => array(
                    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('argb' => 'FFFFFF'),
                ),
                'borders'   => array(
                    'bottom' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array(
                            'rgb' => '3a2a47',
                        ),
                    ),
                    'left'   => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array(
                            'rgb' => '3a2a47',
                        ),
                    ),
                    'right'  => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array(
                            'rgb' => '3a2a47',
                        ),
                    ),
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    'wrap'       => true,
                ),
            ));

        $objPHPExcel->getActiveSheet()->getStyle('C1:F3')->applyFromArray($estiloTituloReporte);
        $objPHPExcel->getActiveSheet()->getStyle('A4:E4')->applyFromArray($estiloTituloColumnas);
        $objPHPExcel->getActiveSheet()->setSharedStyle($estiloInformacion, "A5:E" . $i);

        for ($i = 'A'; $i <= 'E'; $i++) {
            $objPHPExcel->setActiveSheetIndex(0)
                ->getColumnDimension($i)->setAutoSize(true);
        }

        $objPHPExcel->getActiveSheet()->setTitle('Informe');

        // $objPHPExcel->setActiveSheetIndex(0);
        // $objPHPExcel->getActiveSheet()->getSheetView()->setZoomScale(115);

        $fecha = date('Y-m-d');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="informe_usuarios_' . $fecha . '.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        break;
    case '2':
        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("")
            ->setLastModifiedBy("")
            ->setTitle("INFORME")
            ->setSubject("INFORME excel")
            ->setDescription("INFORME")
            ->setKeywords("INFORME")
            ->setCategory("INFORME excel");

        $tituloReporte = "";

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A4', 'FECHA')
            ->setCellValue('B4', 'RNT')
            ->setCellValue('C4', 'NOMBRE')
            ->setCellValue('D4', 'APELLIDO')
            ->setCellValue('E4', 'TELEFONO')
            ->setCellValue('F4', 'CORREO')
            ->setCellValue('G4', 'CONTACTO NOMBRE')
            ->setCellValue('H4', 'CONTACTO CORREO');

        $i = 5;

        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '3500M');

        $lista = $db
            ->objectBuilder()->get('guias_contactos');

        foreach ($lista as $item) {
            $guias = $db
                ->where('user_id', $item->guia_id)
                ->objectBuilder()->get('guias');

            $usuarios_guia = $db
                ->where('id', $item->guia_id)
                ->objectBuilder()->get('users2');

            $usuarios2 = $db
                ->where('id', $item->user_id)
                ->objectBuilder()->get('users2');

            if ($db->count > 0) {
                $contacto = $usuarios2[0]->nombres . ' ' . $usuarios2[0]->apellidos;
            }

            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $i, $item->fecha)
                ->setCellValue('B' . $i, $guias[0]->RNT)
                ->setCellValue('C' . $i, $usuarios_guia[0]->nombres)
                ->setCellValue('D' . $i, $usuarios_guia[0]->apellidos)
                ->setCellValue('E' . $i, $usuarios_guia[0]->telefono1 . ' - ' . $usuarios_guia[0]->telefono2)
                ->setCellValue('F' . $i, $usuarios_guia[0]->username)
                ->setCellValue('G' . $i, $contacto)
                ->setCellValue('H' . $i, $usuarios_guia[0]->username);
            $i++;
        }

        $estiloTituloReporte = array(
            'font'      => array(
                'name'   => 'Calibri',
                'bold'   => true,
                'italic' => false,
                'strike' => false,
                'size'   => 10,
                'color'  => array(
                    'rgb' => '000000',
                ),
            ),
            'borders'   => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_NONE,
                ),
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'rotation'   => 0,
                'wrap'       => true,
            ),
        );

        $estiloTituloColumnas = array(
            'font'      => array(
                'name'  => 'Calibri',
                'bold'  => true,
                'color' => array(
                    'rgb' => '000000',
                ),
            ),
            'fill'      => array(
                'type'       => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
                'rotation'   => 90,
                'startcolor' => array(
                    'rgb' => 'A6D0E2',
                ),
                'endcolor'   => array(
                    'argb' => 'A6D0E2',
                ),
            ),
            'borders'   => array(
                'top'    => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array(
                        'rgb' => '143860',
                    ),
                ),
                'bottom' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array(
                        'rgb' => '143860',
                    ),
                ),
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'wrap'       => true,
            ));

        $estiloInformacion = new PHPExcel_Style();
        $estiloInformacion->applyFromArray(
            array(
                'font'      => array(
                    'name'  => 'Arial',
                    'color' => array(
                        'rgb' => '000000',
                    ),
                ),
                'fill'      => array(
                    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('argb' => 'FFFFFF'),
                ),
                'borders'   => array(
                    'bottom' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array(
                            'rgb' => '3a2a47',
                        ),
                    ),
                    'left'   => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array(
                            'rgb' => '3a2a47',
                        ),
                    ),
                    'right'  => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array(
                            'rgb' => '3a2a47',
                        ),
                    ),
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    'wrap'       => true,
                ),
            ));

        $objPHPExcel->getActiveSheet()->getStyle('C1:F3')->applyFromArray($estiloTituloReporte);
        $objPHPExcel->getActiveSheet()->getStyle('A4:H4')->applyFromArray($estiloTituloColumnas);
        $objPHPExcel->getActiveSheet()->setSharedStyle($estiloInformacion, "A5:H" . $i);

        for ($i = 'A'; $i <= 'H'; $i++) {
            $objPHPExcel->setActiveSheetIndex(0)
                ->getColumnDimension($i)->setAutoSize(true);
        }

        $objPHPExcel->getActiveSheet()->setTitle('Informe');

        // $objPHPExcel->setActiveSheetIndex(0);
        // $objPHPExcel->getActiveSheet()->getSheetView()->setZoomScale(115);

        $fecha = date('Y-m-d');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="informe_contactoss_' . $fecha . '.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        break;
    case '3':
        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("")
            ->setLastModifiedBy("")
            ->setTitle("INFORME")
            ->setSubject("INFORME excel")
            ->setDescription("INFORME")
            ->setKeywords("INFORME")
            ->setCategory("INFORME excel");

        $tituloReporte = "";

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A4', 'GUÍA')
            ->setCellValue('B4', 'TURISTA')
            ->setCellValue('C4', 'FECHA')
            ->setCellValue('D4', 'CIUDAD')
            ->setCellValue('E4', 'CONOCIMIENTOS ESPECIALIZADOS')
            ->setCellValue('F4', 'DOMINIO EN OTROS IDIOMAS')
            ->setCellValue('G4', 'VOCACIÓN DE SERVICIO')
            ->setCellValue('H4', 'PRESENTACIÓN PERSONAL')
            ->setCellValue('I4', 'ENTUSIASMO Y CONVICCIÓN');

        $i = 5;

        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '3500M');

        $lista = $db
            ->join('guias_contactos gc', 'gc.id=cal.contacto_id')
            ->join('users2 u', 'u.id=gc.guia_id')
            ->objectBuilder()->get('guias_calificaciones cal');

        foreach ($lista as $item) {
            $turista = '';
            $ciudad  = '';

            $usuarios = $db
                ->where('id', $item->user_id)
                ->objectBuilder()->get('users2');

            if ($db->count > 0) {
                $turista = $usuarios[0]->nombres . ' ' . $usuarios[0]->apellidos;
            }

            $ciudades = $db
                ->where('id', $item->ciudad)
                ->objectBuilder()->get('ciudades');

            if ($db->count > 0) {
                $ciudad = $ciudades[0]->ciudad;
            }

            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $i, $item->nombres . ' ' . $item->apellidos)
                ->setCellValue('B' . $i, $turista)
                ->setCellValue('C' . $i, $item->fecha)
                ->setCellValue('D' . $i, $ciudad)
                ->setCellValue('E' . $i, $item->calificacion1)
                ->setCellValue('F' . $i, $item->calificacion2)
                ->setCellValue('G' . $i, $item->calificacion3)
                ->setCellValue('H' . $i, $item->calificacion4)
                ->setCellValue('I' . $i, $item->calificacion5);
            $i++;
        }

        $estiloTituloReporte = array(
            'font'      => array(
                'name'   => 'Calibri',
                'bold'   => true,
                'italic' => false,
                'strike' => false,
                'size'   => 10,
                'color'  => array(
                    'rgb' => '000000',
                ),
            ),
            'borders'   => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_NONE,
                ),
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'rotation'   => 0,
                'wrap'       => true,
            ),
        );

        $estiloTituloColumnas = array(
            'font'      => array(
                'name'  => 'Calibri',
                'bold'  => true,
                'color' => array(
                    'rgb' => '000000',
                ),
            ),
            'fill'      => array(
                'type'       => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
                'rotation'   => 90,
                'startcolor' => array(
                    'rgb' => 'A6D0E2',
                ),
                'endcolor'   => array(
                    'argb' => 'A6D0E2',
                ),
            ),
            'borders'   => array(
                'top'    => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array(
                        'rgb' => '143860',
                    ),
                ),
                'bottom' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array(
                        'rgb' => '143860',
                    ),
                ),
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'wrap'       => true,
            ));

        $estiloInformacion = new PHPExcel_Style();
        $estiloInformacion->applyFromArray(
            array(
                'font'      => array(
                    'name'  => 'Arial',
                    'color' => array(
                        'rgb' => '000000',
                    ),
                ),
                'fill'      => array(
                    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('argb' => 'FFFFFF'),
                ),
                'borders'   => array(
                    'bottom' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array(
                            'rgb' => '3a2a47',
                        ),
                    ),
                    'left'   => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array(
                            'rgb' => '3a2a47',
                        ),
                    ),
                    'right'  => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array(
                            'rgb' => '3a2a47',
                        ),
                    ),
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    'wrap'       => true,
                ),
            ));

        $objPHPExcel->getActiveSheet()->getStyle('C1:F3')->applyFromArray($estiloTituloReporte);
        $objPHPExcel->getActiveSheet()->getStyle('A4:I4')->applyFromArray($estiloTituloColumnas);
        $objPHPExcel->getActiveSheet()->setSharedStyle($estiloInformacion, "A5:I" . $i);

        for ($i = 'A'; $i <= 'I'; $i++) {
            $objPHPExcel->setActiveSheetIndex(0)
                ->getColumnDimension($i)->setAutoSize(true);
        }

        $objPHPExcel->getActiveSheet()->setTitle('Informe');

        // $objPHPExcel->setActiveSheetIndex(0);
        // $objPHPExcel->getActiveSheet()->getSheetView()->setZoomScale(115);

        $fecha = date('Y-m-d');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="informe_calificaciones_' . $fecha . '.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        break;
    case '4':
        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("")
            ->setLastModifiedBy("")
            ->setTitle("INFORME")
            ->setSubject("INFORME excel")
            ->setDescription("INFORME")
            ->setKeywords("INFORME")
            ->setCategory("INFORME excel");

        $tituloReporte = "";

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A4', 'N. DE TARJETA')
            ->setCellValue('B4', 'RNT')
            ->setCellValue('C4', 'NOMBRE')
            ->setCellValue('D4', 'APELLIDO')
            ->setCellValue('E4', 'TELEFONOS')
            ->setCellValue('F4', 'CORREO')
            ->setCellValue('G4', 'ESTADO');

        $i = 5;

        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '3500M');

        $lista = $db
            ->join('users2 u', 'u.id=g.user_id', 'LEFT')
            ->objectBuilder()->get('guias g');

        foreach ($lista as $item) {
            $estado = 'Activo';
            if ($item->suspendido == 1) {
                $estado = 'Suspendido';
            }

            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $i, $item->tarjeta_profesional)
                ->setCellValue('B' . $i, $item->RNT)
                ->setCellValue('C' . $i, $item->nombres)
                ->setCellValue('D' . $i, $item->apellidos)
                ->setCellValue('E' . $i, $item->telefono1 . ' - ' . $item->telefono2)
                ->setCellValue('F' . $i, $item->email)
                ->setCellValue('G' . $i, $estado);
            $i++;
        }

        $estiloTituloReporte = array(
            'font'      => array(
                'name'   => 'Calibri',
                'bold'   => true,
                'italic' => false,
                'strike' => false,
                'size'   => 10,
                'color'  => array(
                    'rgb' => '000000',
                ),
            ),
            'borders'   => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_NONE,
                ),
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'rotation'   => 0,
                'wrap'       => true,
            ),
        );

        $estiloTituloColumnas = array(
            'font'      => array(
                'name'  => 'Calibri',
                'bold'  => true,
                'color' => array(
                    'rgb' => '000000',
                ),
            ),
            'fill'      => array(
                'type'       => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
                'rotation'   => 90,
                'startcolor' => array(
                    'rgb' => 'A6D0E2',
                ),
                'endcolor'   => array(
                    'argb' => 'A6D0E2',
                ),
            ),
            'borders'   => array(
                'top'    => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array(
                        'rgb' => '143860',
                    ),
                ),
                'bottom' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array(
                        'rgb' => '143860',
                    ),
                ),
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'wrap'       => true,
            ));

        $estiloInformacion = new PHPExcel_Style();
        $estiloInformacion->applyFromArray(
            array(
                'font'      => array(
                    'name'  => 'Arial',
                    'color' => array(
                        'rgb' => '000000',
                    ),
                ),
                'fill'      => array(
                    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('argb' => 'FFFFFF'),
                ),
                'borders'   => array(
                    'bottom' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array(
                            'rgb' => '3a2a47',
                        ),
                    ),
                    'left'   => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array(
                            'rgb' => '3a2a47',
                        ),
                    ),
                    'right'  => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array(
                            'rgb' => '3a2a47',
                        ),
                    ),
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    'wrap'       => true,
                ),
            ));

        $objPHPExcel->getActiveSheet()->getStyle('C1:F3')->applyFromArray($estiloTituloReporte);
        $objPHPExcel->getActiveSheet()->getStyle('A4:G4')->applyFromArray($estiloTituloColumnas);
        $objPHPExcel->getActiveSheet()->setSharedStyle($estiloInformacion, "A5:G" . $i);

        for ($i = 'A'; $i <= 'G'; $i++) {
            $objPHPExcel->setActiveSheetIndex(0)
                ->getColumnDimension($i)->setAutoSize(true);
        }

        $objPHPExcel->getActiveSheet()->setTitle('Informe');

        // $objPHPExcel->setActiveSheetIndex(0);
        // $objPHPExcel->getActiveSheet()->getSheetView()->setZoomScale(115);

        $fecha = date('Y-m-d');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="informe_guias_' . $fecha . '.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        break;
}
