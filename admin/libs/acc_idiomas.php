<?php

require_once 'conexion.php';

$accion = $_REQUEST['accion'];
$data   = $_REQUEST['idioma'];

switch ($accion) {
    case 'lista-idiomas':

        $pagina         = (int) (isset($data['pagina']) ? $data['pagina'] : 1);
        $pagina         = ($pagina == 0 ? 1 : $pagina);
        $resultados_pag = 50;
        $adyacentes     = 2;

        $totalitems = $db->objectBuilder()->get('idiomas');

        $numpags = ceil($db->count / $resultados_pag);
        if ($numpags >= 1) {
            require_once 'Paginacion.php';
            $listado       = '';
            $db->pageLimit = $resultados_pag;

            $listar = $db->objectBuilder()->paginate('idiomas', $pagina);

            foreach ($listar as $idioma) {
                $listado .= '<div class="Listar-table">
                                <div class="Listar-table-dato">
                                    <span class="" title="' . $idioma->idioma . '">' . $idioma->idioma . '</span>
                                </div>
                                <div class="Listar-table-dato Center">
                                    <span class="" title="">
                                        <a href="#" class="Btn-gris editar" id="ed-' . $idioma->id . '-' . $idioma->idioma . '">Editar</a>
                                    </span>
                                </div>
                                <div class="Listar-table-dato Center">
                                    <span class="" title="">
                                        <a href="javascript://" class="Btn-rojo eliminar" id="el-' . $idioma->id . '">Eliminar</a>
                                    </span>
                                </div>
                            </div>';
            }

            $info['listado']    = $listado;
            $pagconfig          = array('pagina' => $pagina, 'totalrows' => $db->totalPages, 'ultima_pag' => $numpags, 'resultados_pag' => $resultados_pag, 'adyacentes' => $adyacentes);
            $paginar            = new Paginacion($pagconfig);
            $info['paginacion'] = $paginar->crearlinks();
        } else {
            $info['listado'] = '<div class="Listar-table">
                                        <div class="Listar-table-dato">
                                            <span class="" title="">No se encontraron idiomas registrados</span>
                                        </div>
                                    </div>';
            $info['paginacion'] = '';
        }

        echo json_encode($info);
        break;
    case 'crear-idiomas':
        $comprobar = $db
            ->where('idioma', trim($data['nombre']))
            ->objectBuilder()->get('idiomas');

        if ($db->count > 0) {
            $info['status'] = false;
            $info['motivo'] = 'El idioma ya existe';
        } else {
            $nuevo = $db
                ->insert('idiomas', ['idioma' => trim($data['nombre'])]);
            if ($nuevo) {
                $info['status'] = true;
                $info['motivo'] = 'Idioma creado';
            } else {
                $info['status'] = false;
                $info['motivo'] = 'El idioma no se pudo crear';
            }
        }

        echo json_encode($info);
        break;
    case 'editar-idiomas':
        $comprobar = $db
            ->where('idioma', trim($data['nombre']))
            ->where('id', $data['id_idioma'], '!=')
            ->objectBuilder()->get('idiomas');

        if ($db->count > 0) {
            $info['status'] = false;
            $info['motivo'] = 'El idioma ya existe';
        } else {
            $edita = $db
                ->where('id', $data['id_idioma'])
                ->update('idiomas', ['idioma' => trim($data['nombre'])]);
            if ($edita) {
                $info['status'] = true;
                $info['motivo'] = 'Idioma editado';
            } else {
                $info['status'] = false;
                $info['motivo'] = 'El idioma no se pudo editar';
            }
        }

        echo json_encode($info);
        break;
    case 'eliminar-idiomas':
        $comprobar = $db
            ->where('id', $data['id_idioma'])
            ->objectBuilder()->get('idiomas');

        if ($db->count == 0) {
            $info['status'] = false;
            $info['motivo'] = 'El idioma no existe';
        } else {
            $guias = $db
                ->where('idioma_id', $data['id_idioma'])
                ->delete('user_idiomas');

            $eliminar = $db
                ->where('id', $data['id_idioma'])
                ->delete('idiomas');

            if ($eliminar) {
                $info['status'] = true;
                $info['motivo'] = 'Idioma eliminado';
            } else {
                $info['status'] = false;
                $info['motivo'] = 'El idioma no se pudo eliminar';
            }
        }

        echo json_encode($info);
        break;
}
