<?php

require_once 'conexion.php';

$accion = $_REQUEST['accion'];
$data   = $_REQUEST['guia'];

switch ($accion) {
    case 'lista-turistas':
        $pagina         = (int) (isset($data['pagina']) ? $data['pagina'] : 1);
        $pagina         = ($pagina == 0 ? 1 : $pagina);
        $resultados_pag = 50;
        $adyacentes     = 2;

        $bsq = '%';

        if ($data['buscar'] != '0') {
            $bsq = $data['buscar'];
        }

        $totalitems = $db
            ->where('tipo', 3)
            ->where('nombres', '%' . $bsq . '%', 'like')
            ->orWhere('apellidos', '%' . $bsq . '%', 'like')
            ->objectBuilder()->get('users2');

        $numpags = ceil($db->count / $resultados_pag);
        if ($numpags >= 1) {
            require_once 'Paginacion.php';
            $listado       = '';
            $db->pageLimit = $resultados_pag;

            $listar = $db
                ->where('tipo', 3)
                ->where('nombres', '%' . $bsq . '%', 'like')
                ->orWhere('apellidos', '%' . $bsq . '%', 'like')
                ->orderBy('id', 'DESC')
                ->objectBuilder()->paginate('users2', $pagina);

            // print_r($db->getLastQuery());

            foreach ($listar as $turista) {
                $listado .= '<div class="Listar-table">
                                <div class="Listar-table-dato">
                                    <span class="" title="' . $turista->nombres . '">' . $turista->nombres . '</span>
                                </div>
                                <div class="Listar-table-dato">
                                    <span class="" title=" ' . $turista->apellidos . '"> ' . $turista->apellidos . '</span>
                                </div>
                                <div class="Listar-table-dato Center">
                                    <span class="" title="' . $turista->telefono1 . ' - ' . $turista->telefono2 . '">
                                        ' . $turista->telefono1 . ' - ' . $turista->telefono2 . '
                                    </span>
                                </div>
                                <div class="Listar-table-dato Center">
                                    <span class="" title="' . $turista->username . '">
                                        ' . $turista->username . '
                                    </span>
                                </div>
                                 <div class="Listar-table-dato">
                                    <span class="" title="' . $turista->creado . '">' . $turista->creado . '</span>
                                </div>
                            </div>';
            }

            $info['listado']    = $listado;
            $pagconfig          = array('pagina' => $pagina, 'totalrows' => $db->totalPages, 'ultima_pag' => $numpags, 'resultados_pag' => $resultados_pag, 'adyacentes' => $adyacentes);
            $paginar            = new Paginacion($pagconfig);
            $info['paginacion'] = $paginar->crearlinks();
        } else {
            $info['listado'] = '<div class="Listar-table">
                                        <div class="Listar-table-dato">
                                            <span class="" title="">No se encontraron guias registrados</span>
                                        </div>
                                    </div>';
            $info['paginacion'] = '';
        }

        echo json_encode($info);
        break;
    case 'lista-contactos':
        $pagina         = (int) (isset($data['pagina']) ? $data['pagina'] : 1);
        $pagina         = ($pagina == 0 ? 1 : $pagina);
        $resultados_pag = 50;
        $adyacentes     = 2;

        $totalitems = $db
            ->objectBuilder()->get('guias_contactos');

        $numpags = ceil($db->count / $resultados_pag);
        if ($numpags >= 1) {
            require_once 'Paginacion.php';
            $listado       = '';
            $db->pageLimit = $resultados_pag;

            $listar = $db
                ->orderBy('id', 'DESC')
                ->objectBuilder()->paginate('guias_contactos', $pagina);

            foreach ($listar as $guia) {

                $guias = $db
                    ->where('user_id', $guia->guia_id)
                    ->objectBuilder()->get('guias');

                $usuarios_guia = $db
                    ->where('id', $guia->guia_id)
                    ->objectBuilder()->get('users2');

                $usuarios2 = $db
                    ->where('id', $guia->user_id)
                    ->objectBuilder()->get('users2');

                if ($db->count > 0) {
                    $contacto = $usuarios2[0]->nombres . ' ' . $usuarios2[0]->apellidos;
                }

                $listado .= '<div class="Listar-table">
                                 <div class="Listar-table-dato">
                                    <span class="Color-azul-bold" title="' . $guia->fecha . '">' . $guia->fecha . '</span>
                                </div>
                                <div class="Listar-table-dato">
                                    <span class="Color-azul-bold" title="' . $guias[0]->RNT . '">' . $guias[0]->RNT . '</span>
                                </div>
                                <div class="Listar-table-dato">
                                    <span class="" title="' . $usuarios_guia[0]->nombres . '">' . $usuarios_guia[0]->nombres . '</span>
                                </div>
                                <div class="Listar-table-dato">
                                    <span class="" title=" ' . $usuarios_guia[0]->apellidos . '"> ' . $usuarios_guia[0]->apellidos . '</span>
                                </div>
                                <div class="Listar-table-dato Center">
                                    <span class="" title="' . $usuarios_guia[0]->telefono1 . ' - ' . $usuarios_guia[0]->telefono2 . '">
                                        ' . $usuarios_guia[0]->telefono1 . ' - ' . $usuarios_guia[0]->telefono2 . '
                                    </span>
                                </div>
                                <div class="Listar-table-dato Center">
                                    <span class="" title="' . $usuarios_guia[0]->username . '">
                                        ' . $usuarios_guia[0]->username . '
                                    </span>
                                </div>
                                <div class="Listar-table-dato">
                                    <span class="" title="' . $contacto . '">' . $contacto . '</span>
                                </div>
                                <div class="Listar-table-dato Center">
                                    <span class="" title="' . $usuarios_guia[0]->username . '">
                                        ' . $usuarios_guia[0]->username . '
                                    </span>
                                </div>
                            </div>';
            }

            $info['listado']    = $listado;
            $pagconfig          = array('pagina' => $pagina, 'totalrows' => $db->totalPages, 'ultima_pag' => $numpags, 'resultados_pag' => $resultados_pag, 'adyacentes' => $adyacentes);
            $paginar            = new Paginacion($pagconfig);
            $info['paginacion'] = $paginar->crearlinks();
        } else {
            $info['listado'] = '<div class="Listar-table">
                                        <div class="Listar-table-dato">
                                            <span class="" title="">No se encontraron contactos a guias.</span>
                                        </div>
                                    </div>';
            $info['paginacion'] = '';
        }

        echo json_encode($info);
        break;
    case 'lista-calificaciones':
        $pagina         = (int) (isset($data['pagina']) ? $data['pagina'] : 1);
        $pagina         = ($pagina == 0 ? 1 : $pagina);
        $resultados_pag = 50;
        $adyacentes     = 2;

        $bsq = '%';

        if ($data['guia'] != '0') {
            $bsq = $data['guia'];
        }

        $totalitems = $db
            ->join('guias_contactos gc', 'gc.id=cal.contacto_id')
            ->join('users2 u', 'u.id=gc.guia_id')
            ->where('u.nombres', '%' . $bsq . '%', 'like')
            ->orWhere('u.apellidos', '%' . $bsq . '%', 'like')
            ->objectBuilder()->get('guias_calificaciones cal');

        $numpags = ceil($db->count / $resultados_pag);
        if ($numpags >= 1) {
            require_once 'Paginacion.php';
            $listado       = '';
            $db->pageLimit = $resultados_pag;

            $listar = $db
                ->join('guias_contactos gc', 'gc.id=cal.contacto_id')
                ->join('users2 u', 'u.id=gc.guia_id')
                ->where('u.nombres', '%' . $bsq . '%', 'like')
                ->orWhere('u.apellidos', '%' . $bsq . '%', 'like')
                ->orderBy('cal.id', 'DESC')
                ->objectBuilder()->paginate('guias_calificaciones cal', $pagina);

            // print_r($db->getLastQuery());

            foreach ($listar as $guia) {
                $turista = '';
                $ciudad  = '';

                $usuarios = $db
                    ->where('id', $guia->user_id)
                    ->objectBuilder()->get('users2');

                if ($db->count > 0) {
                    $turista = $usuarios[0]->nombres . ' ' . $usuarios[0]->apellidos;
                }

                $ciudades = $db
                    ->where('id', $guia->ciudad)
                    ->objectBuilder()->get('ciudades');

                if ($db->count > 0) {
                    $ciudad = $ciudades[0]->ciudad;
                }

                $listado .= '<div class="Listar-table">
                                <div class="Listar-table-dato">
                                    <span class="" title="' . $guia->nombres . ' ' . $guia->apellidos . '">' . $guia->nombres . ' ' . $guia->apellidos . '</span>
                                </div>
                                <div class="Listar-table-dato">
                                    <span class="" title="' . $turista . '">' . $turista . '</span>
                                </div>
                                <div class="Listar-table-dato">
                                    <span class="" title="' . $guia->fecha . '">' . $guia->fecha . '</span>
                                </div>
                                 <div class="Listar-table-dato">
                                    <span class="" title="' . $ciudad . '">' . $ciudad . '</span>
                                </div>
                                <div class="Listar-table-dato">
                                    <span class="" title=" ' . $guia->calificacion1 . '"> ' . $guia->calificacion1 . '</span>
                                </div>
                                <div class="Listar-table-dato">
                                    <span class="" title=" ' . $guia->calificacion2 . '"> ' . $guia->calificacion2 . '</span>
                                </div>
                                 <div class="Listar-table-dato">
                                    <span class="" title=" ' . $guia->calificacion3 . '"> ' . $guia->calificacion3 . '</span>
                                </div>
                                 <div class="Listar-table-dato">
                                    <span class="" title=" ' . $guia->calificacion4 . '"> ' . $guia->calificacion4 . '</span>
                                </div>
                                 <div class="Listar-table-dato">
                                    <span class="" title=" ' . $guia->calificacion5 . '"> ' . $guia->calificacion5 . '</span>
                                </div>
                            </div>';
            }

            $info['listado']    = $listado;
            $pagconfig          = array('pagina' => $pagina, 'totalrows' => $db->totalPages, 'ultima_pag' => $numpags, 'resultados_pag' => $resultados_pag, 'adyacentes' => $adyacentes);
            $paginar            = new Paginacion($pagconfig);
            $info['paginacion'] = $paginar->crearlinks();
        } else {
            $info['listado'] = '<div class="Listar-table">
                                        <div class="Listar-table-dato">
                                            <span class="" title="">No se encontraron calificaciones registradas.</span>
                                        </div>
                                    </div>';
            $info['paginacion'] = '';
        }

        echo json_encode($info);
        break;
}
