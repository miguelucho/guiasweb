<?php

require_once 'conexion.php';

$accion = $_REQUEST['accion'];

switch ($accion) {
    case 'nueva-slider-min':
        $cate_img   = explode(',', $_POST['img-slide-m']);
        $cate_img   = base64_decode($cate_img[1]);
        $nombre_img = 'sld-' . time() . '.jpg';
        $archivador = '../../images/slider_min/' . $nombre_img;
        if (file_put_contents($archivador, $cate_img)) {
            $datos = ['tipo_sd' => 1, 'imagen_sd' => $archivador, 'link_sd' => $_POST['link']];
            $nueva = $db
                ->insert('slider', $datos);
            if ($nueva) {
                $info['status'] = true;
            } else {
                $info['status'] = false;
                $info['motivo'] = 'Error, no se ha podido subir la imagen';
            }
        } else {
            $info['status'] = false;
            $info['motivo'] = 'Error al subir la imagen';
        }

        echo json_encode($info);
        break;
    case 'listar-slider-min':
        $pagina         = (int) (isset($data['pagina']) ? $data['pagina'] : 1);
        $pagina         = ($pagina == 0 ? 1 : $pagina);
        $resultados_pag = 100;
        $adyacentes     = 2;

        $totalitems = $db
            ->where('tipo_sd', 1)
            ->orderBy('Id_sd', 'desc')
            ->objectBuilder()->get('slider');

        $total   = $db->count;
        $numpags = ceil($db->count / $resultados_pag);
        if ($numpags >= 1) {
            require_once 'Paginacion.php';
            $listado       = '';
            $db->pageLimit = $resultados_pag;

            $listar = $db
                ->where('tipo_sd', 1)
                ->orderBy('Id_sd', 'desc')
                ->objectBuilder()->paginate('slider', $pagina);

            foreach ($listar as $key => $slide) {
                $listado .= '<div class="Listar-table">
                                    <div class="Listar-table-dato">
                                        <span class="" title=""><a href="../images/slider_min/' . $slide->imagen_sd . '" target="_blank">Imagen-' . $total . '</a></span>
                                    </div>
                                     <div class="Listar-table-dato">
                                        <span class="" title=""><a href="' . $slide->link_sd . '" target="_blank">' . $slide->link_sd . '</a></span>
                                    </div>
                                    <div class="Listar-table-dato Center">
                                        <span class="" title="">
                                            <p>
                                                <a href="javascript://" class="Panel-eliminar eliminar-m" id="sdel-' . $slide->Id_sd . '"><i class="icon-bin"> </i> Eliminar</a>
                                            </p>
                                        </span>
                                    </div>
                                </div>';
                $total--;
            }

            $info['listado']    = $listado;
            $pagconfig          = array('pagina' => $pagina, 'totalrows' => $db->totalPages, 'ultima_pag' => $numpags, 'resultados_pag' => $resultados_pag, 'adyacentes' => $adyacentes);
            $paginar            = new Paginacion($pagconfig);
            $info['paginacion'] = $paginar->crearlinks();
        } else {
            $info['listado'] = '<div class="Listar-table">
                                        <div class="Listar-table-dato">
                                            <span class="" title="">No se encontraron imagenes en el slider</span>
                                        </div>
                                    </div>';
            $info['paginacion'] = '';
        }

        echo json_encode($info);
        break;
    case 'eliminar-slider-min':
        if ($_POST['idimagen'] != 0) {
            $db = MysqliDb::getInstance();

            $comprobar = $db
                ->where('Id_sd', $_POST['idimagen'])
                ->where('tipo_sd', 1)
                ->objectBuilder()->get('slider');
            $oldimag = $comprobar[0]->imagen_sd;

            $eliminar = $db
                ->where('Id_sd', $_POST['idimagen'])
                ->delete('slider');
            if ($eliminar) {
                unlink('../../images/slider_min/' . $oldimag);
                $info['status'] = true;
            } else {
                $msg            = 'Error, no se ha podido eliminar la imagen';
                $info['status'] = false;
            }
        }

        echo json_encode($info);
        break;
    case 'nueva-slider-web':
        $cate_img   = explode(',', $_POST['img-slide-w']);
        $cate_img   = base64_decode($cate_img[1]);
        $nombre_img = 'sld-' . time() . '.jpg';
        $archivador = '../../images/slider_web/' . $nombre_img;
        if (file_put_contents($archivador, $cate_img)) {
            $datos = ['tipo_sd' => 2, 'imagen_sd' => $archivador, 'link_sd' => $_POST['link']];
            $nueva = $db
                ->insert('slider', $datos);
            if ($nueva) {
                $info['status'] = true;
            } else {
                $info['status'] = false;
                $info['motivo'] = 'Error, no se ha podido subir la imagen';
            }
        } else {
            $info['status'] = false;
            $info['motivo'] = 'Error al subir la imagen';
        }

        echo json_encode($info);
        break;
    case 'listar-slider-web':
        $pagina         = (int) (isset($data['pagina']) ? $data['pagina'] : 1);
        $pagina         = ($pagina == 0 ? 1 : $pagina);
        $resultados_pag = 100;
        $adyacentes     = 2;

        $totalitems = $db
            ->where('tipo_sd', 2)
            ->orderBy('Id_sd', 'desc')
            ->objectBuilder()->get('slider');

        $total   = $db->count;
        $numpags = ceil($db->count / $resultados_pag);
        if ($numpags >= 1) {
            require_once 'Paginacion.php';
            $listado       = '';
            $db->pageLimit = $resultados_pag;

            $listar = $db
                ->where('tipo_sd', 2)
                ->orderBy('Id_sd', 'desc')
                ->objectBuilder()->paginate('slider', $pagina);

            foreach ($listar as $key => $slide) {
                $listado .= '<div class="Listar-table">
                                    <div class="Listar-table-dato">
                                        <span class="" title=""><a href="../images/slider_web/' . $slide->imagen_sd . '" target="_blank">Imagen-' . $total . '</a></span>
                                    </div>
                                    <div class="Listar-table-dato">
                                        <span class="" title=""><a href="' . $slide->link_sd . '" target="_blank">' . $slide->link_sd . '</a></span>
                                    </div>
                                    <div class="Listar-table-dato Center">
                                        <span class="" title="">
                                            <p>
                                                <a href="javascript://" class="Panel-eliminar eliminar-w" id="sdel-' . $slide->Id_sd . '"><i class="icon-bin"> </i> Eliminar</a>
                                            </p>
                                        </span>
                                    </div>
                                </div>';
                $total--;
            }

            $info['listado']    = $listado;
            $pagconfig          = array('pagina' => $pagina, 'totalrows' => $db->totalPages, 'ultima_pag' => $numpags, 'resultados_pag' => $resultados_pag, 'adyacentes' => $adyacentes);
            $paginar            = new Paginacion($pagconfig);
            $info['paginacion'] = $paginar->crearlinks();
        } else {
            $info['listado'] = '<div class="Listar-table">
                                        <div class="Listar-table-dato">
                                            <span class="" title="">No se encontraron imagenes en el slider</span>
                                        </div>
                                    </div>';
            $info['paginacion'] = '';
        }

        echo json_encode($info);
        break;
    case 'eliminar-slider-web':
        if ($_POST['idimagen'] != 0) {
            $db = MysqliDb::getInstance();

            $comprobar = $db
                ->where('Id_sd', $_POST['idimagen'])
                ->where('tipo_sd', 2)
                ->objectBuilder()->get('slider');
            $oldimag = $comprobar[0]->imagen_sd;

            $eliminar = $db
                ->where('Id_sd', $_POST['idimagen'])
                ->delete('slider');
            if ($eliminar) {
                unlink('../../images/slider_web/' . $oldimag);
                $info['status'] = true;
            } else {
                $msg            = 'Error, no se ha podido eliminar la imagen';
                $info['status'] = false;
            }
        }

        echo json_encode($info);
        break;
}
