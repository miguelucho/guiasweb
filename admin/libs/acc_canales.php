<?php

require_once 'conexion.php';

$accion = $_REQUEST['accion'];
$data   = $_REQUEST['canal'];

switch ($accion) {
    case 'crear-canales':
        $actualizar = $db
            ->update('canales_atencion', ['correo' => $data['correo'], 'telefono' => $data['telefono'], 'horario' => $data['horario']]);

        if ($actualizar) {
            $info['status'] = true;
            $info['motivo'] = 'Canales de atención actualizados';
        } else {
            $info['status'] = false;
            $info['motivo'] = 'No se pudo registrar la información';
        }

        echo json_encode($info);
        break;
}
