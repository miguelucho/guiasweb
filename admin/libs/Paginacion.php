<?php
	/**
	*
	*/
	class Paginacion
	{
		function __construct($parametros = array())
		{
			if(count($parametros > 0))
				$this->iniciar($parametros);
		}

		function iniciar($parametros = array()) {
			foreach ($parametros as $key => $val) {
				$this->$key = $val;
			}
		}

		function crearlinks() {
			$paginacion = '';
			$siguiente = $this->pagina + 1;
			$anterior = $this->pagina - 1;
			$penultima = $this->ultima_pag - 1;

			$paginacion = '';

			if(isset($this->prefijo)){
				$lnk = $this->prefijo;
			}else{
				$lnk = 'javascript://';
			}

			if($this->pagina > 1)
				$paginacion .= "<a href='".$lnk.'1'."' class='responsive-lat mpag' id='1'>&laquo; Primera</a>";
			else
				$paginacion .= "<span class='disabled responsive-lat'>&laquo; Primera</span>";
			if ($this->pagina > 1)
	            $paginacion .= "<a href='".$lnk.$anterior."' id='".($anterior)."' class='responsive-nav mpag'>&laquo; Ant.&nbsp;&nbsp;</a>";
	        else
	            $paginacion .= "<span class='disabled responsive-nav'>&laquo; Ant.&nbsp;&nbsp;</span>";

	        if ($this->ultima_pag < 7 + ($this->adyacentes * 2)) {
	            for ($contador = 1; $contador <= $this->ultima_pag; $contador++) {
	                if ($contador == $this->pagina)
	                    $paginacion .= "<span class='actual responsive-pag'>$contador</span>";
	                else
	                    $paginacion .= "<a href='".$lnk.$contador."' id='".($contador)."' class='responsive-pag mpag'>$contador</a>";
	            }
	        }
	        elseif($this->ultima_pag > 5 + ($this->adyacentes * 2)) {
	            if($this->pagina < 1 + ($this->adyacentes * 2)) {
	                for($contador = 1; $contador < 4 + ($this->adyacentes * 2); $contador++) {
	                    if($contador == $this->pagina)
	                        $paginacion .= "<span class='actual responsive-pag'>$contador</span>";
	                    else
	                        $paginacion .= "<a href='".$lnk.$contador."' id='".($contador)."' class='responsive-pag mpag'>$contador</a>";
	                }
	                $paginacion .= "...";
	                $paginacion .= "<a href='".$lnk.$penultima."' id='".($penultima)."' class='responsive-pag mpag'> $penultima</a>";
	                $paginacion .= "<a href='".$lnk.$this->ultima_pag."' id='".($this->ultima_pag)."' class='responsive-pag mpag'>$this->ultima_pag</a>";

	           }
	           elseif($this->ultima_pag - ($this->adyacentes * 2) > $this->pagina && $this->pagina > ($this->adyacentes * 2)) {
	               $paginacion .= "<a href='".$lnk."' id='1' class='responsive-pag mpag'>1</a>";
	               $paginacion .= "<a href='".$lnk."' id='2' class='responsive-pag mpag'>2</a>";
	               $paginacion .= "...";
	               for($contador = $this->pagina - $this->adyacentes; $contador <= $this->pagina + $this->adyacentes; $contador++) {
	                   if($contador == $this->pagina)
	                       $paginacion .= "<span class='actual responsive-pag'>$contador</span>";
	                   else
	                       $paginacion .= "<a href='".$lnk.$contador."' id='".($contador)."' class='responsive-pag mpag'>$contador</a>";
	               }
	               $paginacion .= "..";
	               $paginacion .= "<a href='".$lnk.$penultima."' id='".($penultima)."' class='responsive-pag mpag'>$penultima</a>";
	               $paginacion .= "<a href='".$lnk.$this->ultima_pag."' id='".($this->ultima_pag)."' class='responsive-pag mpag'>$this->ultima_pag</a>";
	           }
	           else {
	               $paginacion .= "<a href='".$lnk."' id='1;' class='responsive-pag mpag'>1</a>";
	               $paginacion .= "<a href='".$lnk."' id='2;' class='responsive-pag mpag'>2</a>";
	               $paginacion .= "..";
	               for($contador = $this->ultima_pag - (2 + ($this->adyacentes * 2)); $contador <= $this->ultima_pag; $contador++) {
	                   if($contador == $this->pagina)
	                        $paginacion .= "<span class='actual responsive-pag'>$contador</span>";
	                   else
	                        $paginacion .= "<a href='".$lnk.$contador."' id='".($contador)."' class='responsive-pag mpag'>$contador</a>";
	               }
	           }
	        }
	        if($this->pagina < $contador - 1)
	            $paginacion .= "<a href='".$lnk.$siguiente."' id='".($siguiente)."' class='responsive-nav mpag'>&nbsp;&nbsp;Sig. &raquo;</a>";
	        else
	            $paginacion .= "<span class='disabled responsive-nav'>&nbsp;&nbsp;Sig. &raquo;</span>";

			if($this->pagina < $this->ultima_pag)
	            $paginacion .= "<a href='".$lnk.$this->ultima_pag."' id='".($this->ultima_pag)."' class='responsive-lat mpag'>Última &raquo;</a>";
	        else
	            $paginacion .= "<span class='disabled responsive-lat'>Última &raquo;</span>";

	        return $paginacion;
		}
	}

?>