<?php

require_once 'conexion.php';

$accion = $_REQUEST['accion'];
$data   = $_REQUEST['especialidad'];

switch ($accion) {
    case 'lista-especialidades':

        $pagina         = (int) (isset($data['pagina']) ? $data['pagina'] : 1);
        $pagina         = ($pagina == 0 ? 1 : $pagina);
        $resultados_pag = 50;
        $adyacentes     = 2;

        $totalitems = $db->objectBuilder()->get('especialidades');

        $numpags = ceil($db->count / $resultados_pag);
        if ($numpags >= 1) {
            require_once 'Paginacion.php';
            $listado       = '';
            $db->pageLimit = $resultados_pag;

            $listar = $db->objectBuilder()->paginate('especialidades', $pagina);

            foreach ($listar as $especialidad) {
                $listado .= '<div class="Listar-table">
                               <div class="Listar-table-dato">
                                    <span class="" title="' . $especialidad->nombre . '">' . $especialidad->nombre . '</span>
                                </div>
                                <div class="Listar-table-dato Center">
                                    <span class="" title="">
                                        <a href="#" class="Btn-gris editar" id="ed-' . $especialidad->id . '-' . $especialidad->nombre . '">Editar</a>
                                    </span>
                                </div>
                                <div class="Listar-table-dato Center">
                                    <span class="" title="">
                                        <a href="javascript://" class="Btn-rojo eliminar" id="ed-' . $especialidad->id . '">Eliminar</a>
                                    </span>
                                </div>
                            </div>';
            }

            $info['listado']    = $listado;
            $pagconfig          = array('pagina' => $pagina, 'totalrows' => $db->totalPages, 'ultima_pag' => $numpags, 'resultados_pag' => $resultados_pag, 'adyacentes' => $adyacentes);
            $paginar            = new Paginacion($pagconfig);
            $info['paginacion'] = $paginar->crearlinks();
        } else {
            $info['listado'] = '<div class="Listar-table">
                                        <div class="Listar-table-dato">
                                            <span class="" title="">No se encontraron especialidades registradas</span>
                                        </div>
                                    </div>';
            $info['paginacion'] = '';
        }

        echo json_encode($info);
        break;
    case 'crear-especialidades':
        $comprobar = $db
            ->where('nombre', trim($data['nombre']))
            ->objectBuilder()->get('especialidades');

        if ($db->count > 0) {
            $info['status'] = false;
            $info['motivo'] = 'La especialidad ya existe';
        } else {
            $nuevo = $db
                ->insert('especialidades', ['nombre' => trim($data['nombre'])]);
            if ($nuevo) {
                $info['status'] = true;
                $info['motivo'] = 'Especialidad creada';
            } else {
                $info['status'] = false;
                $info['motivo'] = 'La especialidad no se pudo crear';
            }
        }

        echo json_encode($info);
        break;
    case 'editar-especialidades':
        $comprobar = $db
            ->where('nombre', trim($data['nombre']))
            ->where('id', $data['id_espe'], '!=')
            ->objectBuilder()->get('especialidades');

        if ($db->count > 0) {
            $info['status'] = false;
            $info['motivo'] = 'La especialidad ya existe';
        } else {
            $edita = $db
                ->where('id', $data['id_espe'])
                ->update('especialidades', ['nombre' => trim($data['nombre'])]);
            if ($edita) {
                $info['status'] = true;
                $info['motivo'] = 'Especialidad editada';
            } else {
                $info['status'] = false;
                $info['motivo'] = 'La especialidad no se pudo editar';
            }
        }

        echo json_encode($info);
        break;
    case 'eliminar-especialidades':
        $comprobar = $db
            ->where('id', $data['id_espe'])
            ->objectBuilder()->get('especialidades');

        if ($db->count == 0) {
            $info['status'] = false;
            $info['motivo'] = 'La especialidad no existe';
        } else {
            $guias = $db
                ->where('especialidade_id', $data['id_espe'])
                ->delete('especialidades_guias');

            $eliminar = $db
                ->where('id', $data['id_espe'])
                ->delete('especialidades');

            if ($eliminar) {
                $info['status'] = true;
                $info['motivo'] = 'Especialidad eliminada';
            } else {
                $info['status'] = false;
                $info['motivo'] = 'La especialidad no se pudo eliminar';
            }
        }

        echo json_encode($info);
        break;
}
