<?php

require_once 'conexion.php';

$accion = $_REQUEST['accion'];

switch ($accion) {
    case 'nuevo-banner':
        $cate_img   = explode(',', $_POST['img-slide-w']);
        $cate_img   = base64_decode($cate_img[1]);
        $nombre_img = 'bn-' . time() . '.png';
        $archivador = '../../images/banner/' . $nombre_img;
        if (file_put_contents($archivador, $cate_img)) {
            $comprobar = $db
                ->objectBuilder()->get('banner');
            if ($db->count > 0) {
                unlink('../../images/banner/' . $comprobar[0]->imagen_b);
                $nueva = $db
                    ->update('banner', ['imagen_b' => $nombre_img]);
            } else {
                $nueva = $db
                    ->insert('banner', ['imagen_b' => $nombre_img]);
            }
            if ($nueva) {
                $info['status'] = true;
            } else {
                $info['status'] = false;
                $info['motivo'] = 'Error, no se ha podido subir la imagen';
            }
        } else {
            $info['status'] = false;
            $info['motivo'] = 'Error al subir la imagen';
        }

        echo json_encode($info);
        break;
    case 'listar-banner':
        $pagina         = (int) (isset($data['pagina']) ? $data['pagina'] : 1);
        $pagina         = ($pagina == 0 ? 1 : $pagina);
        $resultados_pag = 100;
        $adyacentes     = 2;

        $totalitems = $db
            ->objectBuilder()->get('banner');

        $total   = $db->count;
        $numpags = ceil($db->count / $resultados_pag);
        if ($numpags >= 1) {
            require_once 'Paginacion.php';
            $listado       = '';
            $db->pageLimit = $resultados_pag;

            $listar = $db
                ->objectBuilder()->paginate('banner', $pagina);

            foreach ($listar as $key => $slide) {
                $listado .= '<div class="Listar-table">
                                    <div class="Listar-table-dato">
                                        <span class="" title=""><a href="../images/banner/' . $slide->imagen_b . '" target="_blank">Imagen-' . $total . '</a></span>
                                    </div>
                                    <div class="Listar-table-dato Center">
                                        <span class="" title="">
                                            <p>
                                                <a href="javascript://" class="Panel-eliminar eliminar-w" id="sdel-' . $slide->id_b . '"><i class="icon-bin"> </i> Eliminar</a>
                                            </p>
                                        </span>
                                    </div>
                                </div>';
                $total--;
            }

            $info['listado']    = $listado;
            $pagconfig          = array('pagina' => $pagina, 'totalrows' => $db->totalPages, 'ultima_pag' => $numpags, 'resultados_pag' => $resultados_pag, 'adyacentes' => $adyacentes);
            $paginar            = new Paginacion($pagconfig);
            $info['paginacion'] = $paginar->crearlinks();
        } else {
            $info['listado'] = '<div class="Listar-table">
                                        <div class="Listar-table-dato">
                                            <span class="" title="">No se encontraron imagenes en el banner</span>
                                        </div>
                                    </div>';
            $info['paginacion'] = '';
        }

        echo json_encode($info);
        break;
    case 'eliminar-banner':
        if ($_POST['idimagen'] != 0) {
            $db = MysqliDb::getInstance();

            $comprobar = $db
                ->objectBuilder()->get('banner');
            $oldimag = $comprobar[0]->imagen_b;

            $eliminar = $db
                ->where('id_b', $_POST['idimagen'])
                ->delete('banner');
            if ($eliminar) {
                unlink('../../images/banner/' . $oldimag);
                $info['status'] = true;
            } else {
                $msg            = 'Error, no se ha podido eliminar la imagen';
                $info['status'] = false;
            }
        }

        echo json_encode($info);
        break;
}
