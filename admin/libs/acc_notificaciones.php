<?php

require_once 'conexion.php';
$key      = 'AAAAW5fGraM:APA91bF9qblb5Jgc8If2Vg0NCwqtGebhZpx_lKN6_k2hEnvpW_TzlIBzegNPso6hOpHBG89-VwOrp-41aJlqjUJIxy6tOkh4DNEcQ_3bLlGdckndd0VyT-VVMT085OnWNsqBsX37CRca';
$enviadas = 0;

date_default_timezone_set("America/Bogota");
$fecha_actual = date('Y-m-d H:i:s');

$contactos = $db
    ->where('fecha', $fecha_actual, '<')
    ->where('estado', 2)
    ->where('calificacion', 0)
    ->objectBuilder()->get('guias_contactos');

foreach ($contactos as $contacto) {
    $usuarios = $db
        ->where('id', $contacto->user_id)
        ->where('hashcelular', '', '!=')
        ->objectBuilder()->get('users2');

    if ($db->count > 0) {
        $usuario_guia = $db
            ->where('id', $contacto->guia_id)
            ->where('tipo', '2')
            ->objectBuilder()->get('users2');

        if ($db->count > 0) {
            $token = array($usuarios[0]->hashcelular);

            $nombre_guia = $usuario_guia[0]->nombres . ' ' . $usuario_guia[0]->apellidos;

            $ciudades = $db
                ->where('id', $contacto->ciudad)
                ->objectBuilder()->get('ciudades');

            $ciudad_contacto = $ciudades[0]->ciudad;

            $guias = $db
                ->where('user_id', $contacto->guia_id)
                ->objectBuilder()->get('guias');

            $tarjeta = $guias[0]->tarjeta_profesional;

            $titulo  = 'Cuentanos tu experiencia';
            $mensaje = 'Califica el servicio del guia de turismo ' . $nombre_guia . ', en la ciudad de ' . $ciudad_contacto . ', el dia ' . $contacto->fecha . '.';

            $fields = array(
                'registration_ids' => $token,
                'priority'         => 10,
                'notification'     => array('title' => $titulo, 'body' => $mensaje, 'sound' => 'Default', 'image' => 'Notification Image'),
                'data'             => array('idContacto' => $contacto->id, 'tarjetaGuia' => $tarjeta),
            );
            $headers = array(
                'Authorization:key=' . $key,
                'Content-Type:application/json',
            );

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            $result = curl_exec($ch);

            curl_close($ch);
            $respuesta = json_decode($result);
            // echo $result;
            if ($respuesta->success == '1') {
                $enviadas++;
            }
        }
    }
}

if ($enviadas > 0) {
    $info['status']   = true;
    $info['motivo']   = 'Notificaciones enviadas';
    $info['enviadas'] = $enviadas;
} else {
    $info['status'] = false;
    $info['motivo'] = 'No se enviaron notificaciones';
}

echo json_encode($info);
