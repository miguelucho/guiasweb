<?php
	session_start();
	if(!isset($_SESSION['cpguser'])){
		header('Location: login');
	}

	require_once 'libs/conexion.php';

	$canales = $db
	->objectBuilder()->get('canales_atencion');

	$correo = $canales[0]->correo;
	$telefono = $canales[0]->telefono;
	$horario = $canales[0]->horario;
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
		<link rel="shortcut icon" href="images/favicon2.ico" type="image/x-icon" >
		<meta name="keywords" lang="es" content="">
		<meta name="robots" content="All">
		<title>Administrador</title>
		<link rel="stylesheet" href="css/stylesheet.css" />
		<link rel="stylesheet" href="css/load.css" />
		<link rel="stylesheet" href="css/jquery.modal.css" />
		<link rel="stylesheet" href="css/paginacion.css" />
	</head>
	<body class="">
		<header>
			<?php include "header-top.php";?>
		</header>
		<section>
			<?php include "menu-izq-admin.php";?>
		</section>
		<section>
			<div class="Conten-white2">
				<div class="Conten-white-full">
					<div class="Conten-dashboard">
						<div class="Conten-dashboard-int">
							<div class="Conten-dashboard-int-title">
								<h2 class="Landing_title">CANALES DE ATENCIÓN</h2>
							</div>
							<div class="Admin-listar-propietarios">
								<div class="Contenedor-desc">
									<div class="">
										<div class="Con-filtro">
											<form id="crear-canales">
												<div style="padding-bottom: 20px;">
													<label>Correo:</label>
													<input type="text" placeholder="Correo" name="canal[correo]" value="<?php echo $correo ?>" style="width: 500px;">
												</div>
												<div style="padding-bottom: 20px;">
													<label>Teléfono:</label>
													<input type="text" placeholder="Teléfono"  name="canal[telefono]" value="<?php echo $telefono ?>" style="width: 500px;">
												</div>
												<div style="padding-bottom: 20px;">
													<label>Horario:</label>
													<input type="text" placeholder="Horario" name="canal[horario]" value="<?php echo $horario ?>" style="width: 500px;">
												</div>
												<div>
													<br>
													<input type="submit" value="Guardar" name="">
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/jquery.remodal.js"></script>
	<script src="js/script-menuhome.js"></script>
	<script src="js/jquery.modal.min.js"></script>
	<script src="js/script-menu-slide.js"></script>
	<script src="js/canales.js"></script>
</body>
</html>