<?php
	session_start();
	if(!isset($_SESSION['cpguser'])){
		header('Location: login');
	}
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
		<link rel="shortcut icon" href="images/favicon2.ico" type="image/x-icon" >
		<meta name="keywords" lang="es" content="">
		<meta name="robots" content="All">
		<title>Administrador</title>
		<link rel="stylesheet" href="css/stylesheet.css" />
		<link rel="stylesheet" href="css/load.css" />
		<link rel="stylesheet" href="js/croppie/croppie.css" />
		<link rel="stylesheet" href="css/jquery.modal.css" />
		<link rel="stylesheet" href="css/paginacion.css" />
		<style type="text/css" media="screen">
			.container{
				width: 500px;
				margin: 0 auto;
			}
			.container-w{
				width: 100%;
				margin: 0 auto;
			}
			label.cabinet{
				display: block;
				cursor: pointer;
			}
			label.cabinet input.file{
				position: relative;
				height: 100%;
				width: auto;
				opacity: 0;
				-moz-opacity: 0;
			  	filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0);
			  	margin-top:-30px;
			}
			#upload-demo{
				width: 800px;
				height: 400px;
			    padding-bottom:25px;
			}
			#upload-demo-w{
				width: 1400px;
				height: 500px;
			    padding-bottom:25px;
			}
			.center-block {
			    display: block;
			    margin-right: auto;
			    margin-left: auto;
			}
			figure{
				margin: 0;
			}
			figure img{
				width: 100%;
			}
			#modal-window .modal-box.modal-size-large {
			    width: 90%;
			}
		</style>
	</head>
	<body class="">
		<header>
			<?php include "header-top.php";?>
		</header>
		<section>
			<?php include "menu-izq-admin.php";?>
		</section>
		<section>
			<div class="Conten-white2">
				<div class="Conten-white-full">
					<div class="Conten-dashboard">
						<div class="Conten-dashboard-int">
							<div class="Conten-dashboard-int-title">
								<h2 class="Landing_title">Banner</h2>
							</div>
							<div class="Admin-listar-propietarios">
								<div class="Contenedor-desc">
									<div class="sl-web">
										<div class="Con-filtro">
											<form id="add-banner">
												<div class="container-w">
													<label class="cabinet center-block">
														<figure>
															<img src="images/slider_add_w.jpg"  id="item-img-output-w" />
														</figure>
														<input type="file" class="item-img-w file center-block" name="file_photo"/>
													</label>
												</div>
												<br>
												<input type="submit" class="Blo-btn-rojo-auto" value="Guardar" >
											</form>
										</div>
										<div class="Contenedor-tabla id="listado-imagenes">
											<div class="Listar-table Bg-gris Oculto">
												<div class="Listar-table-dato">
													<span class="Title" title="">Imagen</span>
												</div>
												<div class="Listar-table-dato">
													<span class="Title" title="">Eliminar</span>
												</div>
											</div>
											<div class="load load-w" >
												<img src="images/load.gif" width="50px">
											</div>
											<div style="display: block;" id="ls-imagenes">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<script src="js/jquery-1.11.1.min.js"></script>
		<script src="js/croppie/croppie.min.js" type="text/javascript"></script>
		<script src="js/script-menuhome.js"></script>
		<script src="js/jquery.modal.min.js"></script>
		<script src="js/script-menu-slide.js"></script>
		<script src="js/banner.js"></script>
	</body>
</html>