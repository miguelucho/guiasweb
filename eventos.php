<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" >
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">	
	<title>Conozcanos</title>
	<meta http-equiv="Cache-control" content="public">
	<link rel="stylesheet" href="css/slider.css" />
	<link rel="stylesheet" href="css/stylesheetnew.css" />	
</head>
<body>
<header>
	<?php include "new-header-top.php";?>	
</header>
<div class="Impul">
	<div class="Impul-int">
		<a href="#">
			<img src="images/appicon.png" class="New-App">
		</a>
	</div>
</div>
<section>
		<div class="Slider">
		<div class="Slider-imagen">
			<div class="wide-container">
				    <div id="slides">
				      <ul class="slides-container">
				     	 <li>
				          <img src="images/ImagenColombia1.png" alt="Programa Bioflores">
				          <div class="context">
					        
					      </div>
				        </li>
				        <li>
				          <img src="images/ImagenColombia5.jpg" alt="Programa Biopalama">
				          <div class="context">
					        
					      </div>
				        </li>
				        <li>
				          <img src="images/ImagenColombia6.jpg" alt="Programa Biocafe">
				          <div class="context">
					        
					      </div>
				        </li>
				        <li>
				          <img src="images/ImagenColombia3.jpg" alt="Programa Biocaña">
				          <div class="context">
					        
					      </div>
				        </li>				        
				      </ul>
		   			</div>
	  			</div>
	  			<div class="Slider-conten">
	  				<div class="Slider-conten-int"></div>
	  			</div>				 
		</div>		
		</div>		
	</section>
<div class=""></div>
<section>
	<div class="Conten-global">
		<div class="Conten-global-int">
			<div class="Conten-global-int-bloques">
				<a href="contactenos">
					<img src="images/img1.png">
				</a>
			</div>
			<div class="Conten-global-int-bloques">
				<a href="">
					<img src="images/img3.png">
				</a>
			</div>
			<div class="Conten-global-int-bloques">
				<a href="contactar">
					<img src="images/img2.png">
				</a>
			</div>
		</div>
	</div>
</section>
<section>
	<div class="Bloques">
		<div class="Bloques-int">
			<!-- <div class="Img-header">
				<img src="images/tarejtaprofesional.png">
			</div> -->
			<div class="Bloques-img">				
				<div class="Bloques-img-sec">
					<img src="images/logo.jpg">
				</div>
			</div>
			<p>De conformidad con lo establecido en el Decreto 503 de 1997, Artículo 11, el <strong>CONSEJO PROFESIONAL DE GUIAS DE TURISMO</strong> es un organismo técnico encargado de velar encargado de velar por el desarrollo y el adecuado ejercicio de la profesión y de expedir las Tarjetas Profesionales de los Guías de Turismo, previo cumplimiento de los requisitos exigidos por la ley.</p>

			<div class="Bloques-redes">
				<div class="Bloques-img">				
					<p><h2>Síguenos!</h2></p>
					<p>Para mantenerte en contacto con la  actualidad de tu profesión.</p>
					<div class="Bloques-img-sec">
						<a href="https://twitter.com/consejodeguias" target="_blank"><img src="images/Recurso9.png" style="width:120px;"></a>
						<figcaption>Concejo Profesional de Guiás de Turismo</figcaption>
					</div>
					<div class="Bloques-img-sec">
						<a href="https://www.facebook.com/ConsejoProfesionaldeGuiasdeTurismo/" target="_blank"><img src="images/Recurso11.png" style="width:120px;"></a>
						<figcaption>@consejodeguias</figcaption>
					</div>
					<div class="Bloques-img-sec">
						<a href="https://wa.me/573138489691" target="_blank"><img src="images/Recurso10.png" style="width:120px;"></a>
						<figcaption>Escríbenos por WhatsApp</figcaption>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section>
	<div class="Conten-global">
		<div class="Conten-global-int">
			<div class="Conten-redes">
				<div class="Conten-redes-int">
					<div class="Conten-redes-int-sec">
						<div class="Redesg-facetw">
							<div class="fb-page" data-href="https://web.facebook.com/ConsejoProfesionaldeGuiasdeTurismo" data-tabs="timeline" data-width="500" data-height="400" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://web.facebook.com/ConsejoProfesionaldeGuiasdeTurismo" class="fb-xfbml-parse-ignore"><a href="https://web.facebook.com/ConsejoProfesionaldeGuiasdeTurismo">Consejo Profesional de Guías de Turismo</a></blockquote></div>
						</div>
					</div>
					<div class="Conten-redes-int-sec">
						<div class="Redesg-facetw">
						<a class="twitter-timeline" href="https://twitter.com/consejodeguias?ref_src=twsrc%5Etfw">Tweets by consejodeguias</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<footer>
<?php include "new-footer.php";?>	
</footer>
<script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
<script>
    $(function() {
    $('#slides').superslides({
    inherit_width_from: '.wide-container',
    inherit_height_from: '.wide-container',
    play: 5000,
    animation: 'fade'
    });
    });
</script>
<script type="text/javascript">
	$(function(){
		$('#Drop').bind('click',function() {
		$('.Top-inf').toggleClass('Top-inf-apa');
	});
	});
</script>
<script src="js/jquery.superslides.min.js" type="text/javascript" charset="utf-8"></script>
<script src="js/mostrar-invest-index.js"></script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.7";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
</body>
</html>
