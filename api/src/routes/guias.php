<?php
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app = new \Slim\App;

$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
        ->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

// todos los guias
$app->get('/api/guias', function (Request $request, Response $response) {

    $sql = "SELECT g.id,g.RNT,g.tarjeta_profesional,g.descripcion,g.calificacion_promedio,g.suspendido,u.nombres,u.apellidos,u.telefonos,u.direccion,u.disponible, g.user_id FROM guias g, users u WHERE g.user_id = u.id";

    try {
        $db   = new db();
        $db   = $db->connect();
        $stmt = $db->query($sql);

        $guias = $stmt->fetchAll(PDO::FETCH_OBJ);

        foreach ($guias as $guia) {
            $ls_idiomas = '';
            $sql1       = "SELECT * FROM user_idiomas WHERE user_id = '$guia->user_id' ";
            $stmt       = $db->query($sql1);
            $idiomas    = $stmt->fetchAll(PDO::FETCH_OBJ);
            if ($idiomas != false) {
                foreach ($idiomas as $gidioma) {
                    $sql2      = "SELECT * FROM idiomas WHERE id = '$gidioma->idioma_id' ";
                    $stmt      = $db->query($sql2);
                    $idiomanms = $stmt->fetchAll(PDO::FETCH_OBJ);
                    foreach ($idiomanms as $idiomanm) {
                        $ls_idiomas[] = $idiomanm->idioma;
                    }
                }
            } else {
                $ls_idiomas = array();
            }

            $guia->idiomas = $ls_idiomas;

            $ls_espe        = '';
            $sql1           = "SELECT * FROM especialidades_guias WHERE guia_id = '$guia->id' ";
            $stmt           = $db->query($sql1);
            $especialidades = $stmt->fetchAll(PDO::FETCH_OBJ);
            if ($especialidades != false) {
                foreach ($especialidades as $especial) {
                    $sql2        = "SELECT * FROM especialidades WHERE id = '$especial->especialidade_id' ";
                    $stmt        = $db->query($sql2);
                    $especialnms = $stmt->fetchAll(PDO::FETCH_OBJ);
                    foreach ($especialnms as $especialnm) {
                        $ls_espe[] = $especialnm->nombre;
                    }
                }
            } else {
                $ls_espe = array();
            }

            $guia->especialidades = $ls_espe;

            // //foto
            // $sqli   = "SELECT nombre, url FROM imagenes WHERE user_id = '$guia->user_id' ";
            // $stmt   = $db->query($sqli);
            // $imagen = $stmt->fetch(PDO::FETCH_OBJ);
            // if ($imagen != false) {
            //     $foto_guia = 'https://www.guiasdeturismodecolombia.com.co/demo' . $imagen->url . $imagen->nombre;
            // } else {
            //     $foto_guia = '';
            // }

            // $guia->imagen = $foto_guia;

            // $todos->guia =

            // $respuesta['guias'][] = array('idguia' => $guia->user_id, 'nombre' => $guia->nombres . ' ' . $guia->apellidos, 'RNT' => $guia->RNT, 'tarjeta_profesional' => $guia->tarjeta_profesional, 'idiomas' => $ls_idiomas, 'especialidades' => $ls_espe, 'descripcion' => $guia->descripcion, 'imagen' => $foto_guia);
        }

        // $respuesta = (object) $respuesta['guias'];
        // $object    = new stdClass();
        // foreach ($respuesta as $key => $value) {
        //     $object->$key = $value;
        // }
        $db = null;

        return $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($guias));
    } catch (PDOException $e) {
        $error = $e->getMessage();
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($error));
    }
});

// info un guia
$app->get('/api/guias/{id}', function (Request $request, Response $response) {
    $id = $request->getAttribute('id');

    $sql = "SELECT g.user_id,g.id,g.RNT,g.tarjeta_profesional,g.descripcion,g.calificacion_promedio,g.suspendido,u.nombres,u.apellidos,u.telefono1,u.telefono2,u.username as correo_guia FROM guias g, users2 u WHERE g.tarjeta_profesional = '$id' AND u.id = g.user_id AND u.estado = 1 ";

    try {
        $db = new db();
        $db = $db->connect();

        $stmt        = $db->query($sql);
        $informacion = $stmt->fetch(PDO::FETCH_OBJ);
        if ($informacion != false) {
            //foto
            $sqli   = "SELECT nombre, url FROM imagenes WHERE user_id = '$informacion->user_id' ";
            $stmt   = $db->query($sqli);
            $imagen = $stmt->fetch(PDO::FETCH_OBJ);
            if ($imagen != false) {
                $informacion->imagen = 'https://www.guiasdeturismodecolombia.com.co' . $imagen->url . $imagen->nombre;
            } else {
                $informacion->imagen = '';
            }
            //idiomas
            $sqli    = "SELECT * FROM user_idiomas WHERE user_id = '$informacion->user_id' ";
            $stmt    = $db->query($sqli);
            $idiomas = $stmt->fetchAll(PDO::FETCH_OBJ);
            if ($idiomas != false) {
                $guia_idiomas = array();
                foreach ($idiomas as $idioma) {
                    $sqli           = "SELECT * FROM idiomas WHERE id = '$idioma->idioma_id' ";
                    $stmt           = $db->query($sqli);
                    $gidioma        = $stmt->fetch(PDO::FETCH_OBJ);
                    $guia_idiomas[] = $gidioma->idioma;
                }
                $informacion->idiomas = $guia_idiomas;
            } else {
                $informacion->idiomas = array();
            }

            //especialidades
            $sqli           = "SELECT * FROM especialidades_guias WHERE guia_id = '$informacion->id' ";
            $stmt           = $db->query($sqli);
            $especialidades = $stmt->fetchAll(PDO::FETCH_OBJ);
            if ($especialidades != false) {
                $guia_espe = array();
                foreach ($especialidades as $especialidad) {
                    $sqli        = "SELECT * FROM especialidades WHERE id = '$especialidad->especialidade_id' ";
                    $stmt        = $db->query($sqli);
                    $gespe       = $stmt->fetch(PDO::FETCH_OBJ);
                    $guia_espe[] = $gespe->nombre;
                }
                $informacion->especialidades = $guia_espe;
            } else {
                $informacion->especialidades = array();
            }

            //servicios
            $sql1      = "SELECT * FROM servicios_guias WHERE guia_id = '$informacion->id' ";
            $stmt      = $db->query($sql1);
            $servicios = $stmt->fetchAll(PDO::FETCH_OBJ);
            if ($servicios != false) {
                $guia_serv = array();
                foreach ($servicios as $servi) {
                    $sql2        = "SELECT * FROM servicios WHERE id = '$servi->servicio_id' ";
                    $stmt        = $db->query($sql2);
                    $gserv       = $stmt->fetch(PDO::FETCH_OBJ);
                    $guia_serv[] = $gserv->nombre;
                }
                $informacion->servicios = $guia_serv;
            }

        }
        $db = null;
        return $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($informacion));
    } catch (PDOException $e) {
        $error = $e->getMessage();
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($error));
    }
});

// comprobar tarjeta guias
$app->post('/api/guias/tarjeta/validar', function (Request $request, Response $response) {
    $tarjeta = $request->getParam('tarjetaprofesional');

    // $sql = "SELECT * FROM guias WHERE tarjeta_profesional = '$tarjeta' ";

    $sql = "SELECT g.user_id,g.id,g.RNT,g.tarjeta_profesional,g.descripcion,g.calificacion_promedio,g.suspendido,u.nombres,u.apellidos,u.telefono1,u.telefono2,u.email FROM guias g, users2 u WHERE g.tarjeta_profesional = '$tarjeta' AND u.id = g.user_id AND u.estado = 1 ";
    try {
        $db = new db();
        $db = $db->connect();

        $stmt        = $db->query($sql);
        $informacion = $stmt->fetch(PDO::FETCH_OBJ);
        if ($informacion == false) {
            $informacion->mensaje = '0';
        } else {
            //foto
            $sqli   = "SELECT nombre, url FROM imagenes WHERE user_id = '$informacion->user_id' ";
            $stmt   = $db->query($sqli);
            $imagen = $stmt->fetch(PDO::FETCH_OBJ);
            if ($imagen != false) {
                $informacion->imagen = 'https://www.guiasdeturismodecolombia.com.co' . $imagen->url . $imagen->nombre;
            } else {
                $informacion->imagen = '';
            }
            //idiomas
            $sqli    = "SELECT * FROM user_idiomas WHERE user_id = '$informacion->user_id' ";
            $stmt    = $db->query($sqli);
            $idiomas = $stmt->fetchAll(PDO::FETCH_OBJ);
            if ($idiomas != false) {
                $guia_idiomas = array();
                foreach ($idiomas as $idioma) {
                    $sqli           = "SELECT * FROM idiomas WHERE id = '$idioma->idioma_id' ";
                    $stmt           = $db->query($sqli);
                    $gidioma        = $stmt->fetch(PDO::FETCH_OBJ);
                    $guia_idiomas[] = $gidioma->idioma;
                }
                $informacion->idiomas = $guia_idiomas;
            }
            //especialidades
            $sqli           = "SELECT * FROM especialidades_guias WHERE guia_id = '$informacion->id' ";
            $stmt           = $db->query($sqli);
            $especialidades = $stmt->fetchAll(PDO::FETCH_OBJ);
            if ($especialidades != false) {
                $guia_espe = array();
                foreach ($especialidades as $especialidad) {
                    $sqli        = "SELECT * FROM especialidades WHERE id = '$especialidad->especialidade_id' ";
                    $stmt        = $db->query($sqli);
                    $gespe       = $stmt->fetch(PDO::FETCH_OBJ);
                    $guia_espe[] = $gespe->nombre;
                }
                $informacion->especialidades = $guia_espe;
            }

            //servicios
            $sql1      = "SELECT * FROM servicios_guias WHERE guia_id = '$informacion->id' ";
            $stmt      = $db->query($sql1);
            $servicios = $stmt->fetchAll(PDO::FETCH_OBJ);
            if ($servicios != false) {
                $guia_serv = array();
                foreach ($servicios as $servi) {
                    $sql2        = "SELECT * FROM servicios WHERE id = '$servi->servicio_id' ";
                    $stmt        = $db->query($sql2);
                    $gserv       = $stmt->fetch(PDO::FETCH_OBJ);
                    $guia_serv[] = $gserv->nombre;
                }
                $informacion->servicios = $guia_serv;
            }

            $informacion->mensaje = '1';

        }
        $db = null;
        return $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($informacion));
    } catch (PDOException $e) {
        $error = $e->getMessage();
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($error));
    }
});

// iniciar sesion guia
$app->post('/api/guia/inicio', function (Request $request, Response $response) {
    $correo     = $request->getParam('correo');
    $contrasena = $request->getParam('contrasena');

    $sql = "SELECT * FROM users2 WHERE username = '$correo' AND tipo = '2' ";
    try {
        $db = new db();
        $db = $db->connect();

        $stmt        = $db->query($sql);
        $informacion = $stmt->fetch(PDO::FETCH_OBJ);
        if ($informacion != false) {
            $sql1  = "SELECT * FROM guias WHERE user_id = '$informacion->id' ";
            $stmt1 = $db->query($sql1);
            $guia  = $stmt1->fetch(PDO::FETCH_OBJ);
            if ($guia->suspendido == 0) {
                if (password_verify($contrasena, $informacion->password)) {
                    $respuesta['mensaje'] = 'Datos correctos';
                    $respuesta['idguia']  = $informacion->id;
                    $estado               = 200;
                } else {
                    $respuesta['mensaje'] = 'Correo o Contraseña incorrectos';
                    $estado               = 400;
                }
            } else {
                $respuesta['mensaje'] = 'Su cuenta se encuentra suspendida';
                $estado               = 400;
            }
        } else {
            $respuesta['mensaje'] = 'Correo o Contraseña incorrectos';
            $estado               = 400;
        }
        $db = null;

        return $response->withStatus($estado)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($respuesta));
    } catch (PDOException $e) {
        $error = $e->getMessage();
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($error));
    }
});

// actualizar informacion guia
$app->post('/api/guia/actualizar', function (Request $request, Response $response) {
    $idguia      = $request->getParam('idguia');
    $descripcion = $request->getParam('mensaje');

    date_default_timezone_set("America/Bogota");
    $fecha_actual = date('Y-m-d H:i:s');

    try {
        $db       = new db();
        $db       = $db->connect();
        $correcto = 0;

        $sql     = "SELECT * FROM users2 WHERE id = '$idguia'  ";
        $stmt    = $db->query($sql);
        $usuario = $stmt->fetch(PDO::FETCH_OBJ);
        if ($usuario != false) {
            $sql1  = "SELECT * FROM guias WHERE user_id = '$idguia'  ";
            $stmt  = $db->query($sql1);
            $guias = $stmt->fetch(PDO::FETCH_OBJ);

            $sql2            = "SELECT * FROM canales_atencion ";
            $stmt2           = $db->query($sql2);
            $canal_atencion  = $stmt2->fetch(PDO::FETCH_OBJ);
            $correo_atencion = $canal_atencion->correo;

            $template = __DIR__ . '/guiaactualizar.php';
            $message  = file_get_contents($template);
            $message  = str_replace('$mensaje$', $descripcion, $message);
            $message  = str_replace('$guia$', $usuario->nombres . ' ' . $usuario->apellidos, $message);
            $message  = str_replace('$email$', $usuario->username, $message);
            $message  = str_replace('$RNT$', $guias->RNT, $message);
            include_once 'phpmailer/class.phpmailer.php';
            $mail = new PHPMailer();

            $mail->Host = "localhost";
            $mail->SetFrom('noreply@guiasdeturismodecolombia.com.co');
            // $mail->AddAddress('coordinacion@guiasdeturismodecolombia.com.co');
            $mail->AddAddress($correo_atencion);
            $mail->Subject = 'Actualizar información guía -  guiasdeturismodecolombia.com.co';
            $mail->MsgHTML($message);
            $mail->IsHTML(true);
            $mail->CharSet = "utf-8";

            if ($mail->Send()) {
                $correcto = 1;
            }

            if ($correcto == 1) {
                $estado               = 200;
                $respuesta['mensaje'] = 'Mensaje enviado';
            } else {
                $respuesta['mensaje'] = 'El mensaje no pudo ser enviado.';
                $estado               = 400;
            }
        } else {
            $respuesta['mensaje'] = 'El guía no se encuentra registrado';
            $estado               = 400;
        }

        $db = null;

        return $response->withStatus($estado)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($respuesta));
    } catch (PDOException $e) {
        $error = $e->getMessage();
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($error));
    }
});

// registrar disponibilidad guia
$app->post('/api/guia/disponibilidad/registro', function (Request $request, Response $response) {
    $idguia = $request->getParam('idguia');
    $estado = $request->getParam('estado');
    $ciudad = $request->getParam('ciudad');
    $desde  = $request->getParam('inicio');
    $hasta  = $request->getParam('final');

    try {
        date_default_timezone_set("America/Bogota");
        $fecha_actual = date('Y-m-d');

        if ($desde >= $fecha_actual) {
            if ($hasta >= $desde) {
                $db = new db();
                $db = $db->connect();

                $correcto = 0;
                $sql1     = "SELECT * FROM guias_disponibilidad WHERE guia_id = '$idguia'  ";
                $stmt     = $db->query($sql1);
                $dispo    = $stmt->fetch(PDO::FETCH_OBJ);
                if ($dispo != false) {
                    $sql1     = "SELECT * FROM ciudades WHERE id = '$ciudad'  ";
                    $stmt     = $db->query($sql1);
                    $ciudades = $stmt->fetch(PDO::FETCH_OBJ);

                    $sql      = "UPDATE  guias_disponibilidad SET departamento = '$ciudades->departamento_id', ciudad = '$ciudad', inicio = '$desde', fin = '$hasta', estado = '$estado' WHERE guia_id = '$idguia'  ";
                    $stmt     = $db->query($sql);
                    $correcto = 1;
                } else {
                    $sql1     = "SELECT * FROM ciudades WHERE id = '$ciudad'  ";
                    $stmt     = $db->query($sql1);
                    $ciudades = $stmt->fetch(PDO::FETCH_OBJ);

                    $sql      = "INSERT INTO guias_disponibilidad (guia_id,departamento,ciudad,inicio,fin,estado) VALUES ('$idguia','$ciudades->departamento_id','$ciudad','$desde','$hasta','$estado')";
                    $stmt     = $db->query($sql);
                    $correcto = 1;
                }

                if ($correcto == 1) {
                    $respuesta['mensaje'] = 'Disponibilidad registrada';
                    $estado               = 200;
                } else {
                    $respuesta['mensaje'] = 'No se pudo registrar la disponibilidad';
                    $estado               = 400;
                }
                $db = null;
            } else {
                $respuesta['mensaje'] = 'La fecha de disponibilidad final no puede ser menor a la inicial.';
                $estado               = 400;
            }
        } else {
            $respuesta['mensaje'] = 'La fecha de disponibilidad inicial debe ser mayor o igual a la actual.';
            $estado               = 400;
        }

        return $response->withStatus($estado)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($respuesta));
    } catch (PDOException $e) {
        $error = $e->getMessage();
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($error));
    }
});

// info disponibilidad
$app->get('/api/guia/disponibilidad/historia', function (Request $request, Response $response) {
    $idguia = $request->getParam('idguia');

    $sql = "SELECT guia_id,departamento,ciudad,inicio,fin,estado FROM guias_disponibilidad WHERE guia_id = '$idguia'  ";

    try {
        $db = new db();
        $db = $db->connect();

        $stmt        = $db->query($sql);
        $informacion = $stmt->fetchAll(PDO::FETCH_OBJ);

        if ($informacion != false) {
            $respuesta = $informacion;
            $estado    = 200;
        } else {
            $respuesta = 'No se encontró disponibilidad registrada.';
            $estado    = 400;
        }

        $db = null;
        return $response->withStatus($estado)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($respuesta));
    } catch (PDOException $e) {
        $error = $e->getMessage();
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($error));
    }
});

// recuperar contraseña guia
$app->post('/api/guia/recuperar', function (Request $request, Response $response) {
    $tarjeta = $request->getParam('tarjetaprofesional');

    try {
        $db = new db();
        $db = $db->connect();

        $sql = "SELECT * FROM guias WHERE tarjeta_profesional = '$tarjeta' ";

        $stmt        = $db->query($sql);
        $informacion = $stmt->fetch(PDO::FETCH_OBJ);
        if ($informacion != false) {
            $sql1    = "SELECT * FROM users2 WHERE id = '$informacion->user_id' AND  tipo = '2' ";
            $stmt    = $db->query($sql1);
            $usuario = $stmt->fetch(PDO::FETCH_OBJ);

            $token        = "";
            $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $codeAlphabet .= "abcdefghijklmnopqrstuvwxyz";
            $codeAlphabet .= "0123456789";
            $max = strlen($codeAlphabet);
            for ($i = 0; $i < 8; $i++) {
                $token .= $codeAlphabet[rand(0, $max - 1)];
            }
            require_once "password.php";
            $pass     = password_hash($token, PASSWORD_BCRYPT);
            $sql      = "UPDATE users2 SET password = '$pass' WHERE   id = '$informacion->user_id' AND  tipo = '2' ";
            $stmt     = $db->query($sql);
            $template = __DIR__ . '/passreset.php';

            $message = file_get_contents($template);
            $message = str_replace('$nuevopass$', $token, $message);
            $message = str_replace('$usuario$', $usuario->nombres . ' ' . $usuario->apellidos, $message);
            $message = str_replace('$enlace$', 'http://guiasdeturismodecolombia.com.co/iniciar.php', $message);
            include_once 'phpmailer/class.phpmailer.php';
            $mail = new PHPMailer();

            $mail->Host = "localhost";
            $mail->SetFrom('noreply@guiasdeturismodecolombia.com.co');
            $mail->AddAddress($usuario->username);
            $mail->Subject = 'Restablecer contraseña -  guiasdeturismodecolombia.com.co';
            $mail->MsgHTML($message);
            $mail->IsHTML(true);
            $mail->CharSet = "utf-8";

            $mail->Send();

            // $respuesta['pass']    = $token;
            $respuesta['mensaje'] = 'Correcto';
            $estado               = 200;
        } else {
            $respuesta['mensaje'] = 'No se encontro guia registrado con esa tarjeta';
            $estado               = 400;
        }

        $db = null;

        return $response->withStatus($estado)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($respuesta));
    } catch (PDOException $e) {
        $error = $e->getMessage();
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($error));
    }
});

// historial contactos
$app->get('/api/guia/contacto/historial', function (Request $request, Response $response) {
    $idguia = $request->getParam('idguia');

    try {
        $db       = new db();
        $db       = $db->connect();
        $sql      = "SELECT * FROM guias_contactos WHERE guia_id = '$idguia' ";
        $stmt     = $db->query($sql);
        $historia = $stmt->fetchAll(PDO::FETCH_OBJ);

        if ($historia != false) {
            foreach ($historia as $hs) {
                $sql1     = "SELECT * FROM users2 WHERE id = '$hs->user_id' ";
                $stmt     = $db->query($sql1);
                $usuarios = $stmt->fetch(PDO::FETCH_OBJ);

                $sql1  = "SELECT * FROM guias WHERE user_id = '$hs->guia_id' ";
                $stmt  = $db->query($sql1);
                $guias = $stmt->fetch(PDO::FETCH_OBJ);

                $sqli     = "SELECT * FROM ciudades WHERE id = '$hs->ciudad' ";
                $stmt     = $db->query($sqli);
                $ciudades = $stmt->fetch(PDO::FETCH_OBJ);

                //estados 1 pendiente, 2 contactado, 3 terminado, 4 cancelado

                switch ($hs->estado) {
                    case '1':
                        $estado = 'Pendiente';
                        break;
                    case '2':
                        $estado = 'Contactado';
                        break;
                    case '3':
                        $estado = 'Terminado';
                        break;
                    case '4':
                        $estado = 'Cancelado';
                        break;
                }

                $respuesta['mensaje'][] = array('idcontacto' => $hs->id, 'turista' => $usuarios->nombres . ' ' . $usuarios->apellidos, 'fecha' => $hs->fecha, 'ciudad' => $ciudades->ciudad, 'estado' => $estado);
            }
            $estado = 200;
        } else {
            $respuesta['mensaje'] = 'No existen registros de contacto';
            $estado               = 400;
        }

        $db = null;

        return $response->withStatus($estado)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($respuesta));
    } catch (PDOException $e) {
        $error = $e->getMessage();
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($error));
    }
});

// aceptar-cancelar contactos
$app->post('/api/guia/contacto/responder', function (Request $request, Response $response) {
    $idcontacto = $request->getParam('idcontacto');
    $respuesta  = $request->getParam('respuesta');

    try {
        $db        = new db();
        $db        = $db->connect();
        $sql       = "SELECT * FROM guias_contactos WHERE id = '$idcontacto' ";
        $stmt      = $db->query($sql);
        $contactos = $stmt->fetch(PDO::FETCH_OBJ);

        if ($contactos != false) {
            if ($respuesta == 0) {
                $sql1 = "UPDATE guias_contactos SET estado = '4' WHERE id = '$idcontacto' ";
                $stmt = $db->query($sql1);
                if ($stmt) {
                    $sqli     = "SELECT * FROM ciudades WHERE id = '$contactos->ciudad' ";
                    $stmt     = $db->query($sqli);
                    $ciudades = $stmt->fetch(PDO::FETCH_OBJ);

                    $fecha_contacto  = $contactos->fecha;
                    $ciudad_contacto = $ciudades->ciudad;

                    $sql1    = "SELECT * FROM users2 WHERE id = '$contactos->guia_id' ";
                    $stmt    = $db->query($sql1);
                    $usuario = $stmt->fetch(PDO::FETCH_OBJ);

                    $guia_correo = $usuario->username;
                    $guia_nombre = $usuario->nombres . ' ' . $usuario->apellidos;

                    $sql1    = "SELECT * FROM users2 WHERE id = '$contactos->user_id' ";
                    $stmt    = $db->query($sql1);
                    $usuario = $stmt->fetch(PDO::FETCH_OBJ);

                    $turista_nombre = $usuario->nombres . ' ' . $usuario->apellidos;
                    $turista_correo = $usuario->username;

                    $template = __DIR__ . '/contactocancelar.php';
                    $message  = file_get_contents($template);
                    $message  = str_replace('$turista$', $turista_nombre, $message);
                    $message  = str_replace('$guia$', $guia_nombre, $message);
                    $message  = str_replace('$fecha$', $fecha_contacto, $message);
                    $message  = str_replace('$ciudad$', $ciudad_contacto, $message);
                    include_once 'phpmailer/class.phpmailer.php';
                    $mail = new PHPMailer();

                    $mail->Host = "localhost";
                    $mail->SetFrom('noreply@guiasdeturismodecolombia.com.co');
                    $mail->AddAddress($turista_correo);
                    $mail->Subject = 'Cancelar contacto -  guiasdeturismodecolombia.com.co';
                    $mail->MsgHTML($message);
                    $mail->IsHTML(true);
                    $mail->CharSet = "utf-8";

                    $mail->Send();

                    $info['mensaje'] = 'La solicitud se ha rechazado';
                    $estado          = 200;

                } else {
                    $info['mensaje'] = 'No se ha podido rechazar la solicitud';
                    $estado          = 400;
                }
            } elseif ($respuesta == 1) {
                $sql1 = "UPDATE guias_contactos SET estado = '2' WHERE id = '$idcontacto'";
                $stmt = $db->query($sql1);
                if ($stmt) {
                    $sqli     = "SELECT * FROM ciudades WHERE id = '$contactos->ciudad' ";
                    $stmt     = $db->query($sqli);
                    $ciudades = $stmt->fetch(PDO::FETCH_OBJ);

                    $fecha_contacto  = $contactos->fecha;
                    $ciudad_contacto = $ciudades->ciudad;

                    $sql1    = "SELECT * FROM users2 WHERE id = '$contactos->guia_id' ";
                    $stmt    = $db->query($sql1);
                    $usuario = $stmt->fetch(PDO::FETCH_OBJ);

                    $guia_correo   = $usuario->username;
                    $guia_nombre   = $usuario->nombres . ' ' . $usuario->apellidos;
                    $guia_telefono = $usuario->telefono1;
                    if ($usuario->telefono2 != '') {
                        $guia_telefono = $guia_telefono . ' - ' . $usuario->telefono2;
                    }

                    $guia_correo = $usuario->username;

                    $sql1    = "SELECT * FROM users2 WHERE id = '$contactos->user_id' ";
                    $stmt    = $db->query($sql1);
                    $usuario = $stmt->fetch(PDO::FETCH_OBJ);

                    $turista_nombre = $usuario->nombres . ' ' . $usuario->apellidos;
                    $turista_correo = $usuario->username;

                    $template = __DIR__ . '/contactoaceptar.php';
                    $message  = file_get_contents($template);
                    $message  = str_replace('$turista$', $turista_nombre, $message);
                    $message  = str_replace('$guia$', $guia_nombre, $message);
                    $message  = str_replace('$fecha$', $fecha_contacto, $message);
                    $message  = str_replace('$ciudad$', $ciudad_contacto, $message);
                    $message  = str_replace('$guiatelefono$', $guia_telefono, $message);
                    $message  = str_replace('$guiacorreo$', $guia_correo, $message);
                    include_once 'phpmailer/class.phpmailer.php';
                    $mail = new PHPMailer();

                    $mail->Host = "localhost";
                    $mail->SetFrom('noreply@guiasdeturismodecolombia.com.co');
                    $mail->AddAddress($turista_correo);
                    $mail->Subject = 'Contacto Aceptado -  guiasdeturismodecolombia.com.co';
                    $mail->MsgHTML($message);
                    $mail->IsHTML(true);
                    $mail->CharSet = "utf-8";

                    $mail->Send();

                    $info['mensaje'] = 'La solicitud se ha aceptado';
                    $estado          = 200;

                } else {
                    $info['mensaje'] = 'No se ha podido aceptar la solicitud';
                    $estado          = 400;
                }
            }

        } else {
            $info['mensaje'] = 'No existen registros de contacto';
            $estado          = 400;
        }

        $db = null;

        return $response->withStatus($estado)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($info));
    } catch (PDOException $e) {
        $error = $e->getMessage();
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($error));
    }
});

//////////////

// registrar usuario correo turista
$app->post('/api/turista/registro/normal', function (Request $request, Response $response) {
    $nombre      = $request->getParam('nombre');
    $apellido    = $request->getParam('apellido');
    $correo      = $request->getParam('correo');
    $contrasena  = $request->getParam('contrasena');
    $nacimiento  = $request->getParam('nacimiento');
    $nacimiento2 = $nacimiento;
    $hash        = $request->getParam('hashcelular');

    // $aa = array();
    // $allPostPutVars = $request->getParsedBody();
    // foreach($allPostPutVars as $key => $param){
    //   $aa[] = $key;
    // }

    // $aa = implode(',', $aa);

    date_default_timezone_set("America/Bogota");
    $fecha_actual = date('Y-m-d H:i:s');

    $sql = "SELECT * FROM users2 WHERE username = '$correo' AND tipo = '3' ";
    try {
        if ($contrasena != '') {
            if (!empty($nacimiento) || $nacimiento != '') {
                $nacimiento             = explode('-', $nacimiento);
                $data['nacimiento_dia'] = $nacimiento[2];
                $data['nacimiento_mes'] = $nacimiento[1];
                $data['nacimiento_ano'] = $nacimiento[0];

                $stampBirth     = mktime(0, 0, 0, $data['nacimiento_mes'], $data['nacimiento_dia'], $data['nacimiento_ano']);
                $today['day']   = date('d');
                $today['month'] = date('m');
                $today['year']  = date('Y') - 18;

                $stampToday = mktime(0, 0, 0, $today['month'], $today['day'], $today['year']);

                if ($stampBirth < $stampToday) {
                    $db          = new db();
                    $db          = $db->connect();
                    $stmt        = $db->query($sql);
                    $informacion = $stmt->fetch(PDO::FETCH_OBJ);
                    if ($informacion != false) {
                        $respuesta['mensaje'] = 'Correo ya registrado';
                        $estado               = 400;
                    } else {
                        require_once "password.php";
                        $pass = password_hash($contrasena, PASSWORD_BCRYPT);

                        $sqli = "INSERT INTO users2 (tipo,nombres,apellidos,username,password,nacimiento,hashcelular,creado,estado) VALUES ('3','$nombre','$apellido','$correo','$pass','$nacimiento2','$hash','$fecha_actual',1)";
                        $stmt = $db->query($sqli);
                        if ($stmt) {
                            $sql1        = "SELECT * FROM users2 WHERE username = '$correo' AND tipo = '3' ";
                            $stmt        = $db->query($sql1);
                            $infoturista = $stmt->fetch(PDO::FETCH_OBJ);

                            $template = __DIR__ . '/nuevo-turista.php';

                            $message = file_get_contents($template);
                            $message = str_replace('$usuario$', $nombre . ' ' . $apellido, $message);
                            $message = str_replace('$correo$', $correo, $message);
                            $message = str_replace('$pass$', $contrasena, $message);
                            $message = str_replace('$enlace$', 'http://guiasdeturismodecolombia.com.co/iniciar.php', $message);
                            include_once 'phpmailer/class.phpmailer.php';
                            $mail = new PHPMailer();

                            $mail->Host = "localhost";
                            $mail->SetFrom('noreply@guiasdeturismodecolombia.com.co');
                            $mail->AddAddress($correo);
                            $mail->Subject = 'Registro -  guiasdeturismodecolombia.com.co';
                            $mail->MsgHTML($message);
                            $mail->IsHTML(true);
                            $mail->CharSet = "utf-8";

                            $mail->Send();

                            $respuesta['mensaje']   = 'Registro exitoso';
                            $respuesta['idturista'] = $infoturista->id;
                            $estado                 = 200;
                        } else {
                            $respuesta['mensaje'] = 'No se pudo registrar';
                            $estado               = 400;
                        }
                    }
                    $db = null;
                } else {
                    $respuesta['mensaje'] = 'Debe ser mayor de edad para poder registrarse.';
                    $estado               = 400;
                }
            } else {
                $respuesta['mensaje'] = 'La fecha de nacimiento es requerida.';
                $estado               = 400;
            }

        } else {
            $respuesta['mensaje'] = 'La contraseña es requerida.';
            $estado               = 400;
        }

        return $response->withStatus($estado)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($respuesta));
    } catch (PDOException $e) {
        $error = $e->getMessage();
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($error));
    }
});

// iniciar sesion correo turista
$app->post('/api/turista/inicio/normal', function (Request $request, Response $response) {
    $correo     = $request->getParam('correo');
    $contrasena = $request->getParam('contrasena');
    $hash       = $request->getParam('hashcelular');

    $sql = "SELECT * FROM users2 WHERE username = '$correo' AND tipo = '3' ";
    try {
        $db = new db();
        $db = $db->connect();

        $stmt        = $db->query($sql);
        $informacion = $stmt->fetch(PDO::FETCH_OBJ);
        if ($informacion != false) {
            if (password_verify($contrasena, $informacion->password)) {
                $respuesta['mensaje']   = 'Datos correctos';
                $respuesta['idturista'] = $informacion->id;

                if ($hash != '') {
                    $sql  = "UPDATE users2 SET hashcelular = '$hash' WHERE id = '$informacion->id' AND username = '$correo' AND tipo = '3' ";
                    $stmt = $db->query($sql);
                }
                $estado = 200;
            } else {
                $respuesta['mensaje'] = 'Correo o Contraseña incorrectos';
                $estado               = 400;
            }
        } else {
            $respuesta['mensaje'] = 'Correo o Contraseña incorrectos';
            $estado               = 400;
        }
        $db = null;

        return $response->withStatus($estado)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($respuesta));
    } catch (PDOException $e) {
        $error = $e->getMessage();
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($error));
    }
});

// registrar usuario turista google
$app->post('/api/turista/registro/google', function (Request $request, Response $response) {
    $nombre   = $request->getParam('nombre');
    $apellido = $request->getParam('apellido');
    $correo   = $request->getParam('correo');
    $googleid = $request->getParam('googleid');
    // $contrasena  = $request->getParam('contrasena');
    // $nacimiento  = $request->getParam('nacimiento');
    // $nacimiento2 = $request->getParam('nacimiento');
    $hash = $request->getParam('hashcelular');

    date_default_timezone_set("America/Bogota");
    $fecha_actual = date('Y-m-d H:i:s');

    $sql = "SELECT * FROM users2 WHERE username = '$correo' AND tipo = '3' ";
    try {
        // $nacimiento             = explode('-', $nacimiento);
        // $data['nacimiento_dia'] = $nacimiento[2];
        // $data['nacimiento_mes'] = $nacimiento[1];
        // $data['nacimiento_ano'] = $nacimiento[0];

        // $stampBirth     = mktime(0, 0, 0, $data['nacimiento_mes'], $data['nacimiento_dia'], $data['nacimiento_ano']);
        // $today['day']   = date('d');
        // $today['month'] = date('m');
        // $today['year']  = date('Y') - 18;

        // $stampToday = mktime(0, 0, 0, $today['month'], $today['day'], $today['year']);

        // if ($stampBirth < $stampToday) {
        $db          = new db();
        $db          = $db->connect();
        $stmt        = $db->query($sql);
        $informacion = $stmt->fetch(PDO::FETCH_OBJ);
        if ($informacion != false) {
            $respuesta['mensaje'] = 'Correo ya registrado';
            $estado               = 400;
        } else {
            $sqli = "INSERT INTO users2 (tipo,nombres,apellidos,username,hashcelular,googleid,creado,estado) VALUES ('3','$nombre','$apellido','$correo','$hash','$googleid','$fecha_actual',1)";
            $stmt = $db->query($sqli);
            if ($stmt) {
                $sql1        = "SELECT * FROM users2 WHERE username = '$correo' AND tipo = '3' ";
                $stmt        = $db->query($sql1);
                $infoturista = $stmt->fetch(PDO::FETCH_OBJ);

                $respuesta['mensaje']   = 'Registro exitoso';
                $respuesta['idturista'] = $infoturista->id;
                $estado                 = 200;
            } else {
                $respuesta['mensaje'] = 'No se pudo registrar';
                $estado               = 400;
            }
        }
        $db = null;
        // } else {
        //     $respuesta['mensaje'] = 'Debe ser mayor de edad para poder registrarse.';
        //     $estado               = 400;
        // }

        return $response->withStatus($estado)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($respuesta));
    } catch (PDOException $e) {
        $error = $e->getMessage();
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($error));
    }
});

// iniciar sesion turista google
$app->post('/api/turista/inicio/google', function (Request $request, Response $response) {
    $correo   = $request->getParam('correo');
    $googleid = $request->getParam('googleid');
    $hash     = $request->getParam('hashcelular');

    $sql = "SELECT * FROM users2 WHERE username = '$correo' AND googleid = '$googleid' AND tipo = '3' ";
    try {
        $db = new db();
        $db = $db->connect();

        $stmt        = $db->query($sql);
        $informacion = $stmt->fetch(PDO::FETCH_OBJ);
        if ($informacion != false) {
            $respuesta['mensaje']   = 'Datos correctos';
            $respuesta['idturista'] = $informacion->id;

            if ($hash != '') {
                $sql  = "UPDATE users2 SET hashcelular = '$hash' WHERE id = '$informacion->id' AND username = '$correo' AND tipo = '3' ";
                $stmt = $db->query($sql);
            }
            $estado = 200;
        } else {
            $respuesta['mensaje'] = 'Error, datos no válidos.';
            $estado               = 400;
        }
        $db = null;

        return $response->withStatus($estado)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($respuesta));
    } catch (PDOException $e) {
        $error = $e->getMessage();
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($error));
    }
});

// informacion turista
$app->get('/api/turista/perfil', function (Request $request, Response $response) {
    $idturista = $request->getParam('idturista');

    try {
        $db = new db();
        $db = $db->connect();

        $sql                  = "SELECT nombres, apellidos, username, nacimiento, telefono1 FROM users2 WHERE tipo = '3' AND  id = '$idturista' ";
        $stmt                 = $db->query($sql);
        $informacion          = $stmt->fetch(PDO::FETCH_OBJ);
        $respuesta['mensaje'] = $informacion;

        $db = null;

        return $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($informacion));
    } catch (PDOException $e) {
        $error = $e->getMessage();
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($error));
    }
});

// recuperar contraseña turista
$app->post('/api/turista/recuperar', function (Request $request, Response $response) {
    $correo = $request->getParam('correo');

    try {
        $db = new db();
        $db = $db->connect();

        $sql         = "SELECT * FROM users2 WHERE username = '$correo' AND  tipo = '3' ";
        $stmt        = $db->query($sql);
        $informacion = $stmt->fetch(PDO::FETCH_OBJ);
        if ($informacion != false) {
            $token        = "";
            $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $codeAlphabet .= "abcdefghijklmnopqrstuvwxyz";
            $codeAlphabet .= "0123456789";
            $max = strlen($codeAlphabet);
            for ($i = 0; $i < 8; $i++) {
                $token .= $codeAlphabet[rand(0, $max - 1)];
            }
            require_once "password.php";
            $pass     = password_hash($token, PASSWORD_BCRYPT);
            $sql      = "UPDATE users2 SET password = '$pass' WHERE  username = '$correo' AND  tipo = '3' ";
            $stmt     = $db->query($sql);
            $template = __DIR__ . '/passreset.php';

            $message = file_get_contents($template);
            $message = str_replace('$nuevopass$', $token, $message);
            $message = str_replace('$usuario$', $informacion->nombres . ' ' . $informacion->apellidos, $message);
            $message = str_replace('$enlace$', 'http://guiasdeturismodecolombia.com.co/iniciar.php', $message);
            include_once 'phpmailer/class.phpmailer.php';
            $mail = new PHPMailer();

            $mail->Host = "localhost";
            $mail->SetFrom('noreply@guiasdeturismodecolombia.com.co');
            $mail->AddAddress($correo);
            $mail->Subject = 'Restablecer contraseña -  guiasdeturismodecolombia.com.co';
            $mail->MsgHTML($message);
            $mail->IsHTML(true);
            $mail->CharSet = "utf-8";

            $mail->Send();

            // $respuesta['pass']    = $token;
            $respuesta['mensaje'] = 'Correcto';
            $estado               = 200;
        } else {
            $respuesta['mensaje'] = 'El correo no se encuentra registrado';
            $estado               = 400;
        }

        $db = null;

        return $response->withStatus($estado)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($respuesta));
    } catch (PDOException $e) {
        $error = $e->getMessage();
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($error));
    }
});

// actualizar informacion turista
$app->post('/api/turista/actualizar', function (Request $request, Response $response) {
    $idturista   = $request->getParam('idturista');
    $nacimiento  = $request->getParam('nacimiento');
    $nacimiento2 = $request->getParam('nacimiento');
    $contrasena  = $request->getParam('contrasena');
    $telefono    = $request->getParam('telefono');
    $nombres     = $request->getParam('nombres');
    $apellidos   = $request->getParam('apellidos');

    date_default_timezone_set("America/Bogota");
    $fecha_actual = date('Y-m-d H:i:s');

    $nacimiento             = explode('-', $nacimiento);
    $data['nacimiento_dia'] = $nacimiento[2];
    $data['nacimiento_mes'] = $nacimiento[1];
    $data['nacimiento_ano'] = $nacimiento[0];

    $stampBirth     = mktime(0, 0, 0, $data['nacimiento_mes'], $data['nacimiento_dia'], $data['nacimiento_ano']);
    $today['day']   = date('d');
    $today['month'] = date('m');
    $today['year']  = date('Y') - 18;

    $stampToday = mktime(0, 0, 0, $today['month'], $today['day'], $today['year']);

    try {
        $db          = new db();
        $db          = $db->connect();
        $sql         = "SELECT * FROM users2 WHERE id = '$idturista' AND tipo = '3' ";
        $stmt        = $db->query($sql);
        $stmt        = $db->query($sql);
        $informacion = $stmt->fetch(PDO::FETCH_OBJ);
        if ($informacion != false) {
            if ($stampBirth < $stampToday) {
                $sql  = "UPDATE users2 SET nombres='$nombres', apellidos = '$apellidos', telefono1 = '$telefono', nacimiento = '$nacimiento2', modificado = '$fecha_actual' WHERE id = '$idturista' AND tipo = '3' ";
                $stmt = $db->query($sql);
                if ($stmt) {
                    if ($contrasena != '') {
                        require_once "password.php";
                        $pass = password_hash($contrasena, PASSWORD_BCRYPT);

                        $sql  = "UPDATE users2 SET password='$pass' WHERE id = '$idturista' AND tipo = '3' ";
                        $stmt = $db->query($sql);
                    }

                    $respuesta['mensaje'] = 'Datos actualizados';
                    $estado               = 200;
                } else {
                    $respuesta['mensaje'] = 'Error, los datos no se actualizaron';
                    $estado               = 400;
                }
            } else {
                $respuesta['mensaje'] = 'Error, debe ser mayor de edad.';
                $estado               = 400;
            }
        } else {
            $respuesta['mensaje'] = 'Error, el turista no existe.';
            $estado               = 400;
        }

        $db = null;

        return $response->withStatus($estado)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($respuesta));
    } catch (PDOException $e) {
        $error = $e->getMessage();
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($error));
    }
});

// buscar guia
$app->post('/api/guia/busqueda', function (Request $request, Response $response) {
    $ciudad       = $request->getParam('ciudad');
    $dispo        = $request->getParam('disponibilidad');
    $especialidad = $request->getParam('especialidades');
    $servicio     = $request->getParam('servicios');
    $idioma       = $request->getParam('idiomas');

    try {
        date_default_timezone_set("America/Bogota");
        $fecha_actual = date('Y-m-d');

        if ($dispo >= $fecha_actual) {
            $db = new db();
            $db = $db->connect();

            $sql_idioma = $w_idioma = '';
            if ($idioma != '') {
                $sql_idioma = ',user_idiomas ud';
                $w_idioma   = "AND ud.user_id = g.user_id AND ud.idioma_id IN ($idioma)";
            }

            $sql_espe = $w_espe = '';
            if ($especialidad != '') {
                $sql_espe = ',especialidades_guias eg';
                $w_espe   = "AND eg.guia_id=g.id AND eg.especialidade_id IN ($especialidad)";
            }

            $sql_serv = $w_serv = '';
            if ($servicio != '') {
                $sql_serv = ',servicios_guias sg';
                $w_serv   = "AND sg.guia_id=g.id AND sg.servicio_id IN ($servicio)";
            }

            $sql   = "SELECT g.id, g.user_id, g.RNT, g.tarjeta_profesional, u.nombres, u.apellidos,gd.ciudad as ciudad_dispo FROM guias g, users2 u $sql_idioma $sql_espe $sql_serv, guias_disponibilidad gd  WHERE g.suspendido = '0' AND u.id=g.user_id $w_idioma $w_espe $w_serv  AND gd.inicio <= '$dispo' AND gd.fin >= '$dispo' AND gd.estado = 1 AND gd.ciudad= '$ciudad' AND gd.guia_id = g.user_id ORDER BY  u.apellidos";
            $stmt  = $db->query($sql);
            $guias = $stmt->fetchAll(PDO::FETCH_OBJ);
            if ($guias != false) {
                foreach ($guias as $guia) {
                    $ls_idiomas = '';
                    $sql1       = "SELECT * FROM user_idiomas WHERE user_id = '$guia->user_id' ";
                    $stmt       = $db->query($sql1);
                    $idiomas    = $stmt->fetchAll(PDO::FETCH_OBJ);
                    if ($idiomas != false) {
                        foreach ($idiomas as $gidioma) {
                            $sql2      = "SELECT * FROM idiomas WHERE id = '$gidioma->idioma_id' ";
                            $stmt      = $db->query($sql2);
                            $idiomanms = $stmt->fetchAll(PDO::FETCH_OBJ);
                            foreach ($idiomanms as $idiomanm) {
                                $ls_idiomas[] = $idiomanm->idioma;
                            }
                        }
                    } else {
                        $ls_idiomas = array();
                    }

                    $ls_espe        = '';
                    $sql1           = "SELECT * FROM especialidades_guias WHERE guia_id = '$guia->id' ";
                    $stmt           = $db->query($sql1);
                    $especialidades = $stmt->fetchAll(PDO::FETCH_OBJ);
                    if ($especialidades != false) {
                        foreach ($especialidades as $especial) {
                            $sql2        = "SELECT * FROM especialidades WHERE id = '$especial->especialidade_id' ";
                            $stmt        = $db->query($sql2);
                            $especialnms = $stmt->fetchAll(PDO::FETCH_OBJ);
                            foreach ($especialnms as $especialnm) {
                                $ls_espe[] = $especialnm->nombre;
                            }
                        }
                    } else {
                        $ls_espe = array();
                    }

                    $ls_serv   = '';
                    $sql1      = "SELECT * FROM servicios_guias WHERE guia_id = '$guia->id' ";
                    $stmt      = $db->query($sql1);
                    $servicios = $stmt->fetchAll(PDO::FETCH_OBJ);
                    foreach ($servicios as $servi) {
                        $sql2        = "SELECT * FROM servicios WHERE id = '$servi->servicio_id' ";
                        $stmt        = $db->query($sql2);
                        $servicionms = $stmt->fetchAll(PDO::FETCH_OBJ);
                        foreach ($servicionms as $servicionm) {
                            $ls_serv[] = $servicionm->nombre;
                        }
                    }

                    $ls_ciud     = '';
                    $sql1        = "SELECT * FROM ciudades WHERE id = '$guia->ciudad_dispo' ";
                    $stmt        = $db->query($sql1);
                    $ubicaciones = $stmt->fetchAll(PDO::FETCH_OBJ);
                    foreach ($ubicaciones as $ubicacion) {
                        $ls_ciud[] = $ubicacion->ciudad;
                    }

                    //foto
                    $sqli   = "SELECT nombre, url FROM imagenes WHERE user_id = '$guia->user_id' ";
                    $stmt   = $db->query($sqli);
                    $imagen = $stmt->fetch(PDO::FETCH_OBJ);
                    if ($imagen != false) {
                        $foto_guia = 'https://www.guiasdeturismodecolombia.com.co' . $imagen->url . $imagen->nombre;
                    } else {
                        $foto_guia = '';
                    }

                    $respuesta['guias'][] = array('idguia' => $guia->user_id, 'nombre' => $guia->nombres . ' ' . $guia->apellidos, 'RNT' => $guia->RNT, 'tarjeta' => $guia->tarjeta_profesional, 'idiomas' => $ls_idiomas, 'especialidades' => $ls_espe, 'servicios' => $ls_serv, 'ubicaciones' => $ls_ciud, 'imagen' => $foto_guia);
                    $estado               = 200;

                }
            } else {
                $respuesta['mensaje'] = 'No se encontraron guías con el criterio de busqueda realizado.';
                $estado               = 400;
            }
            $db = null;
        } else {
            $respuesta['mensaje'] = 'La fecha de disponibilidad del guía debe ser mayor o igual a la fecha actual.';
            $estado               = 400;
        }

        return $response->withStatus($estado)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($respuesta));
    } catch (PDOException $e) {
        $error = $e->getMessage();
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($error));
    }
});

///contactar guia

//estados 1 pendiente, 2 contactado, 3 terminado, 4 cancelado
$app->post('/api/turista/contacto', function (Request $request, Response $response) {
    $idturista      = $request->getParam('idturista');
    $idguia         = $request->getParam('idguia');
    $ciudad         = $request->getParam('ciudad');
    $fecha          = $request->getParam('fecha');
    $requerimientos = $request->getParam('requerimientos');

    try {
        $db   = new db();
        $db   = $db->connect();
        $sqli = "INSERT INTO guias_contactos (user_id,guia_id,fecha,ciudad,estado,observaciones_usuario) VALUES ('$idturista','$idguia','$fecha','$ciudad','1','$requerimientos')";
        $stmt = $db->query($sqli);
        // $last_id = $db->lastInsertId();
        $stmt    = $db->query("SELECT LAST_INSERT_ID()");
        $last_id = $stmt->fetchColumn();
        if ($stmt) {
            $sql1      = "SELECT * FROM guias_contactos WHERE id = '$last_id' AND user_id = '$idturista' ";
            $stmt      = $db->query($sql1);
            $contactos = $stmt->fetch(PDO::FETCH_OBJ);

            $sqli     = "SELECT * FROM ciudades WHERE id = '$contactos->ciudad' ";
            $stmt     = $db->query($sqli);
            $ciudades = $stmt->fetch(PDO::FETCH_OBJ);

            $fecha_contacto  = $contactos->fecha;
            $ciudad_contacto = $ciudades->ciudad;

            $sql1    = "SELECT * FROM users2 WHERE id = '$contactos->guia_id' ";
            $stmt    = $db->query($sql1);
            $usuario = $stmt->fetch(PDO::FETCH_OBJ);

            $guia_correo = $usuario->username;
            $guia_nombre = $usuario->nombres . ' ' . $usuario->apellidos;

            $sql1    = "SELECT * FROM users2 WHERE id = '$contactos->user_id' ";
            $stmt    = $db->query($sql1);
            $usuario = $stmt->fetch(PDO::FETCH_OBJ);

            $turista_nombre = $usuario->nombres . ' ' . $usuario->apellidos;

            $template = __DIR__ . '/contactonotificar.php';
            $message  = file_get_contents($template);
            $message  = str_replace('$turista$', $turista_nombre, $message);
            $message  = str_replace('$guia$', $guia_nombre, $message);
            $message  = str_replace('$fecha$', $fecha_contacto, $message);
            $message  = str_replace('$ciudad$', $ciudad_contacto, $message);
            include_once 'phpmailer/class.phpmailer.php';
            $mail = new PHPMailer();

            $mail->Host = "localhost";
            $mail->SetFrom('noreply@guiasdeturismodecolombia.com.co');
            $mail->AddAddress($guia_correo);
            $mail->Subject = 'Notificación de contacto -  guiasdeturismodecolombia.com.co';
            $mail->MsgHTML($message);
            $mail->IsHTML(true);
            $mail->CharSet = "utf-8";

            $mail->Send();

            $respuesta['mensaje'] = 'Petición de contacto enviada';
            $estado               = 200;
        } else {
            $respuesta['mensaje'] = 'Error al crear la petición de contacto';
            $estado               = 400;
        }

        $db = null;

        return $response->withStatus($estado)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($respuesta));
    } catch (PDOException $e) {
        $error = $e->getMessage();
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($error));
    }
});

// historial guias contactados
$app->get('/api/turista/contacto/historial', function (Request $request, Response $response) {
    $idturista = $request->getParam('idturista');

    try {
        $db       = new db();
        $db       = $db->connect();
        $sql      = "SELECT * FROM guias_contactos WHERE user_id = '$idturista' ";
        $stmt     = $db->query($sql);
        $historia = $stmt->fetchAll(PDO::FETCH_OBJ);

        if ($historia != false) {
            foreach ($historia as $hs) {
                $sql1     = "SELECT * FROM users2 WHERE id = '$hs->guia_id' ";
                $stmt     = $db->query($sql1);
                $usuarios = $stmt->fetch(PDO::FETCH_OBJ);

                $sql1  = "SELECT * FROM guias WHERE user_id = '$hs->guia_id' ";
                $stmt  = $db->query($sql1);
                $guias = $stmt->fetch(PDO::FETCH_OBJ);

                //foto
                $sqli   = "SELECT nombre, url FROM imagenes WHERE user_id = '$hs->guia_id' ";
                $stmt   = $db->query($sqli);
                $imagen = $stmt->fetch(PDO::FETCH_OBJ);
                if ($imagen != false) {
                    $foto_guia = 'https://www.guiasdeturismodecolombia.com.co' . $imagen->url . $imagen->nombre;
                } else {
                    $foto_guia = '';
                }

                $sqli     = "SELECT * FROM ciudades WHERE id = '$hs->ciudad' ";
                $stmt     = $db->query($sqli);
                $ciudades = $stmt->fetch(PDO::FETCH_OBJ);

                //estados 1 pendiente, 2 contactado, 3 terminado, 4 cancelado

                switch ($hs->estado) {
                    case '1':
                        $estado = 'Pendiente';
                        break;
                    case '2':
                        $estado = 'Contactado';
                        break;
                    case '3':
                        $estado = 'Terminado';
                        break;
                    case '4':
                        $estado = 'Cancelado';
                        break;
                }

                $respuesta['mensaje'][] = array('idcontacto' => $hs->id, 'guia' => $usuarios->nombres . ' ' . $usuarios->apellidos, 'rnt' => $guias->RNT, 'tarjeta_profesional' => $guias->tarjeta_profesional, 'correo_guia' => $usuarios->username, 'telefono' => $usuarios->telefono1, 'imagen' => $foto_guia, 'fecha' => $hs->fecha, 'ciudad' => $ciudades->ciudad, 'estado' => $estado);
            }
            $estado = 200;
        } else {
            $respuesta['mensaje'] = 'No existen registros de contacto';
            $estado               = 400;
        }

        $db = null;

        return $response->withStatus($estado)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($respuesta));
    } catch (PDOException $e) {
        $error = $e->getMessage();
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($error));
    }
});

// cancelar guias contactados
$app->post('/api/turista/contacto/cancelar', function (Request $request, Response $response) {
    $idcontacto = $request->getParam('idcontacto');
    $idturista  = $request->getParam('idturista');

    try {
        $db        = new db();
        $db        = $db->connect();
        $sql       = "SELECT * FROM guias_contactos WHERE id = '$idcontacto' AND user_id = '$idturista' ";
        $stmt      = $db->query($sql);
        $contactos = $stmt->fetch(PDO::FETCH_OBJ);

        if ($contactos != false) {

            $sql1 = "UPDATE guias_contactos SET estado = '4' WHERE id = '$idcontacto' AND user_id = '$idturista' ";
            $stmt = $db->query($sql1);
            if ($stmt) {

                $sqli     = "SELECT * FROM ciudades WHERE id = '$contactos->ciudad' ";
                $stmt     = $db->query($sqli);
                $ciudades = $stmt->fetch(PDO::FETCH_OBJ);

                $fecha_contacto  = $contactos->fecha;
                $ciudad_contacto = $ciudades->ciudad;

                $sql1    = "SELECT * FROM users2 WHERE id = '$contactos->guia_id' ";
                $stmt    = $db->query($sql1);
                $usuario = $stmt->fetch(PDO::FETCH_OBJ);

                $guia_correo = $usuario->username;
                $guia_nombre = $usuario->nombres . ' ' . $usuario->apellidos;

                $sql1    = "SELECT * FROM users2 WHERE id = '$contactos->user_id' ";
                $stmt    = $db->query($sql1);
                $usuario = $stmt->fetch(PDO::FETCH_OBJ);

                $turista_nombre = $usuario->nombres . ' ' . $usuario->apellidos;

                $template = __DIR__ . '/guiacancelar.php';
                $message  = file_get_contents($template);
                $message  = str_replace('$turista$', $turista_nombre, $message);
                $message  = str_replace('$guia$', $guia_nombre, $message);
                $message  = str_replace('$fecha$', $fecha_contacto, $message);
                $message  = str_replace('$ciudad$', $ciudad_contacto, $message);
                include_once 'phpmailer/class.phpmailer.php';
                $mail = new PHPMailer();

                $mail->Host = "localhost";
                $mail->SetFrom('noreply@guiasdeturismodecolombia.com.co');
                $mail->AddAddress($guia_correo);
                $mail->Subject = 'Cancelar contacto -  guiasdeturismodecolombia.com.co';
                $mail->MsgHTML($message);
                $mail->IsHTML(true);
                $mail->CharSet = "utf-8";

                $mail->Send();

                $respuesta['mensaje'] = 'Contacto cancelado';
                $estado               = 200;

            } else {
                $respuesta['mensaje'] = 'Error al cancelar el contacto';
                $estado               = 400;
            }

        } else {
            $respuesta['mensaje'] = 'No existen registros de contacto';
            $estado               = 400;
        }

        $db = null;

        return $response->withStatus($estado)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($respuesta));
    } catch (PDOException $e) {
        $error = $e->getMessage();
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($error));
    }
});

/////////
// departamentos y ciudades
$app->get('/api/ciudades', function (Request $request, Response $response) {

    $sql = "SELECT id, departamento FROM departamentos WHERE paise_id = 1";

    try {
        $db   = new db();
        $db   = $db->connect();
        $stmt = $db->query($sql);

        $departamentos              = $stmt->fetchAll(PDO::FETCH_OBJ);
        $informacion->departamentos = $departamentos;

        $sqli                  = "SELECT id, departamento_id, ciudad FROM ciudades ";
        $stmt                  = $db->query($sqli);
        $ciudades              = $stmt->fetchAll(PDO::FETCH_OBJ);
        $informacion->ciudades = $ciudades;

        $db = null;

        return $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($informacion));
    } catch (PDOException $e) {
        $error = $e->getMessage();
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($error));
    }
});

// idiomas
$app->get('/api/idiomas', function (Request $request, Response $response) {

    $sql = "SELECT id, idioma FROM idiomas ";

    try {
        $db   = new db();
        $db   = $db->connect();
        $stmt = $db->query($sql);

        $idiomas              = $stmt->fetchAll(PDO::FETCH_OBJ);
        $informacion->idiomas = $idiomas;

        $db = null;

        return $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($informacion));
    } catch (PDOException $e) {
        $error = $e->getMessage();
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($error));
    }
});

// especialidades
$app->get('/api/especialidades', function (Request $request, Response $response) {

    $sql = "SELECT id, nombre FROM especialidades ";

    try {
        $db   = new db();
        $db   = $db->connect();
        $stmt = $db->query($sql);

        $especialidades              = $stmt->fetchAll(PDO::FETCH_OBJ);
        $informacion->especialidades = $especialidades;

        $db = null;

        return $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($informacion));
    } catch (PDOException $e) {
        $error = $e->getMessage();
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($error));
    }
});

// servicios
$app->get('/api/servicios', function (Request $request, Response $response) {

    $sql = "SELECT id, nombre FROM servicios ";

    try {
        $db   = new db();
        $db   = $db->connect();
        $stmt = $db->query($sql);

        $servicios              = $stmt->fetchAll(PDO::FETCH_OBJ);
        $informacion->servicios = $servicios;

        $db = null;

        return $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($informacion));
    } catch (PDOException $e) {
        $error = $e->getMessage();
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($error));
    }
});

// slidder
$app->get('/api/slider', function (Request $request, Response $response) {

    $sql = "SELECT imagen_sd FROM slider WHERE tipo_sd = '1' ";

    try {
        $db   = new db();
        $db   = $db->connect();
        $stmt = $db->query($sql);

        $informacion = $stmt->fetchAll(PDO::FETCH_OBJ);
        foreach ($informacion as $imagen) {
            $sld_img                = substr($imagen->imagen_sd, 6);
            $repuesta['imagenes'][] = 'https://www.guiasdeturismodecolombia.com.co/' . $sld_img;
        }

        $db = null;

        return $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($repuesta));
    } catch (PDOException $e) {
        $error = $e->getMessage();
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($error));
    }
});

/// slidder con link

$app->get('/api/slider2', function (Request $request, Response $response) {

    $sql = "SELECT imagen_sd,link_sd FROM slider WHERE tipo_sd = '1' ";

    try {
        $db   = new db();
        $db   = $db->connect();
        $stmt = $db->query($sql);

        $informacion = $stmt->fetchAll(PDO::FETCH_OBJ);
        foreach ($informacion as $imagen) {
            $sld_img                = substr($imagen->imagen_sd, 6);
            $repuesta['imagenes'][] = array('imagen' => 'https://www.guiasdeturismodecolombia.com.co/' . $sld_img, 'link' => $imagen->link_sd);
        }

        $db = null;

        return $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($repuesta));
    } catch (PDOException $e) {
        $error = $e->getMessage();
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($error));
    }
});

// banner
$app->get('/api/banner', function (Request $request, Response $response) {

    $sql = "SELECT imagen_b AS imagen FROM banner ";

    try {
        $db   = new db();
        $db   = $db->connect();
        $stmt = $db->query($sql);

        $imagen = $stmt->fetch(PDO::FETCH_OBJ);
        if ($imagen != false) {
            $respuesta->imagen = 'https://www.guiasdeturismodecolombia.com.co/images/banner/' . $imagen->imagen;
            $estado            = 200;
        } else {
            $respuesta->imagen = '';
            $estado            = 400;
        }

        $db = null;

        return $response->withStatus($estado)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($respuesta));
    } catch (PDOException $e) {
        $error = $e->getMessage();
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($error));
    }
});

// calificar guia
$app->post('/api/calificaciones/puntuar', function (Request $request, Response $response) {
    $idguia     = $request->getParam('idguia');
    $idcontacto = $request->getParam('idcontacto');
    $c1         = $request->getParam('c1');
    $c2         = $request->getParam('c2');
    $c3         = $request->getParam('c3');
    $c4         = $request->getParam('c4');
    $c5         = $request->getParam('c5');

    try {
        $db = new db();
        $db = $db->connect();

        $sql         = "SELECT * FROM guias_contactos WHERE id = '$idcontacto' ";
        $stmt        = $db->query($sql);
        $informacion = $stmt->fetch(PDO::FETCH_OBJ);

        if ($informacion != false) {
            $sql  = "UPDATE guias_contactos SET calificacion = '1',estado='3' WHERE id = '$idcontacto'";
            $stmt = $db->query($sql);

            date_default_timezone_set("America/Bogota");
            $fecha_actual = date('Y-m-d H:i:s');

            $sql1 = "INSERT INTO guias_calificaciones (contacto_id,calificacion1,calificacion2,calificacion3,calificacion4,calificacion5,creado) VALUES ('$idcontacto','$c1','$c2','$c3','$c4','$c5','$fecha_actual') ";
            $stmt = $db->query($sql1);
            if ($stmt) {
                $estado               = 200;
                $respuesta['mensaje'] = 'Se ha calificado al guía correctamente.';
            } else {
                $estado               = 400;
                $respuesta['mensaje'] = 'Error, no se ha podido llevar a cabo la acción requerida.';
            }

        } else {
            $estado               = 400;
            $respuesta['mensaje'] = 'Error, el contacto de guía a calificar no existe.';
        }
        $db = null;

        return $response->withStatus($estado)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($respuesta));
    } catch (PDOException $e) {
        $error = $e->getMessage();
        return $response->withStatus(400)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($error));
    }
});
