<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
		<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" >
		<meta name="keywords" lang="es" content="">
		<meta name="robots" content="All">
		<meta name="description" lang="es" content="">
		<title>Tarjeta profesional</title>
		<meta http-equiv="Cache-control" content="public">
		<link rel="stylesheet" href="css/slider.css" />
		<link rel="stylesheet" href="css/stylesheet.css" />
		<link rel="stylesheet" href="css/load.css" />
		<link rel="stylesheet" href="css/jquery.modal.css" />
		<link rel="stylesheet" href="css/jquery.remodal.css" />
		<link rel="stylesheet" href="css/stylesheetnew.css" />
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-44406258-20"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-44406258-20');
		</script>
	</head>

	<body>
		<header>
			<?php include "new-header-top.php";?>
		</header>
		<div class="Impul">
			<div class="Impul-int">
				<a href="#">
					<img src="images/appicon.png" class="New-App">
				</a>
			</div>
		</div>
		<?php include "sliderweb.php";?>
		<div class=""></div>
		<section>
			<div class="Conten-global">
				<div class="Conten-global-int">
					<div class="Conten-global-int-bloques">
						<a href="contactenos">
							<img src="images/img1.png">
						</a>
					</div>
					<div class="Conten-global-int-bloques">
						<a href="solicitar-tarjeta">
							<img src="images/img3.png">
						</a>
					</div>
					<div class="Conten-global-int-bloques">
						<a href="contactar">
							<img src="images/img2.png">
						</a>
					</div>
				</div>
			</div>
		</section>
		<section>
			<div class="Bloques">
				<div class="Bloques-int">
					<div class="Img-header">
						<img src="images/tarejtaprofesional.png">
					</div>
					<div class="Bloques-img">
						<div class="Bloques-img-sec">
							<a href="requisitos"><img src="images/Recurso6.png"></a>
						</div>
						<div class="Bloques-img-sec">
							<a href="solicitar-tarjeta"><img src="images/Recurso7.png"></a>
						</div>
						<div class="Bloques-img-sec">
							<a href="#mod_estado"><img src="images/Recurso8.png"></a>
						</div>
					</div>
					<p><strong>IMPORTANTE:</strong> Recuerde que para tramitar su Tarjeta Profesional o un duplicado de la misma, usted debe usar el aplicativo de Solicitud en línea ubicado en esta misma sección (arriba) o enviar su documentación escaneada al correo electrónico:</p>
					<p><span class="Cl-azul">coordinacion@guiasdeturismodecolombia.com.co</span></p>
					<div class="Nota">
						<p><strong>NOTA: </strong> Los valores correspondientes a la TASA de formulario y tramitación de la tarjeta profesional de Guía de Turismo (por primera vez), NO SON REINTEGRABLES en ningún caso, debido a que corresponden a los gastos administrativos y operativos del Consejo Profesional de Guías de Turismo, por concepto del análisis y tramitación de las solicitudes de este documento legal. Por favor, antes de consignar el valor de la Tasa de tramitación, VERIFIQUE que posee toda la documentación requerida, de acuerdo con lo estipulado en las normas vigentes y los requisitos publicados en esta página web, con el fin de evitar posibles inconvenientes posteriores.</p>
					</div>
				</div>
			</div>
		</section>
		<section>
			<div class="Conten-global">
				<div class="Conten-global-int">
					<div class="Conten-redes">
						<div class="Conten-redes-int">
							<div class="Conten-redes-int-sec">
								<div class="Redesg-facetw">
									<div class="fb-page" data-href="https://www.facebook.com/pg/MincomercioCo/posts/?ref=page_internal" data-tabs="timeline" data-width="500" data-height="400" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/pg/MincomercioCo/posts/?ref=page_internal" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/pg/MincomercioCo/posts/?ref=page_internal">Ministerio de Comercio, Industria y Turismo</a></blockquote></div>
								</div>
							</div>
							<div class="Conten-redes-int-sec">
								<div class="Redesg-facetw">
									<a class="twitter-timeline" href="https://twitter.com/mincomercioco?lang=es">Tweets by consejodeguias</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<div class="remodal" data-remodal-id="mod_estado">
			<div class="sol-modal">
				<div class="Login-int">
					<p>Ingrese su número de identificación para comprobar el estado de su trámite</p>
					<form id="ck-tramite">
						<label>Identificaci&oacute;n</label>
		                <select name="tipoid" id="tipoid" required="">
		                    <option value="1">C&eacute;dula de ciudadan&iacute;a</option>
		                    <option value="2">C&eacute;dula Extranjeria</option>
		                    <option value="3">Tarjeta de identidad</option>
		                </select>
						<label>N&uacute;mero:</label>
                  		<input name="numeroid" type="text" size="12"  required="" />
						<input type="submit" class="Btn-azul" value="Comprobar">
					</form>
				</div>
			</div>
		</div>
		<footer>
			<?php include "new-footer.php";?>
		</footer>
		<script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
		<script src="js/jquery.modal.min.js"></script>
		<script src="js/jquery.remodal.js"></script>
		<script src="js/tramite.js"></script>
		<script>
		$(function() {
		$('#slides').superslides({
		inherit_width_from: '.wide-container',
		inherit_height_from: '.wide-container',
		play: 5000,
		animation: 'fade'
		});
		});
		</script>
		<script type="text/javascript">
			$(function(){
				$('#Drop').bind('click',function() {
				$('.Top-inf').toggleClass('Top-inf-apa');
			});
			});
		</script>
		<script src="js/jquery.superslides.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="js/mostrar-invest-index.js"></script>
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.7";
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
	</body>
</html>