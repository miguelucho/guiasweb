<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" >
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">	
	<title>Contactar Guiás de Turismo Colombia</title>
	<meta http-equiv="Cache-control" content="public">
	<link rel="stylesheet" href="contactarguia/css/slider.css" />
	<link rel="stylesheet" href="contactarguia/css/stylesheet.css" />	
	<link rel="stylesheet" href="css/stylesheetnew.css" />	
	<style type="text/css">
		.goog-te-gadget-simple .goog-te-menu-value{
			color: #fff !important;

		}
		.goog-te-gadget-simple{
			background: transparent !important;
			border: none !important;
		}
		.goog-te-gadget-simple img{
			visibility: hidden;
		}
	</style>
</head>
<body>
	<header>
		<?php include "new-header-top.php";?>	
	</header>
	<section>
		<div class="Slider" style="padding-top:  0px;">
		<div class="Slider-imagen">
			<div class="wide-container">
				    <div id="slides">
				      <ul class="slides-container">
				     	 <li>
				          <img src="contactarguia/images/ImagenColombia1.jpg" alt="Programa Bioflores">
				          <div class="context">					        
					      </div>
				        </li>
				        <li>
				          <img src="contactarguia/images/ImagenColombia5.jpg" alt="Programa Biopalama">
				          <div class="context">
					        
					      </div>
				        </li>
				        <li>
				          <img src="contactarguia/images/ImagenColombia6.jpg" alt="Programa Biocafe">
				          <div class="context">
					        
					      </div>
				        </li>
				        <li>
				          <img src="contactarguia/images/ImagenColombia3.jpg" alt="Programa Biocaña">
				          <div class="context">
					        
					      </div>
				        </li>				        
				      </ul>
		   			</div>
	  			</div>
	  			<div class="Slider-conten">
	  				<div class="Slider-conten-int"></div>
	  			</div>				 
		</div>

		<div class="Form-home">
			<div class="Form-home-int">
				<div class="Form-home-botones">
					<div class="Form-home-botones-int">
						<!-- <a href="#">Seleccionar Idioma</a> -->
						<div id="google_translate_element"></div><script type="text/javascript">
					function googleTranslateElementInit() {
					  new google.translate.TranslateElement({pageLanguage: 'es', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
					}
					</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
					        
					</div>
					<div class="Form-home-botones-int">
						<img src="contactarguia/images/index1.png"><a href="guias">¿Requieres servicio de guianza?</a>
					</div>
					<div class="Form-home-botones-int">
						<img src="contactarguia/images/index2.png"><a href="iniciar">Eres guiá de turismo?</a>
					</div>
				</div>
			</div>
		</div>
		</div>		
	</section>
	<section>
		<div class="Menu-2">
			<div class="Menu-2-int">
				<p> <a href="mapa-del-sitio">Mapa del sitio</a> | <a href="terminos">Términos y condiciones</a>
				</p>
			</div>
		</div>
	</section>
	<footer>
		<?php include "new-footer.php";?>	
		<!-- <div class="Foot">
			<div class="Foot-int">
				<p>
					<img src="images/logofontur.png">
					<img src="images/logomincit.png">
				</p>
				<p>Copyright © 2018. Todos los derechos reservados, Consejo Profesional de Guías de Turismo</p>
			</div>
		</div> -->
	</footer>

<script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
<script>
    $(function() {
    $('#slides').superslides({
    inherit_width_from: '.wide-container',
    inherit_height_from: '.wide-container',
    play: 5000,
    animation: 'fade'
    });
    });
</script>
<script type="text/javascript">
	$(function(){
		$('#Drop').bind('click',function() {
		$('.Top-inf').toggleClass('Top-inf-apa');
	});
	});
</script>
<script src="js/jquery.superslides.min.js" type="text/javascript" charset="utf-8"></script>
</body>
</html>
