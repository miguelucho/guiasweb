<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
		<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" >
		<meta name="keywords" lang="es" content="">
		<meta name="robots" content="All">
		<meta name="description" lang="es" content="">
		<title>Contactar Guiás de Turismo Colombia</title>
		<meta http-equiv="Cache-control" content="public">
		<link rel="stylesheet" href="css/slider.css" />
		<link rel="stylesheet" href="css/load.css" />
		<link rel="stylesheet" href="css/jquery.modal.css" />
		<link rel="stylesheet" href="css/stylesheet.css" />
		<link rel="stylesheet" href="css/stylesheetnew.css" />
		<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-44406258-20"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-44406258-20');
</script>
	</head>
	<body>
		<header>
			<?php include "new-header-top.php";?>
		</header>
		<section>
			<div class="Contacto-nuevo Login" style=" padding-top: 200px; padding-bottom: 100px;">
				<div class="Contacto-nuevo-int Login-int">
					<h2>Contáctenos</h2>
					<form id="contactenos">
						<label>Tipo*</label>
						<select name="contacto[tipo]" required="true">
							<option>Solicitud de información</option>
							<option>Sugerencias</option>
							<option>Quejas y reclamos</option>
						</select>
						<label>Nombre*</label>
						<input type="text" placeholder="Nombre" name="contacto[nombre]" required="true">
						<label>Apellido*</label>
						<input type="text" placeholder="Apellido" name="contacto[apellido]" required="true">
						<label>Tipo de identificación*</label>
						<select required="true" name="contacto[tipoiden]">
							<option value="C.C">Cedula de ciudadanía</option>
							<option value="C.E">Cedula extranjera</option>
							<option value="T.I">Tarjeta de identidad</option>
							<option value="Pasaporte">Pasaporte</option>
						</select required="true">
						<label>Numero*</label>
						<input type="text" placeholder="Numero" name="contacto[identificacion]" required="true">
						<label>Email*</label>
						<input type="text" placeholder="Email" name="contacto[correo]" required="true">
						<label>Comentarios*</label>
						<textarea placeholder="Escribir mensaje" required="true" name="contacto[comentario]"></textarea>
						<label>Verifica que toda la información sea correcta</label>
						<input type="submit" class="Btn-azul" value="Enviar mensaje" name="">
					</form>
				</div>
			</div>
		</section>
		<footer>
			<?php include "new-footer.php";?>
		</footer>
		<script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
		<script src="js/jquery.modal.min.js"></script>
		<script src="js/contacto.js" type="text/javascript"></script>
		<script type="text/javascript">
			$(function(){
				$('#Drop').bind('click',function() {
				$('.Top-inf').toggleClass('Top-inf-apa');
			});
			});
		</script>
	</body>
</html>