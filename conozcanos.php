<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" >
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">
	<title>Conozcanos</title>
	<meta http-equiv="Cache-control" content="public">
	<link rel="stylesheet" href="css/slider.css" />
	<link rel="stylesheet" href="css/stylesheetnew.css" />
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-44406258-20"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-44406258-20');
	</script>
</head>
<body>
<header>
	<?php include "new-header-top.php";?>
</header>
<div class="Impul">
	<div class="Impul-int">
		<a href="#">
			<img src="images/appicon.png" class="New-App">
		</a>
	</div>
</div>
	<?php include "sliderweb.php";?>
<div class=""></div>
<section>
	<div class="Conten-global">
		<div class="Conten-global-int">
			<div class="Conten-global-int-bloques">
				<a href="solicitar-tarjeta">
					<img src="images/img1.png">
				</a>
			</div>
			<div class="Conten-global-int-bloques">
				<a href="solicitar-tarjeta">
					<img src="images/img3.png">
				</a>
			</div>
			<div class="Conten-global-int-bloques">
				<a href="contactar">
					<img src="images/img2.png">
				</a>
			</div>
		</div>
	</div>
</section>
<section>
	<div class="Bloques">
		<div class="Bloques-int">
			<!-- <div class="Img-header">
				<img src="images/tarejtaprofesional.png">
			</div> -->
			<div class="Bloques-img">
				<div class="Bloques-img-sec">
					<img src="images/logo.jpg">
				</div>
			</div>
			<p>De conformidad con lo establecido en el Decreto 503 de 1997, Artículo 11, el <strong>CONSEJO PROFESIONAL DE GUIAS DE TURISMO</strong> es un organismo técnico encargado de velar encargado de velar por el desarrollo y el adecuado ejercicio de la profesión y de expedir las Tarjetas Profesionales de los Guías de Turismo, previo cumplimiento de los requisitos exigidos por la ley.</p>

			<div class="Bloques-redes">
				<div class="Bloques-img">
					<p><h2>Síguenos!</h2></p>
					<p>Para mantenerte en contacto con la  actualidad de tu profesión.</p>
					<div class="Bloques-img-sec">
						<a href="https://twitter.com/MincomercioCo" target="_blank"><img src="images/Recurso9.png" style="width:120px;"></a>
						<figcaption>Ministerio de Comercio, Industria y Turismo</figcaption>
					</div>
					<div class="Bloques-img-sec">
						<a href="https://www.facebook.com/MincomercioCo/" target="_blank"><img src="images/Recurso11.png" style="width:120px;"></a>
						<figcaption>@MincomercioCo</figcaption>
					</div>
					<div class="Bloques-img-sec">
						<a href="https://wa.me/573138489691" target="_blank"><img src="images/Recurso10.png" style="width:120px;"></a>
						<figcaption>Escríbenos por WhatsApp</figcaption>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section>
	<div class="Conten-global">
		<div class="Conten-global-int">
			<div class="Conten-redes">
				<div class="Conten-redes-int">
					<div class="Conten-redes-int-sec">
						<div class="Redesg-facetw">
							<div class="fb-page" data-href="https://www.facebook.com/pg/MincomercioCo/posts/?ref=page_internal" data-tabs="timeline" data-width="500" data-height="400" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/pg/MincomercioCo/posts/?ref=page_internal" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/pg/MincomercioCo/posts/?ref=page_internal">Ministerio de Comercio, Industria y Turismo</a></blockquote></div>
						</div>
					</div>
					<div class="Conten-redes-int-sec">
						<div class="Redesg-facetw">
						<a class="twitter-timeline" href="https://twitter.com/mincomercioco?lang=es">Tweets by consejodeguias</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<footer>
<?php include "new-footer.php";?>
</footer>
<script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
<script>
    $(function() {
    $('#slides').superslides({
    inherit_width_from: '.wide-container',
    inherit_height_from: '.wide-container',
    play: 5000,
    animation: 'fade'
    });
    });
</script>
<script type="text/javascript">
	$(function(){
		$('#Drop').bind('click',function() {
		$('.Top-inf').toggleClass('Top-inf-apa');
	});
	});
</script>
<script src="js/jquery.superslides.min.js" type="text/javascript" charset="utf-8"></script>
<script src="js/mostrar-invest-index.js"></script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.7";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
</body>
</html>
