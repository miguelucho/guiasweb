<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Consejo Profesional de Gu�as de Turismo </title>
</head>

<body>
<table width="100%" border="0" cellpadding="6" cellspacing="0">
  <tr bgcolor="#DFDFFF">
    <td colspan="2"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Formatos de documentos requeridos para tr&aacute;mite </font></td>
  </tr>
  <tr bgcolor="#FFFFCA">
    <td><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><b>Documento</b></font></td>
    <td><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><b>Formato</b></font></td>
  </tr>
  <tr bgcolor="#FFFFCA">
    <td width="17%"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Fotograf&iacute;a</font></td>
    <td width="83%"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"> Escaneado de una foto tama&ntilde;o 3x4 con fondo blanco, posici&oacute;n de frente - hombres con corbata. En formato JPG </font></td>
  </tr>
  <tr bgcolor="#FFFFCA">
    <td><font size="2" face="Verdana, Arial, Helvetica, sans-serif">C&eacute;dula </font></td>
    <td><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Escaneado del original en Formato JPG </font></td>
  </tr>
  <tr bgcolor="#FFFFCA">
    <td><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Recibo de consignaci&oacute;n </font></td>
    <td><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Escaneado del original en Formato JPG </font></td>
  </tr>
  <tr bgcolor="#FFFFCA">
    <td><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Certificado de laboratorio cl&iacute;nico. </font></td>
    <td><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Escaneado del original en Formato JPG </font></td>
  </tr>
  <tr bgcolor="#FFFFCA">
    <td><font size="2" face="Verdana, Arial, Helvetica, sans-serif">T&iacute;tulo</font></td>
    <td><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Escaneado del original en Formato JPG </font></td>
  </tr>
</table>
</body>
</html>
