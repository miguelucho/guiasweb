<?php
require_once 'conexion.php';

$data = $_REQUEST['guia'];

switch ($data['accion']) {
    case 'ciudades':
        $ciudades = $db
            ->where('departamento_id', $data['departamento'])
            ->objectBuilder()->get('ciudades', null, ['id,ciudad']);

        if ($db->count > 0) {
            $info['ciudades'] = $ciudades;
        } else {
            $info['ciudades'] = '';
        }
        echo json_encode($info);
        break;
    case 'disponibilidad':
        session_start();

        $comprobar = $db
            ->where('guia_id', $_SESSION['turguia'])
            ->objectBuilder()->get('guias_disponibilidad');

        if ($db->count > 0) {
            $datos = ['departamento' => $data['departamento'], 'ciudad' => $data['ciudad'], 'inicio' => $data['inicio'], 'fin' => $data['fin'], 'estado' => $data['estado']];

            $disponibilidad = $db
                ->where('guia_id', $_SESSION['turguia'])
                ->update('guias_disponibilidad', $datos);
        } else {
            $datos = ['guia_id' => $_SESSION['turguia'], 'departamento' => $data['departamento'], 'ciudad' => $data['ciudad'], 'inicio' => $data['inicio'], 'fin' => $data['fin'], 'estado' => $data['estado']];

            $disponibilidad = $db
                ->insert('guias_disponibilidad', $datos);
        }

        if ($disponibilidad) {
            $msg['status'] = true;
            $msg['motivo'] = 'Disponibilidad registrada';
        } else {
            $msg['status'] = false;
            $msg['motivo'] = 'Error, la disponibilidad no se pudo registrar';
        }

        echo json_encode($msg);
        break;
    case 'contactos':
        session_start();

        $pagina         = (int) (isset($data['pagina']) ? $data['pagina'] : 1);
        $pagina         = ($pagina == 0 ? 1 : $pagina);
        $resultados_pag = 50;
        $adyacentes     = 2;

        $contactos = $db
            ->where('estado', $data['estado'])
            ->where('guia_id', $_SESSION['turguia'])
            ->objectBuilder()->get('guias_contactos');

        $numpags = ceil($db->count / $resultados_pag);

        if ($numpags >= 1) {
            require_once 'Paginacion.php';
            $listado       = '';
            $db->pageLimit = $resultados_pag;

            $contactos = $db
                ->where('estado', $data['estado'])
                ->where('guia_id', $_SESSION['turguia'])
                ->objectBuilder()->paginate('guias_contactos', $pagina);

            foreach ($contactos as $contacto) {
                $nmciudad = $nmdepartamento = '';

                $ciudades = $db
                    ->where('id', $contacto->ciudad)
                    ->objectBuilder()->get('ciudades');

                if ($db->count > 0) {
                    $nmciudad = $ciudades[0]->ciudad;

                    $departamentos = $db
                        ->where('id', $ciudades[0]->departamento_id)
                        ->objectBuilder()->get('departamentos');

                    if ($db->count > 0) {
                        $nmdepartamento = $departamentos[0]->departamento;
                    }
                }

                $turista = '';

                $usuarios = $db
                    ->where('id', $contacto->user_id)
                    ->objectBuilder()->get('users2');

                if ($db->count > 0) {
                    $turista = $usuarios[0]->nombres . ' ' . $usuarios[0]->apellidos;
                }

                $btns = '';

                if ($data['estado'] == 1) {
                    $btns = '<div class="Listar-table-dato Center">
                                    <span class="" title="">
                                        <a href="javascript://" class="Btn-azul btn-aceptar" id="ok-' . $contacto->id . '">Aceptar</a>
                                    </span>
                                </div>
                                <div class="Listar-table-dato Center">
                                    <span class="" title="">
                                        <a href="javascript://" class="Btn-rojo btn-rechazar" id="no-' . $contacto->id . '">Rechazar</a>
                                    </span>
                                </div>';
                }

                $listado .= '<div class="Listar-table">
                                <div class="Listar-table-dato">
                                    <span class="" title="' . $contacto->fecha . '">' . $contacto->fecha . '</span>
                                </div>
                                <div class="Listar-table-dato">
                                    <span class="" title="' . $nmdepartamento . '">' . $nmdepartamento . '</span>
                                </div>
                                <div class="Listar-table-dato">
                                    <span class="" title="' . $nmciudad . '">' . $nmciudad . '</span>
                                </div>
                                <div class="Listar-table-dato">
                                    <span class="" title="' . $turista . '">' . $turista . '</span>
                                </div>
                                <div class="Listar-table-dato">
                                    <span class="" title="">
                                        <a href="javascript://" class="Btn-naranja btn-observacion" id="obs-' . $contacto->id . '">Observación</a>
                                    </span>
                                </div>
                                ' . $btns . '
                            </div>';
            }

            $msg['listado']    = $listado;
            $pagconfig         = array('pagina' => $pagina, 'totalrows' => $db->totalPages, 'ultima_pag' => $numpags, 'resultados_pag' => $resultados_pag, 'adyacentes' => $adyacentes);
            $paginar           = new Paginacion($pagconfig);
            $msg['paginacion'] = $paginar->crearlinks();
        } else {
            $msg['listado'] = '<div class="Listar-table">
                                        <div class="Listar-table-dato">
                                            <span class="" title="">No se encontraron registros</span>
                                        </div>
                                    </div>';
            $msg['paginacion'] = '';
        }

        $pendientes = $aceptadas = $realizadas = 0;

        $contactos = $db
            ->where('guia_id', $_SESSION['turguia'])
            ->objectBuilder()->get('guias_contactos');

        foreach ($contactos as $contacto) {
            switch ($contacto->estado) {
                case '1':
                    $pendientes++;
                    break;
                case '2':
                    $aceptadas++;
                    break;
                case '3':
                    $realizadas++;
                    break;
            }
        }

        $msg['pendientes'] = $pendientes;
        $msg['aceptadas']  = $aceptadas;
        $msg['realizadas'] = $realizadas;

        echo json_encode($msg);
        break;
    case 'contacto-observacion':
        $contactos = $db
            ->where('id', $data['contacto'])
            ->objectBuilder()->get('guias_contactos');

        $msg['observacion'] = $contactos[0]->observaciones_usuario;

        echo json_encode($msg);
        break;
    case 'contacto-rechazar':
        $upestado = $db
            ->where('id', $data['contacto'])
            ->update('guias_contactos', ['estado' => 4]);

        if ($upestado) {
            $contactos = $db
                ->where('id', $data['contacto'])
                ->objectBuilder()->get('guias_contactos');

            $ciudades = $db
                ->where('id', $contactos[0]->ciudad)
                ->objectBuilder()->get('ciudades');

            $ciudad_contacto = $ciudades[0]->ciudad;
            $fecha_contacto  = $contactos[0]->fecha;

            //guia
            $usuarios = $db
                ->where('id', $contactos[0]->guia_id)
                ->objectBuilder()->get('users2');

            $guia_nombre = $usuarios[0]->nombres . ' ' . $usuarios[0]->apellidos;

            //turista
            $usuarios = $db
                ->where('id', $contactos[0]->user_id)
                ->objectBuilder()->get('users2');

            $turista_nombre = $usuarios[0]->nombres . ' ' . $usuarios[0]->apellidos;
            $turista_correo = $usuarios[0]->username;

            $template = '../templates/guiacancelar.php';
            $message  = file_get_contents($template);
            $message  = str_replace('$turista$', $turista_nombre, $message);
            $message  = str_replace('$guia$', $guia_nombre, $message);
            $message  = str_replace('$fecha$', $fecha_contacto, $message);
            $message  = str_replace('$ciudad$', $ciudad_contacto, $message);
            include_once 'phpmailer/class.phpmailer.php';
            $mail = new PHPMailer();

            $mail->Host = "localhost";
            $mail->SetFrom('noreply@guiasdeturismodecolombia.com.co');
            $mail->AddAddress($turista_correo);
            $mail->Subject = 'Cancelar contacto -  guiasdeturismodecolombia.com.co';
            $mail->MsgHTML($message);
            $mail->IsHTML(true);
            $mail->CharSet = "utf-8";

            $mail->Send();
            $msg['status'] = true;
            $msg['motivo'] = 'La solicitud se ha rechazado.';
        } else {
            $msg['status'] = false;
            $msg['motivo'] = 'No se ha podido rechazar la solicitud';
        }

        echo json_encode($msg);
        break;
    case 'contacto-aceptar':
        $upestado = $db
            ->where('id', $data['contacto'])
            ->update('guias_contactos', ['estado' => 2]);

        if ($upestado) {
            $contactos = $db
                ->where('id', $data['contacto'])
                ->objectBuilder()->get('guias_contactos');

            $ciudades = $db
                ->where('id', $contactos[0]->ciudad)
                ->objectBuilder()->get('ciudades');

            $ciudad_contacto = $ciudades[0]->ciudad;
            $fecha_contacto  = $contactos[0]->fecha;

            //guia
            $usuarios = $db
                ->where('id', $contactos[0]->guia_id)
                ->objectBuilder()->get('users2');

            $guia_nombre   = $usuarios[0]->nombres . ' ' . $usuarios[0]->apellidos;
            $guia_telefono = $usuarios[0]->telefono1;
            if ($usuarios[0]->telefono2 != '') {
                $guia_telefono = $guia_telefono . ' - ' . $usuarios[0]->telefono2;
            }

            $guia_correo = $usuarios[0]->username;

            //turista
            $usuarios = $db
                ->where('id', $contactos[0]->user_id)
                ->objectBuilder()->get('users2');

            $turista_nombre = $usuarios[0]->nombres . ' ' . $usuarios[0]->apellidos;
            $turista_correo = $usuarios[0]->username;

            $template = '../templates/guiaaceptar.php';
            $message  = file_get_contents($template);
            $message  = str_replace('$turista$', $turista_nombre, $message);
            $message  = str_replace('$guia$', $guia_nombre, $message);
            $message  = str_replace('$fecha$', $fecha_contacto, $message);
            $message  = str_replace('$ciudad$', $ciudad_contacto, $message);
            $message  = str_replace('$guiatelefono$', $guia_telefono, $message);
            $message  = str_replace('$guiacorreo$', $guia_correo, $message);
            include_once 'phpmailer/class.phpmailer.php';
            $mail = new PHPMailer();

            $mail->Host = "localhost";
            $mail->SetFrom('noreply@guiasdeturismodecolombia.com.co');
            $mail->AddAddress($turista_correo);
            $mail->Subject = 'Contacto Aceptado-  guiasdeturismodecolombia.com.co';
            $mail->MsgHTML($message);
            $mail->IsHTML(true);
            $mail->CharSet = "utf-8";

            $mail->Send();
            $msg['status'] = true;
            $msg['motivo'] = 'La solicitud se ha aceptado.';
        } else {
            $msg['status'] = false;
            $msg['motivo'] = 'No se ha podido aceptar la solicitud';
        }

        echo json_encode($msg);
        break;
        /*case 'perfilcambio':
session_start();
$contrasena = trim($data['contrasena']);

require_once "password.php";
$npass = password_hash($contrasena, PASSWORD_BCRYPT);

$guias = $db
->where('id', $_SESSION['turguia'])
->objectBuilder()->get('guias');

$nuevopass = $db
->where('id', $guias[0]->user_id)
->update('users2', ['password' => $npass]);

if ($nuevopass) {
$msg['status'] = true;
$msg['motivo'] = 'Contraseña actualizada.';
} else {
$msg['status'] = false;
$msg['motivo'] = 'Error, la contraseña no se pudo actualizar.';
}

echo json_encode($msg);
break;*/
}
