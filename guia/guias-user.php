<?php
	session_start();
	if(!isset($_SESSION['turguia'])){
		header('Location: ../demo/iniciar.php');
	}

	require_once 'libs/conexion.php';
	$guias = $db
		->where('user_id', $_SESSION['turguia'])
		->objectBuilder()->get('guias');

	if($db->count > 0){
		$usuario = $db
			->where('id', $guias[0]->user_id)
			->objectBuilder()->get('users2');

		$estado = 'Activo';
		if($guias[0]->suspendido == 1){
			$estado = 'Inactivo';
		}
	}else{
		header('Location: libs/logout');
	}
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
		<link rel="shortcut icon" href="images/favicon2.ico" type="image/x-icon" >
		<meta name="keywords" lang="es" content="">
		<meta name="robots" content="All">
		<title>Administrador</title>
		<link rel="stylesheet" href="css/stylesheet.css" />
	</head>
	<body class="">
		<header>
			<?php include "header-top.php";?>
		</header>
		<section>
			<?php include "menu-izq-admin.php";?>
		</section>
		<section>
			<div class="Conten-white2">
				<div class="Conten-white-full">
					<div class="Conten-dashboard">
						<div class="Conten-dashboard-int">
							<div class="Conten-dashboard-int-title">
								<h2 class="Landing_title">PERFIL</h2>
							</div>
							<div class="Admin-listar-propietarios">
								<div class="Contenedor-desc">
									<div class="">
										<div class="Con-perfil">
											<div class="Con-perfil-int">
												<p>Información de perfil</p>
												<form class="Perfil-guia">
													<label>Nombre</label>
													<p><?php echo $usuario[0]->nombres ?></p>
													<label>Apellido</label>
													<p><?php echo $usuario[0]->apellidos ?></p>
													<label>Estado</label>
													<p><?php echo $estado ?></p>
													<label>RNT</label>
													<p><?php echo  $guias[0]->RNT ?></p>
													<label>Tarjeta Profesional</label>
													<p><?php echo  $guias[0]->tarjeta_profesional ?></p>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/script-menu-slide.js"></script>
</body>
</html>