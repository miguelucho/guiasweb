$(document).ready(function() {
	$('.fecha').flatpickr({
		locale: 'es',
		enableTime: false,
		dateFormat: "Y-m-d",
	});

	if ($('.dispo-departamento').val() != '') {
		tmpciu = $('.tmpciud').val();
		$('.dispo-ciudad option').not(':first').remove();
		$.getJSON('libs/acciones', {
			'guia[accion]': 'ciudades',
			'guia[departamento]': $('.dispo-departamento').val()
		}).done(function(data) {
			if (data.ciudades != '') {
				ar = [];
				$.each(data.ciudades, function(i, dat) {
					ar.push('<option value="' + dat.id + '" ' + (dat.id == tmpciu ? "selected" : "") + '>' + dat.ciudad + '</option>');
				});
				$('.dispo-ciudad').append(ar.join(''));
			}
		});
	}

	estado = 1;
	lista_contactos(1, 1);

	$('.contactos li').on('click', function() {
		$('.contactos li').find('a').removeClass('Vin-active');
		$(this).find('a').addClass('Vin-active');
		lista_contactos($(this).index() + 1, 1);
		estado = $(this).index() + 1;
	});

	function lista_contactos(estado, pagina) {
		$('.load').fadeIn(0);
		$('#listado').empty().fadeOut(0);
		$.getJSON('libs/acciones', {
			'guia[accion]': 'contactos',
			'guia[pagina]': pagina,
			'guia[estado]': estado
		}).done(function(data) {
			$('.load').fadeOut(0);
			$('#listado').html(data.listado);
			$('#paginacion').html(data.paginacion);
			$('#listado').fadeIn();
			$('.cnt-pendientes').html(data.pendientes);
			$('.cnt-aceptadas').html(data.aceptadas);
			$('.cnt-realizadas').html(data.realizadas);
		});
	}

	$('body').on('click', '.mpag', function() {
		lista_contactos(estado, $(this).prop('id'));
	});

	$('body').on('click', '.btn-observacion', function() {
		loader();
		idctn = $(this).prop('id').split('-');
		$.getJSON('libs/acciones', {
			'guia[accion]': 'contacto-observacion',
			'guia[contacto]': idctn[1]
		}).done(function(data) {
			$('#fondo').remove();
			if (data.observacion != '') {
				nmensaje('info', 'Observación', data.observacion);
			} else {
				nmensaje('info', 'Observación', 'El turista no ha ingresado observación.');
			}
		});
	});

	$('body').on('click', '.btn-rechazar', function() {
		idctn = $(this).prop('id').split('-');
		modal({
			type: 'confirm',
			title: 'Rechazar Solicitud',
			text: 'Desea rechazar la solicitud ?',
			size: 'small',
			callback: function(result) {
				if (result === true) {
					loader();
					$.post('libs/acciones', {
						'guia[accion]': 'contacto-rechazar',
						'guia[contacto]': idctn[1]
					}, function(data) {
						$('#fondo').remove();
						if (data.status == true) {
							lista_contactos(estado, 1);
							nmensaje('success', 'Correcto', data.motivo);
						} else {
							nmensaje('error', 'Error', data.motivo);
						}
					}, 'json');
				} else {}
			},
			closeClick: false,
			animate: true,
			buttonText: {
				yes: 'Confirmar',
				cancel: 'Cancelar'
			}
		});
	});

	$('body').on('click', '.btn-aceptar', function() {
		idctn = $(this).prop('id').split('-');
		modal({
			type: 'confirm',
			title: 'Aceptar Solicitud',
			text: 'Desea aceptar la solicitud ?',
			size: 'small',
			callback: function(result) {
				if (result === true) {
					loader();
					$.post('libs/acciones', {
						'guia[accion]': 'contacto-aceptar',
						'guia[contacto]': idctn[1]
					}, function(data) {
						$('#fondo').remove();
						if (data.status == true) {
							lista_contactos(estado, 1);
							nmensaje('success', 'Correcto', data.motivo);
						} else {
							nmensaje('error', 'Error', data.motivo);
						}
					}, 'json');
				} else {}
			},
			closeClick: false,
			animate: true,
			buttonText: {
				yes: 'Confirmar',
				cancel: 'Cancelar'
			}
		});
	});

	$('.dispo-departamento').on('change', function() {
		$('.dispo-ciudad option').not(':first').remove();
		$.getJSON('libs/acciones', {
			'guia[accion]': 'ciudades',
			'guia[departamento]': $(this).val()
		}).done(function(data) {
			if (data.ciudades != '') {
				ar = [];
				$.each(data.ciudades, function(i, dat) {
					ar.push('<option value="' + dat.id + '">' + dat.ciudad + '</option>');
				});
				$('.dispo-ciudad').append(ar.join(''));
			}
		});
	});

	$('.dispo-estado').on('change', function() {
		if ($(this).val() != '') {
			loader();
			invalido = 0;
			$.each($('[class^=dispo]'), function() {
				if ($(this).val() == '') {
					invalido++;
				}
			});
			if (invalido > 0) {
				$('#fondo').remove();
				nmensaje('error', 'Error', 'Todos los campos son requeridos');
				$(this).val('');
			} else {
				data = [];
				data.push({
					name: 'guia[accion]',
					value: 'disponibilidad'
				});
				$.each($('[class^=dispo]'), function() {
					cl = $(this).attr('class').split(' ');
					cl = cl[0].split('-');
					cl = cl[1];
					data.push({
						name: 'guia[' + cl + ']',
						value: $(this).val()
					});
				});
				$.post('libs/acciones', data, function(data) {
					$('#fondo').remove();
					if (data.status == true) {
						$('.inicio-estado').html(($('.dispo-estado').val() == 0 ? 'INICIO - INACTIVO' : 'INICIO - ACTIVO'));
						nmensaje('success', 'Correcto', data.motivo);
					} else {
						nmensaje('error', 'Error', data.motivo);
					}
				}, 'json');
			}
		}
	});

	$('.dispo-departamento, .dispo-ciudad, .dispo-inicio, .dispo-fin').on('change', function() {
		$('.dispo-estado').val('');
	});


	function loader() {
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">' +
			'<div class="loader-inner ball-beat">' +
			'<div></div>' +
			'<div></div>' +
			'<div></div>' +
			'</div>' +
			'</div>');
		setTimeout(function() {
			$('#fondo').fadeIn('fast');
		}, 400);
	}

	function nmensaje(tipo, titulo, mensaje) {
		modal({
			type: tipo,
			title: titulo,
			text: mensaje,
			size: 'small',
			closeClick: false,
			animate: true,
		});
	}
});