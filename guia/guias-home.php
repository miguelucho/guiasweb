<?php
	session_start();
	if(!isset($_SESSION['turguia'])){
		header('Location: ../demo/iniciar.php');
	}

	require_once 'libs/conexion.php';

	$departamentoid = $ciudad = $inicio = $fin = $estado = '';

	@session_start();
	$disponibilidad = $db
		->where('guia_id',$_SESSION['turguia'])
		->objectBuilder()->get('guias_disponibilidad');

	if($db->count > 0){
		$departamentoid = $disponibilidad[0]->departamento;
		$ciudadid       = '<option style="display:none" class="tmpciud">'.$disponibilidad[0]->ciudad.'</option>';
		$inicio       = $disponibilidad[0]->inicio;
		$fin          = $disponibilidad[0]->fin;
		$estado       = $disponibilidad[0]->estado;
	}

	$ls_estados = '';
	$estados = [0=>'Inactivo',1=>'Activo'];
	foreach ($estados as $key => $value) {
		$ls_estados .= '<option value="'.$key.'" '.($key == $estado ? "selected" : "").'>'.$value.'</option>';
	}

	$ls_departamentos = '';

	$departamentos = $db
		->where('paise_id',1)
		->objectBuilder()->get('departamentos');

	foreach ($departamentos as $departamento) {
		$ls_departamentos .= '<option value="'.$departamento->id.'" '.($departamento->id == $departamentoid ? "selected" : "").'>'.$departamento->departamento.'</option>';
	}

/////
	$pendientes = $aceptadas = $realizadas = 0;

	$contactos = $db
		->where('guia_id',$_SESSION['turguia'])
		->objectBuilder()->get('guias_contactos');

	foreach ($contactos as $contacto) {
		switch ($contacto->estado) {
			case '1':
				$pendientes++;
				break;
			case '2':
				$aceptadas++;
				break;
			case '3':
				$realizadas++;
				break;
		}
	}

?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
		<link rel="shortcut icon" href="images/favicon2.ico" type="image/x-icon" >
		<meta name="keywords" lang="es" content="">
		<meta name="robots" content="All">
		<title>Guias Turisticos</title>
		<link rel="stylesheet" href="css/stylesheet.css" />
		<link rel="stylesheet" href="css/load.css" />
		<link rel="stylesheet" href="js/flatpickr/dist/flatpickr.min.css" />
		<link rel="stylesheet" href="css/jquery.modal.css" />
		<link rel="stylesheet" href="css/paginacion.css" />
	</head>
	<body class="">
		<header>
			<?php include "header-top.php";?>
		</header>
		<section>
			<?php include "menu-izq-admin.php";?>
		</section>
		<section>
			<div class="Conten-white2">
				<div class="Conten-white-full">
					<div class="Conten-dashboard">
						<div class="Conten-dashboard-int">
							<div class="Conten-dashboard-int-title">
								<h2 class="Landing_title inicio-estado"><?php echo ($estado == 0 ? 'INICIO - INACTIVO' : 'INICIO - ACTIVO') ?></h2>
							</div>
							<div class="Con-texto-top">
								<p>A continuación selecciona lugar y fecha de disponibilidad para que los turistas te puedan contactar.</p>
								<br>
							</div>

							<div class="Con-filtricos">
								<div class="Con-filtricos-int">
									<label>Departamento</label>
									<select class="dispo-departamento">
										<option value="">Selecciona</option>
										<?php echo $ls_departamentos ?>
									</select>
								</div>
								<div class="Con-filtricos-int">
									<label>Ciudad</label>
									<select class="dispo-ciudad">
										<option value="">Selecciona</option>
										<?php echo $ciudadid ?>
									</select>
								</div>
								<div class="Con-filtricos-int">
									<label>Fecha de inicio</label>
									<input type="text" placeholder="Fecha" class="dispo-inicio fecha" value="<?php echo $inicio ?>" >
								</div>
								<div class="Con-filtricos-int">
									<label>Fecha de fin</label>
									<input type="text" placeholder="Fecha" class="dispo-fin fecha" value="<?php echo $fin ?>">
								</div>
								<div class="Con-filtricos-int">
									<label>Estado</label>
									<select class="dispo-estado" required="">
										<option value="">Selecciona</option>
										<?php echo $ls_estados ?>
									</select>
								</div>
							</div>
							<div class="Admin-listar-propietarios">
								<div class="Contenedor-desc">
									<div class="Contenedor-desc-int">
										<section class="nav_tabs">
											<div class="nav_tabs_back Vin-anc">
												<nav>
													<ul class="tabs contactos">
														<li><a href="javascript://" class="Vin  Vin-active">PENDIENTES <span class="Estat cnt-pendientes"> <?php echo $pendientes ?> </span></a></li>
														<li><a href="javascript://" class="Vin">ACEPTADAS <span class="Estat cnt-aceptadas"> <?php echo $aceptadas ?> </span></a></li>
														<li><a href="javascript://" class="Vin">REALIZADAS <span class="Estat cnt-realizadas"> <?php echo $realizadas ?> </span></a></li>
													</ul>
												</nav>
											</div>
										</section>
									</div>
									<div class="">

										<div class="Contenedor-tabla">
											<div class="Listar-table Bg-gris Oculto">
												<div class="Listar-table-dato">
													<span class="Title" title="">Fecha</span>
												</div>
												<div class="Listar-table-dato">
													<span class="Title" title="">Departamento</span>
												</div>
												<div class="Listar-table-dato">
													<span class="Title" title="">Ciudad</span>
												</div>
												<div class="Listar-table-dato">
													<span class="Title" title="">Nombre</span>
												</div>
												<div class="Listar-table-dato">
													<span class="Title" title="">Observación</span>
												</div>
												<div class="Listar-table-dato Center">

												</div>
												<div class="Listar-table-dato Center">

												</div>
											</div>
											<div class="load" >
												<img src="images/load.gif" width="50px">
											</div>
											<div style="display: block;" id="listado">
											</div>
											<div class="Listar-paginacion">
												<div id="paginacion">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<script src="js/jquery-1.11.1.min.js"></script>
	<!-- <script src="js/jquery.remodal.js"></script> -->
	<script src="js/script-menu-slide.js"></script>
	<script src="js/flatpickr/dist/flatpickr.min.js" type="text/javascript"></script>
	<script src="js/flatpickr/dist/l10n/es.js"></script>
	<script src="js/jquery.modal.min.js"></script>
	<script src="js/guiahome.js"></script>
</body>
</html>