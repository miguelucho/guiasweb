<?php
session_start();
# FileName="Connection_php_mysql.htm"
# Type="MYSQL"
# HTTP="true"
$hostname_datos = "localhost";
$database_datos = "guiasdet_mincomercio";
$username_datos = "guiasdet_web";
$password_datos = "g14d3tur1sm0*2014";
// $username_datos = "root";
// $password_datos = "";
$datos = mysql_pconnect($hostname_datos, $username_datos, $password_datos) or trigger_error(mysql_error(),E_USER_ERROR);
// mysql_set_charset("utf8",$datos);

if (!function_exists("GetSQLValueString")) {
    function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
    {
        $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

        $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

        switch ($theType) {
            case "text":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "long":
            case "int":
                $theValue = ($theValue != "") ? intval($theValue) : "NULL";
                break;
            case "double":
                $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
                break;
            case "date":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "defined":
                $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
                break;
        }
        return $theValue;
    }
}

$colname_rds_departamentos = "-1";
if (isset($_GET['idm'])) {
    $colname_rds_departamentos = (get_magic_quotes_gpc()) ? $_GET['idm'] : addslashes($_GET['idm']);
}
mysql_select_db($database_datos, $datos);
$query_rds_departamentos     = sprintf("SELECT * FROM departamentos WHERE id = %s", GetSQLValueString($colname_rds_departamentos, "int"));
$rds_departamentos           = mysql_query($query_rds_departamentos, $datos) or die(mysql_error());
$row_rds_departamentos       = mysql_fetch_assoc($rds_departamentos);
$totalRows_rds_departamentos = mysql_num_rows($rds_departamentos);

$colname_rds_instituciones = "-1";
if (isset($_GET['idm'])) {
    $colname_rds_instituciones = (get_magic_quotes_gpc()) ? $_GET['idm'] : addslashes($_GET['idm']);
}
mysql_select_db($database_datos, $datos);
$query_rds_instituciones     = sprintf("SELECT * FROM asociaciones WHERE id_dep = '$_GET[idm]' ");
$rds_instituciones           = mysql_query($query_rds_instituciones, $datos) or die(mysql_error());
$totalRows_rds_instituciones = mysql_num_rows($rds_instituciones);

mysql_select_db($database_datos, $datos);
$query_rds_listdep     = "SELECT departamentos.id, departamentos.nombre, departamentos.codigo, Count(asociaciones.id) AS CountOfid FROM departamentos LEFT JOIN asociaciones ON departamentos.id = asociaciones.id_dep GROUP BY departamentos.id, departamentos.nombre, departamentos.codigo  ORDER BY nombre ASC";
$rds_listdep           = mysql_query($query_rds_listdep, $datos) or die(mysql_error());
$row_rds_listdep       = mysql_fetch_assoc($rds_listdep);
$totalRows_rds_listdep = mysql_num_rows($rds_listdep);

if ($totalRows_rds_instituciones > 0) {
    $instituciones = array();

    while ($row_rds_instituciones = mysql_fetch_assoc($rds_instituciones)) {
        $instituciones[] = array('nombre' => $row_rds_instituciones['nombre'], 'representante' => $row_rds_instituciones['representante'], 'ciudad' => $row_rds_instituciones['ciudad'], 'direccion' => $row_rds_instituciones['direccion'], 'telefono' => $row_rds_instituciones['telefono'], 'contacto' => $row_rds_instituciones['contacto']);
    }

    $informacion['informacion'] = array('departamento' => $row_rds_departamentos['nombre'], 'instituciones' => $instituciones);
} else {
   $informacion['informacion'] = '';
}

echo json_encode($informacion);
