<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" >
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">	
	<title>Contactar Guiás de Turismo Colombia</title>
	<meta http-equiv="Cache-control" content="public">
	<link rel="stylesheet" href="css/slider.css" />
	<link rel="stylesheet" href="css/stylesheet.css" />	
</head>
<body>
	<header>
		<?php include "header-top.php";?>
	</header>
	<section>
		
			<div class="Contener-guias">
				<div class="Contener-guias-int">
					<div class="Contener-guias-int-izq Oculto">
						<div class="Form-filtro-guia">
							<img src="images/guias1.png">
							<form class="Cont-filtrar-guia">
								<label>Departamento</label>
								<select>
									<option>Seleccionar departamento</option>
								</select>
								<label>Idiomas</label>
								<select>
									<option>Seleccionar idioma</option>
								</select>
								<label>Servicios</label>
								<select>
									<option>Seleccionar servicios</option>
								</select>
								<label>Especialidades</label>
								<select>
									<option>Seleccionar especialidades</option>
								</select>
								<label>Fecha de contacto</label>
									<input type="date" name="">								
								<label>Guiás certificados</label>
								 <p class="Acepta-t"><input type="checkbox" name="" value=""> Guiás certificados<br></p>
								 <p><a href="#" class="Btn-azul">Reiniciar Búsqueda</a></p>
								 <p><a href="#" class="Btn-azul">Búsqueda Avanzada</a></p>
							</form>
						</div>
					</div>
					<div class="Contener-guias-int-der">
						<div class="Contener-guias-int-der-contexto">
							<div class="Form-busca nombre Forms">
								<label>Buscar por Nombres o Apellidos</label>
								<input type="text" placeholder="Ingresar texto" name="">
								<input type="submit" class="Btn-azul" value="Buscar" style="width: auto;" name="">
							</div>

							<div class="Contener-guias-int-der-contexto">

								<!-- Aca inicia la tarjeta de un guia -->
								<div class="Tarjeta-guia">
									<div class="Tarjeta-guia-int">
										<div class="Tarjeta-guia-int-nombre">
											<h2>CESAR AUGUSTO ANGEL VALENCIA</h2>
										</div>
										<div class="Tarjeta-guia-int-1">
											<img src="images/1193.jpg">
										</div>
										<div class="Tarjeta-guia-int-2">
											<div class="Tarjeta-guia-int-2-titul">
												<h3>Ubicaciones:</h3>
											</div>
											<div class="Tarjeta-guia-int-2-tags">
												<span class="Tarjeta-tag">Pereira</span>
												<span class="Tarjeta-tag">Armenia</span>
												<span class="Tarjeta-tag">Manizales</span>
											</div>
										</div>
										<div class="Tarjeta-guia-int-3">
											<p>
												<a href="#" class="Btn-azul">Ver perfil</a>
											</p>
										</div>
									</div>
									<div class="Tarjeta-guia-int">
										<div class="Tarjeta-guia-int-sub">
											<div class="Tarjeta-guia-int-2">
												<h3>Credenciales oficiales:</h3>
											</div>
											
												<span class="Tarjeta-tag">RNT: 16648</span>
												<span class="Tarjeta-tag">TPG: 1193</span>
										</div>
										<div class="Tarjeta-guia-int-sub">
											<div class="Tarjeta-guia-int-2">
												<h3>Idiomas:</h3>
											</div>

											<span class="Tarjeta-tag">Ingles</span>								
										</div>
									</div>
									<div class="Tarjeta-guia-int">
										<div class="Tarjeta-guia-int-2">
											<h3>Descripción:</h3>
										</div>
										<p>Gerente en Nature Trips Colombia desde 2012. Tour leader. Guía interprete de patrimonio. Enfoque experiencial. Comfamiliar Risaralda. Agencia de viajes. 3 años entre 1998 a 2001 Servicio Nacional de Aprendiz...</p>
									</div>
								</div>
								<!-- Aca  termina la tarjeta de un guia -->


								<!-- Aca inicia la tarjeta de un guia -->
								<div class="Tarjeta-guia">
									<div class="Tarjeta-guia-int">
										<div class="Tarjeta-guia-int-nombre">
											<h2>NELSON MIGUEL ANGEL VARGAS</h2>
										</div>
										<div class="Tarjeta-guia-int-1">
											<img src="images/1193.jpg">
										</div>
										<div class="Tarjeta-guia-int-2">
											<div class="Tarjeta-guia-int-2-titul">
												<h3>Ubicaciones:</h3>
											</div>
											<div class="Tarjeta-guia-int-2-tags">
												<span class="Tarjeta-tag">Pitalito</span>
												
											</div>
										</div>
										<div class="Tarjeta-guia-int-3">
											<p>
												<a href="#" class="Btn-azul">Ver perfil</a>
											</p>
										</div>
									</div>
									<div class="Tarjeta-guia-int">
										<div class="Tarjeta-guia-int-sub">
											<div class="Tarjeta-guia-int-2">
												<h3>Credenciales oficiales:</h3>
											</div>
											
												<span class="Tarjeta-tag">RNT: 16648</span>
												<span class="Tarjeta-tag">TPG: 1193</span>
										</div>
										<div class="Tarjeta-guia-int-sub">
											<div class="Tarjeta-guia-int-2">
												<h3>Idiomas:</h3>
											</div>

											<span class="Tarjeta-tag">Ingles</span>								
										</div>
									</div>
									<div class="Tarjeta-guia-int">
										<div class="Tarjeta-guia-int-2">
											<h3>Descripción:</h3>
										</div>
										<p></p>
									</div>
								</div>
								<!-- Aca  termina la tarjeta de un guia -->


								<!-- Aca inicia la tarjeta de un guia -->
								<div class="Tarjeta-guia">
									<div class="Tarjeta-guia-int">
										<div class="Tarjeta-guia-int-nombre">
											<h2>NELSON MIGUEL ANGEL VARGAS</h2>
										</div>
										<div class="Tarjeta-guia-int-1">
											<img src="images/1193.jpg">
										</div>
										<div class="Tarjeta-guia-int-2">
											<div class="Tarjeta-guia-int-2-titul">
												<h3>Ubicaciones:</h3>
											</div>
											<div class="Tarjeta-guia-int-2-tags">
												<span class="Tarjeta-tag">Pitalito</span>
												
											</div>
										</div>
										<div class="Tarjeta-guia-int-3">
											<p>
												<a href="#" class="Btn-azul">Ver perfil</a>
											</p>
										</div>
									</div>
									<div class="Tarjeta-guia-int">
										<div class="Tarjeta-guia-int-sub">
											<div class="Tarjeta-guia-int-2">
												<h3>Credenciales oficiales:</h3>
											</div>
											
												<span class="Tarjeta-tag">RNT: 16648</span>
												<span class="Tarjeta-tag">TPG: 1193</span>
										</div>
										<div class="Tarjeta-guia-int-sub">
											<div class="Tarjeta-guia-int-2">
												<h3>Idiomas:</h3>
											</div>

											<span class="Tarjeta-tag">Ingles</span>								
										</div>
									</div>
									<div class="Tarjeta-guia-int">
										<div class="Tarjeta-guia-int-2">
											<h3>Descripción:</h3>
										</div>
										<p></p>
									</div>
								</div>
								<!-- Aca  termina la tarjeta de un guia -->



							</div>
						</div>
					</div>
				</div>
			</div>
		
	</section>
	
	<footer>
		<div class="Foot">
			<div class="Foot-int">
				<p>
					<img src="images/logofontur.png">
					<img src="images/logomincit.png">
				</p>
				<p>Copyright © 2018. Todos los derechos reservados, Consejo Profesional de Guías de Turismo</p>
			</div>
		</div>
	</footer>

<script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
</body>
</html>
