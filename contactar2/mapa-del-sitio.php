<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" >
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">	
	<title>Mapa del Sitio | Contactar Guiás de Turismo Colombia</title>
	<meta http-equiv="Cache-control" content="public">
	<link rel="stylesheet" href="css/slider.css" />
	<link rel="stylesheet" href="css/stylesheet.css" />	
</head>
<body>
	<header>
		<?php include "header-top.php";?>
	</header>
	<section>
		<div class="Contener">
			<div class="Contener-int">
				<div class="Contener-int-contenido">
					<h1>Mapa del sitio</h1>
					<p>
						<ul class="Mapadesitio">
							<li><a href="">Inicio</a></li>
							<li><a href="">Registrarse</a></li>
							<li><a href="">Iniciar sesión</a></li>
							<li><a href="">Búsqueda de guiás</a></li>
							<li><a href="mapa-del-sitio">Mapa del sitio</a></li>
							<li><a href="terminos">Términos y condiciones</a></li>
						</ul>
					</p>
				</div>
				<div class="Conten-botones-ff">
					<p>
						<a href="contactar" class="Btn-atras">Atrás</a>
					</p>
				</div>
			</div>
		</div>
	</section>
	
	<footer>
		<div class="Foot">
			<div class="Foot-int">
				<p>
					<img src="images/logofontur.png">
					<img src="images/logomincit.png">
				</p>
				<p>Copyright © 2018. Todos los derechos reservados, Consejo Profesional de Guías de Turismo</p>
			</div>
		</div>
	</footer>

<script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
</body>
</html>
