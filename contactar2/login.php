<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" >
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">	
	<title>Contactar Guiás de Turismo Colombia</title>
	<meta http-equiv="Cache-control" content="public">
	<link rel="stylesheet" href="css/slider.css" />
	<link rel="stylesheet" href="css/stylesheet.css" />	
</head>
<body>
	<header>
		<?php include "header-top.php";?>
	</header>
	<section>
		<div class="Contener">
			<div class="Contener-int">
				<div class="Contener-int-contenido">
					
					<div class="Login">
						<div class="Login-int">
							<h2>Iniciar Sesión</h2>
							<p>Ingrese los datos de acceso</p>
							<form>
								<label>Nombre de Usuario / Correo Electrónico</label>
								<input type="text" placeholder="Usuario" name="">
								<label>Contraseña</label>
								<input type="password" placeholder="Contraseña" name="">
								<label>Verifica que la información sea correcta</label>
								<input type="submit" class="Btn-azul" value="Entrar" name="">
								<p><a href="recuperar" class="Contra">Obtenga o Recuerde su contraseña.</a></p>
							</form>
						</div>	
					</div>	
					<br>
					<p class="Center">Si eres Guía de Turismo, tu nombre de USUARIO se compone de la siguiente manera:<br>
					Iniciales de los NOMBRES: Ejemplo: <span class="Rojo-strong">J</span>uan <span class="Rojo-strong">D</span>arío (jd) 
					Primer apellido completo: Ejemplo: <span class="Rojo-strong">Martinez</span> (martinez)
					Iniciales del segundo APELLIDO Ejemplo: <span class="Rojo-strong">A</span>ngarita (a)
					El USUARIO para el Guía de Turismo JUAN DARIO MARTINEZ ANGARITA será, (todo en letras minúsculas)<br>
					<span class="Rojo-strong">jdmartineza</span>
					En la casilla de CONTRASEÑA deberás escribir la clave que se te enviará automáticamente a tu correo electrónico personal. Si nunca has registrado algún correo electrónico PERSONAL, será necesario informarlo a la coordinación del Consejo Profesional de Guías de Turismo, a cualquiera de los siguientes correos electrónicos: 
					amoreno@guiasdeturismodecolombia.com.co - 					guiaturismo@mincit.gov.co</p>				

				</div>
				<div class="Conten-botones-ff">
					<p>
						<a href="contactar" class="Btn-atras">Atrás</a>
					</p>
				</div>
			</div>
		</div>
	</section>
	
	<footer>
		<div class="Foot">
			<div class="Foot-int">
				<p>
					<img src="images/logofontur.png">
					<img src="images/logomincit.png">
				</p>
				<p>Copyright © 2018. Todos los derechos reservados, Consejo Profesional de Guías de Turismo</p>
			</div>
		</div>
	</footer>

<script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
</body>
</html>
