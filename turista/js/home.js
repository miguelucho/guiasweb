$(document).ready(function() {
	estado = 1;
	lista_contactos(1, 1);

	$('.contactos li').on('click', function() {
		$('.contactos li').find('a').removeClass('Vin-active');
		$(this).find('a').addClass('Vin-active');
		lista_contactos($(this).index() + 1, 1);
		estado = $(this).index() + 1;
	});

	function lista_contactos(estado, pagina) {
		$('.load').fadeIn(0);
		$('#listado').empty().fadeOut(0);
		$.getJSON('libs/acciones', {
			'info[accion]': 'contactos',
			'info[pagina]': pagina,
			'info[estado]': estado
		}).done(function(data) {
			$('.load').fadeOut(0);
			$('#listado').html(data.listado);
			$('#paginacion').html(data.paginacion);
			$('#listado').fadeIn();
			$('.cnt-pendientes').html(data.pendientes);
			$('.cnt-aceptadas').html(data.aceptadas);
			$('.cnt-realizadas').html(data.realizadas);
		});
	}

	$('body').on('click', '.mpag', function() {
		lista_contactos(estado, $(this).prop('id'));
	});

	$('body').on('click', '.btn-observacion', function() {
		loader();
		idctn = $(this).prop('id').split('-');
		$.getJSON('libs/acciones', {
			'info[accion]': 'contacto-observacion',
			'info[contacto]': idctn[1]
		}).done(function(data) {
			$('#fondo').remove();
			if (data.observacion != '') {
				nmensaje('info', 'Observación', data.observacion);
			} else {
				nmensaje('info', 'Observación', 'No has ingresado observación.');
			}
		});
	});

	$('body').on('click', '.btn-rechazar', function() {
		idctn = $(this).prop('id').split('-');
		modal({
			type: 'confirm',
			title: 'Cancelar Solicitud',
			text: 'Desea cancelar la solicitud ?',
			size: 'small',
			callback: function(result) {
				if (result === true) {
					loader();
					$.post('libs/acciones', {
						'info[accion]': 'contacto-rechazar',
						'info[contacto]': idctn[1]
					}, function(data) {
						$('#fondo').remove();
						if (data.status == true) {
							lista_contactos(estado, 1);
							nmensaje('success', 'Correcto', data.motivo);
						} else {
							nmensaje('error', 'Error', data.motivo);
						}
					}, 'json');
				} else {}
			},
			closeClick: false,
			animate: true,
			buttonText: {
				yes: 'Confirmar',
				cancel: 'Cancelar'
			}
		});
	});

	function loader() {
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">' +
			'<div class="loader-inner ball-beat">' +
			'<div></div>' +
			'<div></div>' +
			'<div></div>' +
			'</div>' +
			'</div>');
		setTimeout(function() {
			$('#fondo').fadeIn('fast');
		}, 400);
	}

	function nmensaje(tipo, titulo, mensaje) {
		modal({
			type: tipo,
			title: titulo,
			text: mensaje,
			size: 'small',
			closeClick: false,
			animate: true,
		});
	}
});