$(document).ready(function() {

	$('.fecha').flatpickr({
		locale: 'es',
		enableTime: false,
		dateFormat: "Y-m-d",
	});

	$('.Perfil-guia').on('submit', function(e) {
		e.preventDefault();
		loader();
		data = $(this).serializeArray();
		data.push({
			name: 'info[accion]',
			value: 'perfilcambio'
		});
		$.post('libs/acciones', data, function(data) {
			$('#fondo').remove();
			if (data.status == true) {
				$('#tcontra').val('');
				nmensaje('success', 'Correcto', data.motivo);
			} else {
				nmensaje('error', 'Error', data.motivo);
			}
		}, 'json');
	});

	function loader() {
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader">' +
			'<div class="loader-inner ball-beat">' +
			'<div></div>' +
			'<div></div>' +
			'<div></div>' +
			'</div>' +
			'</div>');
		setTimeout(function() {
			$('#fondo').fadeIn('fast');
		}, 400);
	}

	function nmensaje(tipo, titulo, mensaje) {
		modal({
			type: tipo,
			title: titulo,
			text: mensaje,
			size: 'small',
			closeClick: false,
			animate: true,
		});
	}
});