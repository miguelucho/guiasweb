<?php
require_once "conexion.php";
$data = $_REQUEST['registro'];
$msg  = [];

if (isset($data['turgoogle']) && $data['turgoogle'] == 1) {
    $datos = ['tipo' => 3, 'nombres' => $data['turnombre'], 'apellidos' => $data['turapellido'], 'username' => $data['turcorreo'], 'creado' => $db->now(), 'estado' => 1];
    $nuevo = $db
        ->insert('users2', $datos);
    if ($nuevo) {
        session_start();
        $_SESSION['turturista'] = $check[0]->id;
        $msg['status']          = true;
        $msg['redirect']        = 'turista/turista-home';
    } else {
        $msg['status'] = false;
        $msg['motivo'] = 'Error, datos no válidos.';
    }

    echo json_encode($msg);
} else {
    if (!isset($data['terminos']) || empty($data['terminos']) || $data['terminos'] != 1) {
        $msg['status'] = false;
        $msg['motivo'] = 'Debe aceptar los términos y condiciones.';
    } else {

        $check = $db
            ->where('username', $data['correo'])
            ->objectBuilder()->get('users2');

        if ($db->count > 0) {
            $msg['status'] = false;
            $msg['motivo'] = 'Correo ya registrado.';
        } else {

            if ($data['nacimiento_ano'] != '' && $data['nacimiento_mes'] != '' && $data['nacimiento_dia'] != '') {

                $stampBirth     = mktime(0, 0, 0, $data['nacimiento_mes'], $data['nacimiento_dia'], $data['nacimiento_ano']);
                $today['day']   = date('d');
                $today['month'] = date('m');
                $today['year']  = date('Y') - 18;

                $stampToday = mktime(0, 0, 0, $today['month'], $today['day'], $today['year']);

                if ($stampBirth < $stampToday) {
                    $correo     = trim($data['correo']);
                    $contrasena = trim($data['contrasena']);
                    require_once "password.php";
                    $npass = password_hash($contrasena, PASSWORD_BCRYPT);

                    $nacimiento = $data['nacimiento_ano'] . '-' . $data['nacimiento_mes'] . '-' . $data['nacimiento_dia'];

                    $datos = ['tipo' => 3, 'nombres' => $data['nombres'], 'apellidos' => $data['apellidos'], 'username' => $correo, 'password' => $npass, 'telefono1' => $data['celular'], 'telefono2' => $data['telefono'], 'email' => $correo, 'genero' => $data['genero'], 'nacimiento' => $nacimiento, 'creado' => $db->now(), 'estado' => 1];

                    $nuevo = $db
                        ->insert('users2', $datos);

                    if ($nuevo) {
                        $template = 'nuevo-turista.php';
                        $message  = file_get_contents($template);
                        $message  = str_replace('$usuario$', $data['nombres'] . ' ' . $data['apellidos'], $message);
                        $message  = str_replace('$correo$', $correo, $message);
                        $message  = str_replace('$pass$', $contrasena, $message);
                        $message  = str_replace('$enlace$', 'http://guiasdeturismodecolombia.com.co/demo/iniciar.php', $message);
                        include_once 'phpmailer/class.phpmailer.php';
                        $mail = new PHPMailer();

                        $mail->Host = "localhost";
                        $mail->SetFrom('noreply@guiasdeturismodecolombia.com.co');
                        $mail->AddAddress($correo);
                        $mail->Subject = 'Registro -  guiasdeturismodecolombia.com.co';
                        $mail->MsgHTML($message);
                        $mail->IsHTML(true);
                        $mail->CharSet = "utf-8";

                        $mail->Send();

                        $msg['status'] = true;
                        $msg['motivo'] = 'Se ha enviado un correo con la información para ingresar al sitio.';
                    }
                } else {
                    $msg['status'] = false;
                    $msg['motivo'] = 'Debe ser mayor de edad para poder registrarse.';
                }

            } else {
                $msg['status'] = false;
                $msg['motivo'] = 'Debe ingresar todos los datos';
            }
        }
    }
    echo json_encode($msg);
}
