<?php
	session_start();
	if(!isset($_SESSION['turturista'])){
		header('Location: ../demo/iniciar.php');
	}

	require_once 'libs/conexion.php';

	 $usuarios = $db
        ->where('id', $_SESSION['turturista'])
        ->objectBuilder()->get('users2');

    $turista_nombre = $usuarios[0]->nombres . ' ' . $usuarios[0]->apellidos;

	$pendientes = $aceptadas = $realizadas = 0;

	$contactos = $db
		->where('user_id',$_SESSION['turturista'])
		->objectBuilder()->get('guias_contactos');

	foreach ($contactos as $contacto) {
		switch ($contacto->estado) {
			case '1':
				$pendientes++;
				break;
			case '2':
				$aceptadas++;
				break;
			case '3':
				$realizadas++;
				break;
		}
	}
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
		<link rel="shortcut icon" href="images/favicon2.ico" type="image/x-icon" >
		<meta name="keywords" lang="es" content="">
		<meta name="robots" content="All">
		<title>Guias Turisticos</title>
		<link rel="stylesheet" href="css/stylesheet.css" />
		<link rel="stylesheet" href="css/load.css" />
		<link rel="stylesheet" href="js/flatpickr/dist/flatpickr.min.css" />
		<link rel="stylesheet" href="css/jquery.modal.css" />
		<link rel="stylesheet" href="css/paginacion.css" />
	</head>
	<body class="">
		<header>
			<?php include "header-top.php";?>
		</header>
		<section>
			<?php include "menu-izq-admin.php";?>
		</section>
		<section>
			<div class="Conten-white2">
				<div class="Conten-white-full">
					<div class="Conten-dashboard">
						<div class="Conten-dashboard-int">
							<div class="Conten-dashboard-int-title">
								<h2 class="Landing_title"><?php echo $turista_nombre ?></h2>
							</div>
							<div class="Con-texto-top">
								<p></p>
								<br>
							</div>
							<div class="Admin-listar-propietarios">
								<div class="Contenedor-desc">
									<div class="Contenedor-desc-int">
										<section class="nav_tabs">
											<div class="nav_tabs_back Vin-anc">
												<nav>
													<ul class="tabs contactos">
														<li><a href="javascript://" class="Vin  Vin-active">PENDIENTES <span class="Estat cnt-pendientes"> <?php echo $pendientes ?> </span></a></li>
														<li><a href="javascript://" class="Vin ">ACEPTADAS <span class="Estat cnt-aceptadas"> <?php echo $aceptadas ?> </span></a></li>
														<li><a href="javascript://" class="Vin">REALIZADAS <span class="Estat cnt-realizadas"> <?php echo $realizadas ?> </span></a></li>
													</ul>
												</nav>
											</div>
										</section>
									</div>
									<div class="">
										<div class="Contenedor-tabla">
											<div class="Listar-table Bg-gris Oculto">
												<div class="Listar-table-dato">
													<span class="Title" title="">Fecha</span>
												</div>
												<div class="Listar-table-dato">
													<span class="Title" title="">Departamento</span>
												</div>
												<div class="Listar-table-dato">
													<span class="Title" title="">Ciudad</span>
												</div>
												<div class="Listar-table-dato">
													<span class="Title" title="">Guia</span>
												</div>
												<div class="Listar-table-dato">
													<span class="Title" title="">Observación</span>
												</div>
											</div>
											<div class="load" >
												<img src="images/load.gif" width="50px">
											</div>
											<div style="display: block;" id="listado">
											</div>
											<div class="Listar-paginacion">
												<div id="paginacion">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<script src="js/jquery-1.11.1.min.js"></script>
	<!-- <script src="js/jquery.remodal.js"></script> -->
	<script src="js/script-menu-slide.js"></script>
	<script src="js/flatpickr/dist/flatpickr.min.js" type="text/javascript"></script>
	<script src="js/flatpickr/dist/l10n/es.js"></script>
	<script src="js/jquery.modal.min.js"></script>
	<script src="js/home.js"></script>
</body>
</html>