<?php
	session_start();
	if(!isset($_SESSION['turturista'])){
		header('Location: ../demo/iniciar.php');
	}

	require_once 'libs/conexion.php';
	$usuario = $db
		->where('id', $_SESSION['turturista'])
		->where('tipo', 3)
		->objectBuilder()->get('users2');

	if($db->count > 0){
		$estado = 'Activo';
		if($usuario[0]->estado == 0){
			$estado = 'Inactivo';
		}
	}else{
		header('Location: libs/logout');
	}
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
		<link rel="shortcut icon" href="images/favicon2.ico" type="image/x-icon" >
		<meta name="keywords" lang="es" content="">
		<meta name="robots" content="All">
		<title>Administrador</title>
		<link rel="stylesheet" href="css/stylesheet.css" />
		<link rel="stylesheet" href="css/load.css" />
		<link rel="stylesheet" href="js/flatpickr/dist/flatpickr.min.css" />
		<link rel="stylesheet" href="css/jquery.modal.css" />
	</head>
	<body class="">
		<header>
			<?php include "header-top.php";?>
		</header>
		<section>
			<?php include "menu-izq-admin.php";?>
		</section>
		<section>
			<div class="Conten-white2">
				<div class="Conten-white-full">
					<div class="Conten-dashboard">
						<div class="Conten-dashboard-int">
							<div class="Conten-dashboard-int-title">
								<h2 class="Landing_title">PERFIL</h2>
							</div>
							<div class="Admin-listar-propietarios">
								<div class="Contenedor-desc">
									<div class="">
										<div class="Con-perfil">
											<div class="Con-perfil-int">
												<p>Información de perfil, a continuación podrás actualizar tus datos</p>
												<form class="Perfil-guia">
													<label>Nombre</label>
													<input type="text" value="<?php echo $usuario[0]->nombres ?>" name="info[nombres]" >
													<label>Apellido</label>
													<input type="text" value="<?php echo $usuario[0]->apellidos ?>" name="info[apellidos]">
													<label>Nacimiento</label>
													<input type="date" class="fecha" value="<?php echo $usuario[0]->nacimiento ?>" placeholder="AAAA-MM-DD" name="info[nacimiento]" >
													<label>Teléfono</label>
													<input type="text" placeholder="Teléfono" value="<?php echo $usuario[0]->telefono1 ?>" name="info[telefono]">
													<label>Actualizar contraseña</label>
													<input type="text" placeholder="Contraseña" id="tcontra" name="info[contrasena]">
													<label>Haz clic para actualizar contraseña</label>
													<input type="submit" class="Btn-azul" value="Guardar" name="">
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/script-menu-slide.js"></script>
	<script src="js/jquery.modal.min.js"></script>
	<script src="js/flatpickr/dist/flatpickr.min.js" type="text/javascript"></script>
	<script src="js/flatpickr/dist/l10n/es.js"></script>
	<script src="js/perfil.js"></script>
</body>
</html>