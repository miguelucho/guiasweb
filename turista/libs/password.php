<?php
use Hashids\Hashids;

class Userpass
{
    const HASH = PASSWORD_DEFAULT;
    const COST = 14;

    // Permite el cambio de contraseña:
    public function setPassword($password)
    {
        $n_password = password_hash($password, self::HASH, ['cost' => self::COST]);
        return $n_password;
    }

    // Logear un usuario:
    public function login($usuario, $passlog, $tipo)
    {
        require_once 'conexion.php';
        $hashids = new Hashids('mymobitco1.0', '10');
        if ($tipo == 'Arrendador') {
            $login = $db
                ->where('email_prp', $usuario)
                ->objectBuilder()->get('propietarios');

            if ($db->count > 0) {
                $idus  = $login[0]->Id_prp;
                $email = $login[0]->email_prp;
                $pass  = $login[0]->pass_prp;
                $paso  = $login[0]->paso_prp;
                if (password_verify($passlog, $pass)) {
                    if (password_needs_rehash($pass, self::HASH, ['cost' => self::COST])) {
                        $nuevo  = $this->setPassword($passlog);
                        $datos2 = ['pass_prp' => $nuevo];
                        $db
                            ->where('email_prp', $usuario)
                            ->update('propietarios', $datos2);
                    }
                    session_start();
                    date_default_timezone_set("America/Bogota");

                    $inmuebles = $db
                        ->where('Id_prp', $login[0]->Id_prp)
                        ->ObjectBuilder()->get('inmuebles');
                    if ($db->count > 0) {
                        $_SESSION['mbt_inmueble'] = $hashids->encode($inmuebles[0]->Id_inm);
                    }

                    $_SESSION['mbt_us']   = $email;
                    $_SESSION['mbt_tp']   = 'arrendador';
                    $_SESSION['mbt_paso'] = $paso;

                    return true;
                }
                return false;
            } else {
                return false;
            }
        } elseif ($tipo == 'Arrendatario') {
            $login = $db
                ->where('email_ard', $usuario)
                ->objectBuilder()->get('arrendatarios');

            if ($db->count > 0) {
                $idus  = $login[0]->Id_ard;
                $email = $login[0]->email_ard;
                $pass  = $login[0]->pass_ard;
                if (password_verify($passlog, $pass)) {
                    if (password_needs_rehash($pass, self::HASH, ['cost' => self::COST])) {
                        $nuevo  = $this->setPassword($passlog);
                        $datos2 = ['pass_ard' => $nuevo];
                        $db
                            ->where('email_ard', $usuario)
                            ->update('arrendatarios', $datos2);
                    }
                    session_start();
                    date_default_timezone_set("America/Bogota");

                    $_SESSION['mbt_us'] = $email;
                    $_SESSION['mbt_tp'] = 'arrendatario';
                    return true;
                }
                return false;
            } else {
                return false;
            }
        }
    }
}
