<?php
require_once 'conexion.php';

$data = $_REQUEST['info'];

switch ($data['accion']) {
    case 'contactos':
        session_start();

        $pagina         = (int) (isset($data['pagina']) ? $data['pagina'] : 1);
        $pagina         = ($pagina == 0 ? 1 : $pagina);
        $resultados_pag = 50;
        $adyacentes     = 2;

        $contactos = $db
            ->where('estado', $data['estado'])
            ->where('user_id', $_SESSION['turturista'])
            ->objectBuilder()->get('guias_contactos');

        $numpags = ceil($db->count / $resultados_pag);

        if ($numpags >= 1) {
            require_once 'Paginacion.php';
            $listado       = '';
            $db->pageLimit = $resultados_pag;

            $contactos = $db
                ->where('estado', $data['estado'])
                ->where('user_id', $_SESSION['turturista'])
                ->objectBuilder()->paginate('guias_contactos', $pagina);

            foreach ($contactos as $contacto) {
                $nmciudad = $nmdepartamento = '';

                $ciudades = $db
                    ->where('id', $contacto->ciudad)
                    ->objectBuilder()->get('ciudades');

                if ($db->count > 0) {
                    $nmciudad = $ciudades[0]->ciudad;

                    $departamentos = $db
                        ->where('id', $ciudades[0]->departamento_id)
                        ->objectBuilder()->get('departamentos');

                    if ($db->count > 0) {
                        $nmdepartamento = $departamentos[0]->departamento;
                    }
                }

                $guia = '';

                $usuarios = $db
                    ->where('id', $contacto->guia_id)
                    ->objectBuilder()->get('users2');

                if ($db->count > 0) {
                    $guia          = $usuarios[0]->nombres . ' ' . $usuarios[0]->apellidos;
                    $guia_title    = $guia;
                    $guia_telefono = $usuarios[0]->telefono1;
                    if ($usuarios[0]->telefono2 != '') {
                        $guia_telefono = $guia_telefono . ' - ' . $usuarios[0]->telefono2;
                    }
                    $guia_correo = $usuarios[0]->username;

                }

                $btns = '';

                if ($data['estado'] == 1) {
                    $btns = ' <div class="Listar-table-dato Center">
                                <span class="" title="">
                                    <a href="javascript://" class="Btn-rojo btn-rechazar" id="no-' . $contacto->id . '">Cancelar</a>
                                </span>
                            </div>';
                } else if ($data['estado'] == 2) {
                    $guia_title = $guia . ' - ' . $guia_telefono . ' - ' . $guia_correo;
                    $guia       = '<p>' . $guia . '</p>' . '<p>Tel: ' . $guia_telefono . '</p><p>Correo: ' . $guia_correo . '</p>';
                }

                $listado .= '<div class="Listar-table">
                                <div class="Listar-table-dato">
                                    <span class="" title="' . $contacto->fecha . '">' . $contacto->fecha . '</span>
                                </div>
                                <div class="Listar-table-dato">
                                    <span class="" title="' . $nmdepartamento . '">' . $nmdepartamento . '</span>
                                </div>
                                <div class="Listar-table-dato">
                                    <span class="" title="' . $nmciudad . '">' . $nmciudad . '</span>
                                </div>
                                <div class="Listar-table-dato">
                                    <span class="" title="' . $guia_title . '">' . $guia . '</span>
                                </div>
                                <div class="Listar-table-dato">
                                    <span class="" title="">
                                        <a href="javascript://" class="Btn-naranja btn-observacion" id="obs-' . $contacto->id . '">Observación</a>
                                    </span>
                                </div>
                                ' . $btns . '
                            </div>';
            }

            $msg['listado']    = $listado;
            $pagconfig         = array('pagina' => $pagina, 'totalrows' => $db->totalPages, 'ultima_pag' => $numpags, 'resultados_pag' => $resultados_pag, 'adyacentes' => $adyacentes);
            $paginar           = new Paginacion($pagconfig);
            $msg['paginacion'] = $paginar->crearlinks();
        } else {
            $msg['listado'] = '<div class="Listar-table">
                                        <div class="Listar-table-dato">
                                            <span class="" title="">No se encontraron registros</span>
                                        </div>
                                    </div>';
            $msg['paginacion'] = '';
        }

        $pendientes = $aceptadas = $realizadas = 0;

        $contactos = $db
            ->where('user_id', $_SESSION['turturista'])
            ->objectBuilder()->get('guias_contactos');

        foreach ($contactos as $contacto) {
            switch ($contacto->estado) {
                case '1':
                    $pendientes++;
                    break;
                case '2':
                    $aceptadas++;
                    break;
                case '3':
                    $realizadas++;
                    break;
            }
        }

        $msg['pendientes'] = $pendientes;
        $msg['aceptadas']  = $aceptadas;
        $msg['realizadas'] = $realizadas;

        echo json_encode($msg);
        break;
    case 'contacto-observacion':
        $contactos = $db
            ->where('id', $data['contacto'])
            ->objectBuilder()->get('guias_contactos');

        $msg['observacion'] = $contactos[0]->observaciones_usuario;

        echo json_encode($msg);
        break;
    case 'contacto-rechazar':
        $upestado = $db
            ->where('id', $data['contacto'])
            ->update('guias_contactos', ['estado' => 4]);

        if ($upestado) {
            $contactos = $db
                ->where('id', $data['contacto'])
                ->objectBuilder()->get('guias_contactos');

            $ciudades = $db
                ->where('id', $contactos[0]->ciudad)
                ->objectBuilder()->get('ciudades');

            $ciudad_contacto = $ciudades[0]->ciudad;
            $fecha_contacto  = $contactos[0]->fecha;

            //guia
            $usuarios = $db
                ->where('id', $contactos[0]->guia_id)
                ->objectBuilder()->get('users2');

            $guia_nombre = $usuarios[0]->nombres . ' ' . $usuarios[0]->apellidos;
            $guia_correo = $usuarios[0]->username;

            //turista
            $usuarios = $db
                ->where('id', $contactos[0]->user_id)
                ->objectBuilder()->get('users2');

            $turista_nombre = $usuarios[0]->nombres . ' ' . $usuarios[0]->apellidos;

            $template = '../templates/guiacancelar.php';
            $message  = file_get_contents($template);
            $message  = str_replace('$turista$', $turista_nombre, $message);
            $message  = str_replace('$guia$', $guia_nombre, $message);
            $message  = str_replace('$fecha$', $fecha_contacto, $message);
            $message  = str_replace('$ciudad$', $ciudad_contacto, $message);
            include_once 'phpmailer/class.phpmailer.php';
            $mail = new PHPMailer();

            $mail->Host = "localhost";
            $mail->SetFrom('noreply@guiasdeturismodecolombia.com.co');
            $mail->AddAddress($guia_correo);
            $mail->Subject = 'Cancelar contacto -  guiasdeturismodecolombia.com.co';
            $mail->MsgHTML($message);
            $mail->IsHTML(true);
            $mail->CharSet = "utf-8";

            $mail->Send();
            $msg['status'] = true;
            $msg['motivo'] = 'La solicitud se ha cancelado.';
        } else {
            $msg['status'] = false;
            $msg['motivo'] = 'No se ha podido cancelado la solicitud';
        }

        echo json_encode($msg);
        break;
    case 'perfilcambio':
        session_start();
        $nombres    = trim($data['nombres']);
        $apellidos  = trim($data['apellidos']);
        $telefono   = trim($data['telefono']);
        $contrasena = trim($data['contrasena']);

        $updatos = $db
            ->where('id', $_SESSION['turturista'])
            ->where('tipo', 3)
            ->update('users2', ['nombres' => $nombres, 'apellidos' => $apellidos, 'telefono1' => $telefono, 'nacimiento' => $data['nacimiento'], 'modificado' => $db->now()]);

        if ($contrasena != '') {
            require_once "password.php";
            $npass = password_hash($contrasena, PASSWORD_BCRYPT);

            $nuevopass = $db
                ->where('id', $_SESSION['turturista'])
                ->where('tipo', 3)
                ->update('users2', ['password' => $npass]);
        }

        if ($updatos) {
            $msg['status'] = true;
            $msg['motivo'] = 'Datos actualizados.';
        } else {
            $msg['status'] = false;
            $msg['motivo'] = 'Error, los datos no se actualizaron.';
        }

        echo json_encode($msg);
        break;
}
