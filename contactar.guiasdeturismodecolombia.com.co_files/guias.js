var old_criterios_busqueda;
var tiempo_mantenimiento_paginas = 300000;
var consultar = true;
var url_paginacion;
var url_guias_default;
var idiomas = Array();
var div_idioma;
var cont_idiomas = 0;
var div_servicio;
var cont_servicios = 0;
var servicios = Array();
var especialidades = Array();
var cont_especialidades =  0;
var areas = Array();
var cont_areas =  0;
var calificacion = 0;
var div_paginacion;
var genero = 0;
var nombres;
var apellidos;
var url_ciudades1 = "";
var url_ciudades2 = "";
var rnt = "";
var tp = "";
var certificado = 0;
var actions_user = 0;

function Buscar_Guias(url_guias, criterios, div_pag){
   
    if(div_pag != ""){
        div_paginacion = div_pag;
    }
    if(url_guias != ""){
        url_guias_default = url_guias;
    }    
    //construimos la url de busqueda de guias
    var url = url_guias_default+"/"+criterios;    
    
    //mostramos el icono de carga
    $('#loadingGuias').show(); 
   
    old_criterios_busqueda = criterios;
    var criterios_busqueda = criterios.split('&');
    var valores_paginas = criterios_busqueda[0].split('$');
   
    $.ajax({
        url: (url),        
        error: (function(statusText){
            $("#Guias").html();
            //alert("error: " + statusText.response);
        }),
        complete: (function(transport){   
            var resp = transport.responseText.toString();
            $("#Guias").html(resp);
            $('#loadingGuias').hide();
        }),

    }); 
}

//function ocultar_paginas(){
//    if($('#Guias').length){
//        var paginas = $('#Guias').children("div");
//        if(paginas.length > 0){
//            for(i = 0; i < paginas.length; i++){
//                $('#'+paginas[i].id).hide();
//            } 
//        }    
//    }
//}

//function remover_paginas_viejas(){    
//    var paginas = $('#Guias').children("div");
//    if(paginas.length > 0){
//        for(i = 0; i < paginas.length; i++){
//            $('#'+paginas[i].id).remove();
//        } 
//    }
//}

//function AmpliarDescripcionGuia(guia_id, descripcion){
//    $("#descripcionGuia_"+guia_id).html(descripcion);
//    $("#descripcionGuia_More_"+guia_id).hide();
//}

//function Desplegar_Comentarios(guia_id, action, num_comentarios, url){
//    if(action == 1){
//        $('#loadingComments').show();
//        $("#comentariosGuia_"+guia_id).show();
//        var url = url+'/'+guia_id.toString();
//        $.ajax({
//            url: (url),        
//            error: (function(statusText){
//                alert("error: " + statusText.response);
//            }),
//            complete: (function(transport){   
//                var resp = transport.responseText.toString();                
//                $("#boton-display_comment_"+guia_id).html("Ocultar Comentarios("+num_comentarios+")");
//                $("#boton-display_comment_"+guia_id).attr('onclick',"Desplegar_Comentarios('"+guia_id+"', '2', '"+num_comentarios+"', '"+url+"')");
//                $("#comentariosGuia_"+guia_id).html(resp);
//                $('#loadingComments').hide();
//            }),
//            
//        });  
//    }       
//    else{
//        $("#comentariosGuia_"+guia_id).hide();        
//        $("#boton-display_comment_"+guia_id).html("Ver Comentarios("+num_comentarios+")");
//        $("#boton-display_comment_"+guia_id).attr('onclick',"Desplegar_Comentarios('"+guia_id+"', '1', '"+num_comentarios+"', '"+url+"')");
//        $("#comentariosGuia_"+guia_id).hide();
//    }
//}

function establecer_Departamento(url_ciudades, url_guias){
    if(url_ciudades != ""){
        url_ciudades1 = url_ciudades;
        url_ciudades2 = url_ciudades +"2";
    }
    var departamento_id = $("#departamentos").val();
    $('#loadingGuias').show();
    $("#ciudades").show();
    var url = url_ciudades1+'/'+departamento_id.toString();
    $.ajax({
        url: (url),
        async: false,
        error: (function(statusText){
            //alert("error: " + statusText.response);
        }),
        complete: (function(transport){   
            var resp = transport.responseText.toString();
            $('#ciudades').html(resp);
        }),
    });
    modificar_criterios("departamento", departamento_id, url_guias, 0);
    modificar_criterios("ciudad", 0, url_guias, 1); 
    $("#info_busqueda_ubicacion").html("Ubicación: "+$("#departamentos option:selected").text());
}

function seleccionar_Departamento(url_ciudades){
    var departamento_id = $("#departamentos").val();
    if(departamento_id == ""){
        departamento_id = $("#UserDepartamentos").val();
    }
    $("#ciudades").show();
    var url = url_ciudades+'/'+departamento_id.toString();
    $.ajax({
        url: (url), 
        async: false,
        error: (function(statusText){
            //alert("error: " + statusText.response);
        }),
        complete: (function(transport){   
            var resp = transport.responseText.toString();
            $('#ciudades').html(resp);
        }),
    });
 
}

function seleccionar_Pais(url_departamentos){
    var pais_id = $("#paises").val();
    $("#departamentos").show();
    var url = url_departamentos+'/'+pais_id.toString();
    $.ajax({
        url: (url),   
        async: false,
        error: (function(statusText){
            //alert("error: " + statusText.response);
        }),
        complete: (function(transport){   
            var resp = transport.responseText.toString();
            $('#departamentos').html(resp);
        }),
    });
 
}

function establecer_Ciudad(url_guias){    
    var ciudad_id = $("#SelectCiudades").val();
    $('#loadingGuias').show();
    modificar_criterios("ciudad", ciudad_id, url_guias, 1);   
    $("#info_busqueda_ubicacion").html("Ubicación: "+$("#SelectCiudades option:selected").text()+", "+$("#departamentos option:selected").text());
}

function Busqueda_por_nombre(div){
    $("#"+div).keyup(function(){
        var entrada = $("#"+div).val();
//        if(entrada.length == 0){
//            $("#"+div+"_autoComplete").hide();
//        } 
        modificar_criterios("palabra", entrada, "", 1); 
        //$("#info_busqueda_palabra").html("Palabra: "+entrada);
    });
}
//function Busqueda_por_idioma(div, url_autocomplete){
//    //alert("bpi " + idiomas.length);
//    div_idioma = div;
//    $("#"+div).keyup(function(){
//        var entrada = $("#"+div).val();
//        if(entrada.length == 0){
//            $("#"+div+"_autoComplete").hide();
//        } 
//        else{
//			url = url_autocomplete + '/index/' + entrada;
//			$.ajax({
//				url: (url),        
//				error: (function(statusText){
//					alert("error: " + statusText.response);
//				}),
//				complete: (function(transport){ 
//					$("#"+div+"_autoComplete").html("");
//					var resp = transport.responseText.toString();
//					var idiomasaresp = $("<div/>").html(resp).children("div");
//					if(idiomasaresp.length >0){
//						for(i = 0; i < idiomasaresp.length; i++) {  
//							var idioma = idiomasaresp[i].id;
//							if($.inArray(idioma, idiomas) < 0){
//								$("#"+div+"_autoComplete").append(idiomasaresp[i]);
//							}
//						}                        
//					} 
//					$("#"+div+"_autoComplete").show();
//				}),
//			});
//        }
//       
//    });
//}
function establecer_Idioma(){    
    var idioma_id = $("#idiomas").val();
    if(idioma_id != 0){
        idiomas[cont_idiomas] = idioma_id;
        cont_idiomas++;
       // alert("ei2 " + idiomas.length);
        $("#idiomas-seleccionados").append(
                    '<div class="idioma_seleccionado" id="idioma_Seleccionado_'+idioma_id+'">' 
                    + $('#idiomas option:selected').text()
                    + '<a href="javascript:void(0)" onclick="quitar_Idioma('+"'"+idioma_id+"'"+')">X</a>'
                    +'</div>'                
                );
        $("#idiomas-seleccionados").show();
       // $("#"+div_idioma+"_autoComplete").hide();
        $("#"+div_idioma).val("");
        modificar_criterios("idiomas", idiomas, "", 1); 
    }
}
function quitar_Idioma(idioma_id){
   // alert("qi " + idiomas.length);
    $("#idioma_Seleccionado_"+idioma_id).remove();   
    var id = $.inArray(idioma_id, idiomas);
    idiomas.splice(id,1);
    cont_idiomas--;
    //alert("qi " + idiomas.length);
    modificar_criterios("idiomas", idiomas, "", 1);
    if(cont_idiomas == 0){
        $("#idiomas-seleccionados").hide();
        $("#idiomas").val(0);
    }
    else{
        
        $("#idiomas").val(idiomas[cont_idiomas-1]);
    }
}
//function Busqueda_por_servicio(div, url_autocomplete){
//    //alert("bpi " + idiomas.length);
//    div_servicio = div;
//    $("#"+div).keyup(function(){
//        var entrada = $("#"+div).val();
//        if(entrada.length == 0){
//            $("#"+div+"_autoComplete").hide();
//        } 
//        else{
//			url = url_autocomplete + '/index/' + entrada;
//			$.ajax({
//				url: (url),        
//				error: (function(statusText){
//					alert("error: " + statusText.response);
//				}),
//				complete: (function(transport){ 
//					$("#"+div+"_autoComplete").html("");
//					var resp = transport.responseText.toString();
//					var serviciosresp = $("<div/>").html(resp).children("div");
//					if(serviciosresp.length >0){
//						for(i = 0; i < serviciosresp.length; i++) {  
//							var servicio = serviciosresp[i].id;
//							if($.inArray(servicio, servicios) < 0){
//								$("#"+div+"_autoComplete").append(serviciosresp[i]);
//							}
//						}                        
//					} 
//					$("#"+div+"_autoComplete").show();
//				}),
//			});
//        }
//       
//    });
//}
function establecer_Servicio(){  
    var servicio_id = $("#servicios").val();
    if(servicio_id != 0){
        $('#loadingGuias').show();
        servicios[cont_servicios] = servicio_id;
        cont_servicios++;
       // alert("ei2 " + idiomas.length);
        $("#servicios-seleccionados").append(
                    '<div class="servicio_seleccionado" id="servicio_Seleccionado_'+servicio_id+'">' 
                    + $('#servicios option:selected').text()
                    + '<a href="javascript:void(0)" onclick="quitar_Servicio('+"'"+servicio_id+"'"+')">X</a>'
                    +'</div>'                
                );
        $("#servicios-seleccionados").show();
        //$("#"+div_servicio+"_autoComplete").hide();
        //$("#"+div_servicio).val("");
        modificar_criterios("servicios", servicios, "", 1);  
    }
}
function quitar_Servicio(servicio_id){
   // alert("qi " + idiomas.length);
    $("#servicio_Seleccionado_"+servicio_id).remove();   
    var id = $.inArray(servicio_id, servicios);
    servicios.splice(id,1);
    cont_servicios--;
    //alert("qi " + idiomas.length);
    modificar_criterios("servicios", servicios, "", 1);  
    if(cont_servicios == 0){
        $("#servicios-seleccionados").hide();
        $("#servicios").val(0);
    }
    else{
        
        $("#servicios").val(servicios[cont_idiomas-1]);
    }
}
function establecer_Especialidad(){      
    var especialidad_id = $("#especialidades").val();
    if(especialidad_id != 0){
        $('#loadingGuias').show();
        especialidades[cont_especialidades] = especialidad_id;
        cont_especialidades++;
        $("#especialidad-seleccionados").append(
                    '<div class="especialidad_seleccionado" id="especialidad_Seleccionado_'+especialidad_id+'">' 
                    + $('#especialidades option:selected').text()
                    + '<a href="javascript:void(0)" onclick="quitar_Especialidad('+"'"+especialidad_id+"'"+')">X</a>'
                    +'</div>'                
                );
        $("#especialidad-seleccionados").show();
        modificar_criterios("especialidades", especialidades, "", 1);  
    }
}
function quitar_Especialidad(especialidad_id){
   // alert("qi " + idiomas.length);
    $("#especialidad_Seleccionado_"+especialidad_id).remove();   
    var id = $.inArray(especialidad_id, especialidades);
    especialidades.splice(id,1);
    cont_especialidades--;
    //alert("qi " + idiomas.length);
    modificar_criterios("especialidades", especialidades, "", 1);  
    if(cont_especialidades == 0){
        $("#especialidad-seleccionados").hide();
        $("#especialidades").val(0);
    }
    else{
        
        $("#especialidades").val(especialidades[cont_especialidades-1]);
    }
}
function establecer_Certificados(){
    if($("#certificado").is(':checked')){
        certificado = 1;        
    }
    else{
        certificado = 0;
    }
    modificar_criterios("certificado", certificado, "", 1);  
}
//function establecer_Calificacion(cal){    
//    $('#loadingGuias').show();
//    calificacion = cal;
//    modificar_criterios("calificacion", calificacion, "");   
//    set_calificacion_general(calificacion);
//    estrellas_general();    
//}
function modificar_criterios(criterio_a_modificar, valor, url_guias, exec){
    consultar = true;  
    //alert("modificacion");
    var criterios = old_criterios_busqueda;
    var new_criterios = "";
    var patt = new RegExp(criterio_a_modificar);
    if (patt.test(criterios)){
        //alert("criterio existe");
        var criterios_busqueda = criterios.split("&");
        for(i = 0; i < criterios_busqueda.length; i++){
            var valores_criterios = criterios_busqueda[i].split('$');
            if(valores_criterios[0] !== criterio_a_modificar){
                new_criterios += criterios_busqueda[i];                
            }
            else{
                if(criterio_a_modificar == "idiomas"){
                    var cadena_idiomas = "";
                    for(j = 0 ; j < idiomas.length; j++){                        
                        cadena_idiomas += idiomas[j];
                        if(j < (idiomas.length -1)){
                            cadena_idiomas += ",";
                        }
                    }
                   // alert("criterio existe: "+ cadena_idiomas);
                    new_criterios += criterio_a_modificar+'$'+cadena_idiomas;
                }   
                else{
                    if(criterio_a_modificar == "servicios"){
                        var cadena_servicios = "";
                        for(j = 0 ; j < servicios.length; j++){                        
                            cadena_servicios += servicios[j];
                            if(j < (servicios.length -1)){
                                cadena_servicios += ",";
                            }
                        }
                        new_criterios += criterio_a_modificar+'$'+cadena_servicios;
                    }
                    else{
                        if(criterio_a_modificar == "especialidades"){
                            var cadena_especialidades = "";
                            for(j = 0 ; j < especialidades.length; j++){                        
                                cadena_especialidades += especialidades[j];
                                if(j < (especialidades.length -1)){
                                    cadena_especialidades += ",";
                                }
                            }
                            new_criterios += criterio_a_modificar+'$'+cadena_especialidades;
                        }
                        else{
                            new_criterios += criterio_a_modificar+'$'+valor;
                        }
                    }
                }
            }
            if(i < (criterios_busqueda.length -1)){
                new_criterios += '&';
            }   
            //alert("dentro de for criterios: " + criterios + " / new criterios: " +  new_criterios);
        }
    }
    else
    {
       // alert("nuevo criterio");
        if(criterio_a_modificar == "idiomas"){
            var cadena_idiomas = "";
            for(j = 0 ; j < idiomas.length; j++){
                cadena_idiomas += idiomas[j];
                if(j < (idiomas.length -1)){
                    cadena_idiomas += ",";
                }
            }
            //alert("cadena_idiomas: " + cadena_idiomas);
            new_criterios = criterios + '&'+criterio_a_modificar+'$'+cadena_idiomas;
        }
        else{
            if(criterio_a_modificar == "servicios"){
                var cadena_servicios = "";
                for(j = 0 ; j < servicios.length; j++){                        
                    cadena_servicios += servicios[j];
                    if(j < (servicios.length -1)){
                        cadena_servicios += ",";
                    }
                }
                new_criterios = criterios + '&'+criterio_a_modificar+'$'+cadena_servicios;
            }
            else{
                if(criterio_a_modificar == "especialidades"){
                    var cadena_especialidades = "";
                    for(j = 0 ; j < especialidades.length; j++){                        
                        cadena_especialidades += especialidades[j];
                        if(j < (especialidades.length -1)){
                            cadena_especialidades += ",";
                        }
                    }
                    new_criterios = criterios + '&'+criterio_a_modificar+'$'+cadena_especialidades;
                }
                else{
                    new_criterios = criterios + '&'+criterio_a_modificar+'$'+valor;
                }
            }    
        }
        //alert("dentro de modificaacion criterios: " + criterios + " / new criterios: " +  new_criterios);
    }
    //lert("dentro de modificaacion criterios: / new criterios: " +  new_criterios);
    //Crear_Paginacion("", new_criterios, div_paginacion);
    old_criterios_busqueda = new_criterios;
    if(exec == 1)
        Buscar_Guias(url_guias, new_criterios, "");
}

function Reiniciar_Busqueda(){
    location.reload();
}

function CargarVistaViewGuias(url_cargar, div_carga){
    $(window).scrollTop(200);
    $('#loadingContenido').show(); 
    $.ajax({
        url: (url_cargar),        
        error: (function(statusText){
                //alert("error: " + statusText.response);
        }),
        complete: (function(transport){ 
                var resp = transport.responseText.toString();
                $("#"+div_carga).html(resp);
                $('#loadingContenido').hide(); 
                
        }),
    });
    $('#loadingContenido').hide(); 
}

function EstablecerGenero(gen){
    genero = gen;
    if(gen == 1){        
        $("#M").prop('checked', true);
        $("#F").prop('checked', false);
    }
    if(gen == 2){        
        $("#F").prop('checked', true);
        $("#M").prop('checked', false);
    }
}
function EstablecerAreaEspecialidades(area_id){
    
    if($("#Area_"+area_id).is(':checked')){
        areas[cont_areas] = area_id;
        cont_areas++;
    }else{
        var id = $.inArray(area_id, areas);
        areas.splice(id,1);
        cont_areas--;
    }
}
function establecer_Especialidad2(){  
    var especialidad_id = $("#especialidades2").val();
    if(especialidad_id != 0){
        $('#loadingGuias').show();
        especialidades[cont_especialidades] = especialidad_id;
        cont_especialidades++;
        $("#especialidad-seleccionados2").append(
                    '<div class="especialidad_seleccionado" id="especialidad_Seleccionado2_'+especialidad_id+'">' 
                    + $('#especialidades2 option:selected').text()
                    + '<a href="javascript:void(0)" onclick="quitar_Especialidad2('+"'"+especialidad_id+"'"+')">X</a>'
                    +'</div>'                
                );
        $("#especialidad-seleccionados2").show();
    }
}
function quitar_Especialidad2(especialidad_id){
   // alert("qi " + idiomas.length);
    $("#especialidad_Seleccionado2_"+especialidad_id).remove();   
    var id = $.inArray(especialidad_id, especialidades);
    especialidades.splice(id,1);
    cont_especialidades--;
    //alert("qi " + idiomas.length);
    if(cont_especialidades == 0){
        $("#especialidad-seleccionados2").hide();
        $("#especialidades2").val(0);
    }
    else{
        
        $("#especialidades2").val(especialidades[cont_especialidades-1]);
    }
}
function establecer_Certificados2(){
    if($("#certificado2").is(':checked')){
        certificado = 1;        
    }
    else{
        certificado = 0;
    }
}
function CargarBusquedaAvanzada(){   
    $(window).scrollTop(0);
    nombres = $("#nombres").val();
    apellidos = $("#apellidos").val();
    var palabra = "";
    if(nombres != ""){
        palabra += nombres;
        if(apellidos != ""){
            palabra += " "+ apellidos;
        }
    }
    else if(apellidos != ""){
        palabra += apellidos;
    }    
    $("#info_busqueda_insert").html("");
    if(tp != ""){           
        $("#info_busqueda").show();
        $("#info_busqueda_insert").append("<div>Tarjeta Profesional: "+tp+"</div>");        
    }
    if(rnt != ""){
        $("#info_busqueda").show();
        $("#info_busqueda_insert").append("<div>Reg. Nacional de Turismo: "+rnt+"</div>");        
    }
    if(genero == 1){
        $("#info_busqueda").show();
        $("#info_busqueda_insert").append("<div>Genero: Masculino</div>");  
    }
    if(genero == 2){
        $("#info_busqueda").show();
        $("#info_busqueda_insert").append("<div>Genero: Femenino</div>");  
    }
    if(cont_areas > 0){
        var div_areas = "<div> Listado de intereses: ";
        var areas_envio = "";
        for(var i = 0; i < cont_areas; i++){
            div_areas += $("#Area_"+areas[i]).next('label').text();
            areas_envio += $("#Area_"+areas[i]).next('label').text();
            
            if(i < cont_areas - 1){
                div_areas += ", ";
                areas_envio += ",";
            }
        }
        div_areas += "</div>";
        modificar_criterios("areas", areas_envio, "", 0); 
        $("#info_busqueda").show();
        $("#info_busqueda_insert").append(div_areas);  
    } 
    $("#search-guia").val(palabra);    
    tp = $("#tp").val();
    rnt = $("#rnt").val();
    modificar_criterios("palabra", palabra, "", 0); 
    var departamento_id = $("#departamentos2").val();
    $("#departamentos").val(departamento_id);
    modificar_criterios("departamento", departamento_id, "", 0);  
    var ciudad_id = $("#ciudades2").val();
    $("#ciudades").val(ciudad_id);
    modificar_criterios("ciudad", ciudad_id, "", 0); 
    $("#especialidad-seleccionados").html("");
    for(var  i=0; i<especialidades.length; i++){
        $("#especialidad-seleccionados").append(
                    '<div class="especialidad_seleccionado" id="especialidad_Seleccionado_'+especialidades[i]+'">' 
                    + $('#especialidad_Seleccionado2_'+especialidades[i]).text().substring(0, $('#especialidad_Seleccionado2_'+especialidades[i]).text().length - 1 )
                    + '<a href="javascript:void(0)" onclick="quitar_Especialidad('+"'"+especialidades[i]+"'"+')">X</a>'
                    +'</div>'                
                );
    }
    $("#especialidad-seleccionados").show();
    if(certificado == "1")
        $("#certificado").prop('checked', true);
    else
        $("#certificado").prop('checked', false);
    modificar_criterios("certificado", certificado, "", 0);  
    modificar_criterios("tp", tp, "", 0); 
    modificar_criterios("rnt", rnt, "", 0); 
    var gen = "";
    if(genero == 1)
        gen = "M";
    else if(genero == 2)
        gen = "F" ;
    modificar_criterios("genero", gen, "", 0); 
    $("#bloque_der").show();
    $("#bloque_izq").show();
    $("#container_bloqueizq").attr('class', 'col-sm-9');
    modificar_criterios("especialidades", especialidades, "", 1); 
}

function CargarFormBusquedaAvanzada(url_busqueda_avanzada){
    $(window).scrollTop(0);
    $("#bloque_der").hide();
    $("#bloque_izq").hide();
    $("#container_bloqueizq").attr('class', 'col-sm-12');
    $("#Guias").hide();
    $('#loadingGuias').show(); 
    $.ajax({
        url: (url_busqueda_avanzada),
        async: false,
        error: (function(statusText){
            $("#Guias").html();
            //alert("error: " + statusText.response);
        }),
        complete: (function(transport){   
            var resp = transport.responseText.toString();
            $("#Guias").html(resp);
             $("#Guias").show();
            $('#loadingGuias').hide(); 
        }),
        
    }); 
    $("#nombres").val(nombres);
    $("#apellidos").val(apellidos);    
    $("#departamentos2").val($("#departamentos").val());
    $("#ciudades2").val($("#ciudades").val());    
    $("#tp").val(tp);
    $("#rnt").val(rnt);
    if(certificado == 1)
        $("#certificado2").prop('checked', true);
    else
        $("#certificado2").prop('checked', false);
    EstablecerGenero(genero);
    if(cont_areas > 0){
        for(var i = 0; i < cont_areas; i++){
            $("#Area_"+areas[i]).prop('checked', true);
        }
    }  
    $("#especialidad-seleccionados2").html("");
    for(var  i=0; i<especialidades.length; i++){
        $("#especialidad-seleccionados2").append(
                    '<div class="especialidad_seleccionado2" id="especialidad_Seleccionado2_'+especialidades[i]+'">' 
                    + $('#especialidad_Seleccionado_'+especialidades[i]).text().substring(0, $('#especialidad_Seleccionado_'+especialidades[i]).text().length - 1 )
                    + '<a href="javascript:void(0)" onclick="quitar_Especialidad2('+"'"+especialidades[i]+"'"+')">X</a>'
                    +'</div>'                
                );
    }
        $("#especialidad-seleccionados2").show();
}
function seleccionar_Departamento2(url_ciudades){
    if(url_ciudades != ""){
        url_ciudades2 = url_ciudades;
    }
    var departamento_id = $("#departamentos2").val();
    $("#ciudades2").show();
    var url = url_ciudades2+'/'+departamento_id.toString();
    $.ajax({
        url: (url),   
        error: (function(statusText){
            //alert("error: " + statusText.response);
        }),
        complete: (function(transport){   
            var resp = transport.responseText.toString();
            $('#ciudades2').html(resp);
        }),
    })
 
}


function seleccionar_nivel(){
    var nivel = $("#niveles").val();

    if(nivel != "0"){
        $("#datos_capacitacion").show();
    }
    else{
        $("#datos_capacitacion").hide();
        }
}

function editar_guia(url){
    $("#loadingContenido").show();
    $(window).scrollTop(0);
//    $.ajax({
//        url: (url),   
//        type: "POST",
//        data: {},
//        error: (function(statusText){
//            //alert("error: " + statusText.response);
//        }),
//        complete: (function(transport){   
//            var resp = transport.responseText.toString();
//            $('#ciudades2').html(resp);
//        }),
//    })
}
function gestionarAccionesUsuario(){
    if(actions_user == 0){
        $("#actionslogin").show();
        actions_user = 1;
    }
    else{
        $("#actionslogin").hide();
        actions_user = 0;
    }
}

function cargarFormAddGuia(url, div_carga){
    var tp =  $("#GuiaTarjetaProfesional").val();
    if(tp != ""){
        $.ajax({
            url: (url+'/'+tp),  
            error: (function(statusText){
                //alert("error: " + statusText.response);
            }),
            complete: (function(transport){   
                var resp = transport.responseText.toString();
                $('#'+div_carga).html(resp);
            }),
        })
    }
}

function cargarCiudad(url_ciudades){
    var departamento_id = $("#departamentos").val();
    $("#ciudades").show();
    var url = url_ciudades+'/'+departamento_id.toString();
    $.ajax({
        url: (url),   
        error: (function(statusText){
            //alert("error: " + statusText.response);
        }),
        complete: (function(transport){   
            var resp = transport.responseText.toString();
            $('#ciudades').html(resp);
        }),
    })
 
}