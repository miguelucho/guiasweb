function CambioDeTipoHorario(){
    var horario = $("#horarios").val();
    if(horario == 0){
        $("#fecha_ex").hide();
        $("#predeterminados").show();
        $("#rango_fecha").hide();
    }
    else if(horario == 1)
    {
        $("#fecha_ex").hide();
        $("#predeterminados").hide();
        $("#rango_fecha").show();
    }
    else if(horario == 2)
    {
        $("#fecha_ex").show();
        $("#predeterminados").hide();
        $("#rango_fecha").hide();
    }
}
function cargarFormularioAdd(url_add_disp){
    $.ajax({
        url: (url_add_disp),        
        error: (function(statusText){
            //alert("error: " + statusText.response);
        }),
        complete: (function(transport){   
            var resp = transport.responseText.toString();
            $('#FormDisponibilidad').html(resp);
            $('#FormDisponibilidad').show();
        }),
    });
}
function CargarEditDisponibilidad(url_edit_disp){
    $.ajax({
        url: (url_edit_disp),        
        error: (function(statusText){
            //alert("error: " + statusText.response);
        }),
        complete: (function(transport){   
            var resp = transport.responseText.toString();
            $('#FormDisponibilidad').html(resp);
            $('#FormDisponibilidad').show();
        }),
    });
}
function activar_Todos(){
    if($("#DisponibilidadallDays").is(':checked')){
        $("#Disponibilidadl").prop('checked', true);
        $("#Disponibilidadma").prop('checked', true);
        $("#Disponibilidadmi").prop('checked', true);
        $("#Disponibilidadj").prop('checked', true);
        $("#Disponibilidadv").prop('checked', true);
        $("#Disponibilidads").prop('checked', true);
        $("#Disponibilidadd").prop('checked', true);
    }
}
function verDisponibilidadGuias(url_disponibilidad){
    $.ajax({
        url: (url_disponibilidad),        
        error: (function(statusText){
            $("#disponibilidad").html();
            //alert("error: " + statusText.response);
        }),
        complete: (function(transport){   
            var resp = transport.responseText.toString();
            $("#disponibilidad").html(resp);
        }),
    }); 
}
function CambiarDisponible(url_cambio){
    $("#loadingContenido").show();
    $.ajax({
        url: (url_cambio),        
        error: (function(statusText){
            //alert("error: " + statusText.response);
        }),
        complete: (function(transport){   
            var resp = transport.responseText.toString();
            window.location = resp;          
        }),
    }); 
}