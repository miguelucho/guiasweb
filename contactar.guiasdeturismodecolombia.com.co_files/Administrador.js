var url_lista_user;
var palabra_ingresada = "";
var url_lista_ubicacion;
var palabra_ingresada_ubicacion = "";
var url_lista_reportes;
var palabra_ingresada_reportes = "";
function CargarUsuarios(url_listado_filtros){
    $('#loadingAdministrador').hide(); 
    $.ajax({
            url: (url_listado_filtros),        
            error: (function(statusText){
                //alert("error: " + statusText.response);
            }),
            complete: (function(transport){   
                var resp = transport.responseText.toString();
                $("#contenido_administrador").append(resp);
                $('#loadingAdministrador').hide();
            }),            
        });  
}
function CargarListadoUsuarios(url_listado, pagina){
    if(url_listado != ""){
        url_lista_user = url_listado;
    }
    url = url_lista_user+'/'+$("#Filtros").val();
    url = url+'/'+pagina;
    url = url+'/'+palabra_ingresada; 
    $('#loadingAdministrador').hide(); 
    $.ajax({
            url: (url),        
            error: (function(statusText){
                //alert("error: " + statusText.response);
            }),
            complete: (function(transport){   
                var resp = transport.responseText.toString();
                $("#resultados_busqueda_usuarios").html(resp);
                $('#loadingAdministrador').hide();
            }),
            
        });  
}
function Busqueda_usuarios_por_nombre(div){
    $("#"+div).keyup(function(){
        palabra_ingresada = $("#"+div).val();
        CargarListadoUsuarios(url_lista_user, '1');
    });
}
function CargarListadoUbicacion(url_listado, pagina){
    if(url_listado != ""){
        url_lista_ubicacion = url_listado;
    }
    url = url_lista_ubicacion+'/'+$("#FiltrosU").val();
    url = url+'/'+pagina;
    url = url+'/'+palabra_ingresada_ubicacion; 
    $('#loadingAdministrador').hide(); 
    $.ajax({
            url: (url),        
            error: (function(statusText){
                //alert("error: " + statusText.response);
            }),
            complete: (function(transport){   
                var resp = transport.responseText.toString();
                $("#resultados_busqueda_ubicacion").html(resp);
                $('#loadingAdministrador').hide();
            }),
            
        });  
}
function Busqueda_ubicacion(div){
    $("#"+div).keyup(function(){
        palabra_ingresada_ubicacion = $("#"+div).val();
        CargarListadoUbicacion(url_lista_ubicacion, '1');
    });
}
function CargarListadoComentarios(url_listado, pagina){
    url = url_listado+'/'+$("#FiltrosC").val();
    url = url+'/'+pagina;
    $('#loadingAdministrador').hide(); 
    $.ajax({
            url: (url),        
            error: (function(statusText){
                //alert("error: " + statusText.response);
            }),
            complete: (function(transport){   
                var resp = transport.responseText.toString();
                $("#resultados_busqueda_comentarios").html(resp);
                $('#loadingAdministrador').hide();
            }),
            
        });  
}
function cambiarVisibilidadComentario(url, url_listado){
    $.ajax({
        url: (url),        
        error: (function(statusText){
            //alert("error: " + statusText.response);
        }),
        complete: (function(transport){ 
            CargarListadoComentarios(url_listado, 1);
        }),
    });
}
function cambiarSuspendidoGuia(url, url_listado){
    $.ajax({
        url: (url),        
        error: (function(statusText){
            //alert("error: " + statusText.response);
        }),
        complete: (function(transport){ 
            CargarListadoUsuarios(url_listado, 1);
        }),
    });
}
function Busqueda_guias_por_nombre(div){
    $("#"+div).keyup(function(){
        palabra_ingresada_reportes = $("#"+div).val();
        CargarListadoReportes(url_lista_reportes, '1');
    });
}
function CargarListadoReportes(url_listado, pagina){
    if(url_listado != ""){
        url_lista_reportes = url_listado;
    }
    url = url_listado+'/'+$("#FiltrosR").val();
    url = url+'/'+pagina;
    if(palabra_ingresada_reportes != "")
        url = url+'/'+palabra_ingresada_reportes;
    else
        url = url+'/'+null;
    url = url+'/'+$("#departamentos").val();
    if ($('#SelectCiudades').length){
        url = url+'/'+$("#SelectCiudades").val();
    }
    $('#loadingAdministrador').hide(); 
    $.ajax({
            url: (url),        
            error: (function(statusText){
                //alert("error: " + statusText.response);
            }),
            complete: (function(transport){   
                var resp = transport.responseText.toString();
                $("#resultados_busqueda_reportes").html(resp);
                $('#loadingAdministrador').hide();
            }),
            
        });  
}