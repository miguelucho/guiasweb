var calificacion_general = 0;

function estrellas(num_estrellas, guia_id){    
    var i;
    for(i = 1; i <= 5; i++){
        if(i <= num_estrellas)
            $('#estrella_'+guia_id+'_'+i).attr("class", "estrella seleccionado simple");
        else
            $('#estrella_'+guia_id+'_'+i).attr("class", "estrella simple");
    }    
}
function estrellas_general(){
    var i;
    for(i = 1; i <= 5; i++){
        if(i <= calificacion_general)
            $('#estrella_filtro_'+i).attr("class", "estrella seleccionado");
        else
            $('#estrella_filtro_'+i).attr("class", "estrella");
    }
}
function estrellas_general_mouseover(cal){
    var i;
    for(i = 1; i <= 5; i++){
        if(i <= cal)
            $('#estrella_filtro_'+i).attr("class", "estrella seleccionado");
        else
            $('#estrella_filtro_'+i).attr("class", "estrella");
    }
}
function set_calificacion_general(cal){
    calificacion_general = cal;
}
function estrellas_calificacion_criterio(cal, criterio){
    var i;
    for(i = 1; i <= 5; i++){
        if(i <= cal)
            $('#estrella_cal_criterio'+criterio+'_'+i).attr("class", "estrella seleccionado");
        else
            $('#estrella_cal_criterio'+criterio+'_'+i).attr("class", "estrella");
    } 
}

