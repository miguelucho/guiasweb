var div_sitio;
var cont_sitios = 0;
var sitios = Array();
var url_main = "/fontur/";
var servicios = Array();
var cont_servicios = 0;
var terminos = "false";

/*manejo usuarios reserva*/
function CargarFormulario(tipo){
    if(tipo == 1){
        $('#ReservaGuiaRight_Formulario_Add').show();
        $('#ReservaGuiaRight_Formulario_Login').hide();
        $('#Boton_Reserva_Singup').attr('class', 'boton_reserva select');
        $('#Boton_Reserva_Login').attr('class', 'boton_reserva');
        $('#Boton_Reserva_Registrar_Signup').show();
        $('#Boton_Reserva_Registrar_Login').hide();
    }
    else if(tipo == 2){
        $('#ReservaGuiaRight_Formulario_Add').hide();
        $('#ReservaGuiaRight_Formulario_Login').show();
        $('#Boton_Reserva_Singup').attr('class', 'boton_reserva');
        $('#Boton_Reserva_Login').attr('class', 'boton_reserva select');
        $('#Boton_Reserva_Registrar_Signup').hide();
        $('#Boton_Reserva_Registrar_Login').show();
    }
}


/*Registro reservas*/
function activarDesactivarCheckbox(id_servicio){
    var index_eliminar;
    var bandera = 0;
    if(servicios.length >0){
        for(i = 0; i < servicios.length; i++) {
            if(servicios[i] == id_servicio){
                bandera = 1;
                index_eliminar = i;
                break;
            }
        }
    }
    if(bandera == 0){
        servicios[cont_servicios] = id_servicio;
        cont_servicios++;
    }
    else{
        servicios.splice(index_eliminar,1);
        cont_servicios--;
    }
   
}
function Registrar_Reserva(user_id, guia_id, url_reserva, user_id_guia, url_users_controller){
    if($("#AceptaTerminos").is(':checked'))
    {
        $("#errores_terminos_reserva").hide();
        if(get_fechas_reserva_validas() == 1)
        {      
            $("#errores_fechas_reserva").hide();
            var servicios_cadena = "";
            for(i = 0; i < servicios.length; i++){
                servicios_cadena += servicios[i];
                if(i < (servicios.length - 1)){
                    servicios_cadena += ',';
                }
            }
            if(url_users_controller != ''){
                if(url_users_controller.search('login') > 0){
                    var username = $("#UserUsername").val();
                    var password = $("#UserPasswordL").val();
                    if(username != "" && password != ""){
                        $("#errores_reserva").hide();
                        $.ajax({
                            url: (url_users_controller),  
                            async: false,
                            data: {
                                Username: username,
                                Password: password,
                            },
                            type: "POST",
                            error: (function(statusText){
                                //alert("error: " + statusText.response);
                            }),
                            complete: (function(transport){   
                                var resp = transport.responseText.toString();
                                if(!isNaN(resp)){
                                    user_id = resp;                                  
                                }
                                else{                                    
                                    $("#errores_reserva").html(resp);
                                    $("#errores_reserva").show();
                                }
                            }),

                        });
                    }
                    else{
                        $("#errores_reserva").html("Por favor ingrese su nombre de usuario / correo electrónico para registrar su contacto");
                        $("#errores_reserva").show();
                    }
                }
                else{
                    var nombres = $("#UserNombres").val();
                    var apellidos = $("#UserApellidos").val();
                    var emails = $("#UserEmails").val();
                    var password = $("#UserPasswordA").val();
                    var paise_id = $("#paises").val();
                    var tipo = $("#telefonosOptions").val();
                    var telefonos = "";
                    if(tipo == 0)
                        telefonos += "C&";
                    else if(tipo == 1)
                        telefonos += "F&";
                    telefonos += $("#telefonosField").val();
                    if(nombres != "" && apellidos != "" && emails != "" && password != "" && paise_id != ""){
                        $("#errores_reserva").hide();
                        $.ajax({
                            url: (url_users_controller),  
                            async: false,
                            data: {
                                Nombres: nombres,
                                Password: password,
                                Emails: emails,
                                Apellidos: apellidos,
                                Telefonos: telefonos,
                                Paise_id: paise_id,
                            },
                            type: "POST",
                            error: (function(statusText){
                                //alert("error: " + statusText.response);
                            }),
                            complete: (function(transport){   
                                var resp = transport.responseText.toString();
                                if(!isNaN(resp)){
                                    user_id = resp;                                  
                                }
                                else{
                                    $("#errores_reserva").html(resp);
                                    $("#errores_reserva").show();
                                }
                            }),

                        });
                    }
                    else{
                        $("#errores_reserva").html("Por favor ingrese loss datos de registro de contacto completos");
                        $("#errores_reserva").show();
                    }
                }
            }
            if(user_id > 0 && !isNaN(user_id)){
                servicios_cadena += "";
                var sitios_cadena = $("#sitios").val();
                var fecha_inicio = $("#fecha_inicioYear").val() + "-" + $("#fecha_inicioMonth").val() + "-" + $("#fecha_inicioDay").val(); 
                var fecha_fin = $("#fecha_finYear").val() + "-" + $("#fecha_finMonth").val() + "-" + $("#fecha_finDay").val(); 
                var personas = $("#Numero_personas").val();
                var observaciones = $("#observaciones").val();
                $.ajax({
                    url: (url_reserva),  
                    data: {user_id: user_id,
                        guia_id: guia_id,
                        fecha_inicio: fecha_inicio,
                        fecha_fin: fecha_fin,
                        personas: personas,
                        sitios: sitios_cadena,
                        observaciones: observaciones,
                        servicios: servicios_cadena
                    },
                    type: "POST",
                    error: (function(statusText){
                        //alert("error: " + statusText.response);
                    }),
                    complete: (function(transport){   
                        var resp = transport.responseText.toString();
//                        alert(resp);
                        if(resp != ""){
//                            alert(resp);  
                            window.location = resp;
                        }
                    }),

                });
            }
        }
        else{
            $("#errores_fechas_reserva").html("Error: No se han seleccionado fechas validas para registrar el contacto");
            $(window).scrollTop($("#errores_fechas_reserva").position().top);
        }
    }
    else{
        $("#errores_terminos_reserva").html("Debe aceptar los términos y condiciones para regitrar la reserva");
    }
}

function Busqueda_por_sitio(div, url_autocomplete, guia_id, input_ciudad_id){
    div_sitio = div;    
    $("#"+div).keyup(function(){
        var entrada = $("#"+div).val();
        if(entrada.length == 0){
            $("#"+div+"_autoComplete").hide();
        } 
        else{
            if(event.keyCode!=13){
                actualizar_Sitios(div, url_autocomplete, guia_id, input_ciudad_id);
            }
        }
       
    });
}
function actualizar_Sitios(div, url_autocomplete, guia_id, input_ciudad_id){
    var entrada = $("#"+div).val();
    var ciudad_id = $("#"+input_ciudad_id).val();
    //alert(ciudad_id);
    if (ciudad_id == 0){
        ciudad_id = null;
    }
    //alert(ciudad_id);
    url = url_autocomplete + '/' + guia_id + '/' + ciudad_id + '/' + entrada;
    $.ajax({
        url: (url),        
        error: (function(statusText){
            //alert("error: " + statusText.response);
        }),
        complete: (function(transport){                        
            $("#"+div+"_autoComplete").html("");
            var resp = transport.responseText.toString();
            var serviciosresp = $("<div/>").html(resp).children("div");
            if(serviciosresp.length >0){
                for(i = 0; i < serviciosresp.length; i++) {  
                    var servicio = serviciosresp[i].id;
                    if($.inArray(servicio, servicios) < 0){
                        $("#"+div+"_autoComplete").append(serviciosresp[i]);
                    }
                }                        
            } 
            $("#"+div+"_autoComplete").show();
        }),
    });
}
function establecer_Sitio(sitio_id){  
   // alert("ei " + idiomas.length);
    sitios[cont_sitios] = sitio_id;
    cont_sitios++;
   // alert("ei2 " + idiomas.length);
    $("#sitios-seleccionados").append(
                '<div class="sitios_seleccionado" id="sitio_Seleccionado_'+sitio_id+'">' 
                + $('#Sitio_'+sitio_id).text()
                + '<a href="javascript:void(0)" onclick="quitar_Sitio('+"'"+sitio_id+"'"+')">X</a>'
                +'</div>'                
            );
    $("#sitios-seleccionados").show();
    $("#"+div_sitio+"_autoComplete").hide();
    $("#"+div_sitio).val("");
}
function quitar_Sitio(sitio_id){
   // alert("qi " + idiomas.length);
    $("#sitio_Seleccionado_"+sitio_id).remove();   
    var id = $.inArray(sitio_id, sitios);
    sitios.splice(id,1);
    cont_sitios--;
}



/*Atencion Reservas*/
function Modificar_Reserva(reserva_id, url_reserva, valor, div_contenido, url_listar){ 
    var url;
    if(valor == 5){
        url = url_reserva + '/' + reserva_id + '/' + 2;
    }
    else if(valor == 6){
        url = url_reserva + '/' + reserva_id + '/' + 3;
    }
    else{
        url = url_reserva + '/' + reserva_id + '/' + valor;
    }
    if(valor == 2){
        var razones = $("#razones").val();
        url += '/'+razones;
    }
    else if(valor == 3){
        var costo = $("#costo").val();
        url += '/'+costo;
        var observaciones = $("#Observaciones").val();
        url += '/'+observaciones;
    }
    else if(valor == 5)
    {
        razones = "No me encuentro disponible para atender tu solicitud";
        url += '/'+ razones;
    }
    else if(valor==6){
        observaciones = "";
        url += '/'+observaciones;
    }
    $.ajax({
        url: (url),        
        error: (function(statusText){
            //alert("error: " + statusText.response);
        }),
        complete: (function(transport){ 
            //alert("se ha registrado su respuesta");            
            Listar_Reservas(div_contenido, url_listar,1);            
        }),
    });    
}

/*Cargar Listado de  Reservas*/
function Listar_Reservas(contenido_div, url_lista_reserva, pagina){
    $("#loadingContenido").show();
    $.ajax({
        url: (url_lista_reserva+'/'+contenido_div+'/'+pagina),        
        error: (function(statusText){
            //alert("error: " + statusText.response);
        }),
        complete: (function(transport){
            var resp = transport.responseText.toString();
            $("#"+ contenido_div).html(resp);
            $("#loadingContenido").hide();
        }),
    });
}
function CargarDatosReserva(div_contenido, url_despliegue, id_loading){
    $("#"+id_loading).show();
    $.ajax({
        url: (url_despliegue),        
        error: (function(statusText){
            //alert("error: " + statusText.response);
        }),
        complete: (function(transport){
            var resp = transport.responseText.toString();
            $("#"+ div_contenido).html(resp);
            $("#"+id_loading).hide();
        }),
    });
}


function activar_rechazo(){
    $("#FormRechazo").show();
    $("#FormAcepto").hide();    
}
function activar_aceptacion(){
    $("#FormRechazo").hide();
    $("#FormAcepto").show();    
}

function Listar_Reservas_sin_comentar(url_cargar, div_carga){
    $('#loadingGuias').show(); 
    $.ajax({
        url: (url_cargar),        
        error: (function(statusText){
                //alert("error: " + statusText.response);
        }),
        complete: (function(transport){ 
                var resp = transport.responseText.toString();
                $("#"+div_carga).html(resp);
                $('#loadingGuias').hide(); 
        }),
    });
}
function CargarFormCalificacion(url_calificacion, div_carga){
    $('#loadingGuias').show(); 
    $.ajax({
        url: (url_calificacion),      
        async: false,
        error: (function(statusText){
                //alert("error: " + statusText.response);
        }),
        complete: (function(transport){ 
                var resp = transport.responseText.toString();
                $("#"+div_carga).html(resp);
                $('#loadingGuias').hide(); 
        }),
    });
}

function establecer_Calificacion_reserva(cal){    
    calificacion = cal;
    $('#CalificacionePuntaje').val(cal);
    set_calificacion_general(calificacion);
    estrellas_general();    
      
}