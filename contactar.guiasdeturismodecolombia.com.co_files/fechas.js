var fechas_reserva_validas  = 0;

function validador_fechas_mayor(selectiyear, selectimonth, selectiday, selectfyear,  selectfmonth, selectfday, val_segunda_fecha, anio_actual, mes_actual, dia_actual){
    $("#"+selectiyear).change(function (){
        var value = $("#"+selectiyear).val();        
        var select = $("#"+selectfyear);
        select.empty();
        select.append($("<option></option>").attr("value", "").text("Año"));
        select.append($("<option></option>").attr("value", value).text(value));
        var anio = value;
        value = Number(value)+1;
        select.append($("<option></option>").attr("value", value).text(value));        
        var mes = $("#"+selectimonth).val();        
        if(anio != "" && mes == "02"){ 
            var dias;
            if((Number(anio) % 4) == 0){                
                dias = 29;                
            }
            else {
                dias = 28
            }
            select = $("#"+selectiday);
            select.empty();
            select.append($("<option></option>").attr("value", "").text("Día"));
            for(i = 1; i <= dias; i++ ){
                select.append($("<option></option>").attr("value", i).text(i));
            } 
        }
        if(val_segunda_fecha == "true"){
            validar_segunda_fecha(selectiyear, selectimonth, selectiday, selectfyear,  selectfmonth, selectfday, anio_actual, mes_actual, dia_actual);
        }
    });
    $("#"+selectimonth).change(function (){
        var value = $("#"+selectimonth).val();
        var dias = 30;
        if(idioma == "español"){
            if(value == "01" || value == "03" || value == "05" || value == "07" || value == "08" || value == "10" || value == "12"){
                dias = 31;
            }
            else if(value == "02"){
                dias = 28;
                var anio = $("#"+selectiyear).val();
                if(anio != ""){
                    if((Number(anio) % 4) == 0){
                        dias = 29;
                    }
                }
            }
            select = $("#"+selectiday);
            select.empty();
            select.append($("<option></option>").attr("value", "").text("Día"));
            for(i = 1; i <= dias; i++ ){
                select.append($("<option></option>").attr("value", i).text(i));
            } 
        }    
        if(val_segunda_fecha == "true"){
            validar_segunda_fecha(selectiyear, selectimonth, selectiday, selectfyear,  selectfmonth, selectfday, anio_actual, mes_actual, dia_actual);
        }
    });
    $("#"+selectiday).change(function (){
        if(val_segunda_fecha == "true"){
            validar_segunda_fecha(selectiyear, selectimonth, selectiday, selectfyear,  selectfmonth, selectfday, anio_actual, mes_actual, dia_actual);
        }
    }); 
    $("#"+selectfyear).change(function (){
        var value = $("#"+selectfyear).val();
        var anio = value;
        var mes = $("#"+selectfmonth).val();        
        if(anio != "" && mes == "02"){ 
            var dias;
            if((Number(anio) % 4) == 0){                
                dias = 29;                
            }
            else {
                dias = 28
            }
            select = $("#"+selectfday);
            select.empty();
            select.append($("<option></option>").attr("value", "").text("Día"));
            for(i = 1; i <= dias; i++ ){
                select.append($("<option></option>").attr("value", "").text(i));
            } 
        }
        if(val_segunda_fecha == "true"){
            validar_segunda_fecha(selectiyear, selectimonth, selectiday, selectfyear,  selectfmonth, selectfday, anio_actual, mes_actual, dia_actual);
        }
    });
    $("#"+selectfmonth).change(function (){
        var value = $("#"+selectfmonth).val();
        var dias = 30;
        if(idioma == "español"){
            if(value == "01" || value == "03" || value == "05" || value == "07" || value == "08" || value == "10" || value == "12"){
                dias = 31;
            }
            else if(value == "02"){
                dias = 28;
                var anio = $("#"+selectfyear).val();
                if(anio != ""){
                    if((Number(anio) % 4) == 0){
                        dias = 29;
                    }
                }
            }
            select = $("#"+selectfday);
            select.empty();
            select.append($("<option></option>").attr("value", "").text("Día"));
            for(i = 1; i <= dias; i++ ){
                select.append($("<option></option>").attr("value", i).text(i));
            } 
        }    
        if(val_segunda_fecha == "true"){
            validar_segunda_fecha(selectiyear, selectimonth, selectiday, selectfyear,  selectfmonth, selectfday, anio_actual, mes_actual, dia_actual);
        }
    });
    $("#"+selectfday).change(function (){
        if(val_segunda_fecha == "true"){
            validar_segunda_fecha(selectiyear, selectimonth, selectiday, selectfyear,  selectfmonth, selectfday, anio_actual, mes_actual, dia_actual);
        }
    });
}
function validar_segunda_fecha(selectiyear, selectimonth, selectiday, selectfyear,  selectfmonth, selectfday, anio_actual, mes_actual, dia_actual){
   var Yi = $("#"+selectiyear).val();
   var Mi = $("#"+selectimonth).val();
   var Di = $("#"+selectiday).val();
   var Yf = $("#"+selectfyear).val();
   var Mf = $("#"+selectfmonth).val();
   var Df = $("#"+selectfday).val();
   var ret;
   $("#errores_fechas_reserva").hide();
   if(((!(Yi == "")) && (!(Mi == ""))) && (!(Di == ""))){ 
       if(((!(Yf == "")) && (!(Mf == ""))) && (!(Df == ""))){ 
        ret = comparar_fechas(anio_actual, mes_actual, dia_actual, Yi, Mi, Di);
        if(ret == 1){
            fechas_reserva_validas = 0;
            $("#errores_fechas_reserva").html("la fecha de reserva debe ser mayor o igual a la actual: "+dia_actual+"/"+mes_actual+"/"+anio_actual);
            $("#errores_fechas_reserva").show();
        }
        else{
             if(((!(Yf == "")) && (!(Mf == ""))) && (!(Df == ""))){
                 ret = comparar_fechas(Yi, Mi, Di, Yf, Mf, Df);
                 if(ret == 1){
                     fechas_reserva_validas = 0;
                     $("#errores_fechas_reserva").html("la fecha final de reserva debe ser mayor o igual a la fecha inicial");
                     $("#errores_fechas_reserva").show();
                 }
                 else{                    
                     fechas_reserva_validas = 1;
                 }
             }
         }
       }
       else{
            fechas_reserva_validas = 0;
            $("#errores_fechas_reserva").html("Por Favor registre completamente la fecha de finalización del contacto");
            $("#errores_fechas_reserva").show();
       }
   }
   else{
       fechas_reserva_validas = 0;
       $("#errores_fechas_reserva").html("Por Favor registre completamente la fecha inicial del contacto");
       $("#errores_fechas_reserva").show();
   }
}
function get_fechas_reserva_validas(){
    return fechas_reserva_validas;
}

function comparar_fechas(Yi, Mi, Di, Yf, Mf, Df){
    var ret = 0;
    if(parseInt(Yi) > parseInt(Yf)){
        ret = 1;
    }
    else{
        if(parseInt(Yi) == parseInt(Yf)){
            if(parseInt(Mi) > parseInt(Mf)){
                ret = 1;
            }
            else{
                if(parseInt(Mi) == parseInt(Mf)){
                    if(parseInt(Di) > parseInt(Df)){
                        ret = 1;
                    }
                }
            }
        }
    }
    return ret;
}
function Establecer_meses_español(select){
    idioma = "español";
    var value = $("#"+select+" option:selected").val();
    var select = $("#"+select);        
    select.empty();    
    select.append($("<option></option>").attr("value", "").text("Mes"));
    select.append($("<option></option>").attr("value", "01").text("Enero"));
    select.append($("<option></option>").attr("value", "02").text("Febrero"));
    select.append($("<option></option>").attr("value", "03").text("Marzo"));
    select.append($("<option></option>").attr("value", "04").text("Abril"));
    select.append($("<option></option>").attr("value", "05").text("Mayo"));
    select.append($("<option></option>").attr("value", "06").text("Junio"));
    select.append($("<option></option>").attr("value", "07").text("Julio"));
    select.append($("<option></option>").attr("value", "08").text("Agosto"));
    select.append($("<option></option>").attr("value", "09").text("Septiembre"));
    select.append($("<option></option>").attr("value", "10").text("Octubre"));
    select.append($("<option></option>").attr("value", "11").text("Noviembre"));
    select.append($("<option></option>").attr("value", "12").text("Diciembre"));
    select.val(value);
}