var scroll = 0;
var incrementscroll = 39;
function Crear_Paginacion(url, criterios, div_paginacion){
    scroll = 0;
    if(url != ""){
        url_paginacion = url;
    }
    url = url_paginacion + '/' + criterios;
    $.ajax({
        url: (url),        
        error: (function(statusText){
            //alert("error: " + statusText.response);
        }),
        complete: (function(transport){   
            var resp = transport.responseText.toString();
            $("#"+div_paginacion).html(resp);
        }),

    });    
}
function CorrerPaginacion(id_div_paginacion, tipo){
    if(tipo == "1")
        scroll = scroll + incrementscroll;
    else if(scroll > 0)
        scroll = scroll - incrementscroll;
    $("#"+id_div_paginacion).animate({scrollLeft: scroll}, 100)
}
function establecerScroll(id_div_paginacion, new_scroll){
    scroll = new_scroll;
    if(scroll < 0)
        scroll = 0;
    $("#"+id_div_paginacion).animate({scrollLeft: scroll}, 100)
}