<?php require_once('Connections/datos.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_rds_programas = 5;
$pageNum_rds_programas = 0;
if (isset($_GET['pageNum_rds_programas'])) {
  $pageNum_rds_programas = $_GET['pageNum_rds_programas'];
}
$startRow_rds_programas = $pageNum_rds_programas * $maxRows_rds_programas;

mysql_select_db($database_datos, $datos);
$query_rds_programas = "SELECT * FROM progacademicos ORDER BY id ASC";
$query_limit_rds_programas = sprintf("%s LIMIT %d, %d", $query_rds_programas, $startRow_rds_programas, $maxRows_rds_programas);
$rds_programas = mysql_query($query_limit_rds_programas, $datos) or die(mysql_error());
$row_rds_programas = mysql_fetch_assoc($rds_programas);

if (isset($_GET['totalRows_rds_programas'])) {
  $totalRows_rds_programas = $_GET['totalRows_rds_programas'];
} else {
  $all_rds_programas = mysql_query($query_rds_programas);
  $totalRows_rds_programas = mysql_num_rows($all_rds_programas);
}
$totalPages_rds_programas = ceil($totalRows_rds_programas/$maxRows_rds_programas)-1;

$queryString_rds_programas = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_rds_programas") == false && 
        stristr($param, "totalRows_rds_programas") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_rds_programas = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_rds_programas = sprintf("&totalRows_rds_programas=%d%s", $totalRows_rds_programas, $queryString_rds_programas);
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1 user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" >
	<meta name="keywords" lang="es" content="">
	<meta name="robots" content="All">
	<meta name="description" lang="es" content="">	
	<title>Ofertas Academicas</title>
	<meta http-equiv="Cache-control" content="public">
	<link rel="stylesheet" href="css/slider.css" />
	<link rel="stylesheet" href="css/stylesheetnew.css" />	
</head>
<body>
<header>
	<?php include "new-header-top.php";?>	
</header>
<div class="Impul">
	<div class="Impul-int">
		<a href="#">
			<img src="images/appicon.png" class="New-App">
		</a>
	</div>
</div>
<section>
		<div class="Slider">
		<div class="Slider-imagen">
			<div class="wide-container">
				    <div id="slides">
				      <ul class="slides-container">
				     	 <li>
				          <img src="images/ImagenColombia1.png" alt="Programa Bioflores">
				          <div class="context">
					        
					      </div>
				        </li>
				        <li>
				          <img src="images/ImagenColombia5.jpg" alt="Programa Biopalama">
				          <div class="context">
					        
					      </div>
				        </li>
				        <li>
				          <img src="images/ImagenColombia6.jpg" alt="Programa Biocafe">
				          <div class="context">
					        
					      </div>
				        </li>
				        <li>
				          <img src="images/ImagenColombia3.jpg" alt="Programa Biocaña">
				          <div class="context">
					        
					      </div>
				        </li>				        
				      </ul>
		   			</div>
	  			</div>
	  			<div class="Slider-conten">
	  				<div class="Slider-conten-int"></div>
	  			</div>				 
		</div>		
		</div>		
	</section>
<div class=""></div>
<section>
	<div class="Conten-global">
		<div class="Conten-global-int">
			<div class="Conten-global-int-bloques">
				<a href="contactenos">
					<img src="images/img1.png">
				</a>
			</div>
			<div class="Conten-global-int-bloques">
				<a href="">
					<img src="images/img3.png">
				</a>
			</div>
			<div class="Conten-global-int-bloques">
				<a href="contactar">
					<img src="images/img2.png">
				</a>
			</div>
		</div>
	</div>
</section>
<section>
	<div class="Bloques">
		<div class="Bloques-int">
			<div class="Bloques-img">
				<p><h2>TARJETA PROFESIONAL</h2></p>
			</div>

		<!-- inicio de contenido -->  


			     <?php
				mysql_select_db($database_datos, $datos);
				$query_rds_textos = "SELECT * FROM ttextosvarios WHERE id = 6";
				$rds_textos = mysql_query($query_rds_textos, $datos) or die(mysql_error());
				$row_rds_textos = mysql_fetch_assoc($rds_textos);
				$totalRows_rds_textos = mysql_num_rows($rds_textos);
 // --------------------------------------------------Contenido -------------------------------------------?>
			   <?php echo urldecode($row_rds_textos['contenido']); ?>			  <?php if (!isset($_SESSION)) {   session_start(); } if ((isset($_SESSION['MM_UserGroup'])) and  ($_SESSION['MM_UserGroup']=="1")) { ?><a href="javascript:;" onclick="window.open('editarv2.php?id=6&ec=2','Editor','scrollbars=yes,resizable=yes,width=800,height=600')"><img src="imagenes/iconos/editor.png" alt="editar" width="32" height="32" border="0" /></a><?php } ?>
     
	 <!-- fin de contenido -->  



		</div>			
	</div>
</section>
<section>
	<div class="Conten-global">
		<div class="Conten-global-int">
			<div class="Conten-redes">
				<div class="Conten-redes-int">
					<div class="Conten-redes-int-sec">
						<div class="Redesg-facetw">
							<div class="fb-page" data-href="https://www.facebook.com/pg/MincomercioCo/posts/?ref=page_internal" data-tabs="timeline" data-width="500" data-height="400" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/pg/MincomercioCo/posts/?ref=page_internal" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/pg/MincomercioCo/posts/?ref=page_internal">Ministerio de Comercio, Industria y Turismo</a></blockquote></div>
						</div>
					</div>
					<div class="Conten-redes-int-sec">
						<div class="Redesg-facetw">
						<a class="twitter-timeline" href="https://twitter.com/mincomercioco?lang=es">Tweets by consejodeguias</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<footer>
<?php include "new-footer.php";?>	
</footer>
<script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
<script>
    $(function() {
    $('#slides').superslides({
    inherit_width_from: '.wide-container',
    inherit_height_from: '.wide-container',
    play: 5000,
    animation: 'fade'
    });
    });
</script>
<script type="text/javascript">
	$(function(){
		$('#Drop').bind('click',function() {
		$('.Top-inf').toggleClass('Top-inf-apa');
	});
	});
</script>
<script src="js/jquery.superslides.min.js" type="text/javascript" charset="utf-8"></script>
<script src="js/mostrar-invest-index.js"></script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.7";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
</body>
</html>
